package main_module

import (
	//"bytes"
	//"fmt"
	"io/ioutil"
	"encoding/json"

	"github.com/hjson/hjson-go"
)

func unmarshalHjsonFromFile(path string) map[string]interface{} {

	content, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}

	var dat map[string]interface{}

	// Decode and a check for errors.
	if err := hjson.Unmarshal(content, &dat); err != nil {
		panic(err)
	}
	//fmt.Println(dat)

	return dat
}

// XXX
func unmarshalHjsonFromData(data []byte) map[string]interface{} {

	var dat map[string]interface{}

	// Decode and a check for errors.
	if err := hjson.Unmarshal(data, &dat); err != nil {
		panic(err)
	}
	//fmt.Println(dat)

	return dat
}

// XXX
func marshalHjson(v interface{}) []byte {
	
	//ret, err := hjson.Marshal(v)
	// XXX
	//ret, err := json.Marshal(v)
	ret, err := json.MarshalIndent(v, "", "  ")	
	if err != nil {
		panic(err)
	}
	//fmt.Println(ret)

	return ret
}


func hjHasDef(def map[string]interface{}, name string) bool {
	return def[name] != nil
}

func hjGetDef(def map[string]interface{}, name string) map[string]interface{} {
	x := def[name]
	if x != nil {
		return x.(map[string]interface{})
	} else {
		panic("get def fail key=" + name)
	}
}

func hjGetBool(def map[string]interface{}, name string) bool {
	if v := def[name]; v != nil {
		return v.(bool)
	} else {
		panic("read fail key=" + name)
	}
}

func hjGetStringDefault(def map[string]interface{}, name string, defaultVal string) string {
  if hjHasDef(def, name) {
    return hjGetString(def, name)
  }
  return defaultVal
}

func hjGetFloatDefault(def map[string]interface{}, name string, defaultVal float64) float64 {
  if hjHasDef(def, name) {
    return hjGetFloat(def, name)
  }
  return defaultVal
}

func hjGetIntDefault(def map[string]interface{}, name string, defaultVal int) int {
  if hjHasDef(def, name) {
    return hjGetInt(def, name)
  }
  return defaultVal
}

var hjGetString = _getString
var hjGetFloat = _getFloat
var hjGetInt = _getInt
