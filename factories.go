package main_module

import (
	"fmt"
)

func makeBuilding(name string, campId CampId) *Building {
	_log.Inf("%F name:", name, "campId:", campId)

	b := newBuilding()
	b.name = name
	b.campId = campId
	readBuildingDef(name, campId, b)

	// Attach view
	b.view = makeBuildingView(b)

	return b
}

func MakeBuildingForPlayer(name string, p *Player) *Building {
	_log.Inf("%F name:", name, "p:", p)

	var campId CampId

	if p != nil {
		campId = p.camp.Id
	} else {
		campId = CampId_None
	}
	
	b := makeBuilding(name, campId)
	
	// XXX
	b.assignPlayer(p)

	return b
}

//func readBuildingDef(name string) *Building {
//	b := &Building{}

// Read building's definition without creating it
//func readBuildingDef(name string, p *Player, to *Building) {
func readBuildingDef(name string, campId CampId, to *Building) {
	b := to

	campName := "none"
	/*
	if p != nil {
		camp := p.camp
		campName = camp.Namelc()
	}
	*/
	campName = campId.Namelc()
	buildingsdir := "res/buildings"
	//path := buildingsdir + "/" + name + "/" + campName + "/def.jsonnet"
	path := buildingsdir + "/" + name + "/" + campName + "/def.json"

	found, err := exists(path)
	if err != nil {
		panic(err)
	}

	// Finally
	if !found {
		panic(fmt.Sprintf(
			"cannot make building from data, unknown settings: name=%s camp=%s\npath=%s",
			name, campName, path))
	} else {
		_log.Inf("Loading building from:", path)
	}

	//dat := unmarshalJsonnetFromFile(path)
	dat := xloadJsonFromFile(path)
	_ = dat

	// Building
	{
		def := hjGetDef(dat, "building")

		b.dimX = hjGetInt(def, "dim_x")
		b.dimY = hjGetInt(def, "dim_y")
		b.areaX = hjGetInt(def, "area_x")
		b.areaY = hjGetInt(def, "area_y")

		b.guiName = hjGetString(def, "gui_name")

		//pp(u.guiName)
	}

	// Combat
	{
	}
}

func makeBuildingView(b *Building) *BuildingView {
	v := newBuildingView(b)
	name := b.name
	campId := b.campId

	// TODO: move to def
	if name == "factory" && campId == CampId_Reds {
		loadSmokeAnim := func(fat bool) *AnimatedSprite {
			anim := newAnimatedSprite()
			anim.posType = SpritePositionType_ScreenSpace
			//anim.load("tmp/smoke/out/r1/x.png", 10)
			if !fat {
				anim.load("res/objects/smoke/smoke1/smoke1.png", 10)
			} else {
				anim.load("res/objects/smoke/smoke1/fat_smoke1.png", 10)
			}
			anim.frameTime = 100
			//anim.frameTime = 120
			anim.originX = 28
			anim.originY = 60
			//v.anim.tileSize = 44
			return anim
		}

		v.anims["smoke1"] = loadSmokeAnim(false)
		v.anims["smoke2"] = loadSmokeAnim(false)
		v.animPoints["smoke1"] = Pos{27, 30}
		v.animPoints["smoke2"] = Pos{4, 43}
		v.anims["smoke2"].frame = v.anims["smoke2"].maxFrame / 2
	}

	return v
}
