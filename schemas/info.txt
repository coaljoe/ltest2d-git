- ? schemas / definitions are used for json files defined in res/, and loaded with factories. such as buildings and units
  - buildings
  - units, unit components, weapons

- ? by definition schemas are used to check files / file data
  - json
  - ? not all components / records might be defined in json (or have schemas ?)
    - ? such as camp record
  - for some types schemas are not defined, like camp

- ? ext/external type
  - as in its not defined in res/ or factories / json files, or loaded
  - but rather exist in the game
