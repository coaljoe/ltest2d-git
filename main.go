package main_module

import (
	"kristallos.ga/lib/xlog"
	. "kristallos.ga/rx/math"
	"flag"
	"fmt"
	"os"
	"runtime"
	"strconv"
	"strings"
	"time" //"github.com/veandco/go-sdl2/img"

	xg "kristallos.ga/xgui"
	"github.com/kardianos/osext"
	"github.com/veandco/go-sdl2/sdl"

	"log"
	//"net/http"
	//_ "net/http/pprof"
	"runtime/pprof"

	//"kristallos.ga/ltest2d/gui"
	//"kristallos.ga/ltest2d/gui_base"
	"kristallos.ga/ltest2d/gui/gui_base"
)

var _ = Vec3Zero

var window *sdl.Window
var renderer *sdl.Renderer

func init() {
	// XXX not strictly needed, may improve things
	runtime.LockOSThread()

	println("init()")

	// Change current dir to main's dir
	// Cd to app's root
	//dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	dir, err := osext.ExecutableFolder()
	if err != nil {
		panic(err)
	}
	fmt.Println("dir:", dir)
	//appRoot := filepath.Join(dir, "..")
	appRoot := dir
	fmt.Println("appRoot: ", appRoot)
	os.Chdir(appRoot)

	vars.appRoot = appRoot

	// Set GOMAXPROCS.
	runtime.GOMAXPROCS(1)
	p("GOMAXPROCS:", runtime.GOMAXPROCS(0))
}

// XXX
func Main(guii gui_base.GuiI) {
	main(guii)
}

func main(guii gui_base.GuiI) {
	fmt.Println("main()")
	fmt.Printf("BuildRevision: %s\n", BuildRevision)

	/*
		go func() {
			log.Println(http.ListenAndServe("localhost:6060", nil))
		}()

	*/

	ltestMain(guii)

	pprof.StopCPUProfile()
}

func ltestMain(guii gui_base.GuiI) {
	fmt.Println("main()")
	//initApp()

	// XXX init app check ?

	ok := initAudio()
	if !ok {
		panic("cannot init audio")
	}

	//game := initBaseGame()

	// Check base game init
	if game == nil {
		panic("base game wasn't initialized? game is nil")
        }
        // Set gui
        if guii == nil {
		panic("no gui initialized? guii is nil")
        }
        game.gui = guii
	
	initDefaultGame()

	initGameStates()

	tf := game.testfw	
	tf.runModule("Test1")
	//pp(2)

	runUnderTestFwModMode := app_flagTestFwMod != ""
	_ = runUnderTestFwModMode

	//pp(runUnderTestFwModMode)

	// XXX TODO: move to newgametool?
	if vars.playMap != "" || vars.playMapImage != "" {
		ngt := game.newgametool

		println("play map:", vars.playMap)
		println("play map image:", vars.playMapImage)
		
		ngt.Reset()

		if vars.playMap != "" {
			// XXX set map
			ngt.loadMap = vars.playMap
		}
		if vars.playMapImage != "" {
			ngt.loadMapImage = vars.playMapImage
		}

		if !runUnderTestFwModMode {
			//ngt.createNewEmptyGame()
			ngt.initNewEmptyGame()
		} else {
			// XXX
			// XXX some mods would want opt-out from this? fixme?
			tf.setupDefaultTestGame()
		}

		//if vars.dev {
		//if vars.test {
		//if vars.testGame {
		if vars.testGame || vars.dev {
			// XXX add test players
			p("add test players...")
			
			p2 := NewPlayer(PlayerType_AI, CampId_Arctic)
			p2.name = "AIPlayer1"
			ngt.AddPlayer(p2)
		}

		//ngt.createNewGame()
		
		ngt.StartGame()

	} else {
		//setGameState("game")
		setGameState("menu")
	}

	// XXX run testfw module
	if app_flagTestFwMod != "" {
		tf.runModule(app_flagTestFwMod)
	}
	//pp(2)

	fps_limit := 0 //30

	lastTime := time.Now()
	running := true
	var event sdl.Event
	for running {

		thisTime := time.Now()
		passedTime := thisTime.Sub(lastTime)
		dt := passedTime.Seconds()
		//vars.fps = int(1.0 / dt)
		old_fps := float64(vars.fps)
		new_fps := 1.0 / dt
		//smoothing := 0.9; // larger=more smoothing
		smoothing := 0.1 // larger=more smoothing
		new_fps = (new_fps * smoothing) + (old_fps * (1.0 - smoothing))
		vars.fps = int(new_fps)

		//event := sdl.WaitEvent()
		for event = sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
			switch t := event.(type) {
			case *sdl.QuitEvent:
				running = false
			//case *sdl.KeyUpEvent:
			//	fmt.Printf("[%d ms] Keyboard\ttype:%d\tsym:%c\tmodifiers:%d\tstate:%d\trepeat:%d\n",
			//		t.Timestamp, t.Type, t.Keysym.Sym, t.Keysym.Mod, t.State, t.Repeat)

			case *sdl.KeyboardEvent:
				fmt.Printf("[%d ms] Keyboard\ttype:%d\tsym:%c\tmodifiers:%d\tstate:%d\trepeat:%d\n",
					t.Timestamp, t.Type, t.Keysym.Sym, t.Keysym.Mod, t.State, t.Repeat)
				if t.Keysym.Sym == 'q' {
					running = false
				}

				if t.Type == sdl.KEYDOWN && t.Repeat == 0 {
					if game.gui.Enabled() {
						game.hud.onKeyPress(t.Keysym.Sym)
					}
				}

				if t.Type == sdl.KEYDOWN && t.Keysym.Sym == 'g' && t.Repeat == 0 {
					game.gui.SetEnabled(!game.gui.Enabled())
				}
				
				if t.Type == sdl.KEYDOWN && t.Keysym.Sym == sdl.K_F12 && t.Repeat == 0 {
					game.field.view.d_drawGrid = !game.field.view.d_drawGrid
				}

				if t.Type == sdl.KEYDOWN && t.Keysym.Sym == sdl.K_F11 && t.Repeat == 0  {
					game.fowsys.toggleDisableFowDraw()
				}

				if t.Type == sdl.KEYDOWN && t.Keysym.Sym == 's' && t.Repeat == 0 {
					/*
					//saveGame("tmp/testsave.sav")
					game.field.saveFile("tmp/testmap.map")
					p(game.field.hm.dataOrig)
					*/

					ml := newMapLoader()
					ml.save("tmp/ml.map")
					
					pp(2)
				}

				if t.Type == sdl.KEYDOWN && t.Keysym.Sym == 'b' && t.Repeat == 0 {
					game.gui.GameSheet().ControlMenu().SetMode("unit_menu", nil)
				}

				/*
					//if t.Type == sdl.KEYDOWN && t.Repeat == 0 {
					if t.Type == sdl.KEYDOWN {
						if t.Keysym.Sym == game.keymap.getKey1("scrollUpKey") {
							//pp(2)
							game.vp.shx += int(vars.scrollSpeed)
							p(game.vp.shx)
						}
					}
				*/

			case *sdl.MouseMotionEvent:
				fmt.Printf("[%d ms] MouseMotion\tid:%d\tx:%d\ty:%d\txrel:%d\tyrel:%d\n", t.Timestamp, t.Which, t.X, t.Y, t.XRel, t.YRel)
				vars.mx, vars.my = int(t.X), int(t.Y)
				prevX := int(t.X - t.XRel)
				prevY := int(t.Y - t.YRel)
				sX := float64(t.X) / float64(vars.resX)
				sY := float64(t.Y) / float64(vars.resY)
				evData := xg.EvMouseMoveData{int(t.X), int(t.Y), prevX, prevY, sX, sY,
					float64(prevX) / float64(vars.resX), float64(prevY) / float64(vars.resY)}
				if game.gui.Enabled() {
					p(evData)
					//xg.Pub(xg.Ev_gui_mouse_move, evData)
					game.gui.Xgi().Context.InputSys.OnMouseMove(evData)
					if game.hud.enabled {
						game.hud.onMouseMove(evData)
					}
				}

			case *sdl.MouseButtonEvent:
				pub(ev_mouse_button_event, t)
				if t.State == sdl.PRESSED {
					if game.gui.Enabled() {
						game.gui.Xgi().Context.InputSys.OnMouseButton(t.Button, true)
						if game.hud.enabled {
							game.hud.onMouseButton(t.Button, true)
						}
					}
				} else if t.State == sdl.RELEASED {
					if game.gui.Enabled() {
						game.gui.Xgi().Context.InputSys.OnMouseButton(t.Button, false)
						if game.hud.enabled {
							game.hud.onMouseButton(t.Button, false)
						}
					}
				}

			}
		}

		//println("main.step")
		game.update(dt)
		game.draw()

		/*
			renderer.Clear()
			renderer.SetDrawColor(255, 0, 0, 255)
			renderer.FillRect(&sdl.Rect{0, 0, int32(winWidth), int32(winHeight)})
			//renderer.Copy(texture, &src, &dst)
			renderer.Present()
		*/

		lastTime = thisTime

		if fps_limit != 0 {
			//ms_to_sleep := int64((1e6 / int64(fps_limit)) - app.GetDtMs())
			//time.Sleep(time.Duration(ms_to_sleep) * time.Millisecond)
			s_to_sleep := (1.0 / float64(fps_limit)) - dt
			p(s_to_sleep)
			if s_to_sleep > 0.0 {
				sleepsec(s_to_sleep / 2)
			}
			//sleepsec(0.5)
		}

		//sleepsec(0.1)
		//pp(2)
		if app_flagOnce {
			break
		}
	}

	//deinitAudio()
	println("exiting...")
}

/////////
// Util

// Set new window site.
func resizeWindow(sx, sy int) {
	_log.Inf("Resize window:", sx, sy)
	vars.resX = sx
	vars.resY = sy
	vars.resScaleX = float64(vars.resX) / float64(vars.nativeResX)
	vars.resScaleY = float64(vars.resY) / float64(vars.nativeResY)
	vars.resAspect = float64(vars.resX) / float64(vars.resY)
}

/////////
// Init

var (
	_log        *xlog.Logger
	overrideRes bool // FIXME: add normal app module
	fullscreen  bool
	app_flagOnce      bool // FIXME: move to app module
	app_flagTestFwMod string
	
)

func InitApp() {
	println("Init() begin")
	SetDefaultVars()

	var optAppRoot = flag.String("root", "", "appRoot")
	var optRes = flag.String("res", "", "resolution WxH")
	var optFs = flag.Bool("fs", false, "full screen")
	var optDisableMusic = flag.Bool("disable_music", false, "disable music")
	var optDisableSound = flag.Bool("disable_sound", false, "disable sound")
	var optMap = flag.String("map", "", "play map")
	var optMapImage = flag.String("map_image", "", "play map image")
	var optTestFwMod = flag.String("testfw_mod", "", "use testfw module")
	var optDev = flag.Bool("dev", false, "dev mode")
	var optNoDev = flag.Bool("nodev", false, "disable dev mode")
	var optTestGame = flag.Bool("test_game", false, "test game mode")
	var optFieldW = flag.Int("fw", -1, "field width")
	var optFieldH = flag.Int("fh", -1, "field height")
	var optOnce = flag.Bool("once", false, "render one frame and exit") // TODO: add -1 alias
	var optCpuprofile = flag.String("cpuprofile", "", "write cpu profile to file")

	flag.Parse()
	fmt.Println("args:", os.Args)
	//pp(2)
	if *optAppRoot != "" {
		//pp(*optAppRoot)
		appRoot := *optAppRoot
		fmt.Println("new appRoot:", appRoot)
		os.Chdir(appRoot)
	}
	if *optRes != "" {
		ss := strings.Split(*optRes, "x")
		sx, _ := strconv.Atoi(ss[0])
		sy, _ := strconv.Atoi(ss[1])
		fmt.Printf("new res: (%d, %d)\n", sx, sy)
		//resizeWindow(sx, sy)
		vars.resX = sx
		vars.resY = sy
		vars.resAspect = float64(vars.resX) / float64(vars.resY)
		overrideRes = true
	}
	if *optFs == true {
		// XXX: comment this out to get -res option working with the -fs
		//sx, sy := rx.GetDesktopResolution()
		//vars.resX = sx
		//vars.resY = sy

		//overrideRes = true
		fullscreen = true
		vars.fullscreen = true
	}
	if *optDisableMusic == true {
		conf.disableMusic = true
	}
	if *optDisableSound == true {
		conf.disableSound = true
	}
	if *optMap != "" {
		vars.playMap = *optMap
	}
	if *optMapImage != "" {
			vars.playMapImage = *optMapImage
		}
	if *optTestFwMod != "" {
		if vars.playMap  == "" && vars.playMapImage == ""  {
			pp("check error: in this option, playMap or playMapImage should be specified")
		}
		app_flagTestFwMod =	*optTestFwMod
	}
	
	//if *optDev == true {
	//	vars.dev = true
	//}
	vars.dev = *optDev
	if *optNoDev == true {
		vars.dev = false
	}
	vars.testGame = *optTestGame
	if *optFieldW != -1 && *optFieldH != -1 {
		vars.defaultFieldW = *optFieldW
		vars.defaultFieldH = *optFieldH
	}
	if *optOnce != false {
		app_flagOnce = true
	}
	if *optCpuprofile != "" {
		f, err := os.Create(*optCpuprofile)
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(f)
		//defer pprof.StopCPUProfile()
	}

	// Extra settings
	if !vars.dev {
		vars.debugPathfinding = false
	}

	// Seed
	//rand.Seed(time.Now().UTC().UnixNano())

	// Create logger
	_log = xlog.NewLogger("ltest", xlog.LogDebug)
	if loglev := os.Getenv("LOGLEVEL"); loglev != "" {
		_log.SetLogLevelStr(loglev)
	}
	_log.SetOutputFile("ltest2d.log")
	//_log.SetWriteTime(true)
	_log.SetWriteTime(false)
	_log.SetWriteFuncName(true)
	println("Init() done")
}

func InitBaseGame() *Game {
	println("InitBaseGame() begin")

	// XXX disable settings
	/*
	// Load settings
	defaultSettingsFile := strings.ToLower(AppName) + "_default.json"
	customSettingsFile := strings.ToLower(AppName) + ".json"

	// Pick default or custom settings file
	fp := ""
	if found, err := exists(customSettingsFile); found {
		if err != nil {
			panic(err)
		}
		fp = customSettingsFile
	} else if found, err = exists(defaultSettingsFile); found {
		if err != nil {
			panic(err)
		}
		fp = defaultSettingsFile
	} else {
		pp("Settings file not found in:", defaultSettingsFile, customSettingsFile)
	}
	
	

	// Load settings
	s := newSettings()
	s.load(fp)
	s.verify()
	//pdump(s)

	// Apply settings
	if !overrideRes {
		vars.resX = s.getInt("resX")
		vars.resY = s.getInt("resY")
		vars.resAspect = float64(vars.resX) / float64(vars.resY)
	}
	*/

	var err error
	if err = sdl.Init(sdl.INIT_EVERYTHING); err != nil {
		panic(err)
	}
	//defer sdl.Quit()

	title := AppName + " (rev. " + BuildRevision + ")"
	if vars.dev {
		//title += fmt.Sprintf(" [%d]", os.Getpid())
	}

	if !fullscreen {
		window, err = sdl.CreateWindow(title, sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED,
			int32(vars.resX), int32(vars.resY), sdl.WINDOW_SHOWN|sdl.WINDOW_OPENGL)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to create window: %s\n", err)
			panic(1)
		}
	} else {
		flags := uint32(sdl.WINDOW_SHOWN | sdl.WINDOW_OPENGL)
		if !overrideRes {
			flags |= sdl.WINDOW_FULLSCREEN_DESKTOP

			dm, err := sdl.GetDesktopDisplayMode(0)
			if err != nil {
				panic(err)
			}
			vars.resX = int(dm.W)
			vars.resY = int(dm.H)
			vars.resAspect = float64(vars.resX) / float64(vars.resY)
		} else {
			flags |= sdl.WINDOW_FULLSCREEN
		}
		window, err = sdl.CreateWindow(title, sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED,
			int32(vars.resX), int32(vars.resY), flags)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to create window: %s\n", err)
			panic(1)
		}
	}

	//defer window.Destroy()

	renderer, err = sdl.CreateRenderer(window, -1, sdl.RENDERER_ACCELERATED|sdl.RENDERER_PRESENTVSYNC)
	//renderer, err = sdl.CreateRenderer(window, -1, sdl.RENDERER_ACCELERATED)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create renderer: %s\n", err)
		panic(2)
	}
	//defer renderer.Destroy()

	// XXX set the default to 0 explicitly (seems not presented on all sdl versions)
	sdl.SetHint(sdl.HINT_RENDER_SCALE_QUALITY, "0")

	//val := sdl.GetHint(sdl.HINT_RENDER_SCALE_QUALITY)
	//p("val:", val)
	//pp(val)

	game := newGame()
	// Set debug game options
	game.setGameOptions(newDebugGameOptions())
	println("InitBaseGame() done")

	//app.Win().SetTitle(AppName + "  (rev. " + BuildRevision + ")")

	// Finally
	//s.save("test_settings_out.json")

	return game
}

func initDefaultGame() {
	//game.createNewDefaultGame()
	
	game.start()
}

// XXX util ?

func GetRenderer() *sdl.Renderer {
	return renderer
}
