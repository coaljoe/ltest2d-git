package main_module

type FieldLayer struct {
	f    *Field
	name string
	w, h int
	view *FieldLayerView
}

func newFieldLayer(f *Field, name string, w, h int) *FieldLayer {
	fl := &FieldLayer{
		f:    f,
		name: name,
		w:    w,
		h:    h,
	}
	fl.view = newFieldLayerView(fl)
	return fl
}
