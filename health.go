package main_module

// component
type Health struct {
	health int
}

func newHealth() *Health {
	h := &Health{
		health: 100,
	}
	return h
}

func (h *Health) start() {}

func (h *Health) showHealth() {
	println("Health: ", h.health)
}

func (h *Health) setHealth(v int) {
	println("setHealth: ", v)
	h.health = v
}

func (h *Health) takeHealth(amt int) {
	println("takeHealth: ", amt)
	h.setHealth(h.health - amt)
}

func (h *Health) isAlive() bool {
	return h.health > 0
}

func (h *Health) destroy() {
}
