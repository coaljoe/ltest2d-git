package main_module

import (
	xg "kristallos.ga/xgui"
)

/*
type NewGameMapMode int
const (
	NewGameMapMode_Created NewGameMapMode = iota
	NewGameMapMode_Loaded
)
*/

/*
// New game options
type NewGameOpt struct {
	loadMap string
}

func newNewGameOpt() NewGameOpt {
	opt := NewGameOpt{
	
	}

	return opt
}
*/

// A tool for creating new games.
// TODO: make it a general tool to manage games?
type NewGameTool struct {
	// Waiting players for a new game.
	players    []*Player
	minPlayers int
	maxPlayers int
	fw, fh     int
	seaLevel   float64
	resources  float64
	money      float64

	// XXX load map from file,
	// instead of generating it
	loadMap string

	// XXX load heightmap image from file
	loadMapImage string

	// XXX new game was created
	created bool
}
func (ngt *NewGameTool) SetFw(v int) { ngt.fw = v }
func (ngt *NewGameTool) SetFh(v int) { ngt.fh = v }

func newNewGameTool() *NewGameTool {
	ngt := &NewGameTool{
		minPlayers: 2,
		maxPlayers: 8,
	}
	ngt.Reset()
	return ngt
}

func (ngt *NewGameTool) Reset() {
	_log.Inf("%F")

	ngt.players = make([]*Player, 0)
	ngt.fw = 0
	ngt.fh = 0
	ngt.seaLevel = 0
	ngt.resources = 0
	ngt.money = 0

	ngt.loadMap = ""
	ngt.loadMapImage = ""
	ngt.created = false
}

func (ngt *NewGameTool) AddPlayer(p *Player) {
	_log.Inf("%F p:", p)

	// XXX assign new free game player slot id

	nextN := ngt.numPlayers()
	if nextN >= ngt.maxPlayers {
		pp("can't add player: maxPlayers reached:", ngt.maxPlayers)
	}
	p.playerSlotId = nextN
	
	//println(len(ngt.players))
	//println(nextN)
	//pp(2)

	// Add player

	ngt.players = append(ngt.players, p)
	// XXX FIXME patch player
	p.camp.Money.Set(10000)
	p.camp.Pop.Set(100)
	p.camp.Fuel.Set(20500)
	//p.camp.metal.set(1000)
	p.camp.Metal.Set(999)
}

func (ngt *NewGameTool) addRandomPlayer() {

}

func (ngt *NewGameTool) numPlayers() int {
	return len(ngt.players)
}

func (ngt *NewGameTool) initBaseGame() {
	_log.Inf("%F")
}

// Create new default empty game
//func (ngt *NewGameTool) createNewDefaultGame() {
//func (ngt *NewGameTool) createNewEmptyGame() {
func (ngt *NewGameTool) initNewEmptyGame() {
	_log.Inf("%F")

	ngt.initBaseGame()

	// Add player
	p := NewPlayer(PlayerType_Human, CampId_Reds)
	p.name = "Player1"
	ngt.AddPlayer(p)

	//pp(3)
}

// Create new default game
func (ngt *NewGameTool) InitNewGame() {
	_log.Inf("%F")
	
	ngt.initBaseGame()

	//ngt.doCreateNewGame()
}

//func (ngt *NewGameTool) createNewGame(opt NewGameOpt) {
func (ngt *NewGameTool) doCreateNewGame() {
	_log.Inf("%F")

	//pdump(ngt.players)

	// Add players to game
	game.playersys.reset()
	for _, p := range ngt.players {
		game.playersys.addPlayer(p)
	}

	//pdump(game.playersys.players)

	//ngt.generateField()

	generate := true
	if ngt.loadMap != "" || ngt.loadMapImage != ""{
		generate = false
	}

	if generate {
		// XXX fixme?
		if !vars.s_generateField {
			game.newgametool.generateDefaultField()
		} else {
			game.newgametool.generateField()
		}

	} else {
		if ngt.loadMap != "" {
			// Load data
			//game.field.loadFile(ngt.loadMap)

			ml := newMapLoader()
			ml.load(ngt.loadMap)
			
			//pp(game.field.generated)
		} else if ngt.loadMapImage != "" {
			game.field.loadImage(ngt.loadMapImage)
		} else {
			pp("bad")
		}
	}

	//pp(game.playersys.players)

	//game.gameCreated = true

	ngt.created = true
}

func (ngt *NewGameTool) initNewTestGame() {
	_log.Inf("%F")

	ngt.initBaseGame()
	ngt.generateTestField()
}

func (ngt *NewGameTool) GeneratePreviewField() {
	//rand.Seed(0)
	//rand.Seed(time.Now().UnixNano())
	//pp(rand.Int())

	fsize := 32
	//fsize := 64
	//fsize := 128
	//fsize := 256
	//fsize := 512
	//fsize := 32
	//fsize := 16
	fw, fh := fsize, fsize
	//fh = 64
	//fh = fw - 64
	//fh = fw / 2
	//fh = fw / 4
	//fh = fw / 8

	// Read fw from the gui
	//fw = s.GetWidgetByName("mapWidthSpinButton").(*xg.SpinButton).IntValue()
	//fh = s.GetWidgetByName("mapHeightSpinButton").(*xg.SpinButton).IntValue()
	fw = ngt.fw
	fh = ngt.fh

	hmW := fw //fw * 2
	hmH := fh //fh * 2
	hmOpt := newHeightMapOptions()
	// Set random seed
	//hmOpt.Seed = time.Now().UnixNano()
	// Disable filter
	hmOpt.Filter = false
	game.field.generateHM(hmW, hmH, hmOpt)
	//game.field.generateHM(hmW, hmH, hmOpt)

	// Speed up generation
	// XXX add -nodev flag to disable this mode?
	if !vars.dev {
		// Enable filter
		hmOpt.Filter = true
	}
}

func (ngt *NewGameTool) generateField() {
	// Generate field from session vars
	fw, fh := vars.s_fw, vars.s_fh
	hmW := fw
	hmH := fh
	hmOpt := vars.s_hmOpt
	fgOpt := newFieldGenOptions()
	//fgOpt.disableEverything = true
	fgOpt.generateOreMines = true

	// XXX FIXME
	// for speed up
	if !vars.dev {
		fgOpt.checkZlevels = true
	}

	game.field.generate(fw, fh, hmW, hmH, hmOpt, fgOpt)
}

func (ngt *NewGameTool) generateDefaultField() {
	// Generate Field
	//fsize := 12 //6 //16 //128
	//fsize := 6
	//fsize := 64
	//fsize := 128
	fsize := 256
	//fsize := 512
	//fsize := 32
	//fsize := 16
	fw, fh := fsize, fsize
	if vars.defaultFieldW != -1 && vars.defaultFieldH != -1 {
		fw = vars.defaultFieldW
		fh = vars.defaultFieldH
	}
	hmW := fw //fw * 2
	hmH := fh //fh * 2
	hmOpt := newHeightMapOptions()
	fgOpt := newFieldGenOptions()
	fgOpt.disableEverything = true
	fgOpt.checkZlevels = true
	//fgOpt.checkZlevels = false
	p(game.field.hm.ztol(-4))
	p(game.field.hm.ztol(4))
	p(game.field.hm.regions[7])
	//pp(2)
	game.field.generate(fw, fh, hmW, hmH, hmOpt, fgOpt)
}

func (ngt *NewGameTool) generateTestField() {
	// Generate Field
	//fsize := 12 //6 //16 //128
	//fsize := 6
	fsize := 32
	//fsize := 64
	//fsize := 128
	//fsize := 256
	//fsize := 512
	//fsize := 32
	//fsize := 16
	fw, fh := fsize, fsize
	if vars.defaultFieldW != -1 && vars.defaultFieldH != -1 {
		fw = vars.defaultFieldW
		fh = vars.defaultFieldH
	}
	hmW := fw //fw * 2
	hmH := fh //fh * 2
	hmOpt := newHeightMapOptions()
	fgOpt := newFieldGenOptions()
	fgOpt.disableEverything = true
	fgOpt.checkZlevels = false
	//fgOpt.checkZlevels = false
	//pp(2)
	game.field.generate(fw, fh, hmW, hmH, hmOpt, fgOpt)
}

func (ngt *NewGameTool) checkGame() (bool, error) {
	_log.Inf("%F")

	if ngt.loadMap != "" && ngt.loadMapImage != "" {
		pp("error: both loadMap and loadMapImage specified")
	}
	
	return true, nil
}

/*
func (ngt *NewGameTool) startGame() {
	if !game.gameCreated {
		pp("error: the game wasn't created")
	}

	//setGameState("game")
	setGameState("game")

	for _, p := range game.playersys.players {
		p.start()
	}
}
*/

// XXX rename to startGame? same as startGame?
//func (ngt *NewGameTool) playGame() (bool, error) {
func (ngt *NewGameTool) StartGame() (bool, error) {
	_log.Inf("%F")

	ngt.checkGame()

	//g.lsSheet.Show()
	//game.gui.Xgi().SheetSys.SetActiveSheet(game.gui.LsSheet())
	// XXX
	//game.gui.Xgi().SheetSys.SetActiveSheet(game.gui.LsSheet().AsXgSheetI())
	game.gui.Xgi().SheetSys.SetActiveSheet(game.gui.LsSheet().(xg.SheetI))
	//game.gui.update(0)
	//game.gui.render(renderer)
	//game.update(0)
	// Force redraw
	//game.draw()
	if game != nil { // XXX FIXME
		game.draw()
	}

	//opt := newNewGameOpt()
	//ngt.createNewGame(opt)
	//ngt.createNewGame()
	//ngt.startGame()

	if !ngt.created {
		ngt.doCreateNewGame()
	}

	// XXX
	if !ngt.created {
		pp("error: new game wasn't created")
	}

	//// Start game

	if !game.gameCreated {
		pp("error: the game wasn't created")
	}

	//setGameState("game")
	setGameState("game")

	for _, p := range game.playersys.players {
		p.start()
	}

	return true, nil
}
