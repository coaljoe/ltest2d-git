#include "uniforms.frag"
#include "lighting.frag"


void main() {
    vec3 color;
    vec4 color1;

    color = ambient + calcLambertLight(L, N, mat.diffuse, light.intensity);

#ifdef _TEXTURE_MAP
    color1 = vec4(color, 1.0);
    color1 *= texture2D(texMap, gl_TexCoord[0].st);
    //color *= color1.rgb;
    //color *= texture2D(texMap, gl_TexCoord[0].st).rgb;
#endif

    //color *= calcShadow();

    
    
    // alpha-mul version (fixme: test more?)
    const float alpha_mul = 0.33;
    gl_FragColor = vec4(color1.rgb, 1.0 * (color1.a * alpha_mul));
    
    //gl_FragColor = color1;
    //gl_FragColor = vec4(color, 0.6);
    //gl_FragColor = vec4(0.0, 0.0, 1.0, 0.5);
}

/* vim:set ft=glsl: */
