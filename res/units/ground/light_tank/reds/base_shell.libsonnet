//ammotype
{
  type: "Shell",  // Projectile ?
  name: "76mm_gun_shell_reds",
  caliber: 76,
  cost: 0,
  moveSpeed: 100,
  reliability: 0.9,
  accuracy: 0.8,
  weight: 9,
  affectRadius: 1,
  radius: 10.0,
  locateRadius: 20.0,
  attackRating: 3,
}
