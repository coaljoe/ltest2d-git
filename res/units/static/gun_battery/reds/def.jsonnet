local uc_lib = import 'res/unit_components/lib.libsonnet';

{
    unit: {
        gui_name: "Gun Battery",
        name: "gun_battery",
	weight: 30000.0,
    	armor: 50,
        static: true,
    },
    
    combat: {
        defence_rating: 6,
    },
    
    unitAi: {
    },
    
    turretSlots: {
        main: uc_lib.generic_tank_turret,
    },
    
    // View
    view: {
      turretViews: {
        main: {
          // Offset position
          pos_x: 14,
          //pos_y: 14,
          pos_y: 7,
          snd_fire: "fire.ogg",
          snd_reload: "reload.ogg",
        },
      }
    },
}