local base = import 'res/buildings/housing/base_housing.libsonnet';

{
    // Building
    building: base.building {
        camp: "arctic",
    },
  
    // Combat
    combat: base.combat {
        //defenceRating: 1,
    },

    // View
    view: base.view {
    }
}
