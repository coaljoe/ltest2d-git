local base = import 'res/buildings/factory/base_factory.libsonnet';

{
    // Building
    building: base.building {
        camp: "reds",
    },
  
    // Combat
    combat: base.combat {
    },

    // View
    view: base.view {
    }
}
