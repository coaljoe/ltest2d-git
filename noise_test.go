// XXX not used
package main_module

import (
	"testing"
)

//var _ = `
func TestNoise(t *testing.T) {
	//w, h := 128, 64
	//w, h := 64, 128
	//w, h := 64*4, 128*4
	w, h := 128*4, 128*4
	//w, h := 64, 64

	data := makeNoise(w, h)

	sum := 0.0
	for _, n := range data {
		sum += n
	}
	//pp(sum)

	//pp(data[len(data)-1])
	//pp(data)
	/*
	   var datax []float64
	   //datax := data[:128]
	   //datax := data[8:]
	   copy(data[:8], datax)
	   pp(len(datax))
	   pp(datax)
	*/
	saveNoise("tmp/noise_out.png", data, w, h)
	//pp(4)
}

//`

func TestNoise2(t *testing.T) {
	w, h := 64, 64

	data := makeNoise(w, h)

	sum := 0.0
	for _, n := range data {
		sum += n
	}
	//pp(sum)

	saveNoise("tmp/noise_out.png", data, w, h)
	//pp(4)

	// data2
	data2 := makeNoise(w, h)
	saveNoise("tmp/noise_out2.png", data2, w, h)
}
