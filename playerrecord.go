package main_module

type PlayerRecord struct {
	player	*Player
}

func newPlayerRecord(p *Player) *PlayerRecord {
	pr := &PlayerRecord{
		player: p,
	}
	
	return pr
}

func (pr *PlayerRecord) GetPlayer() *Player { return pr.player }
func (pr *PlayerRecord) SetPlayer(p *Player) { pr.player = p }
func (pr *PlayerRecord) GetPlayerRecord() *PlayerRecord { return pr } // ?

func (pr *PlayerRecord) IsNeutral() bool {
	return pr.player == nil
}

/*
// nil means no player was assigned / no owner (?)
func (pr *PlayerRecord) assignPlayer(p *Player) {
	_log.Inf("%F p:", p)

	// XXX check camp compatibility?
	// XXX setPlayer?

	pr.player = p
}
*/
