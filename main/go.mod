module kristallos.ga/ltest2d/main

require kristallos.ga/ltest2d/main_module v0.0.0-replace

require kristallos.ga/ltest2d/gui v0.0.0-replace

require (
	github.com/WastedCode/serializer v0.0.0-20150605061548-b76508a5f9d4 // indirect
	github.com/bitly/go-simplejson v0.5.0 // indirect
	github.com/bradfitz/iter v0.0.0-20191230175014-e8f45d346db8 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/drbig/perlin v0.0.0-20141206142450-e4c467936143 // indirect
	github.com/emirpasic/gods v1.12.0 // indirect
	github.com/fossoreslp/go-uuid-v4 v1.0.0 // indirect
	github.com/hjson/hjson-go v3.0.1+incompatible // indirect
	github.com/iancoleman/orderedmap v0.3.0 // indirect
	github.com/kardianos/osext v0.0.0-20190222173326-2bc1f35cddc0 // indirect
	github.com/looplab/fsm v0.1.0 // indirect
	github.com/ojrac/opensimplex-go v1.0.1 // indirect
	github.com/peterbourgon/mergemap v0.0.1 // indirect
	github.com/veandco/go-sdl2 v0.4.40 // indirect
	kristallos.ga/lib/debug v0.0.0-20231206203330-ec813b10aad7 // indirect
	kristallos.ga/lib/ider v0.0.0-20231206203330-ec813b10aad7 // indirect
	kristallos.ga/lib/pubsub v0.0.0-20231206203330-ec813b10aad7 // indirect
	kristallos.ga/lib/sr v0.0.0-20231206203330-ec813b10aad7 // indirect
	kristallos.ga/lib/xlog v0.0.0-20231206203330-ec813b10aad7 // indirect
	kristallos.ga/ltest2d/gui/gui_base v0.0.0-replace // indirect
	kristallos.ga/rx/math v0.0.0-20240612080209-1fc7c212cf81 // indirect
	kristallos.ga/rx/transform v0.0.0-20240115160632-7763db890ad7 // indirect
	kristallos.ga/xgui v0.0.0-20240806082116-a6d830fde05b // indirect
)

go 1.22.3

replace kristallos.ga/ltest2d/main_module => ../

replace kristallos.ga/ltest2d/gui => ../gui

replace kristallos.ga/ltest2d/gui/gui_base => ../gui/gui_base

//replace kristallos.ga/xgui => ../../xgui_upstream1_test1
