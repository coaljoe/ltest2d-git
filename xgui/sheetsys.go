package xgui

type SheetSys struct {
	*System
	activeSheet SheetI
}

func newSheetSys() *SheetSys {
	s := &SheetSys{}
	s.System = newSystem("SheetSys", "Sheet system")
	return s
}

func (s *SheetSys) CreateSheet(name string) *Sheet {
	return NewSheet(name)
}

func (s *SheetSys) AddSheet(sh SheetI) {
	s.addElem(sh)

	// Activate sheet if no sheet was set before
	if s.activeSheet == nil {
		s.SetActiveSheet(sh)
	}
}

func (s *SheetSys) SetActiveSheet(sh SheetI) {
	s.activeSheet = sh
}

func (s *SheetSys) ActiveSheet() SheetI {
	return s.activeSheet
}
