package xgui

import (
	ps "kristallos.ga/lib/pubsub"
	"fmt"
	"github.com/veandco/go-sdl2/sdl"
)

// A Widget.
type SpinButton struct {
	*Widget
	// Properties
	text          string
	Value         float64
	ValueStep     float64
	ValueRangeMin float64
	ValueRangeMax float64
	styleVertical bool
	// Components
	Label       *Label // XXX: make it public?
	Panel       *Panel
	PlusButton  *Button
	MinusButton *Button
}

func TestCallback3() {
	p("test callback3")
	pp(2)
}

func NewSpinButton(rect Rect) *SpinButton {
	b := &SpinButton{
		Widget:    NewWidget("SpinButton"),
		ValueStep: 1.0,
		//ValueStep: 0.2,
		ValueRangeMin: 0.0,
		ValueRangeMax: 9999.0,
		styleVertical: false,
	}
	b.rect = rect
	b.passEvents = true

	b.Label = NewLabel(Pos{rect[0], rect[1]}, "Label")
	b.Label.SetName("_Label")
	//b.Label.SetText("SpinButtonLabelText")
	//b.Label.SetText("0")
	//b.Label.SetText(b.Value)
	b.Label.border = false
	b.Label.bgColor.unset()
	b.Label.TextAlign = TextAlignType_Center
	//b.Label.SetPos(Pos{0.5, 1.0})
	//b.Label.SetPos(Pos{0.5, 0.8})
	//b.Label.SetPos(Pos{0.5, 0.33 + (0.33 / 2.0)})
	//pp(b.Label.rect)
	b.Label.MarkAsPrivate()
	b.AddChild(b.Label)

	//b.PlusButton = NewButton(Rect{0, 0, 1, 1})
	//b.PlusButton = NewButton(Rect{0, 0, 1, .33})
	//b.PlusButton = NewButton(Rect{0, 0, 100, 100})
	b.PlusButton = NewButton(Rect{0, 0, -1, -1})
	//b.PlusButton = NewButton(rect)
	b.SetName("_PlusButton")
	//b.PlusButton.Label().SetText("+")
	b.PlusButton.Label().SetText("∆")
	//b.PlusButton.
	b.PlusButton.MarkAsPrivate()
	b.AddChild(b.PlusButton)

	//b.MinusButton = NewButton(Rect{0, 0, 1, 1})
	//b.MinusButton = NewButton(Rect{0, .66, 1, .33})
	//b.MinusButton = NewButton(Rect{0, 0, 100, 100})
	b.MinusButton = NewButton(Rect{0, 0, -1, -1})
	b.SetName("_MinusButton")
	//b.MinusButton.Label().SetText("-")
	b.MinusButton.Label().SetText("∇")
	//b.PlusButton.
	b.MinusButton.MarkAsPrivate()
	b.AddChild(b.MinusButton)

	// Callbacks
	plusCB := func(pl interface{}) {
		nextVal := b.Value + b.ValueStep
		b.Value = nextVal
		if b.Value > b.ValueRangeMax {
			b.Value = b.ValueRangeMax
		}
	}

	//b.PlusButton.SetCallback(TestCallback3)
	b.PlusButton.SetCallback(plusCB, nil)

	minusCB := func(pl interface{}) {
		nextVal := b.Value - b.ValueStep
		b.Value = nextVal
		if b.Value < b.ValueRangeMin {
			b.Value = b.ValueRangeMin
		}
	}

	//b.PlusButton.SetCallback(TestCallback3)
	b.MinusButton.SetCallback(minusCB, nil)

	/*
		Sub(Ev_mouse_enter, b.OnMouseEnterEv)
		Sub(Ev_mouse_out, b.OnMouseOutEv)
		Sub(Ev_mouse_button, b.OnMouseSpinButtonEv)
	*/

	b.build()
	return b
}

func (b *SpinButton) OnMouseEnterEv(ev *ps.Event) {
	if ev.Data.(WidgetI).Id() != b.Id() {
		return
	}
	//pp(12)
}

func (b *SpinButton) OnMouseOutEv(ev *ps.Event) {
	if ev.Data.(WidgetI).Id() != b.Id() {
		return
	}
}

func (b *SpinButton) OnMouseSpinButtonEv(ev *ps.Event) {
	//p("ev:")
	//dump(ev)
	//evData := ev.Data.(*EvMouseButtonData)
	//btn := evData.MouseButton
	//pressed := evData.Pressed
}

func (b *SpinButton) build() {
	if b.styleVertical {
		// XXX TODO
		/*
			b.MinusButton.SetRect(Rect{0, .66, 1, .33})
			b.PlusButton.SetRect(Rect{0, 0, 1, .33})
			b.Label.SetPos(Pos{0.5, 0.33 + (0.33 / 2.0)})
		*/
		b.MinusButton.Label().SetText("∇")
		b.PlusButton.Label().SetText("∆")
	} else {
		/*
			b.MinusButton.SetRect(Rect{0, 0, .33, 1})
			b.PlusButton.SetRect(Rect{.66, 0, .33, 1})
			b.Label.SetPos(Pos{0.33 + (0.33 / 2), 0.5})
		*/
		//pp(b.rect)
		//pp(b.MinusButton.rect)
		//b.MinusButton.SetVisible(false)
		//b.PlusButton.SetVisible(false)
		//b.Label.SetVisible(false)
		//pp(2)
		xw, xh := float64(b.rect.W()), float64(b.rect.H())
		b.MinusButton.SetRect(Rect{0, 0, int(xw * .33), int(xh * 1.0)})
		//pp(b.MinusButton.rect)
		b.PlusButton.SetRect(Rect{int(xw * .66), 0, int(xw * .33), int(xh * 1.0)})
		b.Label.SetPos(Pos{int(xw * (0.33 + (0.33 / 2))), int(xh * 0.5)})
		//b.MinusButton.Label().SetText("⊲")
		//b.PlusButton.Label().SetText("⊳")
		b.MinusButton.Label().SetText("◁")
		//b.MinusButton.Label().SetText("◀")
		//b.MinusButton.Label().SetText("←")
		//b.MinusButton.Label().SetText("<")
		b.PlusButton.Label().SetText("▷")
		//b.PlusButton.Label().SetText("▶")
		//b.PlusButton.Label().SetText("→")
		//b.PlusButton.Label().SetText(">")
	}
}

func (b *SpinButton) SetStyleVertical(v bool) {
	b.styleVertical = v
	b.build()
}

func (b *SpinButton) StyleVertical() bool {
	return b.styleVertical
}

func (b *SpinButton) IsIntegerValueStep() bool {
	return b.ValueStep == float64(int(b.ValueStep))
}

func (b *SpinButton) IntValue() int {
	return int(b.Value)
}

func (b *SpinButton) Render(r *sdl.Renderer) {
	_log.Dbg("%F")
	//b.Panel.Render(r)
	b.Widget.Render(r)
	defer b.Widget.RenderChildren(r)

	//r1 := b.PlusButton.GetAbsoluteRect()
	//r2 := b.PlusButton.Panel.GetAbsoluteRect()
	r1 := b.MinusButton.GetAbsoluteRect()
	r2 := b.MinusButton.Panel.GetAbsoluteRect()
	p("r1:", r1)
	p("r2:", r2)
	p("parent:", b.PlusButton.Panel.HasParent())

	p("YYY:", b.MinusButton.Rect())
	p("panel rect:", b.MinusButton.Panel.Rect())

	//b.Label.Render(r)
	//b.PlusButton.Render(r)
	_log.Dbg("%F done")
}

func (b *SpinButton) Update(dt float64) {
	b.Widget.Update(dt)
	b.PlusButton.Update(dt)
	b.MinusButton.Update(dt)
	//p.label.Update(dt)
	//b.Label.SetPos(Pos{b.rect[0] + (b.rect[2] / 2), b.rect[1] + (b.rect[3] / 2)})
	//fmt.Println("b.rect:", b.rect)

	// Update value
	newValS := ""
	if b.IsIntegerValueStep() {
		newValS = fmt.Sprintf("%d", int(b.Value))
	} else {
		newValS = fmt.Sprintf("%.2f", b.Value)
	}
	b.Label.SetText(newValS)

	//panic(2)
}
