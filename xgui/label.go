package xgui

import (
	"github.com/veandco/go-sdl2/sdl"
)

type TextAlignType int

const (
	TextAlignType_Left TextAlignType = iota
	TextAlignType_Center
	TextAlignType_Right
)

type TextVAlignType int

const (
	TextVAlignType_Top TextVAlignType = iota
	TextVAlignType_Middle
	TextVAlignType_Bottom
)

// A widget.
type Label struct {
	*Widget
	text string
	//textColor Color
	textFont   StyleOption
	textColor  StyleOption
	TextAlign  TextAlignType
	TextVAlign TextVAlignType
	//cb        func()
}

func (la *Label) Text() string { return la.text }

//func (la *Label) SetCallback(cb func()) { la.cb = cb }

//func NewLabel(rect Rect) *Label {
func NewLabel(pos Pos, text string) *Label {
	la := &Label{
		Widget: NewWidget("label"),
		//textColor: ctx.Style.textColor,
		//textColor: newStyleOption("textColor"),
		/*
			textFont:   newStyleOption(ctx.Style.textFont),
			textColor:  newStyleOption(&ctx.Style.textColor),
		*/
		TextAlign:  TextAlignType_Left,
		TextVAlign: TextVAlignType_Top,
	}
	la.rect[0] = pos[0]
	la.rect[1] = pos[1]
	la.rect[2] = -1
	la.rect[3] = -1
	la.SetText(text)
	//Sub(Ev_mouse_enter, w.EvMouseEnter)
	//Sub(Ev_mouse_out, w.EvMouseOut)
	return la
}

func (la *Label) build() {
	/*
		ph := rxi.Renderer().GetPh()
		font := la.textFont.value().(*Font)
		la.rect[3] = float64(font.Height()) * ph
		//la.rect[3] = float64(32) * ph
	*/

	// Update/fix widgets rect width and height
	tw, th := ctx.TextRenderer.SizeText(la.text, la.Style.TextFont)
	la.rect[2] = tw
	la.rect[3] = th
}

func (la *Label) SetText(s string) {
	la.text = s
	la.build()
}

func (la *Label) SetPos(pos Pos) {
	la.rect[0] = pos[0]
	la.rect[1] = pos[1]
	la.build()
}

func (la *Label) Render(r *sdl.Renderer) {
	la.Widget.Render(r)
	defer la.Widget.RenderChildren(r)

	//px := la.rect.X()
	//py := la.rect.Y()

	//p("ZZZ")
	absRect := la.GetAbsoluteRect()
	px := absRect.X()
	py := absRect.Y()
	//p("ZZZ.")

	tw, th := ctx.TextRenderer.SizeText(la.text, la.Style.TextFont)

	if la.TextAlign == TextAlignType_Center {
		px -= tw / 2
		py -= th / 2
	} else if la.TextAlign == TextAlignType_Right {
		pp("not implemented")
	}

	ctx.TextRenderer.RenderTextColor(r, la.text, px, py,
		la.Style.TextColor, la.Style.TextFont)
}

var _ = `
func (la *Label) Render(r *rx.Renderer) {
	//return
	la.Widget.Render(r)
	//sx := 2.0 / float64(r.Width())
	//sy := 2.0 / float64(r.Height())
	px := la.rect.X()
	py := la.rect.Y()
	//ctx.TextRenderer.RenderText(la.text, -1+8*sx, 1-50*sy, sx, sy)

	prevDepthTest := false
	if gl.IsEnabled(gl.DEPTH_TEST) {
		prevDepthTest = true
		gl.Disable(gl.DEPTH_TEST)
	}

	ctx.TextRenderer.RenderTextColor(la.text, px, py,
		*la.textColor.value().(*Color))
	//gl.Enable(gl.DEPTH_TEST)

	if prevDepthTest {
		gl.Enable(gl.DEPTH_TEST)
	}

	if false {
		//ctx.TextRenderer.RenderText("XXX", sx, sy, sx, sy)
		//ctx.TextRenderer.RenderText("YYY", 0, 0, sx, sy)
		//ctx.TextRenderer.RenderText("Последние новости", sx, sy)
		gl.Disable(gl.DEPTH_TEST)
		//ctx.TextRenderer.RenderText("Последние новости", 0.5, 0.5)
		ctx.TextRenderer.RenderText("Последние новости", .5, 0.5)
		ctx.TextRenderer.RenderTextColor("Последние новости 2", 0, 1, ColorGreen)
		gl.Enable(gl.DEPTH_TEST)
		gl.Disable(gl.DEPTH_TEST)
		//ctx.TextRenderer.RenderText("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx", -1+1*sx, 1-100*sy)
		gl.Enable(gl.DEPTH_TEST)
	}
}
`

func (la *Label) Update(dt float64) {
	la.Widget.Update(dt)
}
