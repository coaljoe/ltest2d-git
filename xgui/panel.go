package xgui

import (
	ps "kristallos.ga/lib/pubsub"
	//"fmt"

	"github.com/veandco/go-sdl2/sdl"
)

// A Widget.
type Panel struct {
	*Widget
	// Properties
	color *Color
	text  string
	// Components
	label               *Label // XXX: make it public?
	BgImage             *Image
	ScaleBgImage        bool
	BgImageScaleKx      float64
	BgImageScaleKy      float64
	BgImagePadding      Rect
	BgImageHide         bool
	CustomPatch9        *Patch9
	DrawAsRect          bool
	DrawRectColor       Color
	DrawRectBorderWidth int
	DrawRectBorderColor Color
}

func (p *Panel) Color() *Color     { return p.color }
func (p *Panel) SetColor(c *Color) { p.color = c }
func (p *Panel) Label() *Label     { return p.label }

func NewPanel(rect Rect) *Panel {
	w := &Panel{
		Widget: NewWidget("panel"),
		color:  &ColorWhite,
		//label:          NewLabel(Pos{0.5, 0.5}, "label"),
		label:          NewLabel(Pos{0, 0}, "PanelLabelText"),
		BgImage:        NewImage(),
		BgImageScaleKx: 1.0,
		BgImageScaleKy: 1.0,
		BgImagePadding: Rect{0, 0, 0, 0},
		BgImageHide:    false,
		DrawRectColor:  ColorWhite,
	}
	w.rect = rect
	w.label.SetName("_panel_label")
	//w.label.SetText("PanelLabelText")
	w.label.border = false
	w.label.bgColor.unset()
	w.label.MarkAsPrivate()
	w.AddChild(w.label)
	Sub(Ev_mouse_enter, w.OnMouseEnterEv)
	Sub(Ev_mouse_out, w.OnMouseOutEv)
	return w
}

func (p *Panel) OnMouseEnterEv(ev *ps.Event) {
	if ev.Data.(WidgetI).Id() != p.Id() {
		return
	}
	if p.color != nil {
		//colorObj := Color(p.color.ToVec4().AddScalar(0.5))
		colorObj := Color{}.FromVec4(p.color.ToVec4().AddScalar(0.5))
		p.color = &colorObj
		p.color[3] = 1.0
	}
	//pp(12)
}

func (p *Panel) OnMouseOutEv(ev *ps.Event) {
	if ev.Data.(WidgetI).Id() != p.Id() {
		return
	}
	if p.color != nil {
		//colorObj := Color(p.color.ToVec4().SubScalar(0.5))
		colorObj := Color{}.FromVec4(p.color.ToVec4().SubScalar(0.5))
		p.color = &colorObj
		p.color[3] = 1.0
		//pp(13)
	}
}

func (p *Panel) Render(r *sdl.Renderer) {
	//p_("Panel.Render; name:", p.name, "id:", p.id)
	// Call Base
	p.Widget.Render(r)
	defer p.Widget.RenderChildren(r)
	//pp(2)
	/*
		r.SetDrawColor(255, 0, 0, 255)
		var rectangle sdl.Rect

		rectangle.X = 0
		rectangle.Y = 0
		rectangle.W = 50
		rectangle.H = 50
		r.FillRect(&rectangle)
	*/

	absRect := p.GetAbsoluteRect()
	//_ = rect
	//p_("XXX", p.rect, absRect)

	var rectangle sdl.Rect

	rectangle.X = int32(absRect.X())
	rectangle.Y = int32(absRect.Y())
	rectangle.W = int32(absRect.W())
	rectangle.H = int32(absRect.H())

	if ctx.Style.PanelPatch9 != nil || p.CustomPatch9 != nil {
		if p.CustomPatch9 != nil {
			p.CustomPatch9.Render(int(rectangle.X), int(rectangle.Y), int(rectangle.W), int(rectangle.H))
		} else {
			ctx.Style.PanelPatch9.Render(int(rectangle.X), int(rectangle.Y), int(rectangle.W), int(rectangle.H))
		}
		//pp("derp")
	} else {
		//r.SetDrawColor(255, 0, 0, 255)
		r.SetDrawColor(uint8((*p.color).R()*255), uint8((*p.color).G()*255), uint8((*p.color).B()*255), 255)
		r.FillRect(&rectangle)
	}

	if p.DrawAsRect {
		c := p.DrawRectColor
		r.SetDrawColor(uint8(c.R()), uint8(c.G()), uint8(c.B()), 255)
		r.FillRect(&rectangle)
		if p.DrawRectBorderWidth > 0 {
			c := p.DrawRectBorderColor
			sdlC := sdl.Color{}
			sdlC.R = uint8(c.R())
			sdlC.G = uint8(c.G())
			sdlC.B = uint8(c.B())
			sdlC.A = 255
			//r.SetDrawColor(uint8(c.R()), uint8(c.G()), uint8(c.B()), 255)
			//r.DrawRect(&rectangle)
			drawRectangleColorWidth(r,
				int(rectangle.X), int(rectangle.Y), int(rectangle.W), int(rectangle.H), sdlC, p.DrawRectBorderWidth)
		}
	}

	if p.BgImage.HasImage() && !p.BgImageHide {
		//if true {
		if p.ScaleBgImage {
			//sx, sy := 0.25, 0.25 // XXX FIXME
			//sx, sy := 0.5, 0.5 // XXX FIXME
			//sx, sy := PxW(p.BgImage.W()), PxH(p.BgImage.H())
			//sx, sy := absRect.W(), absRect.H()
			// Need scale
			nsx, nsy := absRect.W(), absRect.H()
			// Have scale
			//hsx, hsy := p.BgImage.W()+(p.BgImagePadding.X()+p.BgImagePadding.W()), p.BgImage.H()+(p.BgImagePadding.Y()+p.BgImagePadding.H())
			//hsx, hsy := p.BgImage.W()+(p.BgImagePadding.W()), p.BgImage.H()+(p.BgImagePadding.H())
			hsx, hsy := p.BgImage.W(), p.BgImage.H()
			kx, ky := float64(nsx)/float64(hsx), float64(nsy)/float64(hsy)
			p_(kx, ky)
			//p_(sx, sy, vars.resX, vars.resY)
			//pp(p.BgImage.W(), p.BgImage.H())
			//p.BgImage.RenderScale(r, int(rectangle.X), int(rectangle.Y), -1, -1, sx, sy)
			//p.BgImage.RenderScale(r, int(rectangle.X), int(rectangle.Y), -1, -1, kx, ky)
			p.BgImage.RenderScale(r, int(rectangle.X)+p.BgImagePadding.X(), int(rectangle.Y)+p.BgImagePadding.Y(), -1, -1, kx, ky)
			// Store ks for later use
			p.BgImageScaleKx = kx
			p.BgImageScaleKy = ky
		} else {
			p.BgImage.Render(r, int(rectangle.X)+p.BgImagePadding.X(), int(rectangle.Y)+p.BgImagePadding.Y(), -1, -1)
		}
		//pp(2)
	}

	//p.label.Render(r)

	/*
		// ??? Render children
		for _, c := range p.Children() {
			c.Render(r)
		}
	*/
	/*
		// ??? Render children
		for _, c := range p.Children() {
			p_("XXX c -> ", c)
			c.Render(r)
		}
		p_("YYY c -> ", p.Widget.Children())
		pp(2)
	*/

	/*
		// ??? Render children
		for _, c := range p.Children() {
			c.Render(r)
		}
	*/
}

var _ = `
func (p *Panel) Render(r *rx.Renderer) {
	println("Panel.Render")

	// Call Base
	p.Widget.Render(r)

	rect := p.GetAbsoluteRect()
	_ = rect
	fmt.Println("p.color", p.color)

	//gl.PolygonOffset(1.0, 1.0)
	//gl.Enable(gl.POLYGON_OFFSET_FILL)

	gl.PushMatrix()
	//gl.Translatef(0.0, 0.0, 0.1)

	//r.RenderQuad(p.rect.X(), p.rect.Y(), p.rect.Width(), p.rect.Height(), 0)
	if p.color != nil {
		rx.DrawBegin()
		//r.Set2DMode()

		rx.DrawSetColor((*p.color).R(), (*p.color).G(), (*p.color).B())

		//rx.DrawSetColor(1.0, 0.0, 0.0)
		//rx.DrawRectV(Vec4(p.rect))
		//rx.DrawQuadV(Vec4(p.rect))
		rx.DrawQuadV(Vec4(rect))
		rx.DrawResetColor()
		//r.Unset2DMode()
		rx.DrawEnd()
	}

	p.label.Render(r)

	gl.PopMatrix()

	//p.label.Render(r)

	//gl.Disable(gl.POLYGON_OFFSET_FILL)

	println("Panel.Render done")
	//panic(2)
}
`

func (p *Panel) Update(dt float64) {
	p.Widget.Update(dt)
	//p.label.Update(dt)
	//p.label.SetPos(Pos{p.rect[0] + (p.rect[2] / 2), p.rect[1] + (p.rect[3] / 2)})
	//p.label.SetPos(Pos{p.rect[2] / 2, p.rect[3] / 2})
	absRect := p.GetAbsoluteRect()
	//absRect := p.parent.GetAbsoluteRect()
	//pAbsRect := p.parent.GetAbsoluteRect()
	//p.label.SetPos(Pos{absRect.X() + absRect.W()/2, absRect.Y() + absRect.H()/2})
	p.label.SetPos(Pos{absRect.W() / 2, absRect.H() / 2})
	/*
		fmt.Println("p.id =", p.Id())
		fmt.Println("p.name:", p.Name())
		fmt.Println("p.rect:", p.rect)
		fmt.Println("p.absRect:", absRect)
	*/
	//fmt.Println("p.pAbsRect:", pAbsRect)
	//panic(2)
	/*
		if p.Id() != 11 && p.Id() != 13 {
			pdump(p)
		}
	*/

	// ??? Update children
	for _, c := range p.Children() {
		c.Update(dt)
	}
}
