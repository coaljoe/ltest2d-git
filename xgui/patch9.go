package xgui

import (
	"github.com/veandco/go-sdl2/sdl"
)

type RectPair struct {
	V0, V1 sdl.Rect
}

type Patch9 struct {
	Im    *Image
	Rects [9]sdl.Rect
	//Rects [9]RectPair
}

func NewPatch9() *Patch9 {
	p := &Patch9{
		Im: NewImage(),
	}
	return p
}

func (p *Patch9) Load(path string) {
	p.Im.Load(path)
}

func (p *Patch9) Render(x, y, w, h int) {
	// Not ideal, but works
	for i := 0; i < 9; i++ {
		//for i := 5; i < 6; i++ {
		/*
			srcrect := p.Rects[i].V0
			dstrect := p.Rects[i].V1
		*/
		srcrect := p.Rects[i]
		//dstrect := sdl.Rect{int32(x) + (srcrect.X + int32(w)), int32(y) + srcrect.Y,
		dstrect := sdl.Rect{int32(x) + srcrect.X, int32(y) + srcrect.Y,
			srcrect.W, srcrect.H}

		debug := false

		if debug {
			p_("i:", i)
			p_("x:", x, "y:", y)
			p_("w:", w, "h:", h)
			p_("s:", srcrect)
			p_("d:", dstrect)
			//pp(2)
		}

		// Scale dstrect
		// Top
		if i == 1 {
			topRightCornerW := p.Rects[2].W
			//pp(dstrect)
			//dstrect.W = int32(w) - topRightCornerW
			dstrect.W = int32(w) - srcrect.W - topRightCornerW
			//dstrect.W = int32(w) - srcrect.W
			//dstrect.W = int32(w) - 10
			if debug {
				p_("d patched:", dstrect)
				//pp(dstrect)
				//pp(2)
			}
		}
		// Top-Right
		if i == 2 {
			topRightCornerW := p.Rects[2].W
			//dstrect.X = int32(x) + int32(w) - srcrect.W - topRightCornerW
			dstrect.X = int32(x) + int32(w) - topRightCornerW
			if debug {
				p_("d patched:", dstrect)
				//dstrect.H = int32(h) - srcrect.H
			}
		}
		// Left
		if i == 3 {
			//dstrect.H = int32(h) - srcrect.H
			bottomLeftCornerH := p.Rects[6].H
			//dstrect.H = int32(h) - srcrect.H
			dstrect.H = int32(h) - bottomLeftCornerH
			if debug {
				p_("d patched:", dstrect)
			}
		}
		//int32(w), int32(h)}
		// Central patch
		if i == 4 {
			//dstrect = sdl.Rect{int32(x), int32(y), int32(h), int32(w)}
			topRightCornerH := p.Rects[2].H
			topRightCornerW := p.Rects[2].W
			bottomLeftCornerH := p.Rects[6].H
			bottomLeftCornerW := p.Rects[6].W
			dstrect.Y = int32(y) + topRightCornerH
			dstrect.H = int32(h) - topRightCornerH - bottomLeftCornerH
			//dstrect.W = 300
			dstrect.W = int32(w) - topRightCornerW - bottomLeftCornerW
			if debug {
				p_("d patched:", dstrect)
				//continue
			}
		}
		// Right
		if i == 5 {
			topRightCornerW := p.Rects[2].W
			//dstrect.X = int32(x) + int32(w) - srcrect.W - topRightCornerW
			dstrect.X = int32(x) + int32(w) - topRightCornerW
			bottomLeftCornerH := p.Rects[6].H
			//dstrect.H = int32(h) - srcrect.H
			dstrect.H = int32(h) - bottomLeftCornerH
			if debug {
				p_("d patched:", dstrect)
			}
		}
		// Bottom-left
		if i == 6 {
			dstrect.Y = int32(y+h) - srcrect.H
			if debug {
				p_("d patched:", dstrect)
			}
		}
		// Bottom
		if i == 7 {
			dstrect.Y = int32(y+h) - srcrect.H
			topRightCornerW := p.Rects[2].W
			//pp(dstrect)
			//dstrect.W = int32(w) - topRightCornerW
			dstrect.W = int32(w) - srcrect.W - topRightCornerW
			if debug {
				p_("d patched:", dstrect)
			}
		}
		// Bottom-right
		if i == 8 {
			dstrect.Y = int32(y+h) - srcrect.H
			topRightCornerW := p.Rects[2].W
			dstrect.X = int32(x) + int32(w) - topRightCornerW
			if debug {
				p_("d patched:", dstrect)
			}
		}

		p.Im.RenderRect(srcrect, dstrect)
	}
	//pp(2)
}
