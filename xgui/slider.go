package xgui

import (
	ps "kristallos.ga/lib/pubsub"
	"github.com/veandco/go-sdl2/sdl"
)

// A Widget.
type Slider struct {
	*Widget
	// Properties
	Value         float64
	ValueStep     float64
	ValueRangeMin float64
	ValueRangeMax float64
	// Components
	//Label       *Label
	//Panel          *Panel
	ScorePanel      *Panel
	SlidingBitPanel *Panel
	//StyleScoreColor    Color
	StyleScoreDashStep int
	sliderPosition     float64
	dragMouseOffsetX   int
	dragging           bool
}

func NewSlider(rect Rect) *Slider {
	s := &Slider{
		Widget:        NewWidget("Slider"),
		ValueStep:     0.0, // For ranges with limited number of steps?
		ValueRangeMin: 0.0,
		ValueRangeMax: 1.0,
		//StyleScoreColor:    ColorBlue,
		StyleScoreDashStep: 20,
	}
	s.rect = rect
	s.passEvents = true

	/*
		b.Label = NewLabel(Pos{rect[0], rect[1]}, "Label")
		//b.Label.SetText("SpinButtonLabelText")
		//b.Label.SetText("0")
		//b.Label.SetText(b.Value)
		b.Label.border = false
		b.Label.bgColor.unset()
		b.Label.TextAlign = TextAlignType_Center
		//b.Label.SetPos(Pos{0.5, 1.0})
		//b.Label.SetPos(Pos{0.5, 0.8})
		//b.Label.SetPos(Pos{0.5, 0.33 + (0.33 / 2.0)})
		//pp(b.Label.rect)
		b.AddChild(b.Label)
	*/
	//s.ScorePanel = NewPanel(Rect{0, 0, -1, 10})
	s.ScorePanel = NewPanel(Rect{0, 0, -1, 8})
	s.ScorePanel.SetName("_ScorePanel")
	s.ScorePanel.Label().SetText("")
	s.ScorePanel.CustomPatch9 = ctx.Style.ButtonPressedPatch9
	// Center position
	s.ScorePanel.rect[1] = s.rect.H()/2 - s.ScorePanel.rect.H()/2
	s.AddChild(s.ScorePanel)

	//s.SlidingBitPanel = NewPanel(Rect{0, 0, -1, -1})
	//s.SlidingBitPanel = NewPanel(Rect{0, 0, 20, 30})
	s.SlidingBitPanel = NewPanel(Rect{0, 0, 16, 30})
	s.SlidingBitPanel.SetName("_SlidingBitPanel")
	s.SlidingBitPanel.Label().SetText("")
	s.SlidingBitPanel.CustomPatch9 = ctx.Style.ButtonPatch9
	// Center position
	s.SlidingBitPanel.rect[1] = s.rect.H()/2 - s.SlidingBitPanel.rect.H()/2
	s.AddChild(s.SlidingBitPanel)

	// Callbacks
	plusCB := func(pl interface{}) {
		pp("DERP")
		nextVal := s.Value + s.ValueStep
		s.Value = nextVal
		if s.Value > s.ValueRangeMax {
			s.Value = s.ValueRangeMax
		}
		s.dragging = true

		s.dragMouseOffsetX = vars.mx - s.SlidingBitPanel.GetAbsoluteRect().X()

		//pp(2)
		p("ZZZ: dragging")
		p("ZZZ: dragMouseOffsetX", s.dragMouseOffsetX)
		state := ctx.InputSys.isMouseButtonPressed(1)
		pp("Z state cb:", state)
	}
	_ = plusCB

	//b.PlusButton.SetCallback(TestCallback3)
	//s.SlidingBitPanel.SetCallback(plusCB, nil)

	/*
		Sub(Ev_mouse_enter, b.OnMouseEnterEv)
		Sub(Ev_mouse_out, b.OnMouseOutEv)
		Sub(Ev_mouse_button, b.OnMouseSpinButtonEv)
	*/
	Sub(Ev_mouse_button, s.OnMouseButtonEv)

	s.build()
	return s
}

func (s *Slider) OnMouseButtonEv(ev *ps.Event) {
	//p("ev:")
	//dump(ev)
	//evData := ev.Data.(*EvMousePressData)
	evData := ev.Data.(*EvMouseButtonData)
	btn := evData.Button
	pressed := evData.Pressed

	_ = btn

	if !pressed {
	}

	/*
		if evData.ActiveWidget == nil ||
			evData.ActiveWidget.Id() != b.Id() {
			return
		}
	*/

	//pp(evData.ActiveWidget)

	if evData.ActiveWidget == s.SlidingBitPanel {
		//pp("DERP")
		nextVal := s.Value + s.ValueStep
		s.Value = nextVal
		if s.Value > s.ValueRangeMax {
			s.Value = s.ValueRangeMax
		}
		s.dragging = true

		s.dragMouseOffsetX = vars.mx - s.SlidingBitPanel.GetAbsoluteRect().X()

		/*
			//pp(2)
			p("ZZZ: dragging")
			p("ZZZ: dragMouseOffsetX", s.dragMouseOffsetX)
			state := ctx.InputSys.isMouseButtonPressed(1)
			pp("Z state cb:", state)
		*/
	}
}

func (s *Slider) SetSliderPosition(p float64) {
	if p < 0 || p > 1.0 {
		pp("position should be in 0..1 range")
	}
	s.sliderPosition = p
	s.build()
}

func (s *Slider) SetSliderValue(v float64) {
	s.SetSliderPosition(v / s.ValueRangeMax)
}

func (s *Slider) build() {
	wrect := s.rect
	rect := s.SlidingBitPanel.rect
	newRect := rect
	//rewRect[1] += 1
	//newRect[0] += 1
	//newX := rect.X() + 1
	//newX := s.GetAbsoluteRect().X() - vars.mx
	//minX := wrect.X()
	minX := 0
	//maxX := wrect.X() + wrect.W()
	//maxX := wrect.W()
	maxX := wrect.W() - rect.W()
	newX := int(float64(maxX) * s.sliderPosition)
	//p(wrect, rect)
	//pp(minX, maxX)
	// Clamp
	if newX < minX {
		newX = minX
	}
	if newX > maxX {
		newX = maxX
	}
	//newRect := s.SlidingBitPanel.rect
	newRect[0] = newX
	s.SlidingBitPanel.SetRect(newRect)

	// Update value
	min1 := s.ValueRangeMin
	max1 := s.ValueRangeMax
	// Normalize
	s.Value = (max1-min1)/(float64(maxX)-float64(minX))*(float64(newX)-float64(maxX)) + max1
	p("Z value:", s.Value)
}

func (s *Slider) IsIntegerValueStep() bool {
	return s.ValueStep == float64(int(s.ValueStep))
}

func (s *Slider) IntValue() int {
	return int(s.Value)
}

func (s *Slider) Render(r *sdl.Renderer) {
	_log.Dbg("%F")
	//b.Panel.Render(r)
	s.Widget.Render(r)

	// Draw score's dashes
	absRect := s.GetAbsoluteRect()
	ycorr2 := s.rect.H() / 2 // Center
	scoreH := s.ScorePanel.rect.H()
	dashH := 5
	padding := 2
	c2 := s.Style.TextColor
	sdlColor2 := sdl.Color{uint8(c2.R()), uint8(c2.G()), uint8(c2.B()), uint8(c2.A())}
	xsh := 15
	for x := 0; x < s.rect.W()-xsh; x += s.StyleScoreDashStep {
		drawVLine(r, xsh+absRect.X()+x+1, absRect.Y()+ycorr2-scoreH-padding, -dashH, sdlColor2)
		drawVLine(r, xsh+absRect.X()+x, absRect.Y()+ycorr2-scoreH-padding, -dashH, sdlColor2)
		drawVLine(r, xsh+absRect.X()+x, absRect.Y()+ycorr2+scoreH+padding, dashH, sdlColor2)
		drawVLine(r, xsh+absRect.X()+x+1, absRect.Y()+ycorr2+scoreH+padding, dashH, sdlColor2)
	}

	s.Widget.RenderChildren(r)

	// Draw score's bar
	//wrect := s.rect
	c := s.Style.SliderScoreBarColor
	sdlColor := sdl.Color{uint8(c.R()), uint8(c.G()), uint8(c.B()), uint8(c.A())}
	xpadding := 1
	//ypadding := 2
	ypadding := 3
	w := int(float64(s.rect.W()-s.SlidingBitPanel.rect.W()) * s.sliderPosition)
	//h := 10
	h := s.ScorePanel.rect.H() - ypadding
	ycorr := (s.rect.H() - h) / 2

	drawFillRect(r, absRect.X()+xpadding, absRect.Y()+ycorr, w-xpadding, h, sdlColor)

	//b.Label.Render(r)
	//b.PlusButton.Render(r)
	_log.Dbg("%F done")
}

func (s *Slider) Update(dt float64) {
	s.Widget.Update(dt)
	//p("Z mx", vars.mx)

	if s.dragging {
		// XXX fixme: cleanup (legacy code)
		wrect := s.rect
		rect := s.SlidingBitPanel.rect
		newRect := rect
		_ = newRect
		//rewRect[1] += 1
		//newRect[0] += 1
		//newX := rect.X() + 1
		//newX := s.GetAbsoluteRect().X() - vars.mx
		newX := vars.mx - s.dragMouseOffsetX - s.GetAbsoluteRect().X()
		//minX := wrect.X()
		minX := 0
		//maxX := wrect.X() + wrect.W()
		//maxX := wrect.W()
		maxX := wrect.W() - rect.W()
		//p(wrect, rect)
		//pp(minX, maxX)
		// Clamp
		if newX < minX {
			newX = minX
		}
		if newX > maxX {
			newX = maxX
		}

		// Update position
		min1 := 0.0
		max1 := 1.0
		// Normalize
		p1 := (max1-min1)/(float64(maxX)-float64(minX))*(float64(newX)-float64(maxX)) + max1
		p("Z p1:", p1)

		s.SetSliderPosition(p1)

		/*
			//newRect := s.SlidingBitPanel.rect
			newRect[0] = newX
			s.SlidingBitPanel.SetRect(newRect)
		*/
	}

	if s.dragging && !ctx.InputSys.isMouseButtonPressed(1) {
		s.dragging = false
		p("Z not dragging")
	}

	//_ = ctx.InputSys.isMouseButtonPressed(1)

	//panic(2)
}
