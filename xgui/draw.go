package xgui

import (
	"github.com/veandco/go-sdl2/gfx"
	"github.com/veandco/go-sdl2/sdl"
)

//"github.com/veandco/go-sdl2/sdl"

/*
func drawLoopY(tex *sdl.Texture, x, y, h int) {
	th := tex.
	for i := 0; i <
}
*/

// XXX copy-pasted from main
func drawRectangleColorWidth(renderer *sdl.Renderer, x, y, w, h int, color sdl.Color, lineWidth int) {
	//lineWidth := int32(2)
	//lineWidth := int32(1)
	//lineColor := sdl.Color{0, 255, 0, sdl.ALPHA_OPAQUE}
	//lineColor := sdl.Color{255, 255, 255, 64}
	//lineColor := sdl.Color{255, 255, 255, 255}
	topLeftPointX := x
	topLeftPointY := y
	topRightPointX := x + w
	topRightPointY := y
	bottomLeftPointX := x
	bottomLeftPointY := y + h
	bottomRightPointX := x + w
	bottomRightPointY := y + h

	// Draw top line
	if !gfx.ThickLineColor(renderer, int32(topLeftPointX), int32(topLeftPointY),
		int32(topRightPointX), int32(topRightPointY),
		int32(lineWidth), color) {
		panic("error")
	}
	// Draw bottom line
	if !gfx.ThickLineColor(renderer, int32(bottomLeftPointX), int32(bottomLeftPointY),
		int32(bottomRightPointX), int32(bottomRightPointY),
		int32(lineWidth), color) {
		panic("error")
	}
	// Draw left line
	if !gfx.ThickLineColor(renderer, int32(topLeftPointX), int32(topLeftPointY),
		int32(bottomLeftPointX), int32(bottomLeftPointY),
		int32(lineWidth), color) {
		panic("error")
	}
	// Draw right line
	if !gfx.ThickLineColor(renderer, int32(topRightPointX), int32(topRightPointY),
		int32(bottomRightPointX), int32(bottomRightPointY),
		int32(lineWidth), color) {
		panic("error")
	}
}

func drawFillRect(renderer *sdl.Renderer, x, y, w, h int, c sdl.Color) {
	/*
		var q sdl.BlendMode
		p(renderer.GetDrawBlendMode(&q))
		p(q)
		pp(c)
	*/
	err := renderer.SetDrawBlendMode(sdl.BLENDMODE_BLEND)
	if err != nil {
		panic(err)
	}

	renderer.SetDrawColor(uint8(c.R), uint8(c.G), uint8(c.B), uint8(c.A))
	var rect sdl.Rect
	rect.X = int32(x)
	rect.Y = int32(y)
	rect.W = int32(w)
	rect.H = int32(h)
	renderer.FillRect(&rect)

	err = renderer.SetDrawBlendMode(sdl.BLENDMODE_NONE)
	if err != nil {
		panic(err)
	}
}

func drawLine(r *sdl.Renderer, x1, y1, x2, y2 int, c sdl.Color) {
	r.SetDrawColor(uint8(c.R), uint8(c.G), uint8(c.B), uint8(c.A))
	r.DrawLine(int32(x1), int32(y1), int32(x2), int32(y2))
}

func drawHLine(r *sdl.Renderer, x, y, w int, c sdl.Color) {
	drawLine(r, x, y, x+w, y, c)
}

func drawVLine(r *sdl.Renderer, x, y, h int, c sdl.Color) {
	drawLine(r, x, y, x, y+h, c)
}
