package main_module

import (
	"fmt"
	"os"
	"strings"

	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
)

type BuildingView struct {
	*View
	anims      map[string]*AnimatedSprite
	animPoints map[string]Pos
	/*
		smoke1Anim      *AnimatedSprite
		smoke2Anim      *AnimatedSprite
		smoke1PointPosX int
		smoke1PointPosY int
	*/
	m *Building
}

func newBuildingView(m *Building) *BuildingView {
	v := &BuildingView{
		View:       newView(),
		anims:      make(map[string]*AnimatedSprite),
		animPoints: make(map[string]Pos),
		m:          m,
	}
	return v
}

func (v *BuildingView) load() {

	//imagePath := "test.png"
	//imagePath := "res/buildings/bunker/reds/image.png"
	imagePath := fmt.Sprintf("res/buildings/%s/%s/image.png",
		v.m.name, strings.ToLower(v.m.campId.String()))

	image, err := img.Load(imagePath)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to load PNG: %s\n", err)
		pp(3)
	}
	//defer image.Free()
	v.image = image

	texture, err := renderer.CreateTextureFromSurface(image)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create texture: %s\n", err)
		pp(4)
	}
	//defer texture.Destroy()
	v.tex = texture

	v.loaded = true
}

func (v *BuildingView) destroy() {
	for _, anim := range v.anims {
		anim.destroy()
	}
}

func (v *BuildingView) spawn() {
	if !v.loaded {
		v.load()
	}
}

func (v *BuildingView) draw() {
	//pp("building draw")
	//dst := sdl.Rect{0, 0, 120, 120}
	var dst sdl.Rect
	/*
		if game.vp.overflowX <= 0 {
			dst = sdl.Rect{int32(-game.vp.shx), int32(-game.vp.shy), 120, 120}
		} else {
			dst = sdl.Rect{int32(game.vp.w - game.vp.overflowX), int32(-game.vp.shy), 120, 120}
		}
		if game.vp.overflowY <= 0 {
			dst.Y = int32(-game.vp.shy)
		} else {
			dst.Y = int32(game.vp.h - game.vp.overflowY)
		}
	*/

	//dst.H = 120
	//dst.W = 120

	//dst.W = int32(v.m.dimX * cell_size)
	//dst.H = int32(v.m.dimY * cell_size)

	nominalY := v.m.areaY * cell_size
	actualY := int(v.image.H)
	ycorr := actualY - nominalY

	if ycorr != 0 {
		p(ycorr)
	}

	dst.W = int32(v.image.W)
	dst.H = int32(v.image.H)

	px, py := int(v.m.PosX()), int(v.m.PosY())-ycorr
	sx, sy := worldToScreenPos(px, py)
	dst.X = int32(sx)
	dst.Y = int32(sy)

	if ycorr != 0 {
		p("2>>>", dst)
	}

	renderer.Copy(v.tex, nil, &dst)

	for k, anim := range v.anims {
		point := v.animPoints[k]
		if point.x != 0 || point.y != 0 {
			anim.posX = sx + point.x
			anim.posY = sy + point.y
			//anim.draw()
			anim.animate()
		}
	}
}

func (v *BuildingView) update(dt float64) {

}
