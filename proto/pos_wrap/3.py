from glm import *

"""
realx=tar.x - bot.x
wrappedx=(map.x - bot.x) + tar.x //These for each axis

relativex=abs(realx) < abs(wrappedx) ? realx : wrappedx;
relativey=abs(realy) < abs(wrappedy) ? realy : wrappedy;

Direction=MakeThisVectorAnUnitVectorOkay(Vector(relativex,relativey));
"""

# want = wrapped tar

#bot = vec2(4, 4)
#tar = vec2(24, 8)
# backwards
#bot = vec2(24, 8)
#tar = vec2(4, 4)
# up
bot = vec2(4, 24)
tar = vec2(4, 4)
want = vec2(4, 36)
# down
#bot = vec2(4, 4)
#tar = vec2(4, 24)
#want = vec2(


map = vec2(32, 32)

realx = tar.x - bot.x

if realx > 0:
    wrappedx = (map.x + bot.x)
else:
    #wrappedx = -(map.x - bot.x)
    wrappedx = bot.x - map.x


realy = tar.y - bot.y

if realy > 0:
    # Real downwards
    #wrappedy = bot.y - map.y
    wrappedy = -(map.y - tar.y)
else:
    # Real upwards
    wrappedy = map.y + tar.y



# Wrap coords

resX = -1
resY = -1

if abs(realx) < abs(wrappedx):
    resX = realx
else:
    resX = wrappedx

if abs(realy) < abs(wrappedy):
    resY = realy
else:
    resY = wrappedy


print("bot:", bot)
print("tar:", tar)
print("want:", want)
print()
print("realx:", realx)
print("wrappedx:", wrappedx)
print("realy:", realy)
print("wrappedy:", wrappedy)
print()
print("abs(realx):", abs(realx))
print("abs(realx) > mapX/2:", abs(realx) > (map.x/2))
print()
print("abs(realy):", abs(realy))
print("abs(realy) > mapY/2:", abs(realy) > (map.y/2))
print()
print("resX:", resX)
print("resY:", resY)
