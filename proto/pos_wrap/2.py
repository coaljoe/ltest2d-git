from glm import *

"""
realx=tar.x - bot.x
wrappedx=(map.x - bot.x) + tar.x //These for each axis

relativex=abs(realx) < abs(wrappedx) ? realx : wrappedx;
relativey=abs(realy) < abs(wrappedy) ? realy : wrappedy;

Direction=MakeThisVectorAnUnitVectorOkay(Vector(relativex,relativey));
"""

tar = vec2(24, 8)
bot = vec2(4, 4)

map = vec2(32, 32)

realx = tar.x - bot.x
wrappedx = (map.x - bot.x) + tar.x

print("realx:", realx)
print("wrappedx:", wrappedx)
