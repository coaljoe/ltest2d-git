from glm import *

"""
rst.x = pos.x-obj.x;
rst.y = pos.y-obj.y;

if (abs(rst.x) > sizeX / 2) {
if (rst.x > 0) {
rst.x = pos.x + sizeX;
} else {
rst.x = pos.x - sizeX;
}
} else {
rst.x = pos.x;
}
if (abs(rst.y) > sizeY / 2) {
if (rst.y > 0) {
rst.y = pos.y + sizeY;
} else {
rst.y = pos.y - sizeY;
}
} else {
rst.y = pos.y;
}
"""

#obj = vec2(4, 4)
#pos = vec2(24, 8)
#obj = vec2(24, 8)
#pos = vec2(4, 4)

# right
obj = vec2(14, 98)
pos = vec2(1778, 98)
want = vec2(-14, 98)

# left
#obj = vec2(24, 4)
#pos = vec2(4, 4)
#want = vec2(36, 4)

# up
#obj = vec2(4, 24)
#pos = vec2(4, 4)
#want = vec2(4, 36)

# down
#obj = vec2(4, 4)
#pos = vec2(4, 24)
#want = vec2(4, -8)

# both dirs (diag)
#obj = vec2(4, 4)
#pos = vec2(24, 8)
#want = vec2(-8, 8)

# full diag
#obj = vec2(0, 0)
#pos = vec2(31, 31)
#want = vec2(-1, -1)


rst = pos - obj 
sizeX = 28 * 64
sizeY = 28 * 64
xWrapped = False
yWrapped = False

print("sizeX:", sizeX)
print("sizeY:", sizeY)
print("rst before:", rst)

if abs(rst.x) > sizeX / 2:
    if rst.x > 0:
        rst.x = pos.x - sizeX
        #rst.x = obj.x - sizeX
        #rst.x = (pos.x - sizeX) - obj.x
    else:
        rst.x = pos.x + sizeX

    xWrapped = True

if abs(rst.y) > sizeY / 2:
    if rst.y > 0:
        #rst.y = pos.y + sizeY
        rst.y = pos.y - sizeY
    else:
        #rst.y = pos.y - sizeY
        rst.y = pos.y + sizeY

    yWrapped = True

print("obj:", obj)
print("pos:", pos)
print("rst:", rst)

res = obj+rst
print()
print("want:", want)
print("res:", res)
print("res wrap:", vec2(res.x % sizeX, res.y % sizeY))
print("rst wrap:", vec2(rst.x % sizeX, rst.y % sizeY))
print("xWrapped:", xWrapped)
print("yWrapped:", yWrapped)

res2 = rst
if not xWrapped:
    res2.x = pos.x
if not yWrapped:
    res2.y = pos.y

print()
print("res2:", res2)
