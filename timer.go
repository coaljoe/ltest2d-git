package main_module

// XXX TODO: use game time based timers / main game timer as base?
// XXX TODO: so it can be adjusted / advanced and synced with the game time / update loop (?)

import (
	"fmt"
	"math/rand"
	"time"
)

type Timer struct {
	id int
	speed float64
	time float64
	start_time float64
	started_ bool
	running_ bool
}

var maxTimerId = 0
var globalSpeed = 1.0

func newTimer(autostart bool) *Timer {
	maxTimerId += 1
	t := &Timer{
		id:    maxTimerId,
		speed: 1.0,
	}
	t._reset()
	if autostart {
		t.start()
	}
	
	timersys().addTimer(t)
	return t
}

//func (t *Timer) speed() float64 { return t.speed }

func (t *Timer) start() {
	t._reset()
	t.started_ = true
	t.running_ = true
}

func (t *Timer) restart() {
	t.start()
}

func (t *Timer) pause() {
	t.running_ = false
}

func (t *Timer) resume() {
	t.running_ = true
}

func (t *Timer) _reset() {
	t.time = 0
	t.start_time = _now()
	//t.Start()
}

// Randomize start_time
func (t *Timer) randomize(maxTime float64) {
	//t.time = maxTime * rand.Float64()
	//t.start_time += maxTime * rand.Float64()
	t.start_time -= maxTime * rand.Float64()
}

// dt from timer start
func (t *Timer) dt() float64 {
	if !t.started_ {
		return 0
	}
	return _now() - t.start_time
}

// ? first run check
// Timer is at zero
func (t *Timer) zero() bool {
	return t.time == 0
	//return false // Fixme: doesn't work
}

func (t *Timer) running() bool {
	return t.running_
}

func (t *Timer) paused() bool {
	return !t.running()
}

func (t *Timer) togglePause() {
	if t.running_ {
		t.pause()
	} else {
		t.resume()
	}
}

func (t *Timer) String() string {
	return fmt.Sprintf("Timer<id=%d>", t.id)
}

func (t *Timer) update(dt float64) {
	if !t.running_ {
		return
	}
	t.time = (_now() - t.start_time) * t.speed * globalSpeed
}

func _now() float64 {
	return _unix_float_seconds()
}

func _unix_float_seconds() float64 {
	return float64(time.Now().UnixNano()) / float64(time.Second)
}
