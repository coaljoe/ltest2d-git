package main_module

import (
	"fmt"
	. "math"
	"strings"
)

type ResourceType int

const (
	ResourceType_Money ResourceType = iota
	ResourceType_Pop
	ResourceType_Fuel
	ResourceType_Metal
)

type Resource struct {
	Typ   ResourceType
	Amt   int
	Limit int
}

func newResource(typ ResourceType) *Resource {
	return &Resource{
		Typ:   typ,
		Limit: MaxInt32,
	}
}

func (r *Resource) Set(n int) {
	r.Amt = mini(n, r.Limit)
	/*
		r.limit &&>0..limit	)
	*/
}

func (r *Resource) Take(n int) int {
	r.Amt -= n
	neg := 0
	if r.Amt < 0 {
		neg = r.Amt
		r.Amt = 0
	}
	return n + neg
}

func (r *Resource) TakeFrom(lhs *Resource) {
	v := lhs.Take(lhs.Amt)
	r.Put(v)
}

func (r *Resource) Put(n int) {
	r.Amt = mini(r.Amt+n, r.Limit)
}

func (r *Resource) TakeByUnits(n int, unitWeight float64) int {
	amt := Ceil(float64(n) * unitWeight)
	return r.Take(int(amt))
	//return int(r.amt)
}

func (r *Resource) IsEmpty() bool {
	if r.Amt > 0 {
		return false
	} else {
		return true
	}
}

func resourceTypeFromString(s string) ResourceType {
	s_lc := strings.ToLower(s)
	switch s_lc {
	case "metal":
		return ResourceType_Metal
	case "fuel":
		return ResourceType_Fuel
	case "money":
		return ResourceType_Money
	case "population":
		return ResourceType_Pop
	default:
		panic("unknown resourceType; s: " + s_lc)
	}
}

func (r *Resource) Name() string {
	switch r.Typ {
	case ResourceType_Metal:
		return "Metal"
	case ResourceType_Fuel:
		return "Fuel"
	case ResourceType_Money:
		return "Money"
	case ResourceType_Pop:
		return "Population"
	default:
		panic(fmt.Sprintf("unknown resource type=%v", r.Typ))
	}
}

func (r *Resource) show() {
	println("Resource.Show\n name:", r.Name(), "\n amt:", r.Amt)
}
