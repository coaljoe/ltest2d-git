package main_module

import (
	"testing"
	"fmt"
)

func TestCombat(t *testing.T) {
	initTest()
	
	println("1")
	
	//u1 := makeUnit("light_tank", getPlayer())
	u1 := makeUnit("light_tank", CampId_Reds)
	_ = u1
	//u2 := makeUnit("light_tank", getPlayer())
	u2 := makeUnit("light_tank", CampId_Arctic)
	_ = u2

	//u2.placeAt(3, 3)

	w0 := u1.combat.weapons[0]
	_ = w0	
	//u1.combat.weapons[0].setTargetRealms(RealmType_Air)
	u1.combat.weapons[0].setTargetRealms(RealmType_Ground, RealmType_Air)
	u1.combat.weapons[1].setTargetRealms(RealmType_Air)
	u1.combat.weapons[2].setTargetRealms(RealmType_Air)

	fmt.Println(u1.combat.weapons[0].targetRealms)
	fmt.Println(u1.combat.weapons[1].targetRealms)
	fmt.Println(u1.combat.weapons[2].targetRealms)

	w0.setTarget(u2)
	
	//c1 := newCombat()
	
	ret := u1.combat.canAttack(u2)
	//ret := u1.combat.canAttack(u2.combat)
	println("ret:", ret)

	u1.combat.setTarget(u2)
	//println("ret2:", ret2)

	u1.combat.attackTargets()
}
