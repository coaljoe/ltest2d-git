ltest2d Copyright (c) 2020 Aleksandr Kovtun

the source code is licensed under GPLv3 license,
see LICENSE-gpl.txt for details.

the media files, unless stated otherwise,
are licensed under CC BY-SA 4.0 license,
see LICENSE-cc.txt for details.
