package main_module

import (
	"fmt"
	//"math"
	. "kristallos.ga/rx/math"

	xg "kristallos.ga/xgui"
	"github.com/veandco/go-sdl2/sdl" //ps "lib/pubsub"
)

//type HudMouseState int
type HudMouseState int

const (
	HudMouseState_Default HudMouseState = iota
	//HudMouseState_GoToCell
	HudMouseState_PlaceBuilding
)

// Hud system. A part of Gui.
type Hud struct {
	*GameSystem
	// Use Entity for all objects?
	//selObjs map[int]ObjI
	//selUnits []*Unit
	selUnits map[int]*Unit
	origSelUnits map[int]*Unit
	// Only one
	selBuilding *Building
	selecting   bool
	// World space
	selStart    Vec2i
	selEnd      Vec2i
	selRect     xg.Rect // x, y, w, h
	//groundPlane Plane
	// A point on the ground that camera ray intersects
	groundPoint Vec3
	// Was the ground hit by the camera ray
	groundHit  bool
	mx, my     int
	mcx, mcy   int
	mouseState HudMouseState
	// Remember last building type that was built by hud
	//lastBuildingType BuildingType
	buildingToPlace  *Building
	canPlaceBuilding bool
	hlTiles          []CPos
	hlColors         []sdl.Color
	lastBuildingType string
	enabled          bool
	view             *HudView
	debug            bool
	// Debug
	d_showMouseRay bool
}

func newHud() *Hud {
	_log.Dbg("%F")
	h := &Hud{
		//selObjs:        make(map[int]ObjI),
		selUnits:       make(map[int]*Unit),
		origSelUnits:       make(map[int]*Unit),
		selecting:      false,
		//selRect:        Vec4Zero,
		selRect:        xg.Rect{},
		mx:             -1,
		my:             -1,
		mouseState:     HudMouseState_Default,
		hlTiles:        make([]CPos, 0),
		hlColors:       make([]sdl.Color, 0),
		debug:          true,
		d_showMouseRay: true,
	}
	h.GameSystem = newGameSystem("Hud", "Hud Subsystem", h)
	h.autostart = false
	
	/*
	h.selecting = true
	//h.selRect = xg.Rect{}
	h.selRect = xg.Rect{0, 0, 100, 100}
	h.selStart = Vec2i{0, 0}
	h.selEnd = Vec2i{100, 100}
	*/
	
	/*
		sub(rx.Ev_mouse_move, h.onMouseMove)
		sub(rx.Ev_mouse_press, h.onMousePress)
		sub(rx.Ev_mouse_release, h.onMouseRelease)
		sub(rx.Ev_key_press, h.onKeyPress)
	*/
	return h
}

func (h *Hud) onKeyPress(key sdl.Keycode) {
	//key := ev.Data.(glfw.Key)
	/*
		if key == game.keymap.getKey1("rebuildLastBuildingKey") {
			//pp("derp")
			// Put mouse state in place building mode
			// Fixme: need to unset previsous mouse state
			h.mouseState = HudMouseState_PlaceBuilding
		}
	*/
}

func (h *Hud) onMouseButton(button uint8, pressed bool) {
	_log.Dbg("%F button:", button, "pressed:", pressed)
	
	if game.gui.HasMouseFocus() {
		return
	}
	
	if !pressed {
	//pp(2)
	
		h.unselectUnits()
		if h.selecting {
			h.performMultipleSelection()
			h._stopSelection()
		}
	}

	if h.mx == -1 && h.my == -1 {
		//h.mx = _InputSys.Mouse.X
		//h.my = _InputSys.Mouse.Y
		h.mx = game.gui.Xgi().Context.InputSys.MX
		h.my = game.gui.Xgi().Context.InputSys.MY
		//pp(h.mx, h.my)
	}
	btn := button
	_log.Dbg("hud.onmousepress", btn)

	if pressed {
		if h.mouseState == HudMouseState_Default {
			//c := game.field.cells[h.mcx][h.mcy]
			c := game.field.GetCell(h.mcx, h.mcy)

			canGo := game.pathfinder.canGoTo(h.mcx, h.mcy)

			// Move selected units
			if len(h.selUnits) > 0 {
				//if c.open {
				if canGo {
					for _, u := range h.selUnits {
						if u.isStaticUnit() {
							continue
						}
						u.mover.moveTo(h.mcx, h.mcy)
					}
				}
				//pp(2)
			}
			
			doneSelect := false
			
			//pp(c.buildingId, c.buildingMask)
			// Select buildings
			if c.buildingId != -1 && c.buildingMask {
				//b := game.objsys.getObjById(c.buildingId)
				b := game.buildingsys.getBuildingById(c.buildingId)
				//h.addObjToSelection(b)
				h.selBuilding = b
				p(c.buildingId)
				//pp(2)

				game.gui.GameSheet().ControlMenu().SetMode("building_menu", b)
				//game.gui.gameSheet.controlMenu.setBasicBuildingMenuMode()
				
				doneSelect = true
			} else {
				// Unselect
				//h.selBuilding = nil
				//game.gui.gameSheet.controlMenu.setMode("default", nil)
				h.unselectBuilding()
			}

			if !doneSelect {
				/*
				// Select Units
				u := game.unitsys.getUnitByPosCheck(h.mcx, h.mcy)
				if u != nil {
					//h.selUnits = []*Unit{u}
					//h.selUnits = make(map[int]*Unit)
					h.unselectUnits()
					h.selUnits[u.id] = u
				} else {
					h.unselectUnits()
				}
				*/
				h._startSelection()
			}

		} else if h.mouseState == HudMouseState_PlaceBuilding {
			if h.canPlaceBuilding {
				h.buildingToPlace.placeAt(h.mcx, h.mcy)
				h.buildingToPlace = nil
				h.mouseState = HudMouseState_Default
			}
		} else {
			pp("unknown mousestate:", h.mouseState)
		}
	}

	var _ = `
	h._doGroundPlaneHitTest()

	
	if btn == glfw.MouseButtonLeft {
		switch h.mouseState {
		case HudMouseState_Default:
			for _, o := range h.selObjs {
				if o.objtype == ObjType_Unit {
					// Move units
					c_Unit(GetEntity(o)).Mover.moveTo(CPos{h.mcx, h.mcy})
				}
				// Unselect
				h.RemoveObjFromSelection(o)
			}
		case HudMouseState_PlaceBuilding:
			// Place building
			btype := BuildingType_Bunker
			h._placeBuilding(btype)

			// Store last placed building type
			h.lastBuildingType = btype

		default:
			pp("unknown mouseState:", h.mouseState)
		}

		// Update mouseState
		switch h.mouseState {
		case HudMouseState_PlaceBuilding:
			h.mouseState = HudMouseState_Default
			//default:
			//	pp("unknown mouseState:", h.mouseState)
		}
	} else if btn == glfw.MouseButtonRight {
		h.mouseState = HudMouseState_PlaceBuilding
	}
	`
}

/*
func (h *Hud) onMouseRelease(button uint8) {
	if game.gui.hasMouseFocus() {
		return
	}

	btn := button
	_log.Dbg("hud.onmouserelease", btn)
	var _ = `
	if !h.selecting {
		//h._doSingleSelection()
	} else {
		h._stopSelection()
	}
	`
}
*/

func (h *Hud) onMouseMove(ev xg.EvMouseMoveData) {

	if game.gui.HasMouseFocus() {
		return
	}

	//p := ev.Data.(rx.EvMouseMoveData)
	h.mx = ev.X
	h.my = ev.Y
	h.mcx = wrapCoordX((game.vp.shx + h.mx) / cell_size)
	h.mcy = wrapCoordY((game.vp.shy + h.my) / cell_size)
	//h.cx = (h.mx - game.vp.shx) / cell_size
	//h.cy = (h.my - game.vp.shy) / cell_size
	//pp(h.mx, h.my)
	_log.Trc("hud.onmousemove", h.mx, h.my)

	// XXX TODO: move to app?
	_, _, state := sdl.GetMouseState()
	leftPressed := false
	if state & sdl.Button(sdl.BUTTON_LEFT) != 0 {
		leftPressed = true
		//pp("left pressed")
	}
	
	if leftPressed {
		if !h.selecting {
			//h._startSelection()
		} else {
			h._updateSelection()
		}
	}
	

	if h.mouseState == HudMouseState_PlaceBuilding {
		h.hlTiles = h.hlTiles[:0]
		h.hlColors = h.hlColors[:0]
		canPlace := true
		for y := 0; y < h.buildingToPlace.areaY; y++ {
			for x := 0; x < h.buildingToPlace.areaX; x++ {
				pos := CPos{h.mcx + x, h.mcy + y}
				h.hlTiles = append(h.hlTiles, pos)
				cell := game.field.GetCell(pos.x, pos.y)
				//cell := game.field.cells[pos.x][pos.y]
				var c sdl.Color
				//cell.z.isGround() {
				if !cell.HasBuilding() && cell.Walkable() {
					//c = sdl.Color{50, 205, 50, sdl.ALPHA_OPAQUE}
					//c = sdl.Color{100, 220, 100, sdl.ALPHA_OPAQUE}
					c = sdl.Color{100, 220, 100, 200}
				} else {
					//c = sdl.Color{220, 100, 100, sdl.ALPHA_OPAQUE}
					c = sdl.Color{220, 100, 100, 200}
					canPlace = false
				}
				h.hlColors = append(h.hlColors, c)
			}
		}

		h.canPlaceBuilding = canPlace

	}

	//h._doGroundPlaneHitTest()
	if h.debug {
		//_Scene.debugBox.SetPos(h.groundPoint)
	}
}

func (h *Hud) hasFocus() bool {
	return !game.gui.HasMouseFocus()
}

func (h *Hud) unselectBuilding() {
	h.selBuilding = nil
	game.gui.GameSheet().ControlMenu().ResetMode()
}

func (h *Hud) unselectUnits() {
	h.selUnits = make(map[int]*Unit)
}

func (h *Hud) PlaceBuilding(b *Building) {
	h.buildingToPlace = b
	h.mouseState = HudMouseState_PlaceBuilding
}

func (h *Hud) _startSelection() {
	if h.selecting {
		return
	}
	
	h.selecting = true
		
	//h.selStart = Vec2i{h.mx, h.my}
	//px := h.mx + -game.vp.shx
	//px := game.vp.shx + h.mx
	//px := wrapCoordXPx((game.vp.shx + h.mx))
	//py := h.my //h.my - game.vp.shy
	//py := game.vp.shy + h.my
	
	px, py := screenToWorldPos(h.mx, h.my)
	
	//p("#> px:", px, py)
	//p("#> mx:", h.mx, h.my)
	//p("#> shx:", game.vp.shx, game.vp.shy)
	h.selStart = Vec2i{px, py}
	h.selEnd = h.selStart
		
	h.selRect[0] = h.selStart[0]
	h.selRect[1] = h.selStart[1]
	h.selRect[2] = 0
	h.selRect[3] = 0
		
	//pp(h.selStart, h.selEnd, h.selRect)	
		
	// Store originally selected units
	//h.origSelUnits = copy(h.selUnits)
	for k, v := range h.selUnits {
		h.origSelUnits[k] = v
	}
}

func (h *Hud) _stopSelection() {
	_log.Inf("HUD Selection:", h.selRect)
	h.selecting = false
	h.selRect = xg.Rect{}
	_log.Inf("HUD Selection:", h.selRect)
}

func (h *Hud) _updateSelection() {
	if !h.selecting {
		return
	}
	
	//h.selEnd = Vec2i{h.mx, h.my}
	
	//px := game.vp.shx + h.mx
	//py := game.vp.shy + h.my
	px, py := screenToWorldPos(h.mx, h.my)
	h.selEnd = Vec2i{px, py}
	
	h.selRect[2] = h.selEnd.X() - h.selStart.X()
	h.selRect[3] = h.selEnd.Y() - h.selStart.Y()
		
	p(h.selRect)

	debug.debugText = fmt.Sprintf("%v\n", h.selRect)
	debug.debugText += fmt.Sprintf("cx: %d cy: %d\ncw: %d ch: %d",
		h.selRect[0] / cell_size,
		h.selRect[1] / cell_size,
		h.selRect[2] / cell_size,
		h.selRect[3] / cell_size)
}

func (h *Hud) performMultipleSelection() {
	_log.Dbg("%F")
	
	//x := int(math.Ceil(float64(h.selRect.X()) / float64(cell_size)))
	//y := int(math.Ceil(float64(h.selRect.Y()) / float64(cell_size)))
	x := h.selRect.X() / cell_size
	y := h.selRect.Y() / cell_size
	//w := int(math.Ceil(float64(h.selRect.W()) / float64(cell_size)))
	//h_ := int(math.Ceil(float64(h.selRect.H()) / float64(cell_size)))
	w := h.selRect.W() / cell_size
	h_ := h.selRect.H() / cell_size
	
	x1 := h.selRect.X1() / cell_size
	y1 := h.selRect.Y1() / cell_size
	
	w = x1 - x
	h_ = y1 - y
	
	p(x, y, w, h_)
	
	// Mirror
	mirrored := false
	_ = mirrored
	if w < 0 {
		x = x + w
		w = -w
		mirrored = true
	}
	if h_ < 0 {
		y = y + h_
		h_ = -h_
		mirrored = true
	}
	
	if mirrored {
		p("mirrored:", x, y, w, h_)
		//pp(2)
	}
	
	// XXX FIXME? minimal selection is 1x1
	if w < 1 {
		w = 1
	} else {
		w += 1
	}
	if h_ < 1 {
		h_ = 1
	} else {
		h_ += 1
	}
	//w += 1
	//h_ += 1
	
	p("fixed:", x, y, w, h_)
	
	cells := game.field.getRectSlice(x, y, w, h_)
	
	for _, c := range cells {
		if c.HasUnit() {
			u := c.GetUnit()
			h.selUnits[u.id] = u
		}
	}
}

// Perform a single-click selection.
func (h *Hud) _doSingleSelection() {
	_log.Inf("[Hud] single selection")
	var _ = `
	ray := h._castCameraRay()
	rc := rx.NewRaycaster()
	nodes := rc.TestRayScene(ray)
	pp("nodes:", nodes)
	`
}

var _ = `
func (h *Hud) _doGroundPlaneHitTest() {
	// Update groundPoint
	ray := h._castCameraRay()
	//h.groundHit, h.groundPoint = h.groundPlane.IntersectsRay(ray)
	hit, p := h.groundPlane.IntersectsRay(ray)

	camn := rx.Rxi().Camera
	cam := camn.Camera
	proj := cam.GetProjectionMatrix()
	view := cam.GetViewMatrix()
	vp := proj.Mul(view)
	invVP := vp.Invert()
	_ = invVP
	//rm := camn.Mat().Upper3()
	//rm := camn.Mat()
	rm := camn.Mat()
	//rm.SetTranslation(0, 0, 0)
	rm = rm.Invert()
	m := camn.Mat()
	mw := camn.WorldMat()
	_, _ = m, mw
	//p_trans := mw.MulVec4(p.ToVec4()).ToVec3()
	//p_trans := p
	sx, sy := rx.Rxi().Renderer().Size()
	p_trans := UnProject(Vec3{float64(h.mx), float64(sy - h.my), 0}, view, proj, 0, 0, sx, sy)
	//p_trans := rm.MulVec3(p)
	fmt.Println("->", p, p_trans)
	fmt.Println("->", unitsToCells(p.X()), unitsToCells(p.Y()))
	fmt.Println("->", unitsToCells(p_trans.X()), unitsToCells(p_trans.Y()))
	//h.cx = unitsToCells(p.X())
	//h.cy = unitsToCells(p.Y())
	h.mcx = unitsToCells(p_trans.X())
	h.mcy = unitsToCells(p_trans.Y())

	h.groundHit = hit
	h.groundPoint = p_trans //p_trans
	//h.groundPoint = p

	if h.groundHit {
		_log.Inf("[Hud] ground hit at:", h.groundPoint)
		if h.debug {
			_Scene.debugBox.SetPos(h.groundPoint)
		}
	}
}

// Cast a ray from camera.
func (h *Hud) _castCameraRay() Ray {
	camn := _Scene.camera.cam
	ray := camn.Camera.CreateRayWithLenght(100)

	if h.debug && h.d_showMouseRay {
		//_Scene.debugBox.SetPos(ray.Direction)
		rx.DrawBegin()
		//rx.DrawSetAtV(ray.Direction)
		//rx.DrawCube(10)
		rx.DrawSetColorV(rx.ColorBlue)
		rx.DrawLineV(ray.Origin, ray.Direction)
		//rx.DrawLineV(ray.Origin, Vec3Zero)
		rx.DrawEnd()
	}
	return ray
}

// Place building at mouse position.
func (h *Hud) _placeBuilding(btype BuildingType) {
	en := makeBuilding(btype, getPlayer())
	//c_Building(en).setCPos(h.cx, h.cy)
	pos := h.groundPoint
	q := 5
	p(pos)
	//pos[0] = (float64(int(pos.X()*q)) / q)
	//pos[1] = (float64(int(pos.Y()*q)) / q)
	pos[0] = float64((int(pos.X()) / q) * q)
	pos[1] = float64((int(pos.Y()) / q) * q)
	//pos[2] = float64((int(pos.Z()) / q) * q)
	//pp(pos)
	//pos.SetY((pos.Y() / q) * q)
	c_Building(en).SetPosX(pos.X())
	c_Building(en).SetPosY(pos.Y())
	//c_Building(en).setZPos(pos.Z())
	spawnEntity(en)

	if true {
		en2 := makeUnit("gunbattery", getPlayer())
		c_Unit(en2).SetPosX(pos.X())
		c_Unit(en2).SetPosY(pos.Y())
		spawnEntity(en2)
		//c_Unit(en2).setZRot(rand.Float64() * 360.0)
		c_Turret(en2).rotateTo(float64(rand.Intn(360)))
		//(float64(rand.Intn(360)))
		//_ = rand.
	}
}
`

/*
func (h *Hud) addObjToSelection(o ObjI) {
	h.selObjs[o.getId()] = o
}

// Has the object in selection.
func (h *Hud) hasObjInSelection(o ObjI) bool {
	return h.selObjs[o.getId()] != nil
}

func (h *Hud) isObjSelected(o ObjI) bool {
	return h.hasObjInSelection(o)
}

func (h *Hud) removeObjFromSelection(o ObjI) {
	if !h.hasObjInSelection(o) {
		pp("can't remove obj from selection, obj is not selected; obj:", o)
	}
	delete(h.selObjs, o.getId())
}
*/

func (h *Hud) start() {
	_log.Dbg("%F")
	h.view = newHudView(h)
	h.enabled = true
	if h.debug {
		//_Scene.debugBox.SetVisible(true)
	}
}

func (h *Hud) draw() {
	if h.view != nil {
		h.view.draw(renderer)
	}
}

func (h *Hud) update(dt float64) {
	if !h.enabled {
		return
	}
	
	if h.selecting {
		h.unselectUnits()
		h.performMultipleSelection()
		
		for _, u := range h.origSelUnits {
			h.selUnits[u.id] = u
		}
	}
	
	//pp("derp")
	h.view.update(dt)

	// Pass the information to the gui
	game.gui.SetCellX(h.mcx)
	game.gui.SetCellY(h.mcy)
}
