import random
import math
from PIL import Image

seed = 0
perm = range(256)
#random.shuffle(perm)
#random.Random(seed).shuffle(perm)
perm += perm
#print perm
#exit()
dirs = [(math.cos(a * 2.0 * math.pi / 256),
         math.sin(a * 2.0 * math.pi / 256))
         for a in range(256)]

def noise(x, y, per):
    def surflet(gridX, gridY):
        distX, distY = abs(x-gridX), abs(y-gridY)
        polyX = 1 - 6*distX**5 + 15*distX**4 - 10*distX**3
        polyY = 1 - 6*distY**5 + 15*distY**4 - 10*distY**3
        hashed = perm[perm[int(gridX)%per] + int(gridY)%per]
        grad = (x-gridX)*dirs[hashed][0] + (y-gridY)*dirs[hashed][1]
        #print polyX, type(polyX)
        #print polyY, type(polyY)
        #print hashed, type(hashed)
        #print grad, type(grad)
        #exit()
        return polyX * polyY * grad
    intX, intY = int(x), int(y)
    return (surflet(intX+0, intY+0) + surflet(intX+1, intY+0) +
            surflet(intX+0, intY+1) + surflet(intX+1, intY+1))

def fBm(x, y, per, octs):
    val = 0
    for o in range(octs):
        #print per*2**o, type(per*2**o)
        #print 2**o, type(2**o)
        #print x*2**o, type(x*2**o)
        #exit()
        val += 0.5**o * noise(x*2**o, y*2**o, per*2**o)
    return val

#size, freq, octs, data = 128, 1/32.0, 5, []
#size, freq, octs, data = 128, 1/64.0, 2, []
size, freq, octs, data = 128, 1/64.0, 8, []
for y in range(size):
    for x in range(size):
        data.append(fBm(x*freq, y*freq, int(size*freq), octs))

print(data)
#print data[:128]
sum = 0.0
for x in data:
    #print x
    #print "sum:", sum
    sum += x
print "sum:", sum
print("done")
im = Image.new("L", (size, size))
im.putdata(data, 128, 128)
im.save("noise.png")