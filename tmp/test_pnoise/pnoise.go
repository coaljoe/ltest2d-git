package main

// #cgo CFLAGS: -g -Wall
// #include "noise1234.h"
import "C"

func Pnoise2(x, y float64, px, py int) float64 {
	cx := C.float(x)
	cy := C.float(y)
	cpx := C.int(px)
	cpy := C.int(py)
	cr := C.pnoise2(cx, cy, cpx, cpy)
	r := float64(cr)
	return r
}
