package main_module

import (
	//"github.com/bpicode/fritzctl/assert"
	"fmt"
	"math/rand"
)

type FieldGenOptions struct {
	checkZlevels bool
	// Generate anything from these options at all
	disableEverything bool
	// HM times bigger than the field (fixme: can be calculated?)
	hmScale              int
	generateFieldObjects bool
	generateOilRigs      bool
	generateOreMines     bool
	amountFieldObjects   float64
	amountOilRigs        float64
	amountOreMines       float64
}

func newFieldGenOptions() FieldGenOptions {
	opt := FieldGenOptions{
		checkZlevels:      true,
		disableEverything: false,
		// Default HM is X times larger than field size
		hmScale:              4,
		generateFieldObjects: true,
		generateOilRigs:      false,
		generateOreMines:     true,
		amountFieldObjects:   1.0,
		amountOilRigs:        1.0,
		amountOreMines:       1.0,
	}
	return opt
}

// Verify options for correctness.
func (fgo FieldGenOptions) verify() {
	// Skip test
	if fgo.disableEverything {
		return
	}
	//if fgo.hmScale < 1 {
	//	pp("hmScale is less than 1;", fgo.hmScale)
	//}
	//assert.IsTrue(fgo.hmScale > 1,
	//	"hmScale is less than 1;", fgo.hmScale)
}

type FieldGen struct {
	f                 *Field
	opt               FieldGenOptions
	// XXX move to something like GameInfo / GameData / GameDataInfo ?
	requiredBuildings map[string]int
	campSites         map[int][]CRect
}

func newFieldGen(f *Field, fgOpt FieldGenOptions) *FieldGen {
	_log.Inf("%F f != nil:", f != nil, "fgOpt:", fgOpt)

	fg := &FieldGen{
		f:                 f,
		opt:               fgOpt,
		requiredBuildings: make(map[string]int),
		// A set of non-overlapping rects
		// of different sizes
		campSites: make(map[int][]CRect), // k=player.id
	}
	fg.requiredBuildings["hq"] = 1 // XXX optional?
	fg.requiredBuildings["factory"] = 1
	fg.requiredBuildings["housing"] = 3

	return fg
}

// TODO: add minRange maxRange randomization
func (fg *FieldGen) generateOreMines() {
	_log.Inf("%F generate field objects...")

	if fg.opt.disableEverything {
		return
	}

	//fw := f.w / 4 //2
	//fh := f.h / 4 //2
	fw := fg.f.w
	fh := fg.f.h

	// Add mines
	if fg.opt.generateOreMines {
		mineCells := make([]Cell, 0)
		mineLocsCandidates := make([]CPos, 0)
		mineLocs := make([]CPos, 0)
		_ = mineCells
		_ = mineLocsCandidates
		_ = mineLocs

		// Props
		siteW, siteH := 4, 4
		//zlev := ZLevel_Ground2
		zlev := ZLevel_Ground1
		radius := 40 // minimal radius between locs

		//pp(f.getCellsAtZLevel(zlev))
		//pp(f.getCellsAtZLevel(ZLevel_Ground0))
		//pp(f.getCellsAtZLevel(ZLevel_Sea1))
		//pp(f.getCellsAtZLevel(ZLevel(-1)))
		//pp(f.getCellsAtZLevels(ZLevel_Sea0, ZLevel_SeaMax))
		//pp(f.getCellsAtZLevels(ZLevel_SeaMax, ZLevel_Sea0))
		//pp(f.getCells())

		// Find suitable places for oilrigs
		for x := 0; x < fw-siteW; x++ {
			for y := 0; y < fh-siteH; y++ {
				cell := &fg.f.cells[x][y]
				if cell.z != zlev {
					continue
				}
				p(cell)

				suitable := true

				nb := fg.f.getRectSlice(x, y, siteW, siteH)

				for _, n := range nb {
					if n.z != zlev {
						suitable = false
					}
				}

				if suitable {
					mineLocsCandidates = append(mineLocsCandidates, CPos{x, y})
				}
			}
		}

		// Filter candidates

		// Randomize
		a := mineLocsCandidates
		//rand.Seed(time.Now().UnixNano())
		rand.Shuffle(len(a), func(i, j int) { a[i], a[j] = a[j], a[i] })

		for i, p := range mineLocsCandidates {
			if i == 0 {
				// Add first location
				mineLocs = append(mineLocs, mineLocsCandidates[0])
				continue
			}

			// Test against mineLocs
			good := true
			for j, p2 := range mineLocs {
				_ = j
				//p_("j:", j)
				//if inRadius(float64(p2.x), float64(p2.y), float64(p.x), float64(p.y), float64(radius)) {
				if inCellRadius(p2.x, p2.y, p.x, p.y, radius) {
					good = false
					//p_("BAD", p, p2)
					break
				}
			}

			if good {
				__p("GOOD", p)
				mineLocs = append(mineLocs, p)
			} else {
				__p("BAD", p)
			}
		}

		//pp(mineLocs)

		for _, p := range mineLocs {
			x, y := p.x, p.y
			b := makeBuilding("ore_mine", CampId_None)
			//b := makeBuilding("ore_mine", nil)
			b.placeAt(x, y)
			SpawnEntity(b)
		}
	}
	_log.Inf("%F generate field objects complete")

}

func (fg *FieldGen) generateFieldAreas() (err error, errMsg string) {
	_log.Inf("%F generate field areas...")

	if fg.opt.disableEverything {
		return
	}

	minAreaVolume := 40 // Continuous cells
	minAreaWidth := 10
	minAreaHeight := 10
	_ = minAreaVolume
	_ = minAreaWidth
	_ = minAreaHeight

	//areaWidth := minAreaWidth
	//areaHeight := minAreaHeight
	size := 30
	size = 20
	areaWidth := size
	areaHeight := size

	//suitable, pos := game.buildingsys.findPlaceForBuilding(4, 4, CRect{0, 0, 8, 8})
	//suitable, poss := game.buildingsys.findPlacesForBuilding(4, 4, CRect{0, 0, 30, 30})
	suitable, poss := game.buildingsys.findPlacesForBuilding(4, 4, CRect{0, 0, areaWidth, areaHeight})
	p("->", suitable, poss)
	//pp(2)
	//return

	//limit := 100
	//poss := f.hm.fill(0, 0, -1, true, limit)
	//poss := fg.f.hm.fill(0, 0, 0xff, true, limit)
	//pp(poss, len(poss))

	campSites := make([]CRect, 0)
	_ = campSites

	/*
		siteSizes := []CPos{CPos{2, 2}, CPos{3, 3}, CPos{4, 4}} // CSize?
		//numSites := []int{0, 0, 2}
		numSites := []int{0, 3, 1}
	*/

	siteSizes := make([]CPos, 0)
	numSites := []int{}

	// XXX Read values from PlayerAI struct
	//requiredBuildings := (&PlayerAi{}).requiredBuildings
	for name, num := range fg.requiredBuildings {
		p("name:", name, "num:", num)
		bDef := &Building{}
		campId := GetPlayer().campId()
		readBuildingDef(name, campId, bDef)
		bSize := CPos{bDef.areaX, bDef.areaY}
		p("bSize:", bSize)

		have := false
		haveIdx := -1
		for i, size := range siteSizes {
			if size == bSize {
				have = true
				haveIdx = i
				break
			}
		}

		if !have {
			siteSizes = append(siteSizes, bSize)
			numSites = append(numSites, num)
		} else {
			numSites[haveIdx] += num
		}
	}

	p("siteSizes:", siteSizes)
	p("numSites:", numSites)
	//pp(2)

	/*
		numLocs := make(map[CPos]int)
		numLocs[siteSizes[0]] = 0
		//numLocs[siteSizes[1]] = 3
		numLocs[siteSizes[1]] = 0
		//numLocs[siteSizes[2]] = 4
		//numLocs[siteSizes[2]] = 1
		numLocs[siteSizes[2]] = 2
	*/

	step := 4 //2
	player := GetPlayer()
	fg.campSites[player.id] = make([]CRect, 0)

	//__disable_p = true
	//pp(5)
	p("")
	p("------ cycle begin ------")
	p("")
	//xsiteSizes := []CPos{CPos{4, 4}}
	//numSites := []int{1}
	//numSites := []int{2}

	if false {
		xsiteSizes := []CPos{CPos{4, 4}, CPos{2, 2}}
		//xsiteSizes := []CPos{CPos{2, 2}, CPos{4, 4}}
		xnumSites := []int{2, 1}
		//numSites := []int{2, 3}

		size_ := 10
		size_ = 20
		suitable, rects := game.buildingsys.findPlacesForBuildings(
			xsiteSizes, xnumSites, CRect{0, 0, size_, size_})
		p("suitable:", suitable, "rects:", rects)
		pp(2)
	}

	// All site candidates on the field
	siteCandidates := make(map[CPos][]CRect, 0)

	excludedAreas := make([]CRect, 0)

	//outer:
	for y := 0; y < fg.f.h-areaHeight; y += step {
		for x := 0; x < fg.f.w-areaWidth; x += step {
			p("area loc:", x, y)

			areaRect := CRect{x, y, areaWidth, areaHeight}
			skip := false
			for _, rect := range excludedAreas {
				if rect.overlap(areaRect) {
					skip = true
				}
			}

			if skip {
				p("XXX skip area:", areaRect)
				continue
			}

			suitable, rects := game.buildingsys.findPlacesForBuildings(
				//siteSizes, numSites, CRect{0, 0, areaWidth, areaHeight})
				siteSizes, numSites, areaRect)
			p("suitable:", suitable, "rects:", rects)

			if suitable {
				k := CPos{x, y}
				siteCandidates[k] = rects

				// Add area to excluded areas to prevent overlapping with
				// other areas (if spep != areasize)
				excludedAreas = append(excludedAreas, CRect{x, y, areaWidth, areaHeight})
				//break outer
			}

			//pp(2)
		}
	}
	//pp(2)
	//pp(siteCandidates)

	var _ = `
	for y := 0; y < fg.f.h-areaHeight; y += step {
		for x := 0; x < fg.f.w-areaWidth; x += step {

			// Walk area
			count := 0
			for j := 0; j < areaHeight; j++ {
				for i := 0; i < areaWidth; i++ {
					px := x + i
					py := y + j
					c := &fg.f.cells[px][py]
					if c.z.isMovable() {
						count++
					}

					// Suitable area rect candidates
					areaRects := make([]CRect, 0)

					// From larger site to smaller
					for k := len(siteSizes) - 1; k >= 0; k-- {
						p("k:", k)
						siteSize := siteSizes[k]
						needNumLocs := numLocs[siteSize]

						if needNumLocs < 1 {
							continue
						}

						//_log.ToggleEnabled()
						//_log.SetEnabled(false)
						//__disable_p = true
						suitable, locs := game.buildingsys.findPlacesForBuilding(siteSize.x, siteSize.y,
							CRect{px, py, areaWidth, areaHeight})
						//__disable_p = false
						//_log.ToggleEnabled()

						if !suitable || len(locs) < needNumLocs {
							continue
						}

						p("area x, y:", x, y)
						p("locs:", locs)

						// TODO: find non-overlapping areas for locs

						shuffledLocs := make([]CPos, len(locs))
						copy(shuffledLocs, locs)
						// Randomize
						a := shuffledLocs
						//rand.Seed(time.Now().UnixNano())
						rand.Shuffle(len(a), func(i, j int) { a[i], a[j] = a[j], a[i] })

						//p(locs)
						//pp(shuffledLocs)

						/*
							//randIdx := rand.Intn(len(locs) - 1)
							//loc := locs[randIdx]
							loc := shuffledLocs[0]
							//rects := fg.campSites[player.id]
							rect := CRect{loc.x, loc.y, siteSize.x, siteSize.y}
							areaRects = append(areaRects, rect)
						*/

						p("shuffledLocs:", shuffledLocs)

						for xi := 0; xi < needNumLocs; xi++ {
							p("loc num:", xi)
							for _, loc := range shuffledLocs {
								locRect := CRect{loc.x, loc.y, siteSize.x, siteSize.y}
								p("processing locRect:", locRect)
								// Check if loc overlap with loc candidates
								good := true
								for _, xrect := range areaRects {
									p("test xrect:", xrect, "agaist locRect:", locRect)
									if xrect == locRect {
										good = false
										p("SAME BAD")
										break
										//continue
									}
									if xrect.overlap(locRect) {
										good = false
										p("BAD")
										break
									}
								}

								if good {
									// Add rect to candidates
									p("adding locRect:", locRect)
									areaRects = append(areaRects, locRect)
									p("-> current areaRects:", areaRects)
								}
							}
						}
						pp("areaRects:", areaRects)

						p("locs for siteSize:", siteSize)
						p(locs)
						p(suitable)

						pp(3)
					}
				}
			}

			p("area loc:", x, y)
			p("count:", count)
			//pp(2)

		}
	}
	`

	//pp(2)

	if false {
		maxSizeW := 4
		maxSizeH := 4
		for y := 0; y < fg.f.h-maxSizeH; y++ {
			for x := 0; x < fg.f.w-maxSizeW; x++ {
				c := &fg.f.cells[x][y]
				for _, size := range siteSizes {
					for j := 0; j < size.y; j++ {
						for i := 0; i < size.x; i++ {

						}
					}
					if !c.z.IsMovable() {

					}
				}
			}
		}
	}

	if len(siteCandidates) <= game.playersys.numPlayers() {
		return fmt.Errorf("no place for camps"), "No place for camps"
	}

	// Generate camp sites
	takenSitesIndices := []int{}
	for _, pl := range game.playersys.players {
		_log.Inf("generate camp site for player:", pl.name)
		zlev := ZLevel_Ground0
		_ = zlev

		randomMapKey := func() (CPos, int) {
			// Get random map key
			i := rand.Intn(len(siteCandidates))
			idx := i
			var k CPos
			for k = range siteCandidates {
				if i == 0 {
					break
				}
				i--
			}
			return k, idx
		}

		//var candidate []CRect

		for {
			k, i := randomMapKey()
			good := true
			for _, idx := range takenSitesIndices {
				if idx == i {
					p("XXX the site index is taken, skipping; idx:", idx)
					good = false
					break
				}
			}
			if good {
				candidate := siteCandidates[k]
				// TODO test candidate against other candidates
				// for example for distance between them

				fg.campSites[pl.id] = candidate
				takenSitesIndices = append(takenSitesIndices, i)
				break
			}
		}

	}

	//pp(fg.campSites)

	_log.Inf("done generate field areas")
	//pp(2)
	return nil, ""
}

var _ = `
func generateFieldObjects(f *Field, fgOpt FieldGenOptions) {
	_log.Inf("[Fieldgen] generate field objects...")
	if fgOpt.disableEverything {
		return
	}

	fw := f.w / 4 //2
	fh := f.h / 4 //2

	// Add field objects
	if fgOpt.generateFieldObjects {
		for x := 0; x < fw; x++ {
			for y := 0; y < fh; y++ {
				cell := &f.cells[x][y]
				if !cell.z.isGround() {
					continue
				}
				if random(0, 1) > 0.9 {
					fo := newFieldObject("tree")
					fo.SetPosX(float64(x*cell_size + half_cell_size))
					fo.SetPosY(float64(y*cell_size + half_cell_size))
					// Set random orientation
					fo.SetRotZ(random(0, 360))
					f.objects = append(f.objects, fo)
				}
			}
		}
	}

	// Add oil rigs
	if fgOpt.generateOilRigs {
		oilRigCells := make([]Cell, 0)
		oilRigLocs := make([]CPos, 0)
		_ = oilRigCells
		_ = oilRigLocs

		// Props
		siteW, siteH := 3, 3
		//zlev := ZLevel_Sea1
		zlev := ZLevel_SeaMax + 1

		//pp(f.getCellsAtZLevel(zlev))
		//pp(f.getCellsAtZLevel(ZLevel_Ground0))
		//pp(f.getCellsAtZLevel(ZLevel_Sea1))
		//pp(f.getCellsAtZLevel(ZLevel(-1)))
		//pp(f.getCellsAtZLevels(ZLevel_Sea0, ZLevel_SeaMax))
		//pp(f.getCellsAtZLevels(ZLevel_SeaMax, ZLevel_Sea0))
		//pp(f.getCells())

		// Find suitable places for oilrigs
		for x := 0; x < f.w-siteW; x++ {
			for y := 0; y < f.h-siteH; y++ {
				//oilRigLocs = append(
				cell := &f.cells[x][y]
				if cell.z != zlev {
					continue
				}
				p(cell)

				suitable := true

				nb := f.getRectSlice(x, y, siteW, siteH)

				for _, n := range nb {
					if n.z != zlev {
						suitable = false
					}
				}

				if suitable {
					oilRigLocs = append(oilRigLocs, CPos{x, y})
				}
			}
		}

		//pp(oilRigLocs)

		for _, p := range oilRigLocs {
			x, y := p.x, p.y
			fo := newFieldObject("tree")
			fo.SetPosX(float64(x*cell_size/2 + half_cell_size/2))
			fo.SetPosY(float64(y*cell_size/2 + half_cell_size/2))
			//fo.setXRot(10)
			f.objects = append(f.objects, fo)
		}
	}

	_log.Inf("[Fieldgen] generate field objects complete")
}

func generateFieldZones(f *Field, fgOpt FieldGenerateOptions) {
	_log.Inf("[Fieldgen] generate field zones...")
	if fgOpt.disableEverything {
		return
	}

	// Generate camp sites
	for _, pl := range game.playersys.players {
		_log.Inf("[Fieldgen] generate camp site for player:", pl.name)
		zlev := ZLevel_Ground0
		minCells := 40 // Continuous cells
		_, _ = zlev, minCells
	}

	_log.Inf("[Fieldgen] generate field zones complete")
}
`
