package main_module

import (
	"math/rand"
)

type PlayerSys struct {
	players []*Player
	player  *Player // Current player
}

func newPlayerSys() *PlayerSys {
	s := &PlayerSys{}
	s.reset()
	return s
}

func (s *PlayerSys) reset() {
	s.player = nil
	s.players = make([]*Player, 0)
}

func (s *PlayerSys) numPlayers() int {
	return len(s.players)
}

func (s *PlayerSys) numAiPlayers() int {
	return len(s.getAiPlayers())
}

// Has local human player.
func (s *PlayerSys) hasPlayer() bool {
	return s.player != nil
}

// Get local human player.
func (s *PlayerSys) getPlayer() *Player {
	if s.player != nil {
		return s.player
	}
	for _, p := range s.players {
		if p.IsHuman() {
			s.player = p
			return p
		}
	}
	panic("no human player?")
}

// Get first player with given campId
func (s *PlayerSys) getPlayerWithCamp(campId CampId) *Player {
	for _, p := range s.players {
		if p.camp.Id == campId {
			return p
		}
	}

	pp("player with campId not found; campId:", campId)
	return nil
}

// Get player by name.
func (s *PlayerSys) getPlayerByName(name string) *Player {
	for _, p := range s.players {
		if p.name == name {
			return p
		}
	}
	pp("no player found; name:", name)
	panic(2)
}

// Get player by playerSlotId
func (s *PlayerSys) getPlayerByPlayerSlotId(slotId int) *Player {
	for _, p := range s.players {
		if p.playerSlotId == slotId {
			return p
		}
	}
	pp("no player found; slotId:", slotId)
	panic(2)
}

func (s *PlayerSys) getAiPlayers() []*Player {
	ret := make([]*Player, 0)
	for _, p := range s.players {
		if !p.IsHuman() {
			ret = append(ret, p)
		}
	}
	
	return ret
}

func (s *PlayerSys) getRandomAiPlayer() *Player {
	num := s.numAiPlayers()
	if num < 1 {
		pp("no ai players")
	}
	idx := rand.Intn(num)
	aiPlayers := s.getAiPlayers()
	
	return aiPlayers[idx]
}

func (s *PlayerSys) addPlayer(p *Player) {
	s.players = append(s.players, p)
	if p.IsHuman() {
		s.player = p
	}
}

func (s *PlayerSys) update(dt float64) {
	for _, p := range s.players {
		p.update(dt)
	}
}

// Have local human player
func HasPlayer() bool {
	return game.playersys.hasPlayer()
}

// Get default local (current) player.
func GetPlayer() *Player {
	return game.playersys.getPlayer()
}

// Return first player with given campId.
func GetPlayerWithCamp(campId CampId) *Player {
	return game.playersys.getPlayerWithCamp(campId)
}

func GetRandomAiPlayer() *Player {
	return game.playersys.getRandomAiPlayer()
}
