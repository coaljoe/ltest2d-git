package main_module

import (
	"github.com/looplab/fsm"
)

type UnitBhvI interface {
	getType() string
	setType(name string)
	start()
	update(dt float64)
}

// Base UnitBhv
type UnitBhv struct {
	type_ string
}

func newUnitBhv() *UnitBhv {
	ub := &UnitBhv{}
	return ub
}

func (ub *UnitBhv) setType(name string) {
	ub.type_ = name
}

func (ub *UnitBhv) getType() string {
	return ub.type_
}

type LoadUnloadUnitBhv struct {
	*UnitBhv
	uai  *UnitAi
	from *Building
	to   *Building
	u    *Unit // Link
	next *Building
	fsm  *fsm.FSM
	// goto_from, goto_to, load, unload
	//state string
}

func newLoadUnloadUnitBhv(uai *UnitAi) *LoadUnloadUnitBhv {
	lu := &LoadUnloadUnitBhv{
		UnitBhv: newUnitBhv(),
		uai:     uai,
		u:       uai.u,
		//state:   "goto_from",
	}
	lu.setType("load_unload")
	lu.fsm = fsm.NewFSM("idle",
		fsm.Events{
			//// idle -> pregoto_next
			//{Name: "pregoto_next", Src: []string{"idle"}, Dst: "pregoto_next"},
			//// pregoto_next -> goto_next
			//{Name: "load_unload", Src: []string{"goto_next"}, Dst: "moving"},
			// idle -> goto_next
			{Name: "goto_next", Src: []string{"idle"}, Dst: "goto_next"},
			// goto_next -> load_unload
			{Name: "load_unload", Src: []string{"goto_next"}, Dst: "load_unload"},
			// load_unload -> goto_next
			{Name: "goto_next", Src: []string{"load_unload"}, Dst: "goto_next"},
			//{Name: "idle", Src: []string{"moving"}, Dst: "idle"},
			//{Name: "moving", Src: []string{"idle"}, Dst: "moving"},
		},
		fsm.Callbacks{
			"enter_state": func(e *fsm.Event) { lu._enterState(e) },
		})
	return lu
}

func (lu *LoadUnloadUnitBhv) start() {
	lu.u.mover.moveTo(lu.from.cx, lu.from.cy)
}

func (lu *LoadUnloadUnitBhv) ready() bool {
	return lu.from != nil && lu.to != nil
}

func (lu *LoadUnloadUnitBhv) state() string {
	return lu.fsm.Current()
}

// Fsm switch
func (lu *LoadUnloadUnitBhv) _enterState(e *fsm.Event) {
	_log.Dbgf("::FSM:: LoadUnloadUnitBhv from %s to %s [cur: %s]\n",
		e.Src, e.Dst, e.FSM.Current())

	//pp(2)

	//u := lu.u

	switch e.Dst {
	case "idle": // Start
		{
		}
		//u.position.speed = 0
		/*
			if e.Src == "moving" {
				pub(ev_unit_move, u)
				pp(2)
			}
		*/

	case "premoving": // Start
		{
		}

	case "goto_next": // Start
		if lu.next == nil {
			lu.next = lu.from
		} else {
			// Swap next
			if lu.next == lu.to {
				lu.next = lu.from
			} else if lu.next == lu.from {
				lu.next = lu.to
			} else {
				pp("unknown lu.next", lu.next)
			}
		}
		lu.u.mover.moveTo(lu.next.cx, lu.next.cy)

	}
}

func (lu *LoadUnloadUnitBhv) update(dt float64) {
	//dump(lu)
	//pp(2, lu.ready())
	if !lu.ready() {
		return
	}

	// Update states
	switch lu.state() {
	case "idle":
		lu.fsm.Event("goto_next")

	case "goto_next":
		{
			// Check
			if lu.u.at(lu.next.cx, lu.next.cy) {
				lu.fsm.Event("load_unload")
			}
		}

	case "load_unload":
		p("do load_unload...")
		lu.fsm.Event("goto_next")

	}

	var _ = `
	// Handle states
	{
		if lu.state == "goto_from" {
			// Check
			if lu.u.isAt(lu.from.cx, lu.from.cy) {
				lu.state = "load"
			} else {
				// Update
			}
		} else if lu.state == "load" {

		} else {
			pp("uknown state:", lu.state)
		}
	}
	`

}

// component?
type UnitAi struct {
	u   *Unit
	bhv UnitBhvI
}

func newUnitAi(u *Unit) *UnitAi {
	uai := &UnitAi{u: u}
	return uai
}

func (uai *UnitAi) start() {

}

func (uai *UnitAi) setBhvType(name string) {
	if name == "load_unload" {
		uai.bhv = newLoadUnloadUnitBhv(uai)
	} else {
		pp("unknown type, name:", name)
	}
}

func (uai *UnitAi) update(dt float64) {
	if uai.bhv != nil {
		uai.bhv.update(dt)
	}

}
