package main_module

import (
	"github.com/veandco/go-sdl2/sdl"
)

type HudView struct {
	m *Hud
}

func newHudView(h *Hud) *HudView {
	hv := &HudView{m: h}

	// XXX: temporary, for testing only
	//rx.Rxi().Renderer().SetPostRenderCB(hv.render)
	return hv
}

func (v *HudView) drawSelectionFrame(o ObjI) {
	if o.getObjType() == "building" {
		b := o.(*Building)
		px, py := int(b.PosX()), int(b.PosY())
		sx, sy := worldToScreenPos(px, py)
		//w, h := cell_size, cell_size
		//w, h := b.dimX*cell_size, b.dimY*cell_size
		w, h := b.areaX*cell_size, b.areaY*cell_size

		p("YYY", o.getId())
		p("YYY", px, py, sx, sy, w, h)

		lineColor := sdl.Color{255, 255, 255, sdl.ALPHA_OPAQUE}
		//lineWidth := 2
		lineWidth := 1
		_ = lineWidth

		/*
			drawRectangleColorWidth(sx, sy, w, h,
				lineColor, lineWidth)
		*/
		//cornerlen := cell_size / 2
		cornerlen := 10
		// Top-left
		DrawLine(sx, sy, sx+cornerlen, sy, lineColor)
		DrawLine(sx, sy, sx, sy+cornerlen, lineColor)
		// Top-right
		DrawLine(sx+w, sy, sx+w-cornerlen, sy, lineColor)
		DrawLine(sx+w, sy, sx+w, sy+cornerlen, lineColor)
		// Bottom-left
		DrawLine(sx, sy+h, sx+cornerlen, sy+h, lineColor)
		DrawLine(sx, sy+h, sx, sy+h-cornerlen, lineColor)
		// Bottom-right
		DrawLine(sx+w, sy+h, sx+w-cornerlen, sy+h, lineColor)
		DrawLine(sx+w, sy+h, sx+w, sy+h-cornerlen, lineColor)

		// Health bar
		//lineColor = sdl.Color{0, 255, 0, sdl.ALPHA_OPAQUE}
		//lineColor = sdl.Color{102, 255, 0, sdl.ALPHA_OPAQUE}
		//lineColor = sdl.Color{11, 218, 81, sdl.ALPHA_OPAQUE}
		lineColor = sdl.Color{50, 205, 50, sdl.ALPHA_OPAQUE}
		padding := 12
		paddingBottom := 3
		length := (sx + w - padding) - (sx + padding)
		//maxLength := 32 * 2
		maxLength := 28 * 2
		if length <= maxLength {
			DrawHLine(sx+padding, sy+h-paddingBottom, length, lineColor)
			DrawHLine(sx+padding, sy+h-paddingBottom-1, length, lineColor)
			DrawHLine(sx+padding, sy+h-paddingBottom-2, length, lineColor)
			//drawHLine(sx+padding, sy+h-paddingBottom-3, length, lineColor)
		} else {
			l := maxLength
			center := sx + (w / 2)
			DrawHLine(center-(l/2), sy+h-paddingBottom, l, lineColor)
			DrawHLine(center-(l/2), sy+h-paddingBottom-1, l, lineColor)
			DrawHLine(center-(l/2), sy+h-paddingBottom-2, l, lineColor)
			//drawHLine(center-(l/2), sy+h-paddingBottom-3, l, lineColor)
			//pp(length)
		}
		/*
			drawLine(sx+padding, sy+h-paddingBottom, sx+w-padding, sy+h-paddingBottom, lineColor)
			drawLine(sx+padding, sy+h-paddingBottom-1, sx+w-padding, sy+h-paddingBottom-1, lineColor)
			drawLine(sx+padding, sy+h-paddingBottom-2, sx+w-padding, sy+h-paddingBottom-2, lineColor)
		*/

	} else if o.getObjType() == "unit" {
		u := o.(*Unit)
		//px, py := int(u.PosX()), int(u.PosY())
		//px, py := u.CellPosX()*cell_size, u.CellPosY()*cell_size
		px, py := int(u.PosX()) - cell_size/2, int(u.PosY()) - cell_size/2
		//pp(u.PosX(), u.CellPosX(), u.CellPosX() * cell_size)
		sx, sy := worldToScreenPos(px, py)
		//w, h := cell_size, cell_size
		//w, h := b.dimX*cell_size, b.dimY*cell_size
		w, h := 28, 28

		p("YYY", o.getId())
		p("YYY", px, py, sx, sy, w, h)

		lineColor := sdl.Color{255, 255, 255, sdl.ALPHA_OPAQUE}
		//lineWidth := 2
		lineWidth := 1
		_ = lineWidth

		/*
			drawRectangleColorWidth(sx, sy, w, h,
				lineColor, lineWidth)
		*/
		//cornerlen := cell_size / 2
		cornerlen := 4
		// Top-left
		DrawLine(sx, sy, sx+cornerlen, sy, lineColor)
		DrawLine(sx, sy, sx, sy+cornerlen, lineColor)
		// Top-right
		DrawLine(sx+w, sy, sx+w-cornerlen, sy, lineColor)
		DrawLine(sx+w, sy, sx+w, sy+cornerlen, lineColor)
		// Bottom-left
		DrawLine(sx, sy+h, sx+cornerlen, sy+h, lineColor)
		DrawLine(sx, sy+h, sx, sy+h-cornerlen, lineColor)
		// Bottom-right
		DrawLine(sx+w, sy+h, sx+w-cornerlen, sy+h, lineColor)
		DrawLine(sx+w, sy+h, sx+w, sy+h-cornerlen, lineColor)

		// Health bar
		//lineColor = sdl.Color{0, 255, 0, sdl.ALPHA_OPAQUE}
		//lineColor = sdl.Color{102, 255, 0, sdl.ALPHA_OPAQUE}
		//lineColor = sdl.Color{11, 218, 81, sdl.ALPHA_OPAQUE}
		lineColor = sdl.Color{50, 205, 50, sdl.ALPHA_OPAQUE}
		padding := 3
		paddingBottom := 3
		length := (sx + w - padding) - (sx + padding)
		//maxLength := 32 * 2
		maxLength := 28 * 2
		if length <= maxLength {
			DrawHLine(sx+padding, sy+h-paddingBottom, length, lineColor)
			DrawHLine(sx+padding, sy+h-paddingBottom-1, length, lineColor)
			DrawHLine(sx+padding, sy+h-paddingBottom-2, length, lineColor)
			//drawHLine(sx+padding, sy+h-paddingBottom-3, length, lineColor)
		} else {
			l := maxLength
			center := sx + (w / 2)
			DrawHLine(center-(l/2), sy+h-paddingBottom, l, lineColor)
			DrawHLine(center-(l/2), sy+h-paddingBottom-1, l, lineColor)
			DrawHLine(center-(l/2), sy+h-paddingBottom-2, l, lineColor)
			//drawHLine(center-(l/2), sy+h-paddingBottom-3, l, lineColor)
			//pp(length)
		}
		/*
			drawLine(sx+padding, sy+h-paddingBottom, sx+w-padding, sy+h-paddingBottom, lineColor)
			drawLine(sx+padding, sy+h-paddingBottom-1, sx+w-padding, sy+h-paddingBottom-1, lineColor)
			drawLine(sx+padding, sy+h-paddingBottom-2, sx+w-padding, sy+h-paddingBottom-2, lineColor)
		*/

	} else {
		pp("unknown objtype:", o.getObjType())
	}
}

var _ = `
// Draw selection box for the object.
func (v *HudView) drawSelectionFrame(o *Obj) {
	frameSizePxW := 26.0
	frameSizePxH := 26.0
	frameColor := Vec3{.92, .94, .96}
	lifebarColor := Vec3{0.1, 0.7, 0.1}

	r := rx.Rxi().Renderer()
	//r.Set2DMode()
	pos := o.Pos()
	sxi, syi := r.Size()
	sx, sy := float64(sxi), float64(syi)
	pw, ph := r.GetPw(), r.GetPh()
	cam := rx.Rxi().Camera.Camera
	screen_point := Project(pos, cam.GetViewMatrix(), cam.GetProjectionMatrix(), 0, 0, sxi, syi)
	//p("screen_point:", screen_point)
	p_sx, p_sy := screen_point.X()/sx, screen_point.Y()/sy
	camDefZoom := 23.0 //game.scene.camera.defaultZoom // XXX fixme
	camCurZoom := game.scene.camera.cam.Camera.Zoom()
	zoom := camDefZoom / camCurZoom
	//p("camDefZoom:", camDefZoom, "camCuzZoom:", camCurZoom, "zoom:", zoom)
	fw := frameSizePxW * pw * zoom * vars.resScaleX
	fh := frameSizePxH * ph * zoom * vars.resScaleY
	coords := Vec4{p_sx - fw/2, p_sy - fh/2, fw, fh}
	//p("coords:", coords)
	//gl.PushMatrix()
	//gl.Translatef(0, 0, 1)
	gl.Disable(gl.DEPTH_TEST)
	gl.Enable(gl.LINE_SMOOTH)

	// Draw box
	rx.DrawSetLineWidth(1)
	//rx.DrawSetColor(.92, .94, .96)
	rx.DrawSetColorV(frameColor)
	rx.DrawRectV(coords)

	// Draw lifebar
	ox := 2.0 * pw * zoom
	oy := 2.0 * ph * zoom
	p0 := Vec3{p_sx - fw/2 + ox, p_sy + fh/2 - oy, 0}
	p1 := Vec3{p_sx + fw/2 - ox, p_sy + fh/2 - oy, 0}
	//rx.DrawSetColor(0.1, 0.7, 0.1)
	rx.DrawSetColorV(lifebarColor)
	rx.DrawSetLineWidth(2)
	rx.DrawLineV(p0, p1)

	gl.Enable(gl.LINE_SMOOTH)
	gl.Enable(gl.DEPTH_TEST)
	//gl.PopMatrix()
	rx.DrawResetColor()
	//r.Unset2DMode()
}
`

func (v *HudView) drawBuildingSelectionFrames() {
	if v.m.selBuilding != nil {
		v.drawSelectionFrame(v.m.selBuilding)
	}
}

func (v *HudView) drawMultipleSelectionFrame() {
	lineColor := sdl.Color{255, 255, 255, sdl.ALPHA_OPAQUE}
	//lineWidth := 2
	lineWidth := 1
	_ = lineWidth

	x := v.m.selRect.X()
	y := v.m.selRect.Y()
	w := v.m.selRect.W()
	h := v.m.selRect.H()
	
	sx, sy := worldToScreenPos(x, y)

	DrawRectangleColorWidth(sx, sy, w, h, lineColor, lineWidth)
}

func (v *HudView) draw(r *sdl.Renderer) {

	if v.m.selecting {
		v.drawMultipleSelectionFrame()
	}

	for _, u := range v.m.selUnits {
		v.drawSelectionFrame(u)
		//pp(3)
	}

	if v.m.mouseState == HudMouseState_PlaceBuilding {

		if v.m.hasFocus() {
			for i, cpos := range v.m.hlTiles {
				c := v.m.hlColors[i]
				sx, sy := worldToScreenPos(cpos.x*cell_size, cpos.y*cell_size)
				p("YYY", cpos, sx, sy)
				//drawRectangleColorWidth(sx, sy, cell_size, cell_size, c, 2)
				DrawFillRect(sx, sy, cell_size, cell_size, c)
			}
		}
	}

	// Draw unit debug
	// TODO: make it more generic? like unit.debugDraw()?
	for _, ui := range game.unitsys.getElems() {
		u := ui.(*Unit)
		//u.mover.debugDraw()
		//u.position.debugDraw()
		u.debugDraw()
	}

	//pp(2)

	var _ = `
	m := v.m
	r.Set2DMode()

	if m.selecting {

		//gl.PushMatrix()
		sxi, syi := r.Size()
		sx, sy := float64(sxi), float64(syi)
		_r := m.selRect
		// Note: mouse Y coord is inverted
		x1, y1 := _r[0]/sx, (sy-_r[1])/sy
		x2, y2 := _r[2]/sx, (sy-_r[3])/sy
		//z := .
		//rx.DrawLine(_r[0], _r[1], 0, _r[2], _r[3], 0)
		//p("rx.DrawLine:", _r[0], _r[1], 0, _r[2], _r[3], 0)

		//p("rx.DrawLine:", x1, y1, 0, x2, y2, 0)
		//rx.DrawLine(x1, y1, 0, x2, y2, 0)
		//gl.PopMatrix()
		//r.Unset2DMode()
		//r.Set2DMode()
		//gl.PushMatrix()
		//gl.Translatef(0, 0, 1)
		gl.Disable(gl.DEPTH_TEST)
		rx.DrawRect(math.Min(x1, x2), math.Min(y1, y2), math.Abs(x1-x2), math.Abs(y1-y2))
		gl.Enable(gl.DEPTH_TEST)
		//gl.PopMatrix()
		//r.Unset2DMode()
	}

	for _, o := range m.selObjs {
		v.drawSelectionFrame(o)
	}

	//pp("derp")

	//r.Set2DMode()
	//gl.PushMatrix()
	//rx.DrawLine(0, 0, 1, 10, 10, 1)
	//p("draw line")

	//rx.DrawLine(0, 0, 0, .5, .5, 0)
	//gl.PopMatrix()
	r.Unset2DMode()

	// Debug

	if m.debug {
		if m.groundHit {
			rx.DebugDrawCube(m.groundPoint, 2, rx.ColorYellow)
		}
	}
	`
}

func (v *HudView) update(dt float64) {
	m := v.m
	_ = m

	// XXX can't execute draw calls here
	var _ = `
	r := rx.Rxi().Renderer()

	if m.selecting {
		r.Set2DMode()
		//gl.PushMatrix()
		sxi, syi := rx.Rxi().Renderer().Size()
		sx, sy := float64(sxi), float64(syi)
		_r := m.selRect
		x1, y1 := _r[0] / sx, _r[1] / sy
		x2, y2 := _r[2] / sx, _r[2] / sy
		z := 1.
		rx.DrawLine(_r[0], _r[1], 0, _r[2], _r[3], 0)
		//p("rx.DrawLine:", _r[0], _r[1], 0, _r[2], _r[3], 0)
		rx.DrawLine(x1, y1, z, x2, y2, z)
		p("rx.DrawLine:", x1, y1, 0, x2, y2, 0)
		//gl.PopMatrix()
		r.Unset2DMode()
	}

	r.Set2DMode()
	//gl.PushMatrix()
	rx.DrawLine(0, 0, 1, 10, 10, 1)
	p("draw line")

	rx.DrawLine(0, 0, 0, .5, .5, 0)
	//gl.PopMatrix()
	r.Unset2DMode()
	`
}
