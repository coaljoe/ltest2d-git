package main_module

type UnitProducer struct {
	prodTypes []ProdType
}

func newUnitProducer() *UnitProducer {
	up := &UnitProducer{}
	return up
}

func (up *UnitProducer) addProdType(pt ProdType) {
	up.prodTypes = append(up.prodTypes, pt)
}

func (up *UnitProducer) produce(ptName string, ptCampId CampId) {
}
