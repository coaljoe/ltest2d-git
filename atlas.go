package main_module

import (
	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
	"fmt"
)

type Atlas struct {
	numImages int
	//numTiles   int
	//tileWidth  int
	//tileHeight int
	cursorPosX  int
	cursorPosY  int
	maxTexWidth int
	texWidth    int
	texHeight   int
	images      []*sdl.Surface
	image       *sdl.Surface
	tex         *sdl.Texture
	rects       []sdl.Rect
	imageMap    map[string]int
	ready       bool
}

func newAtlas() *Atlas {
	a := &Atlas{
		images:      make([]*sdl.Surface, 0),
		rects:       make([]sdl.Rect, 0),
		imageMap:    make(map[string]int),
		maxTexWidth: 1024,
		ready:       false,
	}
	return a
}

func (a *Atlas) addImage(image *sdl.Surface, name string) int {
	_log.Dbg("%F image: (is nil)", image == nil, "name:", name)

	//defer image.Free()
	a.images = append(a.images, image)

	idx := len(a.images) - 1
	if name != "" {
		if _, ok := a.imageMap[name]; ok {
			pp("error: can't add image with such name, it already exists in imageMap. name:", name)
		}
		a.imageMap[name] = idx
	}

	return idx
}

func (a *Atlas) addImageFromPath(path string, name string) int {
	_log.Dbg("%F path:", path, "name:", name)

	image, err := img.Load(path)
	if err != nil {
		pp("error: failed to load image:", err)
	}
	
	return a.addImage(image, name)
}

func (a *Atlas) addTileImageFromPath(path string, tileW, tileH int, namePrefix string) map[string]int {
	_log.Dbg("%F path:", path, "namePrefix:", namePrefix)

	image, err := img.Load(path)
	if err != nil {
		pp("error: failed to load image:", err)
	}

	tilesX := int(image.W) / tileW
	tilesY := int(image.H) / tileH

	idxMap := make(map[string]int)

	for y := 0; y < tilesY; y++ {
		for x := 0; x < tilesX; x++ {
			xname := fmt.Sprintf("%s_tile_%d_%d", namePrefix, x, y)

			ximage, err := sdl.CreateRGBSurface(0, int32(tileW), int32(tileH), 32,
				sdl_rmask, sdl_gmask, sdl_bmask, sdl_amask)
			if err != nil {
				pp("error: can't create image:", err)
			}

			srcRect := sdl.Rect{int32(x * tileW), int32(y * tileH), int32(tileW), int32(tileH)}
			err2 := image.Blit(&srcRect, ximage, nil)
			if err2 != nil {
				pp("error:", err2)
			}

			// Finally
			idx := a.addImage(ximage, xname)

			idxMap[xname] = idx
		}
	}

	return idxMap
}

func (a *Atlas) build() {
	totalWidth := 0
	for _, im := range a.images {
		totalWidth += int(im.W)
	}

	texWidth := 0
	texHeight := 0
	_ = texHeight

	if totalWidth < a.maxTexWidth {
		texWidth = totalWidth
	} else {
		texWidth = a.maxTexWidth
	}

	rects := make([]CRect, 0)
	//minY := 0
	// XXX TODO: not optimized
	// TODO: add sorting from large to small image
	for _, im := range a.images {
		if a.cursorPosX+int(im.W) > texWidth {
			a.cursorPosX = 0
			//a.cursorPosY =
		}

		// Solve PosY
		//y := minY
		//p("minY:", minY)
		y := 0
		for {
			//p("solve PosY, y:", y)
			candidateRect := CRect{a.cursorPosX, y, int(im.W), int(im.H)}
			good := true
			for _, xrect := range rects {
				if xrect.overlap(candidateRect) {
					//p("overlap:", xrect, candidateRect)
					good = false
					break
				}
			}
			if good {
				a.cursorPosY = y
				// Save minY
				//minY = y
				/*
					if y < minY || minY == 0 {
						minY = y
					}
				*/
				/*
					if minY == 0 {
						minY = y
					} else if y < minY {
						minY = y
					}
				*/
				break
			}
			y++
		}
		//rect := sdl.Rect{int(a.cursorPosX), int(a.cursorPosY), im.W, im.H}
		rect := CRect{a.cursorPosX, a.cursorPosY, int(im.W), int(im.H)}
		rects = append(rects, rect)

		a.cursorPosX += int(im.W)
		texHeight = a.cursorPosY + int(im.H)
	}
	p("texHeight:", texHeight, "texWidth:", texWidth)
	//pp(rects)

	// Rects to sdl rects
	for _, rect := range rects {
		sdlRect := sdl.Rect{int32(rect.x), int32(rect.y), int32(rect.w), int32(rect.h)}
		a.rects = append(a.rects, sdlRect)
	}

	image, err := sdl.CreateRGBSurface(0, int32(texWidth), int32(texHeight), 32,
		sdl_rmask, sdl_gmask, sdl_bmask, sdl_amask)
	if err != nil {
		pp("error: can't create image:", err)
	}

	for i, im := range a.images {
		rect := a.rects[i]
		im.Blit(nil, image, &rect)
	}

	tex, err := renderer.CreateTextureFromSurface(image)
	if err != nil {
		pp("error: failed to create texture:", err)
	}

	a.tex = tex
	a.image = image
	a.texWidth = texWidth
	a.texHeight = texHeight
	a.ready = true
}

func (a *Atlas) saveImage(path string) {
	err := img.SavePNG(a.image, path)
	if err != nil {
		pp("error:", err)
	}
}

//func (a *Atlas) getTileRect(x, y int) sdl.Rect {
//		rect := sdl.Rect{int32(x * a.tileWidth),
//}

func (a *Atlas) drawImageIdx(idx int, x, y int) {
	if !a.ready {
		pp("error: atlas isn't ready (wasn't build?)")
	}
	srcRect := a.rects[idx]
	dstRect := sdl.Rect{int32(x), int32(y), srcRect.W, srcRect.H}
	renderer.Copy(a.tex, &srcRect, &dstRect)
}

func (a *Atlas) drawImageName(name string, x, y int) {
	idx, ok := a.imageMap[name]
	if !ok {
		pp("error: no image with such name in imageMap, name:", name)
	}

	a.drawImageIdx(idx, x, y)
}
