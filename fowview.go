package main_module

import (
	"fmt"
	"github.com/veandco/go-sdl2/sdl"
	"path"
	"path/filepath"
	"strings"
)

type FowView struct {
	m       *Field
	fw, fh  int
	fx, fy  int
	atlas   *Atlas
	tilePix map[string]int
	fow     *Fow // Visible fow
	spawned bool
}

func newFowView(m *Field) *FowView {
	v := &FowView{
		m:       m,
		atlas:   newAtlas(),
		tilePix: make(map[string]int),
	}
	v.fow = GetPlayer().GetFow()
	return v
}

func (v *FowView) load() {
	files, err := filepath.Glob("res/field/tiles/fow/[l]*.png")
	if err != nil {
		panic(err)
	}
	//pp(files)
	if len(files) == 0 {
		pp("no files found")
	}

	files = append(files, "res/field/tiles/misc/no_tile.png")

	for _, file := range files {
		fileNoExt := strings.TrimSuffix(filepath.Base(file), path.Ext(file))
		idx := v.atlas.addImageFromPath(file, fileNoExt)
		v.tilePix[fileNoExt] = idx
		//pp(v.tilesPix)
	}
	v.atlas.build()
	//v.atlas.saveImage(vars.tmpDir + "/ltest2d_atlas.png")
}

func (v *FowView) spawn() {
	v.load()
}

func (v *FowView) drawCell(x, y int, dst sdl.Rect) {
	cell := &v.m.cells[x][y]
	_ = cell
	//idx := -1
	if v.fow.IsCellVisible(x, y) {
		//idx = v.tilesPix["l3_0"]
	} else {
		//idx = v.tilesPix["l3_26"]
		//idx = v.tilesPix["l3_0"]
		//tileId := game.field.getCell(x, y).fowTileId
		tileId := v.fow.tileIds[x][y]
		k := fmt.Sprintf("l3_%d", tileId)
		idx, ok := v.tilePix[k]
		//noTile := false
		if !ok {
			//pp("error: bad k:", k)
			//noTile = true
			var ok2 bool
			idx, ok2 = v.tilePix["no_tile"]
			if !ok2 {
				pp("no_tile key error")
			}
		}
		//pp(tileId, k, idx)
		if idx != -1 {
			//v.atlas.drawImageIdx(v.tilesPix[pixId], int(dst.X), int(dst.Y))
			v.atlas.drawImageIdx(idx, int(dst.X), int(dst.Y))
		}
	}
}

func (v *FowView) draw() {
	idx := v.tilePix["l3_26"]
	v.atlas.drawImageIdx(idx, 0, 0)
}
