package main_module

var conf = newConf()

type Conf struct {
	disableMusic bool
	disableSound bool
}

func newConf() *Conf {
	c := &Conf{
		disableMusic: false,
		disableSound: false,
	}
	return c
}
