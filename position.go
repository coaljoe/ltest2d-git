// Позиционный компонент.
// Отвечает за позиционирование и ориентацию, а также перемещение,
// но не трансформацию (для этого есть Transform)
//
package main_module

import (
	. "kristallos.ga/rx/math"
	"math"
)

// component
type Position struct {
	obj *Obj // link
	// Current position on the line vector
	// can be negative
	pos Vec3 // In pixel coords
	//dPos Vec3
	//sPos Pos
	sPos        Vec3  // In pixel coords
	dPos_       *Vec3 // In pixel coords. Fixme
	dPosRelWrap *Vec3 // Wrapped rel. dPos
	//velocity    float64
	speed float64
	//rotVelocity float64
	dir *Dir
	//dirVec Vec3 // Direction vector
	//dDir      Dir
	//travelTime float64 // Total travel time (tt)
	stepTime     float64
	i            float64
	direction    Vec3    // Move direction vector (line vector)
	distance     float64 // Move distance
	negDirection Vec3
	negDistance  float64
	moveOpposite bool // Move in opposite direction mode
	moving       bool
	// Component
	//entityId int
	// Debug
	d_showDirVec bool
}

func (p *Position) dPos() Vec3  { return *p.dPos_ }
func (p *Position) dPos2() Vec2 { return p.dPos().ToVec2() }

func newPosition(obj *Obj) *Position {
	p := &Position{
		obj: obj,
		//pos:    Vec3Zero,
		//dPos: Vec3Zero,
		dir: newDir(),
		//dDir: NewDir(),
		//dirVec: Vec3Zero,
		//d_showDirVec: true,
	}

	return p
}

func (p *Position) start() {}

func (p *Position) setDPos(_p Vec3) {
	p.dPos_ = &_p
	p.sPos = p.obj.Pos()
	p.pos = p.sPos
	p.stepTime = 0 // Fixme: move to update code?
	p.distance = _p.Sub(p.sPos).Len()
	//p.direction = _p.Sub(p.sPos).Norm()
	p.direction = _p.Sub(p.sPos)
	p.moving = true

	//pp(p.distance, game.field.w * cell_size)
	if p.distance > float64((game.field.w*cell_size)/2) {
		p.moveOpposite = true
	} else {
		p.moveOpposite = false
	}

	if p.moveOpposite {
		p.negDirection = p.direction.Negate()
		p.negDistance = p.negDirection.Len()
	} else {
		p.negDirection = Vec3Zero
		p.negDistance = 0
	}

	if p.moveOpposite {
		v := p.getWrappedRelDPos()
		p.dPosRelWrap = &v
	} else {
		p.dPosRelWrap = nil
	}

	//pp(p.distance, p.direction)
}

func (p *Position) setDPos2(_p Vec2) {
	obj := p.obj
	p.setDPos(Vec3{_p.X(), _p.Y(), obj.PosZ()})
}

/*
// XXX direction is always the same
func (p *Position) direction() Vec3 {
    return p.dPos_.Sub(s.sPos)
}

func (p *Position) distance() float64 {
    return p.direction().Len()
}
*/

func (p *Position) isMoving() bool {
	if p.dPos_ != nil {
		return true
	}
	return false
}

func (p *Position) hasDPos() bool {
	return p.dPos_ != nil
}

func (p *Position) setDirTowardsD() {
}

var _ = `
// Get wrapped dpos with the shortest path to it
func (p *Position) getWrappedDPos() Vec3 {
	if !p.hasDPos() {
		pp("DPos is not set")
	}

	pos := p.sPos
	tar := *p.dPos_																																																																																																																																																																																																																			

	// Get direction vector
	rst := tar.Sub(pos)
	sizeX := float64(game.field.w) //32
	sizeY := float64(game.field.h) //32
	xWrapped := false
	yWrapped := false

	//__p("rst before:", rst)

	if math.Abs(rst.X()) > sizeX/2 {
		if rst.X() > 0 {
			rst.SetX(pos.X() - sizeX)
		} else {
			rst.SetX(pos.X() + sizeX)
		}

		xWrapped = true
	}

	if math.Abs(rst.Y()) > sizeY/2 {
		if rst.Y() > 0 {
			rst.SetY(pos.Y() - sizeY)
		} else {
			rst.SetY(pos.Y() + sizeY)
		}

		yWrapped = true
	}

	// Fix unwrapped coords (fixme?)
	res2 := rst
	if !xWrapped {
		res2.SetX(pos.X())
	}
	if !yWrapped {
		res2.SetY(pos.Y())
	}

	return res2
}
`

// Get wrapped dpos with the shortest path to it
// Return values may be negative
func (p *Position) getWrappedRelDPos() Vec3 {
	if !p.hasDPos() {
		pp("DPos is not set")
	}

	pos := p.sPos
	tar := *p.dPos_

	sizeX := game.field.w * cell_size //32
	sizeY := game.field.h * cell_size //32

	res := p.getWrappedRelDPosEx(pos, tar, sizeX, sizeY)

	return res
}

// Generic version. Can be used separately
// Return values may be negative
func (p *Position) getWrappedRelDPosEx(sPos, dPos Vec3, isizeX, isizeY int) Vec3 {
	_log.Dbg("%F sPos:", sPos, "dPos:", dPos, "isizeX:", isizeX, "isiteY:", isizeY)

	pos := sPos
	tar := dPos

	// Get direction vector
	rst := tar.Sub(pos)
	sizeX := float64(isizeX)
	sizeY := float64(isizeY)
	xWrapped := false
	yWrapped := false

	__p("rst before:", rst)

	if math.Abs(rst.X()) > sizeX/2 {
		if rst.X() > 0 {
			rst.SetX(tar.X() - sizeX)
		} else {
			rst.SetX(tar.X() + sizeX)
		}

		xWrapped = true
	}

	if math.Abs(rst.Y()) > sizeY/2 {
		if rst.Y() > 0 {
			rst.SetY(tar.Y() - sizeY)
		} else {
			rst.SetY(tar.Y() + sizeY)
		}

		yWrapped = true
	}

	__p("rst:", rst)
	__p("xWrapped:", xWrapped)
	__p("yWrapped:", yWrapped)

	// Fix unwrapped coords (fixme?)
	res2 := rst
	if !xWrapped {
		res2.SetX(tar.X())
	}
	if !yWrapped {
		res2.SetY(tar.Y())
	}

	return res2
}

func (p *Position) destroy() {
}

// From:
// answers.unity.com/questions/414829/any-one-know-maths-behind-this-movetowards-functio.html
func moveTowards(current, target Vec3, maxDistanceDelta float64) Vec3 {
	//pp(current, target, maxDistanceDelta)
	fmagnitude := func(v Vec3) float64 {
		return math.Sqrt(math.Pow(v[0], 2) + math.Pow(v[1], 2) + math.Pow(v[2], 2))
	}
	_ = fmagnitude
	fdiv := func(v Vec3, b float64) Vec3 {
		return Vec3{v[0] / b, v[1] / b, v[2] / b}
	}
	a := target.Sub(current)

	if a.Len() > float64(game.field.w) {
		oldA := a
		//a = a.MulScalar(-1)
		/*
		   wrapX := wrapCoordXPx(int(a.X()))
		   wrapY := wrapCoordYPx(int(a.Y()))
		   a.SetX(float64(wrapX))
		   a.SetY(float64(wrapY))
		*/
		//wrapX := wrapCellDistX(int(oldA.X()), int(a.X()))
		//wrapY := wrapCellDistY(int(oldA.Y()), int(a.Y()))
		wrapX := wrapDistX(int(oldA.X()), int(a.X()))
		wrapY := wrapDistY(int(oldA.Y()), int(a.Y()))
		a.SetX(float64(wrapX))
		a.SetY(float64(wrapY))
		p(current, target)
		pp(oldA, a)
	}

	//magnitude := fmagnitude(a)
	magnitude := a.Len()
	if magnitude <= maxDistanceDelta || magnitude == 0.0 {
		return target
	}
	//return current + a / magnitude * maxDistanceDelta;
	r1 := fdiv(a, magnitude).MulScalar(maxDistanceDelta)
	return current.Add(r1)
}

func (p *Position) debugDraw() {
	if p.d_showDirVec && p.hasDPos() {
		x1, y1 := int(p.obj.PosX()), int(p.obj.PosY())
		x2, y2 := x1+int(p.direction.X()), y1+int(p.direction.Y())
		//x1, y1 := 100, 100
		//x2, y2 := 200, 100
		sx1, sy1 := worldToScreenPos(x1, y1)
		sx2, sy2 := worldToScreenPos(x2, y2)
		//drawLine(sx1, sy1, sx2, sy2, ColorWhite)
		DrawArrow(sx1, sy1, sx2, sy2, ColorWhite)
		//pp(p.obj.Pos(), p.dirVec)
	}
}

func (p *Position) update(dt float64) {
	//pp("doneMoving:", p.dPos_ == nil, p.speed)
	obj := p.obj
	//var _ = p

	if p.hasDPos() {
		//p.dirVec = p.obj.Pos().Sub(p.dPos())
		//p.dirVec = (p.dPos().Sub(p.obj.Pos())).Norm()
		//p.dirVec = p.dPos().Sub(p.obj.Pos())
		//pp(p.dirVec)
	} else {
		//p.dirVec = Vec3Zero
	}

	// Dir update
	p.dir.update(dt)
	obj.SetRotZ(p.dir.angle()) // Always set orientation

	//newPos := obj.Pos().Add(Vec3{1 * dt, 1 * dt, 0})

	/*
		if p.dPos_ == nil {
			return
		} else {
			// Begin move
			//p.stepTime = 0
		}
	*/

	// Check
	if p.stepTime == 0 {
		p.stepTime = game.timer.time
	}

	doneMoving := false

	//sPos := obj.pos2()
	//dPos := p.dPos2()

	//nextPos := sPos

	// Linear
	{

		/*
			if p.speed == 0.0 {
				return
			}
		*/

		var _ = `
			// From:
			//  http://stackoverflow.com/questions/33092912/moving-a-panel-on-its-x-a-certain-amount-in-unity-5-2
			// See also:
			//  https://www.reddit.com/r/Unity3D/comments/41x6zt/vector3movetowards_without_slow

						// Total time of travel A -> B [constant]
			//tt := (cell_size / p.speed) * 10
			tt := 100.0
			//tt := (float64(cell_size) / 40.0) * 10.0
			//sPos := Pos{u.x * cell_size, u.y * cell_size}
			//sPos := obj.Pos2()
			//dPos := p.dPos

			//fmt.Println(sPos, dPos, p.speed, tt)

			//distCovered := (game.timer.time - p.stepTime) * dt
			distCovered := (game.timer.time - p.stepTime) * p.speed
			step := distCovered / tt
			nextPos := Lerp2(sPos, dPos, step)
			//fmt.Println("step", fmt.Sprintf("%f", step))
			//fmt.Println(fmt.Sprintf("%f %f", game.timer.time, s.stepTime))

			if roundPrec(step, 2) < 1.0 {
				obj.setPos2(nextPos)
			} else {
				obj.setPos2(p.dPos2())
				doneMoving = true
			}
			`
		/*
			a := p.speed * dt
			if (a > 1.0) {
				a = 1.0
			}
			sPos := p.sPos
			dPos := *p.dPos_
			pos := sPos.Add(dPos.Sub(sPos)).MulScalar(a)
			obj.SetPos(pos)
		*/

		/*
			sPos := p.sPos
			dPos := *p.dPos_
			diffVec := dPos.Sub(sPos)
			diffVec = diffVec.Norm()
			diffVec = diffVec.MulScalar(p.speed)
			obj.SetPos(diffVec)
		*/
		// From:
		// https://gamedev.stackexchange.com/questions/23447/moving-from-ax-y-to-bx1-y1-with-constant-speed
		/*
			sPos := p.sPos
			dPos := *p.dPos_
			if p.moving {
				pos := obj.Pos()
				pos = pos.Add(p.direction.MulScalar(p.speed * dt))
				// distance
				if pos.Sub(sPos).Len() >= p.distance {
					pos = dPos
					p.moving = false
				}
				obj.SetPos(pos)
			}
		*/
		//sPos := p.sPos
		if p.moving {
			dPos := *p.dPos_
			__p("dPos:", dPos)
			__p("fw:", game.field.w)
			__p("fwPx:", game.field.w*cell_size)

			if p.moveOpposite {
				var _ = `
				x := Vec3{float64(game.field.w * cell_size), float64(game.field.h * cell_size), 0}
				dPos.SetX(dPos.X() - x.X())
				//dPos.SetY(0)
				//dPos = dPos.Negate()
				//dPos = dPos.Add(x)
				`

				dPos = *p.dPosRelWrap
				__p("ZZZ sPos:", p.sPos)
				__p("ZZZ", dPos, *p.dPos_)
				//pp(2)
			}

			//pos := obj.Pos()
			pos := p.pos
			//pos = moveTowards(pos, dPos, p.speed*dt)
			pos = pos.MoveTowards(dPos, p.speed*dt)
			__p("ZZZ moving", pos, dPos, "dt:", dt, "p.speed:", p.speed)
			// distance

			posWrap := pos
			wrapX := wrapCoordXFloatPx(posWrap.X())
			wrapY := wrapCoordYFloatPx(posWrap.Y())
			__p("ZZZ wrapX:", wrapX)
			__p("ZZZ wrapY:", wrapY)

			posWrap.SetX(wrapX)
			posWrap.SetY(wrapY)

			__p("ZZZ pos:", pos)
			__p("ZZZ posWrap:", posWrap)

			if pos == dPos {
				p.moving = false
				p.dPos_ = nil
			}

			p.pos = pos
			//obj.SetPos(p.pos)
			obj.SetPos(posWrap)

		}
	}

	_ = doneMoving
}
