package main_module

//var _ = `
import (
	"io/ioutil"
	"log"

	"github.com/veandco/go-sdl2/mix"
	"github.com/veandco/go-sdl2/sdl"
)

var (
	music   *mix.Music
	vol_sfx = 85 // max 128
	vol_max = 128
)

func initAudio() bool {
	if err := sdl.Init(sdl.INIT_AUDIO); err != nil {
		log.Println(err)
		return false
	}

	//if err := mix.Init(mix.INIT_MP3); err != nil {
	/*
		// XXX doesn't work on windows?
		if err := mix.Init(mix.INIT_OGG); err != nil {
			log.Println(err)
			return false
		}
	*/
	//if err := mix.OpenAudio(22050, mix.DEFAULT_FORMAT, 2, 4096); err != nil {
	if err := mix.OpenAudio(44100, mix.DEFAULT_FORMAT, 2, 4096); err != nil {
		log.Println(err)
		return false
	}

	return true
}

func deinitAudio() {
	mix.CloseAudio()
	sdl.Quit()
	mix.Quit()
}

func playMusic(path string) {
	if conf.disableMusic || conf.disableSound {
		return
	}
	var err error
	if music, err = mix.LoadMUS(path); err != nil {
		log.Println(err)
	} else if err = music.Play(-1); err != nil {
		log.Println(err)
	} else {
		//sdl.Delay(5000)
		//music.Free()
	}
}

func stopMusic() {
	if conf.disableMusic || conf.disableSound {
		return
	}
	music.Free()
}

func playSound(path string, vol int) {
	playSoundChannel(path, -1, vol)
}

func playSoundChannel(path string, channel int, vol int) {
	if conf.disableSound {
		return
	}

	// Load entire WAV data from file
	data, err := ioutil.ReadFile(path)
	if err != nil {
		log.Println(err)
	}

	// Load WAV from data (memory)
	chunk, err := mix.QuickLoadWAV(data)
	if err != nil {
		log.Println(err)
	}
	//defer chunk.Free()

	// Play 4 times
	//chunk.Play(1, 0)
	//chunk.Play(-1, 0)
	if vol == -1 {
		chunk.Volume(mix.MAX_VOLUME)
	} else {
		chunk.Volume(vol)
	}
	chunk.Play(channel, 0)

	/*
		// Wait until it finishes playing
		for mix.Playing(-1) == 1 {
			sdl.Delay(16)
	*/
}

//`
