package main_module

//import "runtime/debug"

// Autotile field
func (f *Fow) autotile(x_, y_, w, h int) {
	_log.Dbg("%F", x_, y_, w, h)
	//pp(2)
	//p("backtrace:")
	//debug.PrintStack()

	//closedTileZLevel := ZLevel_Ground0
	//openTileZLevel := ZLevel_Sea0
	//closedTileZLevel := 1
	//openTileZLevel := closedTileZLevel - 1
	closedTileZLevel := 1
	openTileZLevel := 0

	for y := y_; y < y_+h; y++ {
		for x := x_; x < x_+w; x++ {

			// Skip non-closed tiles
			maskVal := f.getMaskValInt(x, y)
			if maskVal != closedTileZLevel {
				//if maskVal != openTileZLevel {
				continue
			}

			cells := game.field.getCellNeighbours(x, y, true)
			var data [9]int
			for i, x := range cells {
				//data[i] = int(x.z)
				cx, cy := x.cx, x.cy
				maskVal = f.getMaskValInt(cx, cy)
				data[i] = maskVal
				//data[i] = 1 - maskVal
			}
			//pp(data)
			tileId := autotileSegment(data, openTileZLevel, closedTileZLevel)
			if tileId != -1 {
				/*
				//f.cells[x][y].tileId = tileId
				cell := game.field.getCell(x, y)
				//cell.tileId = tileId
				cell.fowTileId = tileId
				*/
				
				//f.tileIds[x][y] = tileId
				f.setTileIdVal(x, y, tileId)
			}
		}
	}
}
