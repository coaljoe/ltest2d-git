package main_module

import (
	"bytes"
	"encoding/gob"
	//"encoding/json"
	"io/ioutil"
	//"lib/sr"
	"os"
	"reflect"
)

func saveGame(path string) {
	_log.Inf("Saving game to:", path)

	// Save game state

	// Gob test
	buf := &bytes.Buffer{}
	// Writing
	enc := gob.NewEncoder(buf)
	err := enc.Encode(game.timersys)
	if err != nil {
		panic(err)
	}
	err = enc.Encode(game.timersys)
	if err != nil {
		panic(err)
	}

	f, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0666)
	if err != nil {
		panic(err)
	}
	//pp(string(js))
	_, err = f.Write(buf.Bytes())
	if err != nil {
		panic(err)
	}

	if err := f.Sync(); err != nil {
		panic(err)
	}
	f.Close()

	_log.Inf("Saving complete")
	pp(2)
}

func loadGame(path string, verify bool) {
	_log.Inf("Loading game from:", path)

	if ok, err := exists(path); !ok {
		if err != nil {
			panic(err)
		}
		pp("file not found: ", path)
	}

	// Load game state

	dat, err := ioutil.ReadFile(path)
	check(err)

	// Reading
	//buf = bytes.NewBuffer(buf.Bytes())
	buf := bytes.NewBuffer(dat)
	dec := gob.NewDecoder(buf)

	// TimerSys
	_log.Inf("load timersys")
	//ts2 := newTimerSys() // XXX Returns singleton
	ts2 := &TimerSys{}
	dump(game.timersys)
	dump(ts2)
	if true { // Disables loading
		err := dec.Decode(ts2)
		if err != nil {
			panic(err)
		}
	}

	if verify {
		if !reflect.DeepEqual(game.timersys, ts2) {
			pp("not equal")
		}
	}

	// Replace
	game.timersys = ts2

	if true {

		// TimerSys
		_log.Inf("load timersys")
		//ts2 := newTimerSys() // XXX Returns singleton
		ts2 = &TimerSys{}
		dump(game.timersys)
		dump(ts2)
		if true { // Disables loading
			err := dec.Decode(ts2)
			if err != nil {
				panic(err)
			}
		}

		if verify {
			if !reflect.DeepEqual(game.timersys, ts2) {
				pp("TimerSys not equal")
			}
		}

		// Replace
		game.timersys = ts2

	}

	// ObjSys

	// PositionSys

	_log.Inf("Load game complete")
}
