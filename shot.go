package main_module

import (
	. "kristallos.ga/rx/math"
)

// entity
type Shot struct {
	*Obj
	position  *Position
	ptype     string // Shell, burst, rocket, bomb, custom?
	sPos      Vec3
	dPos      Vec3
	moveSpeed float64
	//pathCurveness float64
	//ammotype      *AmmoType
	//emitter       *ecs.Entity // Link to emitter entity
	//target        *ecs.Entity // Link to target
	emitter CombatHostI
	target  CombatHostI
	//moveStartTime  float64
	// props
	name string
}

func newShot(shotName string, sPos, dPos Vec3, moveSpeed float64) *Shot {
	_log.Dbg("%F", shotName, sPos, dPos, moveSpeed)

	s := &Shot{
		Obj: newObj("shot"),
		//ptype:         ammotype.projectileType,
		sPos:      sPos,
		dPos:      dPos,
		moveSpeed: moveSpeed,
		//pathCurveness: pathCurveness,
		//ammotype:      ammotype,
		//emitter:       emitter,
		//target:        target,
		name: shotName,
	}

	s.position = newPosition(s.Obj)
	//p.view = newShotView(p)
	//p.SetPos(sPos)

	return s
}

func (s *Shot) getComponents() []ComponentI {
	return []ComponentI{s.position}
}

func (s *Shot) start() {
	//p.position.dir.setAngle(-90)
	s.position.dir.setAngle(90)
	s.SetPos(s.sPos)
	// Go
	s.position.setDPos(s.dPos)
	s.position.speed = s.moveSpeed

	// ?
	//startEntity(s)
}

func (s *Shot) explode() {
	DestroyEntity(s)
}

func (s *Shot) destroy() {
	s.Obj.destroy()
	s.position.destroy()
}

func (s *Shot) update(dt float64) {

	/*
		// Finised travel
		if !p.isMoving() {
			//p.delete() // XXX call explicit?
			//p.View.destroy()
			pub(ev_projectile_hit,
				EvShotHit{
					pos: p.Pos(), ammotype: p.ammotype, emitter: p.emitter,
					target: p.target,
				})
			println("deleting projectile, eid=", GetEntity(p).Id())
			//deleteEntity(GetEntity(p))
			deleteShot(GetEntity(p))
			//pp(2)
			return
		}
	*/

	/*
		if b.moveStartTime == 0 {
			b.moveStartTime = _now()
		}

			// Total time of travel A -> B [constant]
			tt := distancePos2(Vec2{b.sPos.X(), b.sPos.Y()},
				Vec2{b.dPos.X(), b.dPos.Y()}) / b.moveSpeed
			t := (_now() - b.moveStartTime) / tt
			nextPos := Lerp3(b.sPos, b.dPos, t)

			if roundPrec(t, 2) < 1.0 {
				b.SetPos(nextPos)
			} else {
				b.delete()
				b.View.destroy()
				pub(ev_bullet_hit, b)
				return
			}
	*/

	/*
		// Auto destroy projectiles
		if p.Pos().Y() >= gameWindowH || p.Pos().Y() <= -1.0 {
			//c.destroy()
			//destroyEntity(p)
			p.dead = true
		}
	*/

	s.position.update(dt)
}
