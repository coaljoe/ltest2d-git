#### Weapon ####

o = optional
is_t = is_type
default = devault value

| name              | type            | o | is_t | d    | info
|-------------------|-----------------|---|------|------|---
| type              | string          |   |      |      | ?
| name              | string          |   |      |      | ?
| caliber           | float           |   |      |      | caliber
| fire_rate         | int?            | ? |      | 60 ? | shots/min
| max_rounds        | int?            | ? |      |      | ?
| maintenance_cost  | int?            | ? |      |      | ?
| ammo              | int?            | ? |      |      | ?
| ammo_charge       | int?            | ? |      |      | ?
| ammotype          | string          | ? |      |      | ?
| ammo_type         | string          | ? |      |      | ?
| _ammo_type        | string (type)   | ? | Y    |      | ?
| _ammo_types       | [string (type)] | ? | Y ?  |      | accept ammo types / kinds ?
| ?_ammo_kinds      | [?]             | ? |      |      |
| attack_delay      | float           | ? |      | 0    | ?
| realm             | string (mask?)  | ? |      | ?    | RealmType / Mask (?)

not used (?):
  maintenance_cost, max_rounds

### Examples ###

?
