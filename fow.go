// Fog of war.
package main_module

import (
	"github.com/bradfitz/iter"
)

// Fog of war.
type Fow struct {
	w, h int
	// Cell mask, 0: not closed, 1: closed
	mask_ MaskTiles

	dirtyTilePoss []CPos
	dirty         bool

	// Frame view of the mask
	fh, fw int
	fx, fy int
	//_fmask MaskTiles
	
	// Cell fow tile ids (cell.fowTileId)
	tileIds [][]int
}
func (f *Fow) Dirty() bool { return f.dirty }
func (f *Fow) DirtyTilePoss() []CPos { return f.dirtyTilePoss }
func (f *Fow) TileIds() [][]int  { return f.tileIds }

func newFow(w, h int) *Fow {
	f := &Fow{w: w, h: h}

	/*
		f.mask = make(MaskTiles, w)
		for i := range f.mask {
			f.mask[i] = make([]bool, h)
		}
	*/
	f.mask_ = newMaskTiles(w, h)

	// Initialize with zeros
	//f.fill(false)
	//f.fill(true)
	f.clear()

	// Create the frame
	//f._fmask = newMaskTiles(w, h)
	//f._fmask.clear()
	f.fw = w
	f.fh = h

	return f
}

func (f *Fow) setTileIdVal(x, y int, v int) {
	wrapX, wrapY := wrapCoordXY(x, y)

	f.tileIds[wrapX][wrapY] = v
}

func (f *Fow) getTileIdVal(x, y int) int {
	wrapX, wrapY := wrapCoordXY(x, y)

	return f.tileIds[wrapX][wrapY]
}

func (f *Fow) setMaskVal(x, y int, v bool) {
	wrapX, wrapY := wrapCoordXY(x, y)
	vPrev := f.mask_[wrapX][wrapY]

	f.mask_[wrapX][wrapY] = v

	if vPrev != v {
		f.dirty = true
		f.dirtyTilePoss = append(f.dirtyTilePoss, CPos{wrapX, wrapY})
	}
}

func (f *Fow) getMaskVal(x, y int) bool {
	wrapX, wrapY := wrapCoordXY(x, y)
	return f.mask_[wrapX][wrapY]
}

func (f *Fow) getMaskValInt(x, y int) int {
	maskVal := f.getMaskVal(x, y)
	if maskVal {
		return 1
	} else {
		return 0
	}
}

func (f *Fow) MarkAsClean() {

	// Run autotile
	if true {
		for _, pos := range f.dirtyTilePoss {
			//f.autotile(pos.x, pos.y, 1, 1) // XXX slow?
			f.autotile(pos.x-1, pos.y-1, 3, 3) // XXX slow?
		}
	}

	f.dirty = false
	f.dirtyTilePoss = f.dirtyTilePoss[:0]

	// XXX Autotile entire map
	//f.autotile(0, 0, f.w, f.h)
}

func (f *Fow) getFmask(fx, fy, fw, fh int) MaskTiles {
	p("getFmask:", fx, fy, fw, fh)
	p("f.w:", f.w, "f.h:", f.h)
	if fx < 0 || fy < 0 {
		pp("negative coords not supported;", fx, fy)
	}
	if fx > f.w || fy > f.h {
		pp("the coords is out of range;", fx, fy)
	}
	//ret := newMaskTiles(fw, fh)
	rw, rh := fw, fh
	if fx+fw > f.w {
		rw = fw - fx
	}
	if fy+fh > f.h {
		rh = (fh + fy) - f.h
	}
	p("rw:", rw, "rh:", rh)
	ret := newMaskTiles(rw, rh)
	//fmask := f.mask.copy()
	// Extracting frame region from the mask
	//p(len(ret), ret)
	i, j := 0, 0
	//for y := fy; y < rh; y++ {
	//	for x := fx; x < rw; x++ {
	for y := 0; y < rh; y++ {
		for x := 0; x < rw; x++ {
			//p("->", x, y)
			if x < 0 || y < 0 {
				// Skip
				continue
			}
			//p(i, j, x, y, fx, fy, fw, fh)
			//ret[i][j] = f.mask[x][y]
			ret[i][j] = f.getMaskVal(fx+x, fy+y)

			i++
		}
		i = 0
		j++
	}
	//p(len(ret), ret)
	return ret
	//return fmask
}

func (f *Fow) clear() {
	// Init tile ids
	f.tileIds = make([][]int, game.field.w)
	for i := range f.tileIds {
		f.tileIds[i] = make([]int, game.field.h)
	}
	
	f.fill(true)
}

func (f *Fow) IsCellVisible(cx, cy int) bool {
	return f.getMaskVal(cx, cy) == false
}

func (f *Fow) fill(v bool) {
	for y := 0; y < f.h; y++ {
		for x := 0; x < f.w; x++ {
			f.setMaskVal(x, y, v)
		}
	}
}

func (f *Fow) fillPoint(x, y int, v bool) {
	/*
		//if x < 0 || y < 0 {
		if x < 0 || y < 0 || x > f.w-1 || y > f.h-1 {
			if Debug {
				_log.Err("pixel is out of range;", x, y)
			}
			return
		}
	*/
	f.setMaskVal(x, y, v)
}

func (f *Fow) fillCircle(x0, y0 int, r int, v bool) {
	// Midpoint
	_ = `
	x := r
	y := 0
	err := 0

	for x >= y {
		f.fillPoint(x0+x, y0+y, v)
		f.fillPoint(x0+y, y0+x, v)
		f.fillPoint(x0-y, y0+x, v)
		f.fillPoint(x0-x, y0+y, v)
		f.fillPoint(x0-x, y0-y, v)
		f.fillPoint(x0-y, y0-x, v)
		f.fillPoint(x0+y, y0-x, v)
		f.fillPoint(x0+x, y0-y, v)

		if err <= 0 {
			y += 1
			err += 2*y + 1
		}
		if err > 0 {
			x -= 1
			err -= 2*x + 1
		}
	}
	`

	radius := r
	for y := -radius; y <= radius; y++ {
		for x := -radius; x <= radius; x++ {
			if x*x+y*y <= radius*radius {
				f.fillPoint(x0+x, y0+y, v)
			}
		}
	}
}

// Get rectangular slice of tiles.
func (f *Fow) getRectSlice(cx, cy, w, h int) MaskTiles {
	// TODO: add more boundaries tests
	ret := newMaskTiles(w, h)
	//p(len(ret), ret)
	i, j := 0, 0
	for y := cy; y < h+cy; y++ {
		for x := cx; x < w+cx; x++ {
			//p("->", x, y)
			if x < 0 || y < 0 {
				// Skip
				continue
			}
			//p(i, j, x, y, cx, cy, w, h)
			ret[i][j] = f.getMaskVal(x, y)

			i++
		}
		i = 0
		j++
	}
	//p(len(ret), ret)
	return ret
}

func (f *Fow) itH() []struct{} {
	return iter.N(f.h)
}

func (f *Fow) itW() []struct{} {
	return iter.N(f.w)
}

/*
func (f *Fow) show() string {
	s := ""
	for y := range f.itH() {
		for x := range f.itW() {
			v := "0"
			if f.mask[x][y] != false {
				v = "1"
			}
			s += v
		}
		s += "\n"
	}
	return s
}
*/

//////////////
// MaskTiles

type MaskTiles [][]bool

func newMaskTiles(w, h int) MaskTiles {
	mask := make(MaskTiles, w)
	for i := range mask {
		mask[i] = make([]bool, h)
	}
	return mask
}

func (mt MaskTiles) show() string {
	w := len(mt[0])
	h := len(mt)
	s := ""
	for y := range iter.N(h) {
		for x := range iter.N(w) {
			v := "0"
			if mt[x][y] != false {
				v = "1"
			}
			s += v
		}
		s += "\n"
	}
	return s
}

func (mt MaskTiles) showFlip() string {
	w := len(mt[0])
	h := len(mt)
	s := ""
	for y := range iter.N(h) {
		for x := range iter.N(w) {
			yInv := (h - 1) - y
			p("yInv:", yInv, h, y, w, h)
			v := "0"
			if mt[x][yInv] != false {
				v = "1"
			}
			s += v
		}
		s += "\n"
	}
	return s
}

func (mt MaskTiles) copy() MaskTiles {
	w := len(mt[0])
	h := len(mt)
	// Copy data
	dcopy := make([][]bool, w)
	for i := range dcopy {
		dcopy[i] = make([]bool, h)
	}
	for x := 0; x < w; x++ {
		for y := 0; y < h; y++ {
			dcopy[x][y] = mt[x][y]
		}
	}
	return dcopy
}
