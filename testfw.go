package main_module

import (
	"reflect"
)

type TestFw struct {
	// Test module name to run
	runModuleName string
	// Test modules
	testModules map[string](func())
}

func newTestFw() *TestFw {
	_log.Inf("%F")
	
	tf := &TestFw{
		testModules: make(map[string](func())),
	}
	
	return tf
}

func (tf *TestFw) setupDefaultTestGame() {
	_log.Inf("%F")

	ngt := game.newgametool

	// XXX?
	ngt.initBaseGame()
	
	p1 := NewPlayer(PlayerType_Human, CampId_Reds)
	ngt.AddPlayer(p1)
	
	p2 := NewPlayer(PlayerType_AI, CampId_Arctic)
	ngt.AddPlayer(p2)
}

func (tf *TestFw) runTestGame() {
	_log.Inf("%F")
	
	ngt := game.newgametool
	ngt.StartGame()
}

func (tf *TestFw) runModule(name string) {
	_log.Inf("%F", name)

	//name := "Test1"
	
	reflect.ValueOf(tf).MethodByName(name).Call([]reflect.Value{})
}

func (tf *TestFw) Test1() {
	println("Test1")
}
