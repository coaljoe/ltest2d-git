package main_module

// TODO add "Don't Cross Corners" option / mode
// TODO ? add octile distance score function (for 8 dir paths)

import (
	"fmt"
	//. "math"
	"github.com/emirpasic/gods/sets"
	"github.com/emirpasic/gods/sets/linkedhashset"
)

//// Node

type Node struct {
	x, y   int
	fs     int // score
	//closed bool // ?
	parent *Node
}

func newNode(x, y int) *Node {
	return &Node{x: x, y: y}
}

func (n *Node) equalPos(oth *Node) bool {
	if n.x == oth.x && n.y == oth.y {
		return true
	}
	return false
}

func (n *Node) String() string {
	return fmt.Sprintf("Node<%d, %d, fs=%d>", n.x, n.y, n.fs)
}

//// Path

// XXX not concrete type (?)
// a slice of nodes
type Path []*Node

func makePath() Path {
	p := make(Path, 0)
	return p
}

func (p Path) firstNode() *Node {
	return p[0]
}

func (p Path) lastNode() *Node {
	pLen := len(p)
	return p[pLen-1]
}

// Pop last node
func (p *Path) pop() *Node {
	//x, a := (*p)[0], (*p)[1:]
	x, a := (*p)[len(*p)-1], (*p)[:len(*p)-1]
	*p = a
	return x
}

// Pop first (front) node
func (p *Path) popFront() *Node {
	x, a := (*p)[0], (*p)[1:]
	*p = a
	return x
}

func (p *Path) append(n *Node) {
	*p = append(*p, n)
}

func (p *Path) insert(n *Node) {
	a := append([]*Node{n}, *(p)...)
	*p = a
}

// In-place
func (p Path) reverse() {
	for i := len(p)/2-1; i >= 0; i-- {
		opp := len(p)-1-i
		p[i], p[opp] = p[opp], p[i]
	}
}

func (p *Path) remove(n *Node) {
	i := -1
	for j, n := range *p {
		if n.equalPos(n) {
			i = j
			break	
		}
	}

	if i == -1 {
		pp("error: node not found")
	}

	(*p)[i] = nil // For gc safety
	(*p) = append((*p)[:i], (*p)[i+1:]...)
}

func (p Path) length() int {
	return len(p)
}

/*
func (p Path) pop() *Node {
	pLen := p.length()
	var x *Node
	x, p = p[pLen-1], p[:pLen-1]
	return x
}
*/

type Pathfinder struct {
	runs int
	maxRuns int
}

func newPathfinder() *Pathfinder {
	p := &Pathfinder{
		//maxRuns: 30, // XXX arbitrary
		maxRuns: 100,
	}

	return p
}

func (p *Pathfinder) pathfind(s, g CPos) Path {
	return p.pathfind_ex(s, g, false)
}

// Find path between two points
// Returned path includes both start and end points
func (p *Pathfinder) pathfind_ex(s, g CPos, ignoreChecks bool) Path {
	start := newNode(s.x, s.y)
	goal := newNode(g.x, g.y)
	//cset := make(Path, 0)
	//oset := make(Path, 0)
	//cset := makePath()
	//oset := makePath()
	// XXX change to TreeSet with fscore as sort func?
	// (like in OrderedQueue)
	oset := linkedhashset.New()
	cset := linkedhashset.New()
	//cset := make([]*Node, 0)
	//oset := make([]*Node, 0)
	//*oset = append(*oset, start)
	oset.Add(start)
	
	//path := make(Path, 0)
	path := makePath()
	fscore := _fscore_func
	field := game.field

	// Init
	p.runs = 0

	__p("FROM", start, "TO", goal)
	//pp(2)

	// Checks
	if !ignoreChecks {
		/*
		// XXX
		if !field.isCellOpen(start.x, start.y) {
			c := field.getCell(start.x, start.y)
			//dump(c)
			pf("%#v", c)
			pp("pathfind error: start position is not open:", start)
		}
		*/
	
		if !field.isCellOpen(goal.x, goal.y) {
			c := field.GetCell(goal.x, goal.y)
			pf("%#v", c)
			pp("pathfind error: goal position is not open:", goal)
		}
	}

	//var node *Node
	
	for oset.Size() > 0 {
		p.runs++

		if p.runs > p.maxRuns {
			pp("too much runs:", p.runs)
		}

		// Check neightbours
	
		node := p.getLowestFscoreNode(oset)

		// Return path
		if node.equalPos(goal) {
			println("GOAL")
			path = p.tracebackPath(node)
			//path.pop()
			// XXX add start node, a workaround. fixme.
			path.append(start)
			path.reverse()
			__p("PATH:", path)
			return path
		}

		oset.Remove(node)
		cset.Add(node)

		neighbourPos := p.getNeighbourPos(node.x, node.y)

		__p("neighbourPos:", neighbourPos)
		
		for _, pos := range neighbourPos {
			nbr := newNode(pos.x, pos.y)

			println("nbr:", "check", nbr.x, nbr.y)

			cont := false

			// nbr in cset
			for _, ni := range cset.Values() {
				n := ni.(*Node)
				if n.equalPos(nbr) {
					println("in cset, continue")
					cont = true
					break
				}
			}

			// XXX
			if cont {
				continue
			}

			///*
			if !ignoreChecks {
				// Check empty
				if !field.isCellOpen(nbr.x, nbr.y) {
					println("not open, continue")
					//cset.append(y)
					continue
				}

				c := field.GetCell(nbr.x, nbr.y)
				if !c.Walkable() {
					println("not walkable, continue")
					continue
				}
			}
			//*/

			nbr.parent = node
			nbr.fs += fscore(nbr, goal)
			// nbr not in oset
			nbrInOset := false
			for _, ni := range oset.Values() {
				n := ni.(*Node)
				if n.equalPos(nbr) {
					nbrInOset = true
				}
			}
			if !nbrInOset {
				oset.Add(nbr)
			}
		}
	}

	println("No path found")
	
	return path
}

// XXX TODO add naval version
func (p *Pathfinder) canGoTo(cx, cy int) bool {

	if !game.field.isCellOpen(cx, cy) {
		return false
	}

	c := game.field.GetCell(cx, cy)
	if !c.Walkable() {
		return false
	}
	
	return true
}

func (p *Pathfinder) getNeighbourPos(x, y int) []CPos {
	//r := make([]CPos, 7)
	r := make([]CPos, 0)

	next := func(x, y int) {
		// Filter negative
		/*
			if x < 0 || y < 0 {
				return
			}
		*/
		wrapX := wrapCoordX(x)
		wrapY := wrapCoordY(y)
		//c := CPos{x, y}
		c := CPos{wrapX, wrapY}
		r = append(r, c)
	}

	// Diagonal (8 directions)
	next(x-1, y-1) // Top Left
	next(x, y-1)   // Top Center
	next(x+1, y-1) // Top Right

	next(x-1, y) // Center Left
	next(x+1, y) // Center Right

	next(x-1, y+1) // Bottom Left
	next(x, y+1)   // Bottom Center
	next(x+1, y+1) // Bottom Right

	/*
	// Non-diagonal (4 directions)
	next(x, y-1)   // Top Center
	
	next(x-1, y) // Center Left
	next(x+1, y) // Center Right
	
	next(x, y+1)   // Bottom Center
	*/

	return r
}

func (p *Pathfinder) tracebackPath(n *Node) Path {
	//ret := make(Path, 0)
	path := makePath()
	node := n
	for node.parent != nil {
		path.append(node)
		node = node.parent
	}
	
	return path
}

func (p *Pathfinder) getLowestFscoreNode(nodeSet sets.Set) *Node {
	values := nodeSet.Values()
	ret := values[0].(*Node)
	for _, ni := range values {
		n := ni.(*Node)
		if n.fs < ret.fs {
			ret = n
		}
	}
	return ret
}

func _fscore_func(a, b *Node) int {
	abs := func(x int) int {
		if x < 0 {
			return -x
		}
		return x
	}
	_ = abs

	//return abs(a.x-b.x) + abs(a.y-b.y) // Manhattan
	//return wrapCellDistX(a.x, b.x) + wrapCellDistY(a.y, b.y)

	md := abs(a.x-b.x) + abs(a.y-b.y) // Manhattan
	wcd := wrapCellDistX(a.x, b.x) + wrapCellDistY(a.y, b.y)
	p("dist:", "md:", md, "wcd:", wcd, "a:", a, "b:", b)
	//return md
	return wcd
}
