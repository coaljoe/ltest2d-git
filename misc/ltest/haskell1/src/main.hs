module Main where
import Unit

main = do
    let u1 = makeUnit DumpTruck
        u2 = makeUnit LightTank
    print u1
    print u2
    attack u1
    attack u2
    print "test"