open Gg

module Transform = struct
  type transform = {
    mutable pos : V3.t;
    mutable rot : V3.t;
    mutable ori : Quat.t;
    mutable scale : V3.t;
  }

  let create = {
    pos = V3.v 0.1 0.1 0.1;
    rot = V3.v 0. 0. 0.;
    ori = Quat.v 1. 1. 1. 1.;
    scale = V3.v 1. 1. 1.;
  }

  let transform px py pz rx ry rz s =
    s.pos <- V3.v px py pz;
    s.rot <- V3.v rx ry rz
    (*()*)
    (*
    if sx || sy || sz then
      scale <- V3.v sx sy sz;
    *)

  let x s = V3.x s.pos
  let y s = V3.y s.pos
  let z s = V3.z s.pos
  let rx s = V3.x s.rot
  let ry s = V3.y s.rot
  let rz s = V3.z s.rot
  let pos s = s.pos
  let set_pos x s = s.pos <- x
end

module type Node = sig  
  type t
  val set_name : t -> unit
end

module Node = struct
  (*include Transform*)
  type t = {
      mutable name : string;
      mutable visible : bool;
  }

  let create = {name="undefined"; visible = true}
  let set_name x s = s.name <- x 
  let visible s = s.visible
end
