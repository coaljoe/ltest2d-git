open Printf

type node = {mutable name: string}
(*type camera = {mutable x: int; node: node}*)
type camera = {mutable x: int}

module type NODE = sig
  type t
  val setn : t -> string -> unit
  val print_node : t -> unit
  val create : t
end

module type X = sig
  type t
  (*val setx : t -> int -> unit*)
  val print : t -> unit
  val create : t
end

module Node : NODE = struct
  type t = node
  let create = {name = "NOTSET"}
  let setn s x = s.name <- x
  let print_node s = printf "N print (name=%s) \n" s.name
end

module F_X : X = struct
  type t = Node of node | Camera of camera
  (*type t = Int of int | String of string*)
(*
  type node_t = node
  type camera_t = camera
*)

  let create = 
    {x = 1}
  (*let setx s x = s.x <- x*)
  let print s = printf "X print (x=%d) \n" s.x
end

(*module X = F_X(Node)*)
module X = F_X
(*module X(M : N) : X = struct*)

let _ =
  let x = X.create in
  X.print x;
  (*X.print_node x;;*)
  (* N.print_node x ;; *)
  (*N.setn x.node "hello_test_node";*)
  (* x#setn "hello_test_node" *)
  (*N.print_node x.node;*)
  (*N.print x;;*)
  (*X.setx x 10;;*)
  (*X.print x;;*)
  (*X.setn "test";;*)
  (*X.setx 10*)
  ()
  ;;
