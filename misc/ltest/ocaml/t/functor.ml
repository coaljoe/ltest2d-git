open Printf

module type N = sig
  type t = {mutable name: string}
  val setn : t -> string -> unit
  val print_node : t -> unit
end

module N = struct
  type t = {mutable name: string}
  let setn s x = s.name <- x
  let print_node s = printf "N print (name=%s) \n" s.name
end

module Make_X(M : N) = struct
  type t = {mutable x: int}

  let create = {x = 1}
  let setx x s = s.x <- x
  let print s = printf "X print (x=%d) \n" s.x
end

module X = Make_X(N)
(*module X(M : N) : X = struct*)

let _ =
  let x = X.create in
  (*X.print x;;*)
  (*X.print_node x;;*)
  N.print_node x ;;
  (*N.print x;;*)
  (*X.setx x 10;;*)
  (*X.print x;;*)
  (*X.setn "test";;*)
  (*X.setx 10*)
  ()
  ;;
