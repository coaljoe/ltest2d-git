open Printf

type node = {mutable name: string}
type camera = {mutable x: int}

module type NSig = sig
  (* type maybe *)
  val setn : node -> string -> unit
  val print_node : node -> unit
  val create : node
end

module N : NSig = struct
  let create = {name = "NOTSET"}
  let setn s x = s.name <- x
  let print_node s = printf "N print (name=%s) \n" s.name
end

module F_X =
  functor(M: NSig) -> struct
  (* type maybe = node | camera *)
  type node_t = node
  type camera_t = camera

  let create = {x = 1}
  let setx x s = s.x <- x
  let print s = printf "X print (x=%d) \n" s.x
end

module X = F_X(N)
(*module X(M : N) : X = struct*)

let _ =
  let x = X.create in
  (*X.print x;;*)
  (*X.print_node x;;*)
  N.print_node x ;;
  N.print_node x.node;;
  (*N.print x;;*)
  (*X.setx x 10;;*)
  (*X.print x;;*)
  (*X.setn "test";;*)
  (*X.setx 10*)
  ()
  ;;
