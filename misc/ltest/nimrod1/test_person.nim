# class.nim
type
    TPerson = object of TObject
        name: string
        age: int

proc setPerson(p: ref TPerson, name: string, age:int) =
    p.name = name
    p.age = age

proc newPerson(name: string, age:int): ref TPerson =
    new(result)
    result.setPerson(name,age)

method greeting(p: ref TPerson):string = "Hello " & p.name & ", age " & $p.age

type
    TGerman = object of TPerson

proc newGerman(name: string, age:int): ref TGerman =
    new(result)
    result.setPerson(name,age)

method greeting(p: ref TGerman):string = "Hallo " & p.name & ", " & $p.age & " Jahre alt"

var bob = newPerson("Bob",32)
var hans = newGerman("Hans",30)

proc sayit(p: ref TPerson) = echo p.greeting

sayit(bob)
sayit(hans)