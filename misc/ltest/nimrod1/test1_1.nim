var maxId = 0
var maxUnnamedId = 0

type
  Obj = ref object of TObject
    id: int
    name: string

proc initObj(a: Obj, name: string="") =
  echo "::initObj"
  a.id = maxId + 1
  var xname = if name.len > 0: name else: "unnamed#" & $(maxUnnamedId + 1)
  a.name = xname
  maxId += 1
  maxUnnamedId += 1

type
  Unit = ref object of Obj
    typename: string
    mass: float

type
  Building = ref object of Obj
    height: float

proc newBuilding(): Building =
  initObj(result)
  new(result)

type
  Tank = ref object of Unit

proc newTank(): Tank =
  new(result)
  initObj(result)

proc attack(u: Unit, oth: Unit) =
  echo "attack"

proc main() =
  var u1 = newTank()
  var u2 = newTank()
  echo u1.id, " ", u1.name
  echo u2.id, " ", u2.name
  u1.attack(u2)
  attack(u1, u2)

main()