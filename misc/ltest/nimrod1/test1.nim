var maxId = 0
var maxUnnamedId = 0

type
  Obj = ref object of TObject
    id: int
    name: string

proc initObj(a: Obj, name: string="") =
  echo "::initObj"
  a.id = maxId + 1
  let xname = if name.len > 0: name else: "unnamed#" & $(maxUnnamedId + 1)
  a.name = xname
  inc maxId
  inc maxUnnamedId

# trait
type Attackable = generic x
  #reload(x)
  takeDamage(x)

type
  Unit = ref object of Obj
    typename: string
    mass: float

proc takeDamage(u: Unit) =
  echo "::takeDamage"

type
  Building = ref object of Obj
    height: float

proc newBuilding(): Building =
  new(result)
  initObj(result)

type
  Tank = ref object of Unit

proc newTank(): Tank =
  new(result)
  initObj(result)

type
  DumpTruck = ref object of Unit

proc newDumpTruck(): DumpTruck =
  new(result)
  initObj(result)

proc attack(u: Unit, oth: Unit) =
  echo "attack"

proc attack(u: Unit, b: Building) =
  echo "attack building"

proc attack_g1(a: Attackable, b: Attackable) =
  echo "attack_g1 "

proc attack_g2(a: Attackable) =
  echo "attack_g2 "

proc main() =
  let u1 = newTank()
  let u2 = newTank()
  echo u1.id, " ", u1.name
  echo u2.id, " ", u2.name
  u1.attack(u2)
  attack(u1, u2)

  # test building
  let b1 = newBuilding()
  u1.attack(b1)

  # test dumptruck
  let dt1 = newDumpTruck()
  u1.attack(dt1)
  dt1.attack(u1)

  #discard
  # test trait
  attack_g2(u1)
  attack_g2(u2)
  attack_g2(dt1)
  attack_g1(u1, u2)
  attack_g1(u2, u1)
  attack_g1(dt1, dt1)
  u1.attack_g1(u2)
  attack_g1(dt1, u1) # error
  #u1.attack_g1(dt1)
  #dt1.attack_g1(u1)
  #attack_g2(b1)

main()
