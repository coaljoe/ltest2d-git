import obj, unit

type
  Tank* = ref object of Unit

proc newTank*(): Tank =
  new result
  initObj(result)
