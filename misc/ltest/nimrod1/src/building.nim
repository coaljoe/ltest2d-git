import obj

type
  Building* = ref object of Obj
    height: float

proc newBuilding*(): Building =
  new result
  initObj(result)
