use std::cell::{RefCell};
use std::rc::Rc;

struct Entity {
    x: int
}

struct Tile {
    entity: Rc<RefCell<Entity>>
}

impl Entity {
    pub fn new(value: int) -> Rc<RefCell<Entity>> {
        let en = Entity { x: value };
        Rc::new(RefCell::new(en)) // en_boxed
    }
}

impl Tile {
    pub fn try_read_entity(&self){
        let en = self.entity.borrow();
        println!(".en.x = {}", en.x);
    }
}

fn main()
{
    let en_boxed = Entity::new(1);
    en_boxed.borrow_mut().x = 2;
    let tile = Tile { entity: en_boxed.clone() }; // clone is important
    en_boxed.borrow_mut().x = 3; // worcs after clone
    tile.try_read_entity();

    tile.try_read_entity();
}