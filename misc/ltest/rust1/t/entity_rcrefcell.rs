use std::cell::{RefCell,Ref,RefMut};
use std::rc::Rc;

struct Entity;

struct Tile {
    entity: Option<Rc<RefCell<Entity>>>
}

impl Tile {
    pub fn try_read_entity<'a>(&'a self) -> Option<Ref<'a, Entity>> {
        self.entity.as_ref().map(|e| e.borrow())
    }
}

fn main()
{
    let en = Rc::new(RefCell::new(Entity));
    let mut tile = Tile { entity: Some(en) };
    tile.try_read_entity();
}