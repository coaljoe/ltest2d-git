#![feature(struct_inherit, struct_variant)]

use cgmath::Vector2;

use unit::{ExtUnit, UnitLike, ExtUnitProps};
use obj::{Obj};
use mountpoint::{MountPoint};
use views::units::truckview::{TruckView};
use render::renderable::{Renderable};


pub struct Truck : ExtUnit {
  //pub trunk: Option<None>,
  pub view: TruckView,
  pub props: &'static TruckProps,
}

pub struct TruckProps : ExtUnitProps {
  pub unloadTime: f32,
  pub truckSize: f32,
}

static DumpTruckProps: &'static TruckProps = &TruckProps {
  name: "Dump Truck",
  weight: 2000.0,
  maxSpeed: 40.0,
  accSpeed: 10.0,
  /* truck props */
  unloadTime: 14.0,
  truckSize: 100.0,
};

impl Truck {
  pub fn new_dumpTruck(name: &'static str) -> Truck {
    Truck
    {
      speed: 0.0,
      props: DumpTruckProps,
      view: TruckView::new(), // standard class
      obj: Obj::new(name),
    }
  }

  pub fn spawn(&self) {}

}

impl UnitLike for Truck {
  fn get_obj(&self) -> Obj {
    self.obj
  }
}

impl Renderable for Truck {
  fn render(&self) {
    self.view.render(self);
  }
}