#![feature(struct_inherit, struct_variant)]

use std::rc::Rc;
use std::cell::RefCell;
use cgmath::Vector2;

use unit::{ExtUnit, UnitLike, Armed, ExtUnitProps};
use obj::{Obj};
use mountpoint::{MountPoint};
use units::turret::{Turret};
use views::units::tankview::{TankView};
use render::renderable::{Renderable};


/* structure for any tank */
pub struct Tank : ExtUnit {
  pub turret: Turret,
  pub mpTurret: MountPoint,
  pub mpAAGun: Option<MountPoint>,
  pub view: TankView,
  pub props: &'static TankProps,
}

/* type's props for light tank */
pub struct TankProps : ExtUnitProps;

static LightTankProps: &'static TankProps = &TankProps {
  name: "Light Tank",
  weight: 2000.0,
  maxSpeed: 40.0,
  accSpeed: 10.0,
};

impl Tank {
  /*pub fn initTank(&self) {
    //self.initUnit();
    self.rounds = 44;
    self.reloadTime = 4.0
  }*/
  pub fn new_lightTank(name: &'static str) -> Tank {
    let mut o = Obj::new(name);
    let mut u = Tank
    {
      speed: 0.0,
      props: LightTankProps,
      turret: Turret::new_ltsTurret(),
      mpTurret: MountPoint::new(&o, Vector2::new(0.0f32, 0.0f32)),
      mpAAGun: None,
      view: TankView::new(),
      obj: o,
    };
    u
  }
  pub fn spawn(&self) {
  }
}

impl Armed for Tank {
  fn reload(&self) {
    println!("tank.reload");
  }
  fn fire(&self) {
    println!("tank.fire");
  }
}

impl UnitLike for Tank {
  fn get_obj(&self) -> Obj {
    self.obj
  }
}

impl Renderable for Tank {
  fn render(&self) {
    self.view.render(self);
  }
}