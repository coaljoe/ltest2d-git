#![feature(struct_inherit, struct_variant)]

use super::tank::{Tank};
use unit::{Unit, Armed};
use obj::{Obj};

pub struct LightTank : Tank {
  rounds: int,
  reloadTime: f32
}

impl LightTank {
  pub fn new(name: &'static str) -> Tank {
    Tank { rounds: 44, reloadTime: 4.0, speed: 0.0,
           obj: Obj::new(name) }
  }
  pub fn new_lightTank(name: &'static str) -> Tank {
    Tank { rounds: 10, reloadTime: 1.0, speed: 0.0,
           obj: Obj::new(name) }
  }
}