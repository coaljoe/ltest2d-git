#![feature(struct_inherit, struct_variant)]

use obj::{Obj};
use render::renderable::{Renderable};

type time = f32;

pub mod utype {   
  pub enum UType {  
    lightTank,
    heavyTank,
    dumpTruck
  }
}

pub struct UnitWeaponProps {
  pub maxRounds: uint,
  pub reloadTime: uint,
  pub caliber: f32,
  pub snd_fire: &'static str,
  pub snd_reload: &'static str,
}

pub struct UnitWeaponState {
  pub rounds: uint, // rounds left
  pub lastFireTime: time,
  //pub weaponcls: UnitWeapon,
}

/* type information for an unit */
#[deriving(Show)]
pub virtual struct ExtUnitProps {
  pub name: &'static str,
  pub weight: f32,
  pub maxSpeed: f32,
  pub accSpeed: f32,
}

/* unit state */
#[deriving(Show)]
pub virtual struct ExtUnit {
  pub obj: Obj,
  pub speed: f32,
  //pub props: &'static UnitProps,
  //pub utype: utype::UType,
}

impl ExtUnit {
  pub fn initUnit(&self) {
    println!("initUnit()");
  }
}

pub trait UnitLike : Renderable {
  fn get_obj(&self) -> Obj;
  /*fn get_obj_static_trait<T>(u: T) -> Obj {
      u.obj
    }
  */
  //fn startMove(&self);
  //fn stopMove(&self);
}

pub trait Armed {
  fn reload(&self);
  fn fire(&self);
  //fn setTarget(&self);
}

pub trait Swimable {
  fn swim(&self);
}

/*
enum Unit  {
  //LightTank { rounds = 10, reloadTime = 10 }
  Tank, Truck
}
*/


/* factory */
/*
fn makeUnit(utype: utype::UType) -> Unit {
  let mut u;
  match utype {
    utype::dumpTruck =>
      u = Truck { payload: "coal", dumpTime: 2.5, speed: 0.0, obj: Obj::new() },

    utype::lightTank =>
      u = Tank { rounds: 44, reloadTime: 4.0, speed: 0.0, obj: Obj::new() },

    Nil => fail!("can't make unit")
  }
  u
}
*/
/*
fn makeUnitG<T: Unit>(t: &T) -> T {
  let mut u;
    match t {
    Truck =>
      u = T::new("A Truck"),

    Tank =>
      u = T::new("A Tank"),

    Nil => fail!("can't make unit")
  }
  u
}
*/