use std::rc::Rc;
use std::cell::RefCell;
use units::tank::{Tank};

/*#[deriving(Show)]*/
pub struct TankView {
  //pub m: Rc<RefCell<Tank>>,
  pub model: &'static str,
}

impl TankView {
  pub fn new() -> TankView {
    TankView { model: "test_model" }
  }
  pub fn render(&self, m: &Tank) {
  	println!("tankview.render()");
    //println!("tankview.render(), self: {}", self);
  }
}