package main_module

type SpriteI interface {
	draw()
}

type SpritePositionType int

const (
	SpritePositionType_WorldSpace SpritePositionType = iota
	SpritePositionType_ScreenSpace
)
