package main_module

import (
	"fmt"
	//"os"
	"strings"

	//"github.com/veandco/go-sdl2/img"
	//"github.com/veandco/go-sdl2/sdl"
)

type TurretView struct {
	*View
	// Offset position
	posX, posY int
	anim       *AnimatedSprite
	m          *Turret
}

func newTurretView(m *Turret) *TurretView {
	//_log.Dbg("%F")

	v := &TurretView{
		View: newView(),
		m:    m,
	}
	return v
}

func (v *TurretView) load() {
	_log.Dbg("%F")

	realmName := strings.ToLower(v.m.unit.realm.name())
	if v.m.unit.isStaticUnit() {
		// XXX fixme?
		realmName = "static"
		//pp(2)
	}

	//imagePath := "test.png"
	//imagePath := "res/buildings/bunker/reds/image.png"
	imagePath := fmt.Sprintf("res/units/%s/%s/%s/turret_%s.png",
		realmName, v.m.unit.name,
		strings.ToLower(v.m.unit.campId.Name()),
		v.m.slotName)

	/*
	image, err := img.Load(imagePath)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to load PNG: %s\n", err)
		pp(3)
	}
	//defer image.Free()
	v.image = image

	texture, err := renderer.CreateTextureFromSurface(image)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create texture: %s\n", err)
		pp(4)
	}
	//defer texture.Destroy()
	v.tex = texture
	*/

	v.anim = newAnimatedSprite()
	v.anim.load(imagePath, 24)
	v.anim.playing = false
	v.anim.originX = v.anim.tileWidth / 2
	v.anim.originY = v.anim.tileHeight / 2

	v.loaded = true

	//pp(4)

	_log.Dbg("done %F")
}

func (v *TurretView) destroy() {
}

func (v *TurretView) spawn() {
	if !v.loaded {
		v.load()
	}
	
	//v.m.rotateTo(179)
	v.m.rotateTo(240)
}

func (v *TurretView) draw() {
	//pp("unitview draw")

	dirStepDeg := 15.0
	//normAngle := normalizeAngle(v.m.unit.position.dir.angle())
	//normAngle := normalizeAngle(45)
	normAngle := normalizeAngle(v.m.rotAngle())
	p("Z", v.m.unit.position.dir.angle(), normAngle, int(normAngle/dirStepDeg))
	v.anim.frame = int(normAngle / dirStepDeg)

	//dst := sdl.Rect{0, 0, 120, 120}
	//var dst sdl.Rect

	//dst.H = 120
	//dst.W = 120

	//dst.W = int32(v.m.dimX * cell_size)
	//dst.H = int32(v.m.dimY * cell_size)

	//nominalY := v.m.areaY * cell_size
	//actualY := int(v.image.H)
	//ycorr := actualY - nominalY

	ycorr := 0

	if ycorr != 0 {
		p(ycorr)
	}

	//dst.W = int32(v.image.W)
	//dst.H = int32(v.image.H)

	//px, py := int(v.m.unit.PosX()), int(v.m.unit.PosY())-ycorr
	px := v.posX + int(v.m.unit.PosX())
	py := (v.posY + int(v.m.unit.PosY())) - ycorr
	/*
	sx, sy := worldToScreenPos(px, py)
	dst.X = int32(sx)
	dst.Y = int32(sy)

	if ycorr != 0 {
		p("2>>>", dst)
	}
	*/

	//renderer.Copy(v.tex, nil, &dst)

	v.anim.posX = px
	v.anim.posY = py
}

func (v *TurretView) update(dt float64) {

}
