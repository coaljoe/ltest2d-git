// Game's keyboard configuration.
package main_module

import (
	"encoding/json"
	"fmt"

	"github.com/veandco/go-sdl2/sdl"
)

type KeymapRec struct {
	Name    string
	Key1    sdl.Keycode // Default key combo, currently supports
	Key2    sdl.Keycode // Only two keys, ex: Ctrl+Q
	AltKey1 sdl.Keycode // Alt. key combo
	AltKey2 sdl.Keycode
	//action      func
	Description     string `json:"-"` // Key description
	DescriptionLong string `json:"-"` // Long key description
	Disabled        bool   // The keymap is disabled
}

type Keymap struct {
	*GameSystem
	Keymap map[string]KeymapRec `json:"keymap"`
}

func newKeymap() *Keymap {
	k := &Keymap{
		Keymap: make(map[string]KeymapRec, 0),
	}
	k.GameSystem = newGameSystem("Keymap", "Keymap subsystem", k)
	return k
}

func (k *Keymap) init() {

}

func (k *Keymap) reinit() {

}

func (k *Keymap) start() {
	// Add some keymaps

	// Scroll keys
	k.addKey(KeymapRec{Name: "scrollUpKey", Key1: sdl.K_UP,
		Description: "Viewport scroll up"})
	k.addKey(KeymapRec{Name: "scrollDownKey", Key1: sdl.K_DOWN,
		Description: "Viewport scroll down"})
	k.addKey(KeymapRec{Name: "scrollLeftKey", Key1: sdl.K_LEFT,
		Description: "Viewport scroll left"})
	k.addKey(KeymapRec{Name: "scrollRightKey", Key1: sdl.K_RIGHT,
		Description: "Viewport scroll right"})
	k.addKey(KeymapRec{Name: "scrollFastKey",
		//Key1: glfw.KeyLeftShift, AltKey1: glfw.KeyRightShift,
		Key1: sdl.K_LSHIFT, AltKey1: sdl.K_RSHIFT,
		Description: "Viewport scroll fast"})

	// Game keys
	k.addKey(KeymapRec{Name: "rebuildLastBuildingKey", Key1: sdl.K_b,
		Description:     "Rebuild last building",
		DescriptionLong: "Rebuild the last building again."})

	/*
		k.addKey(KeymapRec{Name: "rebuildLastBuildingKey", Key1: glfw.KeyF3,
			Description:     "SaveLoad test",
			DescriptionLong: ""})
	*/
	k.addKey(KeymapRec{Name: "pause", Key1: sdl.K_SPACE,
		Description:     "Game pause",
		DescriptionLong: ""})

	//pp(k.getKey1("scrollUpKey"))
	//pp(k.getAltKey1("scrollUpKey"))
}

func (k *Keymap) addKey(rec KeymapRec) {
	k.Keymap[rec.Name] = rec
}

func (k *Keymap) getKey1(recName string) sdl.Keycode {
	return k.Keymap[recName].Key1
}

func (k *Keymap) getKey2(recName string) sdl.Keycode {
	return k.Keymap[recName].Key2
}

func (k *Keymap) getAltKey1(recName string) sdl.Keycode {
	return k.Keymap[recName].AltKey1
}

func (k *Keymap) getAltKey2(recName string) sdl.Keycode {
	return k.Keymap[recName].AltKey2
}

func (k *Keymap) saveToJson() string {
	//di, err := json.Marshal(k)
	di, err := json.MarshalIndent(k, "", "  ")
	if err != nil {
		p("[Keymap] saveToJson failed.")
		panic(err)
	}
	fmt.Println(di)
	fmt.Println(string(di))
	return string(di)
}

func (k *Keymap) loadFromJson(s string) {
	err := json.Unmarshal([]byte(s), &k)
	if err != nil {
		p("[Keymap] loadFromFile failed.")
		panic(err)
	}
}

func (k *Keymap) update(dt float64) {

}
