package main_module

import (
	xg "kristallos.ga/xgui"
	"fmt"
	"strings"
)

type Camp struct {
	_id int    // Numerical id
	Id  CampId // Camp identity
	// Resources
	Money *Resource
	Pop   *Resource
	Fuel  *Resource
	Metal *Resource
	// Link to camp's player
	player *Player
}

func newCamp(id CampId) *Camp {
	return &Camp{
		_id:   _ider.GetNextId("Camp"),
		Id:    id,
		Money: newResource(ResourceType_Money),
		Pop:   newResource(ResourceType_Pop),
		Fuel:  newResource(ResourceType_Fuel),
		Metal: newResource(ResourceType_Metal),
	}
}

func (c *Camp) Name() string {
	return c.Id.Name()
}

func (c *Camp) Namelc() string {
	return c.Id.Namelc()
}

/***** CampId *****/

type CampId int

const (
	// XXX add CampId_Unknown / CampId_NotSet (?)
	CampId_None    CampId = iota // 0
	CampId_Reds                  // 1
	CampId_Yeolcha               // 2
	CampId_Arctic                // 3
	CampId_Suur                  // 4
	CampId_Atlanta               // 5
	CampId_Krak                  // 6
	CampId_Xinhai                // 7
	CampId_Rax                   // 8
	CampId_Len     = iota        // XXX: replace with MaxCampId?
	CampId_Min     = 1           // Minimal playable
	CampId_Max     = 8           // Maximum playable
)

func (ct CampId) Name() string {
	switch ct {
	case CampId_None:
		return "None"
	case CampId_Reds:
		return "Reds"
	case CampId_Yeolcha:
		return "Yeolcha"
	case CampId_Arctic:
		return "Arctic"
	case CampId_Suur:
		return "Suur"
	case CampId_Atlanta:
		return "Atlanta"
	case CampId_Krak:
		return "Krak"
	case CampId_Xinhai:
		return "Xinhai"
	case CampId_Rax:
		return "Rax"
	default:
		//panic(fmt.Sprintf("unknown CampId ct=%v", ct))
		panic(fmt.Sprintf("unknown CampId ct=%#v", ct))
	}
}

func (ct CampId) Namelc() string {
	name := ct.Name()
	namelc := strings.ToLower(name)
	return namelc
}

func (ct CampId) Color() xg.Color {
	switch ct {
	case CampId_None:
		return xg.Color{255, 255, 255}
	case CampId_Reds:
		//return xg.ColorRed
		return xg.Color{255, 99, 71}
	case CampId_Yeolcha:
		//return xg.ColorRed
		return xg.Color{40, 40, 60}
	case CampId_Arctic:
		return xg.ColorWhite
	case CampId_Suur:
		//return xg.Color{0, 255, 255}
		return xg.Color{70, 240, 255}
	case CampId_Atlanta:
		return xg.Color{210, 210, 210}
	case CampId_Krak:
		return xg.Color{230, 8, 8}
	case CampId_Xinhai:
		return xg.Color{255, 206, 33}
	case CampId_Rax:
		return xg.Color{216, 111, 6}
	default:
		panic(fmt.Sprintf("unknown CampId ct=%#v", ct))
	}
}

func (ct CampId) Playable() bool {
	return ct != CampId_None
}

/*
func randomCampId() CampId {
	return CampId(rand.Intn(CampId_Len))
}
*/

func (ct *CampId) setFromString(s string) {
	id := ct.fromString(s)
	*ct = id
}

func (ct CampId) fromString(s string) CampId {
	switch s {
	case "None":
		return CampId_None
	case "Reds":
		return CampId_Reds
	case "Yeolcha":
		return CampId_Yeolcha
	case "Arctic":
		return CampId_Arctic
	case "Suur":
		return CampId_Suur
	case "Atlanta":
		return CampId_Atlanta
	case "Krak":
		return CampId_Krak
	case "Xinhai":
		return CampId_Xinhai
	case "Rax":
		return CampId_Rax
	default:
		//panic(fmt.Sprintf("unknown CampId ct=%v", ct))
		panic(fmt.Sprintf("unknown CampId string s=%#v", s))
	}
}

func (ct CampId) String() string { return ct.Name() }
