package gui

import (
	"fmt"

	xg "kristallos.ga/xgui"
	//"github.com/veandco/go-sdl2/gfx"
	//"github.com/veandco/go-sdl2/sdl"
	//. "kristallos.ga/rx/math"

	"kristallos.ga/ltest2d/main_module"
)

type GuiControlButton struct {
	cm *GuiControlMenu
	// Is button presented
	active    bool
	disabled  bool
	action    string
	actionObj interface{}
	//action func(GuiControlMenu)
	// Resource labels values
	moneyValue int
	popValue   int
	fuelValue  int
	metalValue int
	x, y       int
	page       int
	button     *xg.Button
}

func newGuiControlButton(cm *GuiControlMenu, x, y, page int) *GuiControlButton {
	cb := &GuiControlButton{
		cm: cm,
	}
	cb.reset()
	cb.x = x
	cb.y = y
	cb.page = page
	/*
		spacing := 10
		w := 60
		h := 50
	*/
	border := 2
	//border := 0
	//spacing := 10 + border*2
	spacing := 0 + border*2
	w := 60 - border*2
	//h := 50 - border*2
	h := 60 - border*2
	px := 20 + border + ((w + spacing) * x)
	py := 360 + border + ((h + spacing) * y)
	b := xg.NewButton(xg.Rect{px, py, w, h})
	//pp(b)
	b.SetName(fmt.Sprintf("control button %d %d %d", x, y, page))
	//b.Label().SetText("")
	//if vars.dev {
	//if main_module.GetVars().dev {
	if main_module.GetVarsDev() {
		b.Label.SetText(fmt.Sprintf("%d %d %d", x, y, page))
	} else {
		b.Label.SetText("")
	}
	//b.SetIcon("res/gui/images/icons/control_menu/button_icon_dynamite.png")

	customPatch9 := xg.NewPatch9()
	customPatch9.Load("res/gui/elements/control_button.9.png", 3, 3, 3, 3)

	/*
		customPatch9Copy := *customPatch9
		customPressedPatch9 := &customPatch9Copy
		customPressedPatch9.Load("res/gui/elements/control_button_pressed.9.png")
		//pp(customPatch9.Im, customPressedPatch9)
		fmt.Printf("%p, %p\n", customPatch9, customPressedPatch9)
		fmt.Printf("%p, %p\n", customPatch9.Im, customPressedPatch9.Im)
		pp(2)
	*/
	customPressedPatch9 := xg.NewPatch9()
	customPressedPatch9.Load("res/gui/elements/control_button_pressed.9.png", 3, 3, 3, 3)
	customPressedPatch9.Rects = customPatch9.Rects

	b.WidgetStyle().Patch9 = customPatch9
	b.WidgetStyle().PressedPatch9 = customPressedPatch9
	//b.Style.ButtonPressedPatch9 = customPatch9

	b.Style.OuterBorderWidth = border
	//b.Style.OuterBorderColor = xg.ColorBlack
	b.Style.OuterBorderColor = xg.Color{10, 10, 10}
	//b.Style.OuterBorderColor = xg.ColorBlue

	/*
		cm.controlButtons[x][y] = b
		s.sidePanel.AddChild(b)
	*/

	cb.button = b
	cb.setIcon("dynamite")
	cb.hide()

	return cb
}

func (cb *GuiControlButton) reset() {
	cb.active = false
	cb.disabled = false
	cb.moneyValue = -1
	cb.popValue = -1
	cb.fuelValue = -1
	cb.metalValue = -1
	cb.x = 0
	cb.y = 0
	cb.page = 0
	cb.action = ""
	cb.actionObj = nil
}

func (cb *GuiControlButton) setIcon(name string) {
	path := fmt.Sprintf("res/gui/images/icons/control_menu/button_icon_%s.png", name)
	cb.button.SetIcon(path)
}

func (cb *GuiControlButton) unsetIcon() {
	cb.button.UnsetIcon()
}

func (cb *GuiControlButton) hide() {
	cb.button.SetVisible(false)
}

func (cb *GuiControlButton) show() {
	cb.button.SetVisible(true)
}

func (cb *GuiControlButton) setActive(v bool) {
	cb.active = v
	cb.button.SetActive(v)
}

func (cb *GuiControlButton) update(dt float64) {
	if cb.active && cb.button.Hover() {
		setMode := false
		if cb.moneyValue != -1 {
			cb.cm.s.rlMoney.SetText(fmt.Sprintf("%d", cb.moneyValue))
			setMode = true
		}
		if cb.popValue != -1 {
			cb.cm.s.rlPop.SetText(fmt.Sprintf("%d", cb.popValue))
			setMode = true
		}
		if cb.fuelValue != -1 {
			cb.cm.s.rlFuel.SetText(fmt.Sprintf("%d", cb.fuelValue))
			setMode = true
		}
		if cb.metalValue != -1 {
			cb.cm.s.rlMetal.SetText(fmt.Sprintf("%d", cb.metalValue))
			setMode = true
		}

		if setMode {
			cb.cm.s.rlControlMenuMode = true
		}
	}
}

type GuiControlMenu struct {
	s *GuiGameSheet
	// Main buttons block
	//controlButtons [3][3]*xg.Button
	controlButtons [][3][3]*GuiControlButton
	page           int
	maxPage        int
	mode           string
	menuObj        interface{}
	lastPages      map[string]int
}

func newGuiControlMenu(s *GuiGameSheet) *GuiControlMenu {
	cm := &GuiControlMenu{
		s:              s,
		controlButtons: make([][3][3]*GuiControlButton, 0),
		page:           0,
		maxPage:        1,
		lastPages:      make(map[string]int),
	}

	// Control buttons
	{
		for i := 0; i <= cm.maxPage; i++ {
			page := i
			fmt.Println("page:", page)
			var v [3][3]*GuiControlButton
			cm.controlButtons = append(cm.controlButtons, v)
			for y := 0; y < 3; y++ {
				for x := 0; x < 3; x++ {
					cb := newGuiControlButton(cm, x, y, page)
					cm.controlButtons[page][x][y] = cb

					cb.button.SetCallback(cm.onButton, cb)
					fmt.Println("cb:", cb)

					s.sidePanel.AddChild(cb.button)
				}
			}
		}
	}
	//pp(2)

	//cm.resetButtons()
	cm.setPage(0)
	//cm.setPage(1)

	return cm
}

func (cm *GuiControlMenu) onButton(arg interface{}) {
	b := arg.(*GuiControlButton)
	fmt.Println(b)
	fmt.Println("ZZZ", b.action, b.button.Name())

	// XXX
	game := main_module.GetGame()
	//hud := game.GetSystem("hud")
	hud := game.GetSystem("Hud").(*main_module.Hud)

	if b.action == "factory_make_light_tank" {
		f := b.actionObj
		panic(fmt.Sprintf("queueing light_tank to:", f))
	} else if b.action == "factory_make_heavy_tank" {
		f := b.actionObj
		panic(fmt.Sprintf("queueing heavytank to:", f))
	} else if b.action == "bulldozer_make_bunker" {
		//building := makeBuilding("bulker", main_module.GetPlayer().CampId)
		//building := makeBuilding("bunker", CampId_Reds)
		building := main_module.MakeBuildingForPlayer("bunker", main_module.GetPlayer())
		main_module.SpawnEntity(building)
		hud.PlaceBuilding(building)
	} else if b.action == "bulldozer_make_housing" {
		building := main_module.MakeBuildingForPlayer("housing", main_module.GetPlayer())
		main_module.SpawnEntity(building)
		hud.PlaceBuilding(building)
	} else if b.action == "bulldozer_make_factory" {
		building := main_module.MakeBuildingForPlayer("factory", main_module.GetPlayer())
		main_module.SpawnEntity(building)
		hud.PlaceBuilding(building)
	} else if b.action == "bulldozer_make_steel_mill" {
		building := main_module.MakeBuildingForPlayer("steel_mill", main_module.GetPlayer())
		main_module.SpawnEntity(building)
		hud.PlaceBuilding(building)
	} else if b.action == "bulldozer_make_ore_mine" {
		building := main_module.MakeBuildingForPlayer("ore_mine", nil)
		main_module.SpawnEntity(building)
		hud.PlaceBuilding(building)
	} else if b.action == "next_page" {
		page := max(cm.page+1, cm.maxPage)
		cm.setPage(page)
		cm.rememberPageForMode(cm.mode)
	} else if b.action == "prev_page" {
		page := max(cm.page-1, 0)
		cm.setPage(page)
		//pp("page:", page)
		cm.rememberPageForMode(cm.mode)
	} else if b.action == "destroy_building" {
		building := b.actionObj.(*main_module.Building)
		building.Explode()
	} else if b.action == "" {
		fmt.Println("warn: no action, skipping")
	} else {
		panic(fmt.Sprintf("error: unknown action:", b.action))
	}

	//pp(2)
}

func (cm *GuiControlMenu) setPage(n int) {
	cm.page = n
	for page := 0; page <= cm.maxPage; page++ {
		for y := 0; y < 3; y++ {
			for x := 0; x < 3; x++ {
				cb := cm.controlButtons[page][x][y]
				if page != cm.page {
					cb.hide()
				} else {
					cb.show()
				}
			}
		}
	}
}

func (cm *GuiControlMenu) addControlButton() {

}

func (cm *GuiControlMenu) setupButton(x, y, page int, icon string,
	action string, actionObj interface{}) {
	b := cm.controlButtons[page][x][y]
	b.setIcon(icon)
	b.action = action
	b.actionObj = actionObj
	b.setActive(true)
}

func (cm *GuiControlMenu) getFreeButtonIdx() (x, y, page int) {
	freeButtonIdxX := 0
	freeButtonIdxY := 0
	freeButtonIdxPage := 0
	found := false
outer:
	for page := 0; page <= cm.maxPage; page++ {
		for y := 0; y < 3; y++ {
			for x := 0; x < 3; x++ {
				b := cm.controlButtons[page][x][y]
				fmt.Println("page:", page, "y:", y, "x:", x, "b.active:", b.active)
				if !b.active {
					freeButtonIdxX = x
					freeButtonIdxY = y
					freeButtonIdxPage = page
					found = true
					break outer
				}
			}
		}
	}

	if !found {
		panic("can't append button, no free slot?")
	}

	return freeButtonIdxX, freeButtonIdxY, freeButtonIdxPage
}

func (cm *GuiControlMenu) appendButton(icon string, action string,
	actionObj interface{}) *GuiControlButton {

	//pp(freeButtonIdxX, freeButtonIdxY, freeButtonIdxPage)

	fX, fY, fPage := cm.getFreeButtonIdx()

	cm.setupButton(fX, fY, fPage, icon, action, actionObj)

	if fPage == 0 && fX == 2 && fY == 2 {
		// Insert navigation buttons
		cm.setupButton(2, 2, 0, "rarrow", "next_page", nil)
		cm.setupButton(0, 0, 1, "larrow", "prev_page", nil)
		fX, fY, fPage = 1, 0, 1
	}

	b := cm.controlButtons[fPage][fX][fY]

	return b
}

func (cm *GuiControlMenu) SetMode(name string, menuObj interface{}) {
	cm.mode = name
	cm.menuObj = menuObj

	if cm.mode == "default" {
		cm.setDefaultMenuMode()
	} else if cm.mode == "building_menu" {
		b := menuObj.(*main_module.Building)
		// Set basic menu
		if b.CampId().Playable() {
			cm.setBasicBuildingMenuMode()
		} else {
			cm.setDefaultMenuMode()
		}

		if b.Name() == "bunker" {
			println("do something")
		}

		if b.Name() == "factory" {
			var _ = `
			cm.setupButton(0, 0, 0, "n1", "factory_make_lighttank", menuObj)
			cm.setupButton(1, 0, 0, "n2", "factory_make_heavytank", menuObj)
			cm.setupButton(2, 0, 0, "n3", "", nil)
			cm.setupButton(0, 1, 0, "n4", "", nil)
			cm.setupButton(1, 1, 0, "n5", "", nil)
			cm.setupButton(2, 1, 0, "n6", "", nil)
			cm.setupButton(0, 2, 0, "n7", "", nil)
			cm.setupButton(1, 2, 0, "n8", "", nil)
			cm.setupButton(2, 2, 0, "rarrow", "next_page", nil)
			cm.setupButton(0, 0, 1, "larrow", "prev_page", nil)
			cm.setupButton(1, 0, 1, "n11", "", nil)
			`

			pl := main_module.GetPlayer()
			
			//ps := game.prodsys
			//game := main_module.GetGame()
			//ps := game.GetSystem("prodsys").(*main_module.ProdSys)
			ps := main_module.GetProdSys()
			prodName := "light_tank"
			if ps.CanProduce(pl, prodName) {
				iconPath := fmt.Sprintf("units/%s/%s", pl.Camp().Namelc(), prodName)
				xb := cm.appendButton(iconPath, "factory_make_"+prodName, menuObj)
				//dump(xb.button.Icon)
				//pdump(xb)
				pt := ps.GetAvailProdType(main_module.GetPlayer(), prodName)
				xb.moneyValue = pt.CostMoney
				xb.popValue = pt.CostPop
				xb.fuelValue = pt.CostFuel
				xb.metalValue = pt.CostMetal
			} else {
				panic("dont have")
			}
			prodName = "truck"
			if ps.CanProduce(pl, prodName) {
				iconPath := fmt.Sprintf("units/%s/%s", pl.Camp().Namelc(), prodName)
				xb := cm.appendButton(iconPath, "factory_make_"+prodName, menuObj)
				//dump(xb.button.Icon)
				//pdump(xb)
				pt := ps.GetAvailProdType(main_module.GetPlayer(), prodName)
				xb.moneyValue = pt.CostMoney
				xb.popValue = pt.CostPop
				xb.fuelValue = pt.CostFuel
				xb.metalValue = pt.CostMetal
			} else {
				panic("dont have")
			}
			/*
				if ps.canProduce(main_module.GetPlayer(), "gas_truck") {
					cm.appendButton("n2", "factory_make_gas_truck", menuObj)
				}
				if ps.canProduce(main_module.GetPlayer(), "heavy_tank") {
					cm.appendButton("n3", "factory_make_heavy_tank", menuObj)
				}

				cm.appendButton("n11", "factory_make_gas_truck", menuObj)
				cm.appendButton("n11", "factory_make_gas_truck", menuObj)
				cm.appendButton("n11", "factory_make_gas_truck", menuObj)
				cm.appendButton("n11", "factory_make_gas_truck", menuObj)
				cm.appendButton("n11", "factory_make_gas_truck", menuObj)
				cm.appendButton("n11", "factory_make_gas_truck", menuObj)
				cm.appendButton("n11", "factory_make_gas_truck", menuObj)
				cm.appendButton("n11", "factory_make_gas_truck", menuObj)
				cm.appendButton("n11", "factory_make_gas_truck", menuObj)
				cm.appendButton("n11", "factory_make_gas_truck", menuObj)
			*/

			cm.restorePageForMode(cm.mode)
		}

		cm.postSetBasicBuildingMenuMode()

	} else if cm.mode == "unit_menu" {

		// Bulldozer menu
		if true {
			cm.setupButton(0, 0, 0, "n1", "bulldozer_make_bunker", menuObj)
			cm.setupButton(1, 0, 0, "n2", "bulldozer_make_housing", menuObj)
			cm.setupButton(2, 0, 0, "n3", "bulldozer_make_factory", menuObj)
			cm.setupButton(0, 1, 0, "n4", "bulldozer_make_steel_mill", nil)
			cm.setupButton(1, 1, 0, "n5", "bulldozer_make_ore_mine", nil)
			cm.setupButton(2, 1, 0, "n6", "", nil)
			cm.setupButton(0, 2, 0, "n7", "", nil)
			cm.setupButton(1, 2, 0, "n8", "", nil)
			cm.setupButton(2, 2, 0, "rarrow", "next_page", nil)
			cm.setupButton(0, 0, 1, "larrow", "prev_page", nil)
			cm.setupButton(1, 0, 1, "n11", "", nil)
			cm.restorePageForMode(cm.mode)
		}

	} else {
		panic(fmt.Sprintf("unknown mode:", cm.mode))
	}
}

func (cm *GuiControlMenu) setDefaultMenuMode() {
	cm.resetButtons()
	cm.setPage(0)
}
func (cm *GuiControlMenu) ResetMode() {
	cm.SetMode("default", nil)
}

func (cm *GuiControlMenu) setBasicBuildingMenuMode() {
	cm.resetButtons()
}

func (cm *GuiControlMenu) postSetBasicBuildingMenuMode() {
	freeX, freeY, freePage := cm.getFreeButtonIdx()
	b1 := cm.controlButtons[freePage][freeX][freeY]
	b1.setIcon("dynamite")
	b1.action = "destroy_building"
	b1.actionObj = cm.menuObj
	b1.setActive(true)
}

func (cm *GuiControlMenu) rememberPageForMode(name string) {
	cm.lastPages[name] = cm.page
}

func (cm *GuiControlMenu) restorePageForMode(name string) {
	page := cm.lastPages[name]
	cm.setPage(page)
}

func (cm *GuiControlMenu) resetButtons() {
	for page := 0; page <= cm.maxPage; page++ {
		for y := 0; y < 3; y++ {
			for x := 0; x < 3; x++ {
				b := cm.controlButtons[page][x][y]
				//p("YYY", b)
				//b.unsetIcon()
				//b.active = false
				b.reset()
				b.unsetIcon()
				b.button.SetActive(false)
			}
		}
	}
}

func (cm *GuiControlMenu) update(dt float64) {
	/*
		if game.hud.selBuilding != nil {

		}
	*/

	// Reset control menu mode
	cm.s.rlControlMenuMode = false

	for page := 0; page <= cm.maxPage; page++ {
		for y := 0; y < 3; y++ {
			for x := 0; x < 3; x++ {
				b := cm.controlButtons[page][x][y]
				b.update(dt)
			}
		}
	}
}
