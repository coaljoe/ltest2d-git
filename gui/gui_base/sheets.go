package gui_base

type GuiGameSheetI interface {
	ControlMenu() GuiControlMenuI
	UpdateMinimapData()

	Render()
	Update(dt float64)
}

type GuiControlMenuI interface {
	SetMode(name string, menuObj interface{})
	ResetMode()
}

type GuiLsSheetI interface {

	Render()
	Update(dt float64)
}
