package gui

import (
	xg "kristallos.ga/xgui"

	"kristallos.ga/ltest2d/main_module"
)

type GuiLoadingScreenSheet struct {
	*xg.Sheet
	centerlLabel *xg.Label
	debugText    *xg.Label
}

func newLoadingScreenSheet(g *Gui) *GuiLoadingScreenSheet {
	vars := main_module.GetVars()

	s := &GuiLoadingScreenSheet{
		Sheet: xg.NewSheet("NewLoadingScreenSheet"),
	}
	g.xgi.SheetSys.AddSheet(s)
	//g.xgi.SetEnabled(g.enabled)
	//g.xgi.SetEnabled(false)

	//s.centerlLabel = xg.NewLabel(xg.Pos{.5, .5}, "Loading")
	s.centerlLabel = xg.NewLabel(xg.Pos{vars.ResX() / 2, vars.ResY() / 2}, "Loading")
	s.centerlLabel.TextAlign = xg.TextAlignType_Center
	s.Root().AddChild(s.centerlLabel)

	s.debugText = xg.NewLabel(xg.Pos{0, 20}, "debugText")
	s.debugText.SetVisible(false)
	s.debugText.SetText("")
	s.Root().AddChild(s.debugText)

	//p := xg.NewPanel(xg.Rect{0, 0, 1, 1})
	//s.Root().AddChild(p)

	return s
}

func (s *GuiLoadingScreenSheet) Render() {
	// Call base
	s.Sheet.Render()

}

func (s *GuiLoadingScreenSheet) Update(dt float64) {
	// Call base
	s.Sheet.Update(dt)
}
