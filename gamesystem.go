// Модуль игровых подсистем.
//
package main_module

type GameSystemI interface {
	Name() string
	Autostart() bool
	update(dt float64)
	addElem(el interface{})
	removeElem(el interface{})
	hasElem(el interface{}) bool
	getElems() []interface{}
	listElems()
}

// XXX rename to SubSystem?
type GameSystem struct {
	*System
	autostart bool
}

func (s *GameSystem) Name() string    { return s.name }
func (s *GameSystem) Autostart() bool { return s.autostart }

func newGameSystem(name, desc string, _s GameSystemI) *GameSystem {
	s := &GameSystem{
		System:    newSystem(name, desc),
		autostart: true,
	}
	//s.Register()
	game.addSystem(name, _s)
	return s
}

func (s *GameSystem) Register() {
	//game.AddSystem(s)
}

/*
func (s *GameSystem) Update(dt float64) {
}
*/
