package main_module

var debug *Debug

type Debug struct {
	debugText string
	debugTextLines []string
	initialized bool
}
func (d *Debug) DebugText() string { return d.debugText }

func initDebug() {
	if debug != nil {
		return
	}

	d := &Debug{
		debugText: "debugText",
		debugTextLines: make([]string, 0),
		initialized: true,
	}

	debug = d
}

func (d *Debug) showText(text string) {
	d.debugText = text
}

func (d *Debug) showTextLine(text string) {
	d.debugTextLines = append(d.debugTextLines, text)
}

func (d *Debug) showLabelText(labelName string, text string) {

}

func (d *Debug) showTextTimeout(text string, time int) {

}

func (d *Debug) update(dt float64) {
	// Clear debug lines
	d.debugTextLines = d.debugTextLines[:0]
}

// XXX util ?

func GetDebug() *Debug {
	return debug
}
