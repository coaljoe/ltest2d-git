package main_module

import (
	"fmt"
	"strings"
	"github.com/peterbourgon/mergemap"
)



func makeUnit(name string, campId CampId) *Unit {
	_log.Inf("%F name:", name, "campId:", campId, "camp: ", campId.Name())

	camp := campId
	unitsdir := "res/units"
	//realmName := strings.ToLower(realm.name())
	campName := strings.ToLower(camp.Name())
	realms := []string{"ground", "air", "navy", "static"} // Static is not a real realm
	realmName := ""                                       // Store realm name here
	isStatic := false
	found := false
	path := ""
	_ = realmName

	// Try load units
	for _, rname := range realms {
		// For each realm
		//path = unitsdir + "/" + rname + "/" + name + "/" + campName + "/def.jsonnet"
		path = unitsdir + "/" + rname + "/" + name + "/" + campName + "/def.json"
		__p("try path:", path)
		if ok, _ := exists(path); ok {
			found = true
			realmName = rname
			break
		}
	}

	// Finally
	if !found {
		panic(fmt.Sprintf(
			"cannot make unit from data, unknown settings: name=%s camp=%s\npath=%s",
			name, camp.Name(), path))
	} else {
		_log.Inf("Loading unit from:", path)
	}

	//dat := unmarshalJsonnetFromFile(path)
	//dat := _getJsonFromFile(path)
	// XXX
	//var dat map[string]interface{}
	//dat := _getJson5DataFromFile(path).(map[string]interface{})
	//dat := unmarshalHjsonFromFile(path)
	dat := xloadJsonFromFile(path)
	_ = dat

	u := newUnit()
	u.name = name
	if realmName == "static" {
		isStatic = true
		u.convertToStaticUnit()
	} else {
		// XXX all static units are ground/default realm?
		u.realm = realmTypeFromString(realmName)
	}

	// Unit
	{
		def := hjGetDef(dat, "unit")
		
		u.campId = camp

		//u.static = hjGetBool(def, "static")
		if hjHasDef(def, "static") {
			u.static = hjGetBool(def, "static")
		}

		u.guiName = hjGetString(def, "gui_name")
		if !isStatic {
			u.moveSpeed = hjGetFloat(def, "move_speed")
		}
		//pp(u.guiName)
	}

	// Combat
	{
		def := hjGetDef(dat, "combat")

		u.combat.defenceRating = hjGetInt(def, "defence_rating")
		if hjHasDef(def, "weapons") {
			for _, xdefi := range def["weapons"].([]interface{}) {
				weaponDef := xdefi.(map[string]interface{})
				w := makeWeapon(weaponDef, u.combat)
				u.combat.weapons = append(u.combat.weapons, w)
			}
		}
	}

	// UnitAi
	{
		if hjHasDef(dat, "unit_ai") {
			def := hjGetDef(dat, "unit_ai")

			if hjHasDef(def, "unitBhv") {
				xdef := hjGetDef(def, "unitBhv")

				type_ := hjGetString(xdef, "type")
				u.unitAi.setBhvType(type_)
			}
		}
	}
  
  // TurretSlots
	{
    if hjHasDef(dat, "turret_slots") {
      def := hjGetDef(dat, "turret_slots")
      
      //pp(def)
      
      ts := newTurretSlots()
      u.turretSlots = ts

      //if hjHasDef(def, "turrets") {
      //  for _, xdefi := range def["turrets"].([]interface{}) {
      for slotName, xdefi := range def {
        //for _, xdefi := range def {
          turretDef := xdefi.(map[string]interface{})
          t := makeTurret(turretDef, u)
          // XXX add turret, fixme?
          //ts.turrets = append(ts.turrets, t)
          //ts.addTurret(t, "")
          ts.addTurret(t, slotName)
        }
      //}
    }
	}

	// Attach view
  var viewDef map[string]interface{}
  if hjHasDef(dat, "view") {
    viewDef = hjGetDef(dat, "view")
  }
	u.view = makeUnitView(viewDef, u)

	return u
}

func makeUnitForPlayer(name string, p *Player) *Unit {
	_log.Inf("%F name:", name, "player:", p.name, "camp: ", p.camp.Name())
	
	cknil(p)
	
	var campId CampId
	campId = p.camp.Id
	
	u := makeUnit(name, campId)
	
	// XXX
	u.assignPlayer(p)
	
	return u
}

func makeUnitView(def map[string]interface{}, u *Unit) *UnitView {
	_log.Dbg("%F")
  
	v := newUnitView(u)
  
	// TurretSlots
	{
    if hjHasDef(def, "turret_views") {
      xdef := hjGetDef(def, "turret_views")
      for slotName, xdefi := range xdef {
        //pp(slotName)
        turretViewDef := xdefi.(map[string]interface{})
        _ = turretViewDef
        //v := makeTurret(turretDef, u)
        //ts.addTurret(t, slotName)
        t := u.turretSlots.getTurretBySlotName(slotName)
        //t.view = newTurretView(t)
        
        //tv := newTurretView(t)
        tv := makeTurretView(turretViewDef, t)
        v.turretViews[slotName] = tv
      }
    }
  }
  
	return v
}

func makeWeapon(def map[string]interface{}, c *Combat) *Weapon {
	fmt.Println("makeWeapon")
	
	fmt.Println("def:")
	dump(def)
	
	var _ = `
	// Has key
	if hjHasDef(def, "def_path") {
		//pp(2)
		
		// XXX resolve def
		path := hjGetString(def, "def_path")
		extDef := unmarshalHjsonFromFile(path)
		
		fmt.Println("extDef:")
		dump(extDef)
		
		// Result map ?
		//resMap := mergemap.Merge(m1, m2)
		resMap := mergemap.Merge(extDef, def)
		
		fmt.Println("resMap:")
		dump(resMap)
		
		// XXX finally ?
		
		def = resMap
		
		//pp(2)
	}
	`
	
	def = expandDefPath(def)
	
	// new/updated def
	fmt.Println("2 def:")
	dump(def)

	w := newWeapon(c)

	w.name = hjGetString(def, "name")
	w.fireRate = hjGetFloat(def, "fire_rate")
	w.maxRounds = hjGetInt(def, "max_rounds")
	w.maintanceCost = hjGetInt(def, "maintance_cost")
	//w.ammo.amt = hjGetInt(def, "ammo")
	//w.realm = realmTypeFromString(hjGetString(def, "realm")) // XXX fixme
	w.attackDelay = hjGetFloat(def, "attack_delay")
	/*
		if d, ok := def.CheckGet("resuplyTime"); ok {
			w.resuplyTime = d.MustFloat64()
		} // Optional
	*/

	//pdump(w)

	// Load ammotype
	{
		xdef := hjGetDef(def, "ammotype")
		
		// XXX
		xdef = expandDefPath(xdef)
		
		at := w.ammoType
		at.caliber = hjGetFloat(xdef, "caliber")
		at.cost = hjGetInt(xdef, "cost")
		at.moveSpeed = hjGetFloat(xdef, "move_speed")
		at.reliability = hjGetFloat(xdef, "reliability")
		at.accuracy = hjGetFloat(xdef, "accuracy")
		at.weight = hjGetFloat(xdef, "weight")
		at.affectRadius = hjGetInt(xdef, "affect_radius")
		at.attackRating = hjGetInt(xdef, "attack_rating")
		at.radius = hjGetFloat(xdef, "radius")
		at.locateRadius = hjGetFloat(xdef, "locate_radius")
		if hjHasDef(xdef, "projectile_type") {
			at.projectileType = hjGetString(xdef, "projectile_type")
		} // Optional

		// TODO: other fields

		//panic("bad ammotype def")
	}

	//pdump(w)

	w.reload()
	return w
}

func makeTurret(def map[string]interface{}, u *Unit) *Turret {
	fmt.Println("makeTurret")
	
	fmt.Println("def:")
	dump(def)

	t := newTurret(u)

	t.name = hjGetString(def, "name")
	t.guiName = hjGetStringDefault(def, "gui_name", t.guiName)
	t.reloadTime = hjGetFloatDefault(def, "reload_time", t.reloadTime)
  
  //pdump(t)
	
	return t
}

func makeTurretView(def map[string]interface{}, t *Turret) *TurretView {
	fmt.Println("makeTurretView")
	
	v := newTurretView(t)
  
	v.posX = hjGetIntDefault(def, "pos_x", v.posX)
	v.posY = hjGetIntDefault(def, "pos_y", v.posX)
  
	return v
}

// XXX util ?
func expandDefPath(def map[string]interface{}) map[string]interface{} {
	fmt.Println("expandDefPath")
	
	ret := def
	
	// Has key
	if hjHasDef(def, "def_path") {
		fmt.Println("has def_path")
		//fmt.Println("expanding def_path...")
	
		//pp(2)
		
		// XXX resolve def
		path := hjGetString(def, "def_path")
		extDef := unmarshalHjsonFromFile(path)
		
		fmt.Printf("expanding def_path... (path=%s)\n", path)
		
		fmt.Println("extDef:")
		dump(extDef)
		
		// Result map ?
		//resMap := mergemap.Merge(m1, m2)
		resMap := mergemap.Merge(extDef, def)
		
		fmt.Println("resMap:")
		dump(resMap)
		
		// XXX finally ?
		
		ret = resMap
		
		//pp(2)
	}
	
	return ret
}
