// Utils and extra field tools / ops.
// XXX rename to fieldutil?
package main_module

// Util?
type FieldTool struct {
	f *Field
}

func newFieldTool(f *Field) *FieldTool {
	_log.Inf("%F")

	ft := &FieldTool{
		f: f,
	}

	return ft
}

// Util
func (ft *FieldTool) checkMalformedZLevels(zlevel ZLevel, checkall bool) (bool, int, int, string) {
	_log.Inf("%F")

	f := ft.f

	//for y := 1; y < f.h-1; y++ {
	//	for x := 1; x < f.w-1; x++ {
	for y := 0; y < f.h; y++ {
		for x := 0; x < f.w; x++ {

			//cell := &f.cells[x][y]
			cell := f.GetCell(x, y)

			if !checkall && cell.z != zlevel {
				continue
			}

			// topLeft
			if iabs(int(f.GetCell(x-1, y-1).z)-int(cell.z)) > 1 {
				return false, x, y, "topLeft"
			}
			// topRight
			if iabs(int(f.GetCell(x+1, y-1).z)-int(cell.z)) > 1 {
				return false, x, y, "topRight"
			}
			// bottomLeft
			if iabs(int(f.GetCell(x-1, y+1).z)-int(cell.z)) > 1 {
				return false, x, y, "bottomLeft"
			}
			// bottomRight
			if iabs(int(f.GetCell(x+1, y+1).z)-int(cell.z)) > 1 {
				return false, x, y, "bottomRight"
			}

			// top
			if iabs(int(f.GetCell(x, y-1).z)-int(cell.z)) > 1 {
				return false, x, y, "top"
			}
			// right
			if iabs(int(f.GetCell(x+1, y).z)-int(cell.z)) > 1 {
				return false, x, y, "right"
			}
			// bottom
			if iabs(int(f.GetCell(x, y+1).z)-int(cell.z)) > 1 {
				return false, x, y, "bottom"
			}
			// left
			if iabs(int(f.GetCell(x-1, y).z)-int(cell.z)) > 1 {
				return false, x, y, "left"
			}
		}
	}
	return true, -1, -1, "-"
}

// Util
func (ft *FieldTool) fixMalformedZLevels() {
	_log.Inf("%F")

	f := ft.f

	p("Field.fixMalformedZLevels")
	p("zlevels before fixing:")
	f.dumpZLevels()
	//zlevel := ZLevel_Ground3
	for zlevel := ZLevel_Min; zlevel <= ZLevel_Max; zlevel++ {
		for {
			p("fixMalformedZLevels iter")
			ok, x, y, dir := ft.checkMalformedZLevels(zlevel, false)
			if !ok {
				//f.cells[x][y].printNeighbours()
				p("bad val:", x, y, dir)
			} else {
				break
			}
			centerZVal := f.cells[x][y].z
			var badZVal *ZLevel
			var badCell *Cell
			if dir == "topLeft" {
				badZVal = &f.GetCell(x-1, y-1).z
				badCell = f.GetCell(x-1, y-1)
			} else if dir == "topRight" {
				badZVal = &f.GetCell(x+1, y-1).z
				badCell = f.GetCell(x+1, y-1)
			} else if dir == "bottomLeft" {
				badZVal = &f.GetCell(x-1, y+1).z
				badCell = f.GetCell(x-1, y+1)
			} else if dir == "bottomRight" {
				badZVal = &f.GetCell(x+1, y+1).z
				badCell = f.GetCell(x+1, y+1)
			} else if dir == "top" {
				badZVal = &f.GetCell(x, y-1).z
				badCell = f.GetCell(x, y-1)
			} else if dir == "right" {
				badZVal = &f.GetCell(x+1, y).z
				badCell = f.GetCell(x+1, y)
			} else if dir == "bottom" {
				badZVal = &f.GetCell(x, y+1).z
				badCell = f.GetCell(x, y+1)
			} else if dir == "left" {
				badZVal = &f.GetCell(x-1, y).z
				badCell = f.GetCell(x-1, y)
			} else {
				pp("bad dir:", dir)
			}
			p("badCell before ->", int(badCell.z), badCell)
			p("current bad val:", int(*badZVal))
			//var fixedZVal ZLevel
			if centerZVal > *badZVal {
				*badZVal = centerZVal - 1
			} else {
				*badZVal = centerZVal + 1
			}
			p("new fixed val:", int(*badZVal))
			p("check vals:")
			p("current bad val:", int(*badZVal))
			//var fixedZVal ZLevel
			if centerZVal > *badZVal {
				*badZVal = centerZVal - 1
			} else {
				*badZVal = centerZVal + 1
			}
			p("new fixed val:", int(*badZVal))
			p("check vals:")
			p("current bad val:", int(*badZVal))
			//var fixedZVal ZLevel
			if centerZVal > *badZVal {
				*badZVal = centerZVal - 1
			} else {
				*badZVal = centerZVal + 1
			}
			p("new fixed val:", int(*badZVal))
			p("check vals:")
			p("badCell after ->", int(badCell.z), badCell)
			//println()
			//p("after:")
			//f.cells[x][y].printNeighbours()
			//println()
			//p(f.getCellNeighbours(CPos{x, y}, true))
		}
		f.dumpZLevels()
		p("fixed; zlevel:", zlevel)
		//pp(2)
	}
	p("zlevels after fixing:")
	f.dumpZLevels()
}