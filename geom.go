package main_module

import (
	. "kristallos.ga/rx/math"
	. "math"
	"math/rand"
)

type RotDir int

const (
	CW  RotDir = 1
	CCW RotDir = -1
)

func (rd RotDir) String() string {
	switch rd {
	case 0:
		return "NONE"
	case CW:
		return "CW"
	case CCW:
		return "CCW"
	default:
		panic("unknown RotDir")
	}
}

func geomDegrees(r float64) float64 {
	return r * (180 / Pi)
}

func degBetween(p1, p2 Vec2) float64 {
	x1, y1 := p1.X(), p1.Y()
	x2, y2 := p2.X(), p2.Y()
	return geomDegrees(Atan2(y2-y1, x2-x1))
}

func degBetweenI(x1, y1, x2, y2 int) float64 {
	return degBetween(Vec2{float64(x1), float64(y1)},
		Vec2{float64(x2), float64(y2)})
}

func distancePos2(a, b Vec2) float64 {
	//return Sqrt(Pow(b.x - a.x, 2) - Pow(b.y - a.y, 2))
	return Hypot(b.X()-a.X(), b.Y()-a.Y())
}

/*
func Lerp(a, b, t float64) float64 {
	return a + (b-a)*t
}
*/

// Generic version.
func inRadius(x, y, cx, cy, r float64) bool {
	/*
	   cx, cy - center of circle
	   x, y - point
	   r - radius
	*/
	if Pow(x-cx, 2)+Pow(y-cy, 2) <= Pow(r, 2) {
		return true
	} else {
		return false
	}
}

// Wrapped cell version.
func inCellRadius(x, y, cx, cy, r int) bool {
	dx := wrapCellDistX(x, cx)
	dy := wrapCellDistY(y, cy)

	if Pow(float64(dx), 2)+Pow(float64(dy), 2) <= Pow(float64(r), 2) {
		return true
	} else {
		return false
	}
}

func _wrapDistXSign(x1, x2, w int) int {
	// Swap hack
	if x1 > x2 {
		x1, x2 = x2, x1
	}

	dx := x2 - x1

	if dx > (w / 2) {
		dx -= w
	}

	return dx
}

func _wrapDistYSign(y1, y2, h int) int {
	// Swap hack
	if y1 > y2 {
		y1, y2 = y2, y1
	}

	dy := y2 - y1

	if dy > (h / 2) {
		dy -= h
	}

	return dy
}

// Px sign version.
func wrapDistXSign(x1, x2 int) int {
	return _wrapDistYSign(x1, x2, game.field.w*cell_size)
}

// Px sign version.
func wrapDistYSign(y1, y2 int) int {
	return _wrapDistYSign(y1, y2, game.field.h*cell_size)
}

// Px version.
func wrapDistY(y1, y2 int) int {
	return iabs(wrapDistYSign(y1, y2))
}

// Px version.
func wrapDistX(x1, x2 int) int {
	return iabs(wrapDistXSign(x1, x2))
}

// Cell sign version.
func wrapCellDistXSign(x1, x2 int) int {
	return _wrapDistXSign(x1, x2, game.field.w)
}

// Cell sign version.
func wrapCellDistYSign(y1, y2 int) int {
	return _wrapDistYSign(y1, y2, game.field.h)
}

// Cell no sign version.
func wrapCellDistX(x1, x2 int) int {
	return iabs(wrapCellDistXSign(x1, x2))
}

// Cell no sign version.
func wrapCellDistY(y1, y2 int) int {
	return iabs(wrapCellDistYSign(y1, y2))
}

func roundPrec(x float64, prec int) float64 {
	var rounder float64
	pow := Pow(10, float64(prec))
	intermed := x * pow
	_, frac := Modf(intermed)
	if frac >= 0.5 {
		rounder = Ceil(intermed)
	} else {
		rounder = Floor(intermed)
	}

	return rounder / pow
}

// False - negative dir
// True - positive dir
func shortestRotDir(from, to float64) RotDir {
	// Special case for angles starting with 0 and negative targets (fixme?)
	if from == 0 && to < 0 {
		return CW
	}
	// (cpu_facing-player_degree+360)%360>180
	if Mod(to-from+360, 360) > 180 {
		return CW
	} else {
		return CCW
	}
}

// Normalize angle.
func unwrapAngle(a float64) float64 {
	/*
		//return Mod(a, 360)
		if a >= 0 {
			tmpa := Mod(a, 360)
			if tmpa == 360 {
				return 0
			} else {
				return tmpa
			}
		} else {
			return Mod(360-(1*a), 360)
		}
	*/

	/*
		// [0,360]
		// From: http://stackoverflow.com/questions/11498169/dealing-with-angle-wrap-in-c-code
		a = Mod(a, 360)
		if a < 0 {
			a += 360
		}
	*/

	// [-180,180]
	a = Mod(a+180, 360)
	if a < 0 {
		a += 360
	}
	return a - 180
}

func normalizeAngle(a float64) float64 {
	// [0,360]
	// From: http://stackoverflow.com/questions/11498169/dealing-with-angle-wrap-in-c-code
	a = Mod(a, 360)
	if a < 0 {
		a += 360
	}
	return a
}

func flipAngleX(a float64) float64 {
	a = normalizeAngle(a)
	a = 360 - a
	return a
}

// XXX not tested
func flipAngleY(a float64) float64 {
	a = normalizeAngle(a)
	if a < 180 {
		a = 180 - a
	} else {
		a = 360 - a + 180
	}
	return a
}

// Like lerp but for angles.
func rotAngle(start, end, amount float64) float64 {
	//shortest_angle := ((((end - start) % 360) + 540) % 360) - 180;
	rotDir := shortestRotDir(start, end) // Fixme?
	if rotDir == CW {
		return start - amount
	} else {
		return start + amount
	}
}

func maxi(a, b int) int {
	if a > b {
		return a
	} else {
		return b
	}
}

func minf(a, b float64) float64 {
	if a < b {
		return a
	} else {
		return b
	}
}

func maxf(a, b float64) float64 {
	if a > b {
		return a
	} else {
		return b
	}
}

func mini(a, b int) int {
	if a < b {
		return a
	} else {
		return b
	}
}

func random(min, max float64) float64 {
	return rand.Float64()*(max-min) + min
}

func iabs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

/*
// Check if from angle meets to angle
func CheckDAngle(from, to float64) bool {
  sd := ShortestRotDir(from, to)

  if (sd && from <= to) ||
    (!sd && from >= to) {
    return true
  } else {
    return false
  }
}
*/

// Doesn't work?
/*
func RotAngle(start, end, amount float64) float64 {
  //shortest_angle := ((((end - start) % 360) + 540) % 360) - 180;
  shortest_angle := Mod((Mod(end - start, 360) + 540), 360) - 180
  return shortest_angle * amount
}
*/

/*
func modLikePython(d, m int) int {
	var res int = d % m
	if ((res < 0 && m > 0) || (res > 0 && m < 0)) {
	   return res + m
	}
	return res
 }
*/

// % like python
// XXX faster version exists(?)
func pmod(i, n int) int {
	return ((i % n) + n) % n
}

func pmodf(i, n float64) float64 {
	return Mod(((Mod(i, n)) + n), n)
}

// Fixme: rename to wrapCellCoordX?
func wrapCoordX(x int) int {
	//r := (x % game.field.w)
	//r2 := pmod(x, game.field.w)
	//p("wrapCoordX:", x, game.field.w, r, r2)
	//p("XXX", game.field.w)
	return pmod(x, game.field.w)
}

func wrapCoordY(y int) int {
	return pmod(y, game.field.h)
}

func wrapCoordXY(x, y int) (int, int) {
	return wrapCoordX(x), wrapCoordY(y)
}

func wrapCoordXPx(x int) int {
	return pmod(x, game.field.w*cell_size)
}

func wrapCoordYPx(y int) int {
	return pmod(y, game.field.h*cell_size)
}

func wrapCoordXFloatPx(x float64) float64 {
	return pmodf(x, float64(game.field.w*cell_size))
}

func wrapCoordYFloatPx(y float64) float64 {
	return pmodf(y, float64(game.field.h*cell_size))
}

// World pos to screen pos
func worldToScreenPos(x, y int) (int, int) {
	//sx := x + -game.vp.shx
	//sy := y + -game.vp.shy

	// A hack for objects half-outside the VP
	framePadding := cell_size * 3

	var sx, sy int

	if game.vp.overflowX <= 0 {
		sx = x + -game.vp.shx
	} else {
		//sx = x + game.vp.w - game.vp.overflowX
		//sx = wrapCoordXPx(x + game.vp.w - game.vp.overflowX)
		if x >= game.vp.shx-framePadding {
			//p("X UNDERFLOW", x, y)
			sx = x - game.vp.shx
		} else {
			//p("X OVERFLOW", x, y)
			sx = game.vp.underflowX + x
		}
	}
	if game.vp.overflowY <= 0 {
		sy = y + -game.vp.shy
	} else {
		//sy = y + game.vp.h - game.vp.overflowY
		//sy = wrapCoordYPx(y + game.vp.h - game.vp.overflowY)
		//sy = y + -game.vp.shy

		if y >= game.vp.shy-framePadding {
			//p("UNDERFLOW", x, y)
			//sy = 9999
			//sy = wrapCoordYPx(y + game.vp.h - game.vp.overflowY)
			//sy = game.vp.underflowY + y
			sy = y - game.vp.shy
		} else {
			//p("OVERFLOW", x, y)
			//sy = y + -game.vp.shy
			sy = game.vp.underflowY + y
			//pp("y", y, "sy", sy, "sy/cell", sy/cell_size, game.vp.h, game.vp.overflowY, "shy", game.vp.shy)
		}
	}

	return sx, sy
}

// Screen pos to world pos
func screenToWorldPos(x, y int) (int, int) {
	wx := game.vp.shx + x
	wy := game.vp.shy + y
	
	//p("wx:", wx)
	//p("wy:", wy)
	
	return wx, wy
}

func screenToWorldPosX(x int) int {
	wx, _ := screenToWorldPos(x, 0) // XXX
	return wx
}

func screenToWorldPosY(y int) int {
	_, wy := screenToWorldPos(0, y) // XXX
	return wy
}
