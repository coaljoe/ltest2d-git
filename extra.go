package main_module

// Extra functions ?

// ?
//func GetVars() vars {
func GetVars() *varsT {
	return vars
}

// VarsIsDev ?
func GetVarsDev() bool {
	return vars.dev
}

// Read-only copy of vars,
// returned as map
// XXX some may be omitted
func GetVarsAsMap() map[string]interface{} {
	panic("not implemented")
	return nil
}

// ?
func GetGame() *Game {
	return game
}

// ?
func GetField() *Field {
	return game.field
}

// ?
func GetVP() *Viewport {
	return game.vp
}

// XXX
func GetProdSys() *ProdSys {
	return game.prodsys
}

// XXX
func GetNewGameTool() *NewGameTool {
	return game.newgametool
}

/*
// ?
func GetRenderer() *sdl.Renderer {
	return renderer
}
*/

//
// varsex ?
//

func (*varsT) ResX() int { return vars.resX }
func (*varsT) ResY() int { return vars.resY }
func (*varsT) ResAspect() float64 { return vars.resAspect }
func (*varsT) NativeResX() int { return vars.nativeResX }
func (*varsT) NativeResY() int { return vars.nativeResY }
func (*varsT) TmpDir() string { return vars.tmpDir }
func (*varsT) Fps() int { return vars.fps }
// ?
func (*varsT) Set_s_fw(v int) { vars.s_fw = v }
func (*varsT) Set_s_fh(v int) { vars.s_fh = v }
func (*varsT) Set_s_hmOpt(v HeightMapOptions) { vars.s_hmOpt = v }
func (*varsT) Set_s_generateField(v bool) { vars.s_generateField = v }
