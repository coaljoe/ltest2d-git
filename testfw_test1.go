package main_module

func tsTest1() {
	f := game.field
	_ = f

	// Fill

	/*
		f.cells[4][1].z = 1

		f.cells[1][2].z = 1
		f.cells[2][2].z = 1
		f.cells[3][2].z = 1
		f.cells[4][2].z = 1

		f.cells[2][3].z = 1
		f.cells[4][3].z = 1

		f.cells[3][4].z = 1
		f.cells[4][4].z = 1
	*/

	//f.cells[1][1].z = 1

	// Block
	/*
		f.cells[1][1].z = 1
		f.cells[2][1].z = 1
		f.cells[3][1].z = 1
		f.cells[4][1].z = 1

		f.cells[1][2].z = 1
		f.cells[2][2].z = 1
		f.cells[3][2].z = 1
		f.cells[4][2].z = 1

		f.cells[1][3].z = 1
		f.cells[2][3].z = 1
		f.cells[3][3].z = 1
		f.cells[4][3].z = 1
	*/

	/*
		// Block with a hole (bad)
		f.cells[1][1].z = 1
		f.cells[2][1].z = 1
		f.cells[3][1].z = 1
		f.cells[4][1].z = 1

		f.cells[1][2].z = 1
		f.cells[2][2].z = 0
		f.cells[3][2].z = 0
		f.cells[4][2].z = 1

		f.cells[1][3].z = 1
		f.cells[2][3].z = 1
		f.cells[3][3].z = 1
		f.cells[4][3].z = 1
	*/

	/*
		// Block with corners (bad?)
		f.cells[1][1].z = 1
		f.cells[2][1].z = 1
		f.cells[3][1].z = 1
		f.cells[4][1].z = 1

		f.cells[1][2].z = 1
		f.cells[2][2].z = 1
		f.cells[3][2].z = 1
		f.cells[4][2].z = 1

		f.cells[1][3].z = 1
		f.cells[2][3].z = 1
		f.cells[3][3].z = 1
		f.cells[4][3].z = 1

		f.cells[1][4].z = 1
		f.cells[2][4].z = 0
		f.cells[3][4].z = 0
		f.cells[4][4].z = 1
	*/

	/*
		// Block with corners (bad?)
		f.cells[1][1].z = 1
		f.cells[2][1].z = 1
		f.cells[3][1].z = 1
		f.cells[4][1].z = 1
		f.cells[5][1].z = 1
		f.cells[6][1].z = 1

		f.cells[1][2].z = 1
		f.cells[2][2].z = 1
		f.cells[3][2].z = 1
		f.cells[4][2].z = 1
		f.cells[5][2].z = 1
		f.cells[6][2].z = 1

		f.cells[1][3].z = 1
		f.cells[2][3].z = 1
		f.cells[3][3].z = 0
		f.cells[4][3].z = 0
		f.cells[5][3].z = 1
		f.cells[6][3].z = 1

		f.cells[1][4].z = 1
		f.cells[2][4].z = 1
		f.cells[3][4].z = 0
		f.cells[4][4].z = 0
		f.cells[5][4].z = 1
		f.cells[6][4].z = 1

		f.cells[1][5].z = 1
		f.cells[2][5].z = 1
		f.cells[3][5].z = 1
		f.cells[4][5].z = 1
		f.cells[5][5].z = 1
		f.cells[6][5].z = 1

		f.cells[1][6].z = 1
		f.cells[2][6].z = 1
		f.cells[3][6].z = 1
		f.cells[4][6].z = 1
		f.cells[5][6].z = 1
		f.cells[6][6].z = 1
	*/

	//game.field.cleanZLevels()

	/*
		// Block
		// Malformed: levels 2 and 0 meet
		f.cells[1][1].z = 1
		f.cells[2][1].z = 1
		f.cells[3][1].z = 1
		f.cells[4][1].z = 1

		f.cells[1][2].z = 1
		f.cells[2][2].z = 1
		f.cells[3][2].z = 1
		f.cells[4][2].z = 1

		f.cells[1][3].z = 2
		f.cells[2][3].z = 2
		f.cells[3][3].z = 2
		f.cells[4][3].z = 2

		f.cells[1][4].z = 2
		f.cells[2][4].z = 2
		f.cells[3][4].z = 2
		f.cells[4][4].z = 2
	*/

	//f.fixMalformedZLevels()

	/*
		// Block (correct different terrains)
		f.cells[1][1].z = 1
		f.cells[2][1].z = 1
		f.cells[3][1].z = 1
		f.cells[4][1].z = 1
		f.cells[5][1].z = 1
		f.cells[6][1].z = 1

		f.cells[1][2].z = 1
		f.cells[2][2].z = 2
		f.cells[3][2].z = 2
		f.cells[4][2].z = 2
		f.cells[5][2].z = 2
		f.cells[6][2].z = 1

		f.cells[1][3].z = 1
		f.cells[2][3].z = 2
		f.cells[3][3].z = 2
		f.cells[4][3].z = 2
		f.cells[5][3].z = 2
		f.cells[6][3].z = 1

		f.cells[1][4].z = 1
		f.cells[2][4].z = 2
		f.cells[3][4].z = 2
		f.cells[4][4].z = 2
		f.cells[5][4].z = 2
		f.cells[6][4].z = 1

		f.cells[1][5].z = 1
		f.cells[2][5].z = 2
		f.cells[3][5].z = 2
		f.cells[4][5].z = 2
		f.cells[5][5].z = 2
		f.cells[6][5].z = 1

		f.cells[1][6].z = 1
		f.cells[2][6].z = 1
		f.cells[3][6].z = 1
		f.cells[4][6].z = 1
		f.cells[5][6].z = 1
		f.cells[6][6].z = 1
	*/

	//f.check()
}