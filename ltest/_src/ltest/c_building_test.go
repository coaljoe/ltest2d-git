package main

var _ = `
import (
  "testing"
  //. "math"
)

/*
func TestSqrt(t *testing.T) {
  const in, out = 4, 2
  if x := Sqrt(in); x != out {
    t.Errorf("Sqrt(%v) = %v, want %v", in, x, out)
  }
}
*/

func TestBuilding(t *testing.T) {
  b := MakeBuilding("housing", CRedCamp).(*Building)
  b.Show()
  b2 := MakeBuilding("factory", CRedCamp).(*Building)
  b2.Show()
}

func TestAirfield(t *testing.T) {
  p := MakeUnit("tbomber", CRedCamp).(UnitI)
  b := MakeBuilding("airfield", CRedCamp).(*Building)
  b.Airfield.AddUnit(p)
  b.Airfield.Show()
  b.Airfield.RemoveUnit(p)
  b.Show()
}

func TestComponentsAccess(t *testing.T) {
  b := MakeBuilding("airfield", CRedCamp).(*Building)
  // Airfield component acces
  println("airfield:")
  if b.Airfield != nil {
    println("has airfield component")
  }
  if b.Housing == nil {
    println("hasn't housing component")
  }
}
`
