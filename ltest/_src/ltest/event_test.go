package main

var _ = `

import (
  "testing"
  //. "fmt"
  ps "lib/pubsub"
  //"lib/ecs"
)

func onUnitSpawn(ev *ps.Event) {
  println("on_unit_spawn")
  u := ev.Data.(*Unit)
  println("unit health:", u.health)
}

func onUnitSpawn2(ev *ps.Event) {
  println("on_unit_spawn2")
  u := ev.Data.(*Unit)
  println("unit health*100:", u.health*100)
}

func onUnitDestroy(ev *ps.Event) {
  println("on_unit_destroy")
}

func TestEvent(t *testing.T) {

  InitTest()

  u1 := MakeUnitFromData("lighttank", CRedCamp)
  u1.Spawn()

  println("::subpub")
  // Sub
  Sub(ev_unit_spawn, onUnitSpawn)
  Sub(ev_unit_spawn, onUnitSpawn2)
  Sub(ev_unit_destroy, onUnitDestroy)

  // Pub
  Pub(ev_unit_spawn, u1)
  Pub(ev_unit_destroy, u1)
  println("::end")
}
`
