// Хост оружия.
package main

import (
	//"fmt"
	//. "math"
	"lib/ecs"
	. "rx/math"
)

type TargetLockerI interface {
	setTarget(t *ecs.Entity)
	isTargetLocked() bool
	isTargetInRadius() bool
	targetLockedCB()
}

type WeaponHostI interface {
	isTargetLocked() bool
	hasTarget() bool
	getWeapon() WeaponI
	//weapon(w WeaponI)
	//Pos2() Pos
}

type WeaponHost struct {
	weapon WeaponI
	//target AttackableI
	*TtTargeter
	targetLocker     TargetLockerI
	targetLastLocked float64
	/* links */
	host *ecs.Entity
}

func newWeaponHost(tl TargetLockerI, host *ecs.Entity) WeaponHost {
	wh := WeaponHost{
		weapon:       nil,
		targetLocker: tl,
		host:         host,
		TtTargeter:   newTtTargeter(),
	}
	return wh
}

func (wh *WeaponHost) hasTarget() bool {
	return wh.target != nil
}

func (wh *WeaponHost) pos2() Vec2 {
	return c_Obj(wh.host).pos2()
}

func (wh *WeaponHost) canAttack(t *ecs.Entity) bool {
	// проверка возможности атаки
	// Weapon attack checks
	if getRealm(t) != wh.weapon.getRealm() {
		return false
	}
	return true
}

func (wh *WeaponHost) isTargetLocked() bool {
	return wh.targetLocker.isTargetLocked()
}

func (wh *WeaponHost) targetLockedCB() {
	wh.targetLastLocked = _now()
}

func (wh *WeaponHost) isTargetInRadius() bool {
	targetPos := c_Obj(wh.target).pos2()
	weaponPos := wh.pos2()
	if inRadius(targetPos.X(), targetPos.Y(), wh.weapon.radius(),
		weaponPos.X(), weaponPos.Y()) {
		return true
	}
	return false
}

func (wh *WeaponHost) ready() bool {
	maxReaimWait := wh.weapon.attackDelay()
	//fmt.Println(now() - wh.targetLastLocked, maxReaimWait, now() - wh.targetLastLocked > maxReaimWait)
	return _now()-wh.targetLastLocked > maxReaimWait
}

func (wh *WeaponHost) setTarget(t *ecs.Entity) bool {
	cknil(wh.weapon)
	if !wh.canAttack(t) {
		//println("err: target's weapon cannnot attack this target")
		return false
	}
	//wh.target = t
	wh.TtTargeter.setTarget(t)
	wh.targetLocker.setTarget(t)
	//println("target was set")
	_log.Dbg("target was set")
	return true
}

func (wh *WeaponHost) unsetTarget() bool {
	cknil(wh.weapon)
	wh.TtTargeter.unsetTarget()
	//println("target was unset")
	_log.Dbg("target was unset")
	return true
}

func (wh *WeaponHost) setWeapon(w WeaponI) {
	wh.weapon = w
	cknil(wh.weapon)
}

//func (wh *WeaponHost) weapon() WeaponI {
//	return wh.weapon
//}

func (wh *WeaponHost) fire() {
	println("wh.fire()")
	wh.weapon.fire()
	pub(ev_weaponhost_fire, wh)
}

func (wh *WeaponHost) reload() {
	println("wh.reload()")
	wh.weapon.reload()
}
