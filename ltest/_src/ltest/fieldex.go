// Field extras.
package main

// Cell Z level.
type ZLevel int

const (
	ZLevel_Sea0    ZLevel = 0
	ZLevel_Sea1    ZLevel = -1
	ZLevel_Sea2    ZLevel = -2
	ZLevel_Sea3    ZLevel = -3
	ZLevel_Ground0 ZLevel = 1
	ZLevel_Ground1 ZLevel = 2
	ZLevel_Ground2 ZLevel = 3
	ZLevel_Ground3 ZLevel = 4

	ZLevel_SeaMax    = ZLevel_Sea3    // Deeper
	ZLevel_GroundMax = ZLevel_Ground3 // Higher
)

func (zl ZLevel) isSea() bool {
	return zl <= 0
}

func (zl ZLevel) isGround() bool {
	return zl > 0
}

func (zl ZLevel) lower(oth ZLevel) bool {
	return zl < oth
}

func (zl ZLevel) higher(oth ZLevel) bool {
	return zl > oth
}

//func (zl ZLevel) linearize() ZLevel {}
