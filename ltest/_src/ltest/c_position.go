// Позиционный компонент.
// Отвечает за позиционирование и ориентацию, а также перемещение,
// но не трансформацию (для этого есть Transform)
//
package main

import (
	"fmt"
	"lib/ecs"
	. "rx/math"
)

type PositionMovingType int

const (
	PositionMovingType_Linear PositionMovingType = iota
	PositionMovingType_Lerp
)

var PositionT = &Position{}

// Component
type Position struct {
	*ecs.Component
	//pos      Vec3
	//dPos Vec3
	//sPos Pos
	dPos_ *Vec3 // Fixme
	//velocity    float64
	speed float64
	//rotVelocity float64
	dir Dir
	//dDir      Dir
	//travelTime float64 // Total travel time (tt)
	stepTime float64
	mtype    PositionMovingType
	i        float64
	// Component
	//entityId int
}

func (p *Position) dPos() Vec3  { return *p.dPos_ }
func (p *Position) dPos2() Vec2 { return p.dPos().ToVec2() }

func newPosition(en *ecs.Entity) *Position {
	p := &Position{
		Component: ecs.NewComponent(),
		//pos:    Vec3Zero,
		//dPos: Vec3Zero,
		dir: newDir(),
		//dDir: NewDir(),
		mtype: PositionMovingType_Linear,
	}
	en.AddComponent(p)

	fmt.Println(game.getSystem("PositionSys").getElems())
	//s := game.getSystem("PositionSys").(*PositionSys)
	s := game.getSystem("PositionSys")
	s.addElem(p)
	//s.ListElems()
	return p
}

func (p *Position) setDPos(_p Vec3) {
	p.dPos_ = &_p
	p.stepTime = 0 // Fixme: move to update code?
}

func (p *Position) setDPos2(_p Vec2) {
	obj := GetEntity(p).Get(ObjT).(*Obj)
	p.setDPos(Vec3{_p.X(), _p.Y(), obj.PosZ()})
}

func (p *Position) isMoving() bool {
	if p.dPos_ != nil {
		return true
	}
	return false
}

func (p *Position) hasDPos() bool {
	return p.dPos_ != nil
}

func (p *Position) setDirTowardsD() {
}

func (p *Position) delete() {
	//fmt.Println(game.getSystem("PositionSys").hasElem(p))
	//pp(p)
	game.getSystem("PositionSys").removeElem(p)
}
