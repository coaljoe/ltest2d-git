package main

// AI
// Unit AI behavior

import "lib/ecs"

var UnitAiT = &UnitAi{}

type UnitAiI interface {
	update(dt float64)
}

// Component
type UnitAi struct {
	*ecs.Component
	MovingBhv MovingBhvI
}

func newUnitAi(en *ecs.Entity) *UnitAi {
	uai := &UnitAi{
		Component: ecs.NewComponent(),
	}
	mb := newStandardMovingBhv(en)
	uai.MovingBhv = mb
	//t.MovingBhv = NewScoutMovingBhv(en)
	en.AddComponent(uai)
	return uai
}

func (uai *UnitAi) setMovingBhv(name string) {
	/*
		mb := MakeMovingBhv(name)
		curmbc := GetEntity(uai).GetComponent()
		GetEntity(uai).RemoveComponent()
		uai.MovingBhv = mb
	*/
	mb := makeMovingBhv(name, GetEntity(uai))
	uai.MovingBhv = mb
}

func (uai *UnitAi) update(dt float64) {
	uai.MovingBhv.update(dt)
}
