package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"
)

type Settings struct {
	data map[string]interface{}
	//datai interface{}
}

func newSettings() *Settings {
	return &Settings{}
}

func (s *Settings) load(fp string) {
	if ok, err := exists(fp); !ok {
		pp(err)
	}

	//s.data = _getJson5DataFromFile(fp)
	s.data = _getJson5DataFromFile(fp).(map[string]interface{})
}

func (s *Settings) save(fp string) {
	/*
		if ok, err := exists(fp); ok {
			p("file already exists: ", fp)
			pp(err)
		}
	*/

	if game == nil || !game.initialized {
		pp("the game is not initialized")
	}

	// Add json section to the document
	add_section := func(t string) string {
		//strings.TrimLeft(t, "{")
		//strings.TrimRight(t, "}")
		t = strings.TrimSpace(t)
		t = t[1:]
		t = t[:len(t)-1]
		return t
	}

	sep := ",\n\n" // Doesn't truly work like needed

	ss := ""
	ss = "{\n"
	// App settings
	ss += fmt.Sprintf("\"resX\":%d, \"resY\":%d", vars.resX, vars.resY)
	ss += sep
	// Game settings
	//ss += "  // Game settings\n"
	ss += add_section(_Keymap.saveToJson())
	//ss += ",\n\n"
	ss += sep
	ss += add_section(_Keymap.saveToJson())
	ss += "\n}"

	// Pretty printer for json
	var prettyJSON bytes.Buffer
	error := json.Indent(&prettyJSON, []byte(ss), "", "  ")
	if error != nil {
		fmt.Println(ss)
		log.Println("JSON parse error: ", error)
		panic(error)
	}
	ss = string(prettyJSON.Bytes())

	f, err := os.OpenFile(fp, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		panic(err)
	}
	//defer(f.Close())
	if _, err := f.WriteString(ss); err != nil {
		panic(err)
	}

	//s.datai = _getJsonDataFromFile(fp)
	f.Close()
}

func (s *Settings) getInt(k string) int {
	return _getInt(s.data, k)
}

func (s *Settings) getFloat(k string) float64 {
	return _getFloat(s.data, k)
}

func (s *Settings) getString(k string) string {
	return _getString(s.data, k)
}

func (s *Settings) verify() {
	{
		sx := s.getInt("resX")
		sy := s.getInt("resY")
		if sx < 1 || sy < 1 {
			pp("the resolution should be > 0;", sx, sy)
		}
	}
}
