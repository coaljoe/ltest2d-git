package main

import (
	"lib/sr"
	"testing"
)

func TestSaveGame(t *testing.T) {
	initTest()

	sr.Register(&Obj{})
	saveGame(vars.tmpDir + "/testsave.sav")
	loadGame(vars.tmpDir+"/testsave.sav", true)
}
