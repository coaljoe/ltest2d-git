package main

import (
	//"github.com/bradfitz/iter"
	"testing"
)

func TestFow(t *testing.T) {
	initTest()

	fow := newFow(4, 4)

	_ = `
	p("not loop")
	for y := 0; y < fow.h; y++ {
		for x := 0; x < fow.w; x++ {
			p(y, x)
		}
	}

	p("not loop")
	for y := range iter.N(fow.h) {
		for x := range iter.N(fow.w) {
			p(y, x)
		}
	}

	p("iter")
	for y := range fow.itH() {
		for x := range fow.itW() {
			p(y, x)
		}
	}

	for y := range fow.itH() {
		for x := range fow.itW() {
			if fow.mask[x][y] != true {
				p("derp")
				t.Fail()
			}
		}
	}
	`

	// Test rect
	fow.clear()
	fow.mask[1][1] = false

	println(fow.mask.showFlip())

	//println(fow.show())
	println(fow.mask.show())
	//println(fow.fmask.show())
	//println(fow.getFmask().show())
	println(fow.getFmask(0, 0, 4, 4).show())
	println(fow.getFmask(0, 0, 2, 2).show())
	println(fow.getFmask(1, 1, 2, 2).show())
	//println(fow.getFmask(1, 1, fow.w, fow.h).show())

	for y := range fow.itH() {
		for x := range fow.itW() {
			if fow.mask[x][y] != true {
				p("derp2")
				t.Fail()
			}
		}
	}
}
