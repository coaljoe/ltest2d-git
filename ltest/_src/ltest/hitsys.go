// Система отвечающая за обработку попаданий.
package main

import (
	ps "lib/pubsub"
)

type HitSys struct {
	*GameSystem
}

func newHitSys() *HitSys {
	s := &HitSys{}
	s.GameSystem = newGameSystem("HitSys", "Hit system", s)
	sub(ev_projectile_hit, s.onProjectileHit)
	return s
}

func (s *HitSys) onProjectileHit(ev *ps.Event) {
	_log.Dbg("onProjectileHit")
	e := ev.Data.(EvProjectileHit)
	hitPos := e.pos
	hitRadius := e.ammotype.affectRadius // 1
	attackRating := e.ammotype.attackRating
	emitter := e.emitter
	target := e.target                       // Warn: not really used for calculations
	radius := float64(hitRadius) * cell_size // Cells to units
	p(radius)
	p("target:", target)

	// Walk affected objects
	objs := _ObjSys.getObjsInRadius(hitPos.ToVec2(), radius)
	objCount := 0 // Affected objs count
	for _, obj := range objs {
		en := GetEntity(obj)

		// Can't hit the emitter
		if en == emitter {
			/*
				p("skip emitter")
				emitter.PrintComponents()
				p("derp:", en, emitter, target, objs, hitPos, hitPos.ToVec2(), radius)
			*/
			continue
		}

		// Should have combat components on both sides to perform attack
		if (!en.HasComponent(CombatT)) || (!emitter.HasComponent(CombatT)) {
			continue
		}

		objCount++
		p("--> hit obj:", obj.name, obj.Pos(), "id:", GetEntity(obj).Id(), "count:", objCount, "en:", en)
		u := en.Get(UnitT).(*Unit)
		dump(u)

		// Check if object has Combat component
		if _, ok := en.GetComponentCheck(CombatT); ok {
			//target := en.Get(UnitT).(*Unit)
			c := emitter.GetComponent(CombatT).(*Combat)
			//c._performAttack(attackRating, target) // XXX fixme?
			//c._performAttack(attackRating, obj) // XXX fixme?
			c._performAttack(attackRating, en) // XXX fixme?
		} else {
			// The object doesn't have a combat component, skip
			//p("SKIP")
			continue
		}

		//en.PrintComponents()
		if en.HasComponent(UnitT) {
			// Unit type
			_log.Dbg("hit unit...")

		} else if en.HasComponent(BuildingT) {
			// Building type
			_log.Dbg("hit building...")

		} else {
			// Unknown type
		}
	}
	p("Projectile hit at", e.pos)
	//pp(2)
}

func (s *HitSys) update(dt float64) {}
