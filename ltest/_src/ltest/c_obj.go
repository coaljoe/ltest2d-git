package main

import (
	"fmt"
	"lib/ecs"
)

type Objs []*Obj

// Obj component.
// Base of any game object.
type Obj struct {
	*ecs.Component
	*Transform
	id         int
	name       string
	active     bool
	objtype    ObjType
	selectable bool
}

func (o *Obj) getType() string  { return "Obj" }
func (o *Obj) isActive() bool   { return o.active }
func (o *Obj) setActive(v bool) { o.active = v }
func (o *Obj) objId() int       { return o.id }

func newObj(en *ecs.Entity, name string) *Obj {
	o := &Obj{
		Component: ecs.NewComponent(),
		id:         _ider.GetNextId("obj"),
		name:       name,
		Transform:  newTransform(),
		active:     false,
		objtype:    ObjType_Unknown,
		selectable: true,
	}
	en.AddComponent(o)
	_ObjSys.addElem(o) // Store component, not entity
	return o
}

func (o *Obj) spawn() {
	o.active = true
}

func (o *Obj) delete() {
	_log.Inf("Obj.Delete")
	_ObjSys.removeElem(o)
}

func (o *Obj) String() string {
	return fmt.Sprintf("Obj<id=%d>", o.id)
}

/*
// Sort interface
type ByDistance []*Obj

func (s ByDistance) Len() int {
  return len(s)
}

func (s ByDistance) Swap(i, j int) {
  s[i], s[j] = s[j], s[i]
}

func (s ByDistance) Less(i, j int) bool {
  a, b := s[i], s[j]
  DistancePos2(a.Pos)
}
*/

/***** SubObj *****/

/*
type SubObj struct {
  *Transform
  parent *Obj
}

func NewSubObj(parent *Obj) *SubObj {
  t := &Obj{
    Transform: NewTransform(),
    parent: parent,
  }
  t.Transform.SetParent(t.parent.Transform)
  return obj
}
*/
