package main

import (
	"flag"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	_ "net/http/pprof"
	"os"
	"path/filepath"
	"reflect"
	"runtime"
	"strconv"
	"strings"

	"lib/xlog"
	"rx"
	. "rx/math"

	"github.com/kardianos/osext"
)

var _ = reflect.Bool
var _ = Vec3Zero

func init() {
	var runUnderTest = false
	if flag.Lookup("test.v") == nil {
		fmt.Println("normal run")
	} else {
		fmt.Println("run under go test")
		runUnderTest = true
	}

	// Change current dir to main's dir
	// Cd to app's root
	//dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	dir, err := osext.ExecutableFolder()
	if err != nil {
		panic(err)
	}
	fmt.Println("dir:", dir)
	appRoot := filepath.Join(dir, "..")
	if runUnderTest {
		wd, _ := os.Getwd()
		appRoot = wd // S/ltest
		//println("derp1", appRoot)
		appRoot = filepath.Join(appRoot, "..", "..")
		//println("derp2", appRoot)
		//println("wd", wd)
		//panic(2)
	}
	fmt.Println("appRoot: ", appRoot)
	os.Chdir(appRoot)

	// Set GOMAXPROCS.
	runtime.GOMAXPROCS(1)
	p("GOMAXPROCS:", runtime.GOMAXPROCS(0))
}

func main() {
	fmt.Println("main()")
	fmt.Printf("BuildRevision: %s", BuildRevision)

	ltestMain()
}

func ltestMain() {
	fmt.Println("main()")
	initApp()
	game, app := initBaseGame()
	initDefaultGame()

	rxi := rx.Rxi()
	//rxi.SetResPath("s/rx/res/")
	_ = rxi

	sl := rx.NewSceneLoader()
	_ = sl

	// Setup scene //

	c := _Scene.camera
	c.setNamedCamera("blender1")

	// Field //
	f := _Field
	//f.LoadHeightMap("res/field/heightmaps/noise.png")
	//f.GenerateHeightMap()
	//f.Generate(32, 32, 64, 64, 4.0)
	/*
		fsize := 64
		fw, fh := fsize, fsize
		hmW := fw * 2
		hmH := fh * 2
		hmScale := 4.0
		f.generate(fw, fh, hmW, hmH, hmScale)
	*/
	/*
		fsize := 64 * 4
		fw, fh := fsize, fsize
		hmW := fw * 2
		hmH := fh * 2
		hmScale := 1.0
	*/
	//fsize := 16 * 4
	//fsize := 16 * 4
	fsize := 64
	fw, fh := fsize, fsize
	hmW := fw * 8 //* 4
	hmH := fh * 8 //* 4
	hmOpt := newHeightMapOptions()
	//hmOpt.Seed = 3232
	hmOpt.Seed = 3230
	hmOpt.Octaves = 4
	//hmOpt.NoiseScale = 2.0
	hmOpt.NoiseScale = 16.
	//hmOpt.NoiseScale = 4.0
	fgOpt := newFieldGenerateOptions()
	f.generate(fw, fh, hmW, hmH, hmOpt, fgOpt)
	//dump(f.cells)
	//pp("test")
	f.spawn()
	f.hm.save(vars.tmpDir + "/ltest__hm_last.png")

	//p(_Keymap.saveToJson())
	s := _Keymap.saveToJson()
	p(s)
	_Keymap.loadFromJson(s)
	//pp(2)

	// Move camera target to 0, 0
	//c.MoveTargetTo(Vec3{0, c.trg.Y, 0})

	//

	var u1, u2, u3 *Unit

	// Create a heavy tank (u2)
	test1 := func() {
		en := makeUnit("lighttank", getPlayer())
		//println("*** test1")
		//en.PrintComponents()
		//en._p.PrintComponents()
		//panic(1)
		//en.Spawn()
		u2 = en.Get(UnitT).(*Unit)
		//u2 = MakeUnitFromData("lighttank", CRedCamp).Get(UnitT).(*Unit)
		u2.spawn()
		//u2.SetPos2(Pos{0, 0})
		u2.setCPos(0, 0)

		if u2.player.camp.name() != "Reds" {
			panic("camp check failed")
		}

		// Test cpos
		{
			prevPos := u2.Pos()

			u2.setCPos(1, 1)
			have := u2.cpos()
			want := CPos{1, 1}
			if have != want {
				panic("cpos test failed")
			}
			u2.setCPos(5, 10)
			have = u2.cpos()
			want = CPos{5, 10}
			if have != want {
				panic("cpos test failed")
			}
			u2.setCPos(0, 0)
			have = u2.cpos()
			want = CPos{0, 0}
			if have != want {
				panic("cpos test failed")
			}

			// Return unit back
			u2.SetPos(prevPos)
		}
	}
	_ = test1

	// Create a light tank as a target and move it to position
	test2 := func() {
		en := makeUnit("lighttank", getPlayer())
		u1 = en.Get(UnitT).(*Unit)
		//en.Spawn()
		u1.spawn()
		u1.setPlayer(_PlayerSys.players[1])
		//u1.SetPos2(Pos{10, 0})
		//u1.SetCPos(1, 0)
		u1.setPos2(Vec2{12, 4})
		//pp(u1.Pos())
		//u1.SetPos2(Pos{0, 0})
		//u1.MoveTo(CPos{5, 10})
		// Test _Entity
		//en.Unit.MoveTo(CPos{5, 10})
		en.Get(UnitT).(*Unit).moveTo(CPos{5, 10})

		//u1.dir.SetAngle(90)

		/*
		   u1.SetPos2(Pos{0,60})
		   p(u1.Pos2())
		   p(u1.CPos())
		   p(u1.Path())
		   u1.MoveTo(CPos{10, 20})
		   p(u1.Path())
		   p(u1.CPos())
		*/
		//-pp(u1.Pos2())
		//u1.MoveTo(CPos{5, 0})

		//u1.SetTarget(u2)

		//u1.MoveTo(CPos{10, 0})
		//u1.MoveTo(CPos{2, 2})
		//u1.MoveTo(CPos{10, 10})
	}
	_ = test2

	// Create static light tank as a target
	test3 := func() {
		u3 = makeUnit("lighttank", getPlayer()).Get(UnitT).(*Unit)
		u3.spawn()
		u3.setPos2(Vec2{16, 16})
		u3.setPlayer(_PlayerSys.players[1])
	}
	_ = test3

	// Spawn many lighttanks at random positions
	test4 := func() {
		n := 5
		//n := 20
		//n := 50
		//n := 150
		//w, h := 20, 30
		pw, ph := 50, 50
		mw, mh := 50, 50

		for i := 0; i < n; i++ {
			u := makeUnit("lighttank", getPlayer()).Get(UnitT).(*Unit)
			u.spawn()
			u.setPos2(Vec2{10.0 + float64(rand.Intn(pw)),
				10.0 + float64(rand.Intn(ph))})
			//u.SetRot(Vec3{0, rand.Float64()*360, 0})
			//u.rot.Y = rand.Float64()*360
			u.dir.setAngle(rand.Float64() * 360)
			//u.SetCampType(RandomCampType())
			//u.setPlayer(_PlayerSys.players[1])
			u.moveTo(CPos{rand.Intn(mw), rand.Intn(mh)})
		}
	}
	_ = test4

	// Move heavy tank to a position
	test1a := func() {
		u2.moveTo(CPos{10, 10})
	}
	_ = test1a

	// Test turret
	test1b := func() {
		//en := _NewEntity()
		//tt := NewTurret(en)
		u1 = makeUnit("lighttank", getPlayer()).Get(UnitT).(*Unit)
		t := GetEntity(u1).Get(TurretT).(*Turret)

		u2 = makeUnit("lighttank", getPlayer()).Get(UnitT).(*Unit)
		u2.setPlayer(_PlayerSys.players[1])

		// Spawn
		//u1.GetEntity().Spawn()
		//u2.GetEntity().Spawn()
		spawnEntity(GetEntity(u1))
		spawnEntity(GetEntity(u2))

		// Place units
		u1.setCPos(0, 0)
		//u1.SetCPos(10, 10)
		//u2.setCPos(0, 1)
		u2.setCPos(1, 0)
		//u2.SetPos2(Pos{12, 4})
		//u1.MoveTo(CPos{5, 10})
		uaic2 := GetEntity(u2).Get(UnitAiT).(*UnitAi)
		uaic2.setMovingBhv("Hold")
		uaic1 := GetEntity(u1).Get(UnitAiT).(*UnitAi)
		uaic1.setMovingBhv("Hold")

		go func() {
			sleepsec(3)
			//spawnEntity(u2.GetEntity())
		}()
		// Attack
		//u1.SetTarget(u2)

		fmt.Println(t.dir.angle())
	}
	_ = test1b

	// Test static units
	test_static_unit := func() {
		e1 := makeUnit("gunbattery", getPlayer())
		u1 := e1.Get(UnitT).(*Unit)
		t := GetEntity(u1).Get(TurretT).(*Turret)
		_ = t
		//pp(u1.objId())

		u2 = makeUnit("lighttank", getPlayer()).Get(UnitT).(*Unit)
		//u2.setCampId(game.campsys.camps[1])

		// Spawn
		//u1.GetEntity().Spawn()
		//u2.GetEntity().Spawn()
		spawnEntity(GetEntity(u1))
		spawnEntity(GetEntity(u2))
		//pp(e1.ListComponents())
		//deleteEntity(u2.GetEntity())

		// Hud
		_Hud.AddObjToSelection(c_Obj(GetEntity(u2)))

		// Place units
		u1.setCPos(0, 0)
		//u1.SetCPos(10, 10)
		u2.setCPos(0, 1)
		//pp(u2.cpos())
		//p(c_Obj(u2.GetEntity()))
		//p(_ObjSys.getObjsInRadius(u2.Pos().ToVec2(), cell_size))
		//pp(u2.Pos().ToVec2())
		//u2.setCPos(2, 2)
		//u2.SetPos2(Pos{12, 4})
		//u1.MoveTo(CPos{5, 10})
		//uaic2 := u2.GetEntity().Get(UnitAiT).(*UnitAi)
		//uaic2.setMovingBhv("Hold")

		// Second battery
		e2 := makeUnit("rocketbattery", getPlayer())
		e2u := e2.Get(UnitT).(*Unit)
		e2u.setCPos(1, 1)
		spawnEntity(e2)
		//e2u.setCampId(game.campsys.camps[1]) // Unset for a bug

		e2w := e2.Get(CombatT).(*Combat).getWeapons()[0]
		p(e2.Get(CombatT).(*Combat).getWeapons())
		p(e2w.getShells())
		//e2w.setDisabled(true)
		//pp(e2w.getShells())

		// Attack
		//u1.SetTarget(u2)

		// Add gas truck
		e3 := makeUnit("gastruck", getPlayer())
		e3u := e3.Get(UnitT).(*Unit)
		e3u.setCPos(2, 2)
		//spawnEntity(e3)
		//e3u.setCampId(game.campsys.camps[1])
	}
	_ = test_static_unit

	// Test navy units
	test_navy_unit := func() {
		e1 := makeUnit("motorboat", getPlayer())
		//c_Unit(e1).setCPos(5, 15)
		c_Unit(e1).setCPos(6, 18)
		spawnEntity(e1)
		//pp(c_Unit(e1).Pos())
		// Hud
		_Hud.AddObjToSelection(c_Obj(e1))
	}
	_ = test_navy_unit

	// Test building
	test_building := func() {
		e1 := makeBuilding(BuildingType_Bunker, getPlayer())
		//c_Unit(e1).setCPos(5, 15)
		//c_Building(e1).setCPos(5, 0)
		c_Building(e1).setCPos(1, 1)
		//pp(c_Building(e1).Pos())
		spawnEntity(e1)
		//pp(c_Unit(e1).Pos())
		// Hud
		_Hud.AddObjToSelection(c_Obj(e1))

		e2 := makeBuilding(BuildingType_Bunker, getPlayer())
		c_Building(e2).setCPos(0, 0)
		spawnEntity(e2)

		if false {
			// Shift building by offset
			ox := cell_size / 4.
			oy := cell_size / 2.
			c_Building(e2).AdjPos(Vec3{ox, oy, 0})
			static_unit_objid := 0 // Static turret's (e1) objid
			staticUnit := _ObjSys.getObjById(static_unit_objid)
			staticUnit.AdjPos(Vec3{ox, oy, 0})

			// Set relation
			c_Building(e2).staticUnit = c_Unit(GetEntity(staticUnit))
		}

		//_Hud.AddObjToSelection(c_Obj(e2))

	}
	_ = test_building

	// Test player ai
	test_player_ai := func() {
		//p := _PlayerSys.getPlayerByName("ai1")
		p3 := _PlayerSys.getPlayerByName("ai2")
		p3.ai.test()
		//pp(_UnitSys.getUnitsForPlayer(getPlayer()))
		//pdump(getPlayer().getFow())
		//pdump(p3.getFow())
	}
	_ = test_player_ai

	// Perform tests //

	//test1()
	//test2()
	//test3()
	//test4()
	//test1a()
	//test1b()
	test_static_unit()
	//test_navy_unit()
	test_building()
	//test_player_ai()

	en := newEntity()
	_ = newObj(en, "test")
	pc := newPosition(en)
	_ = pc
	en.PrintComponents()
	deleteEntity(en)
	en.PrintComponents()
	//pp(2)

	// Spawn many lighttanks at random positions
	side := 10
	n := 5
	//n := 50 //* 2
	test5 := func() {
		for i := 0; i < n; i++ {
			u := makeUnit("heavytank", getPlayer()).Get(UnitT).(*Unit)
			//u.Spawn()
			spawnEntity(GetEntity(u))
			//u.setCPos(10+(i%side), 10+i/side)
			u.setCPos(i%side, i/side)
			u.dir.setAngle(rand.Float64() * 360)
			//u.setCampId(randomCampId())
			u.setRandomPlayer()
			//u.SetCampType(CSuurCamp)
			//SpawnEntity(GetEntity(u))
			//u.moveTo(CPos{rand.Intn(10), rand.Intn(10)})
			go func() {
				sleepsec(rand.Float64() * 10)
				u.moveTo(CPos{rand.Intn(10), rand.Intn(10)})
			}()
		}
	}
	_ = test5
	//test5()

	/*
	  sl.Load("res/units/ground/heavytank/reds/model/model.dae")
	  sl.Spawn()
	*/

	// Load tank //

	/*
	  sl.Load("s/rx/res/test/tank_renew2/tank-renew2.dae")
	  sl.Spawn()
	  //rxi.Scene().GetNode("body").SetPos(Vec3{0, -5, 0})
	  //rxi.Scene().GetNode("turret").SetPos(Vec3{0, -5, 0})
	  rxi.Scene().GetNode("body").SetPos(Vec3{5, 0, 0})
	  rxi.Scene().GetNode("turret").SetPos(Vec3{5, 0, 0})
	  rxi.Scene().GetNode("turret").SetRot(Vec3{0, 45, 0})
	*/

	//body := rxi.Scene().GetNode("body")
	//rxi.Scene().GetNode("turret").SetParent(body.Transform)

	//u2.rot.Y = 45
	/*
	  turret0 := u2.GetComponentTag(&Turret{}, "main").(*Turret)
	  turret0.SetRotTrgAngle(-(45/2))
	  turret1 := u2.GetComponentTag(&Turret{}, "mg").(*Turret)
	  turret1.SetRotTrgAngle(45)
	*/

	//u2.MoveTo(CPos{10, 10})

	//u2.SetTarget(u1)

	//u1.Destroy()
	//u2.Destroy()
	//u2 = nil
	//p(u2.Camp().Name())
	//p(u1.target == nil, reflect.TypeOf(u1.target))
	//p(u1.target.GetHealth())
	//p(u1.CombatCmp.whs[0].target.GetHealth())
	//u1.target = nil // Fixme: manual
	//panic(2)

	//rxi.ResourceSys.PrintResources()
	//pp(2)

	/*
	  rx.Rxi().Scene().GetNode("turret").SetPos(Vec3{0, 2, 0})
	  rx.Rxi().Scene().GetNode("body").SetPos(Vec3{0, 2, 0})
	  Println(rx.Rxi().Scene().GetNode("turret").Pos())
	  Println(rx.Rxi().Scene().GetNode("mg").Pos())
	  //panic(2)
	*/

	//game.unitsys.AddUnit(u1)
	//game.unitsys.AddUnit(u2)

	// Start profile (?)
	go func() {
		log.Println(http.ListenAndServe("localhost:6060", nil))
	}()

	fps_limit := 0 //30

	for app.Step() {
		//println("main.step")
		dt := app.GetDt()
		game.update(dt)

		if fps_limit != 0 {
			//ms_to_sleep := int64((1e6 / int64(fps_limit)) - app.GetDtMs())
			//time.Sleep(time.Duration(ms_to_sleep) * time.Millisecond)
			s_to_sleep := (1.0 / float64(fps_limit)) - dt
			p(s_to_sleep)
			if s_to_sleep > 0.0 {
				sleepsec(s_to_sleep / 2)
			}
			//sleepsec(0.5)
		}

		//sleepsec(0.1)
		//pp(u2.GetTargets())
		//p(u2.Get(TurretT).(*Turret).dir.Angle())
	}

	println("exiting...")
}

/////////
// Util

// Set new window site.
func resizeWindow(sx, sy int) {
	_log.Inf("Resize window:", sx, sy)
	vars.resX = sx
	vars.resY = sy
	vars.resScaleX = float64(vars.resX) / float64(vars.nativeResX)
	vars.resScaleY = float64(vars.resY) / float64(vars.nativeResY)
}

/////////
// Init

var (
	_log        *xlog.Logger
	overrideRes bool // FIXME: add normal app module
	fullscreen  bool
)

func initApp() {
	println("Init() begin")
	SetDefaultVars()

	var optAppRoot = flag.String("root", "", "appRoot")
	var optRes = flag.String("res", "", "resolution WxH")
	var optFs = flag.Bool("fs", false, "full screen")

	flag.Parse()
	fmt.Println("args:", os.Args)
	//pp(2)
	if *optAppRoot != "" {
		//pp(*optAppRoot)
		appRoot := *optAppRoot
		fmt.Println("new appRoot:", appRoot)
		os.Chdir(appRoot)
	}
	if *optRes != "" {
		ss := strings.Split(*optRes, "x")
		sx, _ := strconv.Atoi(ss[0])
		sy, _ := strconv.Atoi(ss[1])
		fmt.Printf("new res: (%d, %d)\n", sx, sy)
		//resizeWindow(sx, sy)
		vars.resX = sx
		vars.resY = sy
		overrideRes = true
	}
	if *optFs == true {
		// XXX: comment this out to get -res option working with the -fs
		sx, sy := rx.GetDesktopResolution()
		vars.resX = sx
		vars.resY = sy

		overrideRes = true
		fullscreen = true
	}

	// Seed
	//rand.Seed(time.Now().UTC().UnixNano())

	// Create logger
	_log = xlog.NewLogger("ltest", xlog.LogDebug)
	if loglev := os.Getenv("LOGLEVEL"); loglev != "" {
		_log.SetLogLevelStr(loglev)
	}
	_log.SetOutputFile("ltest.log")
	_log.SetWriteTime(true)
	println("Init() done")
}

func initBaseGame() (*Game, *rx.App) {
	println("InitGame() begin")

	// Load settings
	defaultSettingsFile := strings.ToLower(AppName) + "_default.json"
	customSettingsFile := strings.ToLower(AppName) + ".json"

	// Pick default or custom settings file
	fp := ""
	if found, err := exists(customSettingsFile); found {
		if err != nil {
			panic(err)
		}
		fp = customSettingsFile
	} else if found, err = exists(defaultSettingsFile); found {
		if err != nil {
			panic(err)
		}
		fp = defaultSettingsFile
	} else {
		pp("Settings file not found in:", defaultSettingsFile, customSettingsFile)
	}

	// Load settings
	s := newSettings()
	s.load(fp)
	s.verify()
	//pdump(s)

	// Apply settings
	if !overrideRes {
		vars.resX = s.getInt("resX")
		vars.resY = s.getInt("resY")
	}

	// Set config
	rx.SetDefaultConf()
	//rx.ConfSetResPath("/home/j/dev/go/s/rx/res/") // Fixme
	rx.ConfSetResPath("res/") // Fixme
	// Set extra conf
	rx.Conf.EnableCulling = true

	//rx.Init()
	//win := rx.NewWindow()
	//win.Init(640, 480)
	//win.Init(1024, 576)
	//rxi := rx.Init(1024, 576)
	var rxi *rx.Rx
	if !fullscreen {
		rxi = rx.Init(vars.resX, vars.resY)
	} else {
		rxi = rx.InitFullscreen(vars.resX, vars.resY)
	}
	app := rxi.App
	game := newGame(app)
	// Set debug game options
	game.setGameOptions(newDebugGameOptions())
	println("InitGame() done")

	app.Win().SetTitle(AppName + "  (rev. " + BuildRevision + ")")

	// Update globals
	_rxi = rxi

	// Finally
	s.save("test_settings_out.json")

	return game, app
}

func initDefaultGame() {
	game.createNewDefaultGame()
	game.start()
}
