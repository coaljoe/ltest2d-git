package main

import (
	"fmt"
	"path"
	. "strconv"
	"strings"

	"lib/ecs"

	js "github.com/bitly/go-simplejson"
)

/////////////////
// Unit factory

func makeUnit(utype string, player *Player) *ecs.Entity {
	_log.Inf("MakeUnit, utype:", utype, "camp: ", player.camp.name())
	camp := player.camp
	unitsdir := "res/units"
	//realmName := strings.ToLower(realm.name())
	campName := strings.ToLower(camp.name())
	realms := []string{"ground", "air", "navy", "static"} // Static is not a real realm
	realmName := ""                                       // Store realm name here
	found := false
	var fp string

	// Try load units
	for _, rname := range realms {
		// For each realm
		fp = unitsdir + "/" + rname + "/" + utype + "/" + campName + "/def.json"
		if ok, _ := exists(fp); ok {
			found = true
			realmName = rname
			break
		}
	}

	// Finally
	if !found {
		panic(fmt.Sprintf(
			"cannot make unit from data, unknown settings: utype=%s camp=%s\nfp=%s",
			utype, camp.name(), fp))
	} else {
		_log.Inf("Loading unit from: ", fp)
	}

	//fp = fmt.Sprintf("%s/%s.json", fp, utype)
	js := _getJsonFromFile(fp)

	/***** create unit *****/

	//ent := NewEntity() // Fixme?
	//ent := _World._CreateEntity() // Fixme?
	ent := newEntity()
	u := newUnit(ent)
	u.objtype = ObjType_Unit

	/***** unit *****/
	//u.Unit.LoadData(json)
	_log.Inf("load unit data")
	//v, _ := d.Get("weight").Float64()
	//u.Weight = float64(v)
	def := js.Get("unit")
	u.utype = utype
	u.weight = _readFloat(def, "weight")

	u.static = def.Get("static").MustBool()
	if u.static {
		u.convertToStaticUnit()
	}
	if !u.isStaticUnit() {
		u.maxSpeed = _readFloat(def, "maxSpeed")
		u.accSpeed = _readFloat(def, "accSpeed")

	}

	// Automatically set player
	u.setPlayer(player)

	/*
		ent.PrintComponents()
		__ent.PrintComponents()
		pp(3)
	*/
	/***** weapons *****/
	make_weapon := func(typename string) WeaponI {
		if weaponsBlk, ok := js.CheckGet("weapons"); ok {
			// Load weapons
			for i := range weaponsBlk.MustArray() {
				weaponBlk := js.Get("weapons").GetIndex(i)
				if d, ok := weaponBlk.CheckGet("typename"); ok {
					if d.MustString() == typename {
						w := makeWeapon(weaponBlk)
						return w
					}
				} else {
					panic("cant read weapon typename")
				}
			}
		} else {
			panic("cant read weapons block")
		}
		return nil
	}

	/***** turret *****/
	for i := 0; i < 3; i++ {
		if def, ok := js.CheckGet("turret" + Itoa(i)); ok {
			//_log.Inf("load turret")
			_log.Inf("load turret")
			t := newTurret(ent)
			t.name = def.Get("name").MustString("unset")
			t.reloadTime = _readFloat(def, "reloadTime")
			t.rotSpeed = def.Get("rotSpeed").MustFloat64(t.rotSpeed)
			t.rotAcc = def.Get("rotAcc").MustFloat64(t.rotAcc)
			if d, ok := def.CheckGet("returnEnabled"); ok {
				t.returnEnabled = d.MustBool()
			}
			if d, ok := def.CheckGet("returnSpeedCoeff"); ok {
				t.returnSpeedCoeff = d.MustFloat64()
			}
			if d, ok := def.CheckGet("ecs_tag"); ok {
				GetEntity(t).SetComponentTag(t, d.MustString())
				//u.Entity.PrintComponents()

				//v := ent.GetComponentTagOnly("main")
				v := ent.GetComponentTag(&Turret{}, "main")
				fmt.Println(v)
				ent.PrintComponents()
				//panic(2)
			}
			if d, ok := def.CheckGet("hostedWeapon"); ok {
				/*
				   cc := u.CombatCmp
				   w := cc.GetWeaponByTypename(d.MustString())
				   cc.AddHostedWeapon(w, t)
				*/
				typename := d.MustString()
				if w := make_weapon(typename); w != nil {
					t.WeaponHost.setWeapon(w)
					//u.ListWeapons()
					//pp(w.Typename())
				} else {
					panic("cant load weapon by typename; typename: " + typename)
				}
			}
		}
	}

	/***** view *****/
	if def, ok := js.CheckGet("view"); ok {
		_log.Inf("view definition found, loading")
		//u.Entity.PrintComponents()
		//v := MakeUnitView(utype, camp, u.Entity)
		v := makeUnitView(ent, def, path.Dir(fp)+"/model")
		u.View = v
	} else {
		panic("view definition not found")
	}

	/***** combat *****/
	if def, ok := js.CheckGet("combat"); ok {
		_log.Inf("combat definition found, loading")
		c := u.Combat
		c.defenceRating_ = _readInt(def, "defenceRating")
	} else {
		panic("combat definition not found")
	}

	/***** tank *****/
	if def, ok := js.CheckGet("tank"); ok {
		_log.Inf("tank definition found, loading")
		_ = def
		//ent.PrintComponents()
		//ent.AddComponent(NewTank(ent))
		/*
			println("******")
			__ent.PrintComponents()
			ent.PrintComponents()
			__ent._p.PrintComponents()
			println(ent.Id(), __ent._p.Id(), __ent.Id())
		*/
		//pp(2)
		_ = newTank(ent)
	}

	/***** truck *****/
	if def, ok := js.CheckGet("truck"); ok {
		_log.Inf("truck definition found, loading")
		_ = def
		rtype := resourceTypeFromString(_readString(def, "resourceType"))
		_ = newTruck(ent, rtype)
	}

	//	println("derp")
	//	ent.PrintComponents()
	//	__ent.PrintComponents()
	//	pp(3)

	/***** unitai *****/

	if def, ok := js.CheckGet("unitai"); ok {
		uaic := newUnitAi(ent)
		_log.Inf("unitai definition found, loading")
		/*
		   any, ok := u.Entity.GetComponentCheck(&UnitAI{})
		   if !ok {
		     pp("fail cant get unitai component")
		   }
		   uac := any.(*UnitAI)
		*/
		mbDef := def.Get("movingBehavior")
		if !uaic.MovingBhv.loadFromData(mbDef) {
			pp("fail")
		}
		/*
		   mbDef := def.Get("movingBehavior")
		   if d, ok := mbDef.CheckGet("targetLocateRadius"); ok {
		     uac.MovingBhv.targetLocateRange = d.MustFloat64() } else { pp("fail") }
		   if d, ok := mbDef.CheckGet("targetPursuitTime"); ok {
		     uac.MovingBhv.targetPursuitTime = d.MustFloat64() } else { pp("fail") }
		*/
	}

	/*
	  //XXX fixme: move to WeaponCmp.LoadData?
	  if weaponsBlk, ok := d.CheckGet("weapons"); ok {
	    // Load weapons
	    for i, _ := range weaponsBlk.MustArray() {
	      weaponBlk := d.Get("weapons").GetIndex(i)
	      w := MakeWeaponFromData(weaponBlk)
	      u.AddWeapon(w)
	    }
	  } else {
	    panic("cant read weapons block")
	  }
	*/

	// Make adjustments for the realm "ground"
	if realmName == "ground" {
		u.SetPosZ(zelev_ground0)
		//u.setZPos(-100)
		//pp(u.Pos())
	}

	// Make adjustments for the realm "navy"
	if realmName == "navy" {
		u.SetPosZ(zelev_sea0)
		//u.setZPos(-100)
		//pp(u.Pos())
	}

	_log.Inf("done loading unit")

	return ent
}

///////////////////
// Weapon factory

func makeWeapon(js *js.Json) WeaponI {
	def := js

	w := newWeapon()

	w.typename_ = _readString(def, "typename")
	w.fireRate = _readFloat(def, "fireRate")
	w.maxShells = _readInt(def, "maxShells")
	w.maintanceCost = _readFloat(def, "maintanceCost")
	w.ammo.amt = _readInt(def, "ammo")
	w.realm = realmTypeFromString(_readString(def, "realm"))
	w.attackDelay_ = _readFloat(def, "attackDelay")
	if d, ok := def.CheckGet("resuplyTime"); ok {
		w.resuplyTime = d.MustFloat64()
	} // Optional

	//pdump(w)

	// Load ammotype
	if def, ok := js.CheckGet("ammotype"); ok {
		at := w.ammoType_
		at.caliber = _readFloat(def, "caliber")
		at.cost = _readInt(def, "cost")
		at.moveSpeed = _readFloat(def, "moveSpeed")
		at.reliability = _readFloat(def, "reliability")
		at.accuracy = _readFloat(def, "accuracy")
		at.weight = _readFloat(def, "weight")
		at.affectRadius = _readInt(def, "affectRadius")
		at.attackRating = _readInt(def, "attackRating")
		at.radius = _readFloat(def, "radius")
		at.locateRadius = _readFloat(def, "locateRadius")
		if d, ok := def.CheckGet("projectileType"); ok {
			at.projectileType = d.MustString()
		} // Optional

		// TODO: other fields

	} else {
		_fail()
	}

	w.reload()
	return w
}

/////////////////////
// UnitView factory

func makeUnitView(ent *ecs.Entity, js *js.Json, ddir string) *UnitView {
	// Entity
	//v := NewView(ent)

	// View asset
	vac := newViewAsset(ent, ddir)
	vac.load()
	_ = vac

	//ent.PrintComponents()
	u := ent.Get(UnitT).(*Unit)

	uvc := newUnitView(ent, u)
	_ = uvc

	/***** turret *****/
	for i := 0; i < 3; i++ {
		if def, ok := js.CheckGet("turret" + Itoa(i)); ok {
			_log.Inf("load turret view component")

			tag := def.Get("ecs_tag").MustString()
			println("tag", tag)

			t := ent.GetComponentTag(&Turret{}, tag).(*Turret)

			tvc := newTurretView(ent, t)
			//tvc.snd_fire = def.Get("snd_fire").MustString()
			//tvc.snd_reload = def.Get("snd_reload").MustString()
			if d, ok := def.CheckGet("nodeName"); ok {
				tvc.nodeName = d.MustString()
			} else {
				panic("cant find nodeName")
			}
			/*
			   if d, ok := def.CheckGet("parentNodeName"); ok {
			     tvc.parentNodeName = d.MustString()
			   }
			*/
			//v.AddComponentTag(tvc, tag)
			ent.SetComponentTag(tvc, tag)
		}
	}

	return uvc
}

/////////////////////
// Building factory

func makeBuilding(btype BuildingType, player *Player) *ecs.Entity {
	_log.Inf("MakeBuilding, btype:", btype, "camp: ", player.camp.name())
	camp := player.camp
	buildingsdir := "res/buildings"
	//realmName := strings.ToLower(realm.name())
	campName := strings.ToLower(camp.name())
	fp := buildingsdir + "/" + btype.name() + "/" + campName + "/def.json"

	if ok, _ := exists(fp); !ok {
		panic(fmt.Sprintf(
			"cannot make building from data, "+
				"unknown settings: btype=%s camp=%s\nfp=%s",
			btype.name(), camp.name(), fp))
	} else {
		_log.Inf("Loading building from: ", fp)
	}

	datai := _getJson5DataFromFile(fp)
	if datai == nil {
		panic("failed to get data from json5")
	}

	//type js5object map[string]interface{}
	//type js5array []interface{}
	data := datai.(map[string]interface{})
	var def map[string]interface{}
	var ok bool

	// Create building

	ent := newEntity()
	b := newBuilding(ent)
	b.objtype = ObjType_Building
	b.name = btype.name()

	// Building
	_log.Inf("[factories] load building")
	//def = data["building"].(map[string]interface{})
	//def = _getObject(data["building"])
	if def, ok = _getObjectCheck(data["building"]); !ok {
		panic("building definition not found")
	}
	b.btype.setFromString(_getString(def, "buildingType"))
	//b.setCampId(camp)
	b.setPlayer(player)
	b.typename = _getString(def, "typename")

	// Combat
	_log.Inf("[factories] load combat")
	if def, ok = _getObjectCheck(data["combat"]); !ok {
		panic("combat definition not found")
	}
	c := b.Combat
	c.defenceRating_ = _getInt(def, "defenceRating")
	//c.defenceRating_ = _getInt(def["defenceRating"])

	// View
	_log.Inf("[factories] load view")
	v := makeBuildingView(ent, data, path.Dir(fp)+"/model")
	b.View = v

	return ent
}

/////////////////////////
// BuildingView factory

func makeBuildingView(ent *ecs.Entity, data interface{},
	ddir string) *BuildingView {

	// View asset
	vac := newViewAsset(ent, ddir)
	vac.load()
	_ = vac

	//ent.PrintComponents()
	b := ent.Get(BuildingT).(*Building)

	bvc := newBuildingView(ent, b)
	_ = bvc

	return bvc
}

/////////
// Misc

func _fail() { panic("json read fail") }
