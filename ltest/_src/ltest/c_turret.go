package main

import (
	"lib/ecs"

	. "github.com/artyom/fsm"
)

var TurretT = &Turret{}

/*
type TargetLocator struct {
  target AttackableI
}

 func NewTargetLocator() *TargetLocator {
  t := &TargetLocator{}
  return t
}
*/

/*
type TurretI interface {
  Fire()
  Reload()
}
*/

/* abstract */
type Turret struct {
	*ecs.Component
	WeaponHost
	fsm              *Fsm
	name             string
	reloadTime       float64
	rotSpeed, rotAcc float64
	dir              Dir
	trgRotAngle      float64
	//linkedObj        *Obj
	// Return system
	returnEnabled    bool    // Auto-return enabled
	returnDelay      float64 // Delay before return turret back
	returnSpeedCoeff float64 // Speed coeff.
	returnLock       bool    // Lock
	/* state */
	curRotAcc float64
}

func newTurret(en *ecs.Entity) *Turret {
	var (
		idle   = State{"idle"}
		aiming = State{"aiming"}
	)
	transitions := map[State]map[State]bool{
		idle:   {aiming: true},
		aiming: {idle: true},
	}

	t := &Turret{
		Component: ecs.NewComponent(),
		name:             "abstract turret",
		reloadTime:       10.0,
		rotSpeed:         10.0,
		rotAcc:           1.00,
		dir:              newDir(),
		trgRotAngle:      -1,
		returnEnabled:    true,
		returnDelay:      4.0,
		returnSpeedCoeff: 0.6,
		fsm:              NewFsm(idle, transitions),
	}
	en.AddComponent(t)

	// Entity links
	//uc := GetEntity(t).Get(UnitT).(*Unit)
	//t.linkedObj = uc.Obj

	t.WeaponHost = newWeaponHost(t, en)

	// Register weapon host, fixme: ugly
	//t.Entity.PrintComponents()
	//p(en.Id())
	//p(t.GetEntity())
	//pp(t.GetEntityId())
	cc := GetEntity(t).Get(CombatT).(*Combat)
	cc.addWeaponHost(&t.WeaponHost)
	return t
}

func (t *Turret) rotAngle() float64 {
	return t.dir.angle()
}

// -1: angle not set (?) now nil = unset?
func (t *Turret) rotateTo(v float64) {
	t.rotateToSpeedCoeff(v, 1)
}

func (t *Turret) rotateToSpeedCoeff(v float64, speedCoeff float64) {
	t.dir.rotTo(v, t.rotSpeed*speedCoeff)
}

// TargetLockerI
func (t *Turret) isTargetLocked() bool {
	if t.target == nil {
		return false
	}
	//return Round(t.rotAngle, 1) == Round(t.rotTrgAngle, 1)
	//return t.dir.HasD() && t.dir.Locked()
	//return t.dir.Locked()
	return t.trgRotAngle == t.dir.angle() // Fixme?
}

func (t *Turret) setTarget(tt *ecs.Entity) {
	t.target = tt
}

func (t *Turret) targetLockedCB() {
	t.WeaponHost.targetLockedCB()
}

// Update turret return system
func (t *Turret) _updateReturnSys(dt float64) {
	if t.hasTarget() {
		return
	}
	//p("derp")
	//mt.SetRotTrgAngle(-1)

	// Return turret back
	if t.returnEnabled &&
		!t.dir.isZero() && !t.returnLock {
		t.returnLock = true
		/*
		   go func(){
		       sleepsec(tc.returnTurretDelay)
		       if tc != nil && mt != nil {
		         // Rotate back with slower speed
		         mt.RotateToSpeedK(0, tc.returnTurretSpeedK)
		         tc.returnTurretLk = false
		       }
		   }()
		*/
		/*
		   mt.RotateToSpeedK(0, tc.returnTurretSpeedK)
		*/
		_ = newShed(t.returnDelay,
			func() {
				if t != nil {
					// Rotate back with slower speed
					t.rotateToSpeedCoeff(0, t.returnSpeedCoeff)
					t.returnLock = false
				}
			})
	} else {
		//p(mt.dir.Angle())
	}
}

func (t *Turret) update(dt float64) {
	//println("turret.update")

	// Entity links update

	uc := GetEntity(t).Get(UnitT).(*Unit)
	//t.SetTarget(uc.target)

	// Fsm update

	idle := State{"idle"}
	aiming := State{"aiming"}

	if t.target != nil && t.fsm.State() == idle {
	}

	//p(uc.dir.Angle())

	updateRotAngle := func() {
		//if CheckDAngle(t.rotAngle, t.rotTrgAngle) {

		// Return if no target was set
		if !t.hasTarget() {
			return
		}

		if t.dir.locked() {
			//p("locked")
			gpos := t.pos2()
			ang := degBetween(gpos, c_Obj(t.target).pos2())
			ang = unwrapAngle(ang - uc.dir.angle())
			//t.dir.setAngle(ang)
			//t.dir.rotTo(ang, t.rotSpeed)
			t.rotateTo(ang)
			//t.rotateTo(-90)
		} else {
			/*
			   t.curRotAcc = Min(t.curRotAcc + t.rotAcc, 1.0)
			   v := Pi * t.rotSpeed
			   if ang > 0 {
			     v = -v
			   }
			   t.rotAngle += v * t.curRotAcc * dt
			*/
			// Always update target angle
			if t.target != nil {
				//gpos := t.pos.Add(t.linkedObj.pos) // Fixme, global pos
				gpos := t.pos2()
				//f.Println(">> ", t.rotTrgAngle, gpos, t.target.Pos())
				ang := degBetween(gpos, c_Obj(t.target).pos2())

				//gpos = t.linkedObj.Transform.WorldPos()
				//gpos_ := uc.WorldPos()
				//tgpos := t.target.WorldPos()
				//t.RotateTo(uc.rot.Z + -ang)
				//t.RotateTo(ang)
				//t.trgRotAngle = UnwrapAngle(uc.rot.Z + -ang)
				//p(ang, uc.dir.Angle(), uc.rot.Z, gpos, t.target.Pos2())
				//p(ang, uc.Rot().Z, uc.Rot().Z)
				//p(gpos_, tgpos)
				//t.RotateTo(-161)
				//t.trgRotAngle = UnwrapAngle(-161)

				t.rotateTo(ang)
				t.trgRotAngle = unwrapAngle(ang)
				//pp("ang", -uc.rot.Z + -ang)
				_ = uc
				_ = ang
			} else {
				// Unset target angle
				t.trgRotAngle = -1
				//println("no target")
			}
		}
	}

	// Always update rot angle
	updateRotAngle()

	// Fsm check
	switch t.fsm.State() {
	case idle:
		//println("ck idle")
		if t.target != nil {
			if !t.isTargetLocked() {
				t.fsm.To(aiming)
			}
		}

	case aiming:
		//println("ck aiming")
		if t.isTargetLocked() {
			t.fsm.To(idle)
			println("target locked")
			t.targetLockedCB()
		}
	}

	// Fsm update
	switch t.fsm.State() {
	case idle:
		//println("upd idle")
		t.curRotAcc = 0
		{
		}

	case aiming:
		//println("upd aiming")
		//updateRotAngle()
		{
		}
	}

	// Always update dir
	t.dir.update(dt)

	t._updateReturnSys(dt)
}

func (t *Turret) delete() {
	println("Turret.Delete")
	//t.View.delete()
}
