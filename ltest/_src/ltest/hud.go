package main

import (
	"fmt"
	ps "lib/pubsub"
	"math/rand"
	"rx"
	. "rx/math"

	"github.com/go-gl/glfw/v3.1/glfw"
)

//type HudMouseState int
type HudMouseState int

const (
	HudMouseState_Default HudMouseState = iota
	//HudMouseState_GoToCell
	HudMouseState_PlaceBuilding
)

// Hud system. A part of Gui.
type Hud struct {
	*GameSystem
	// Use Entity for all objects?
	selObjs     map[int]*Obj
	selecting   bool
	selRect     Vec4 // SelStart, selEnd
	groundPlane Plane
	// A point on the ground that camera ray intersects
	groundPoint Vec3
	// Was the ground hit by the camera ray
	groundHit  bool
	mx, my     int
	cx, cy     int
	mouseState HudMouseState
	// Remember last building type that was built by hud
	lastBuildingType BuildingType
	enabled          bool
	view             *HudView
	debug            bool
	// Debug
	d_showMouseRay bool
}

func newHud() *Hud {
	h := &Hud{
		selObjs:        make(map[int]*Obj),
		selecting:      false,
		selRect:        Vec4Zero,
		groundPlane:    NewPlane(Vec3{0, 0, 1}, 0), // Z-up facing, 0 distance
		mx:             -1,
		my:             -1,
		debug:          true,
		d_showMouseRay: true,
	}
	h.GameSystem = newGameSystem("Hud", "Hud Subsystem", h)
	sub(rx.Ev_mouse_move, h.onMouseMove)
	sub(rx.Ev_mouse_press, h.onMousePress)
	sub(rx.Ev_mouse_release, h.onMouseRelease)
	sub(rx.Ev_key_press, h.onKeyPress)
	return h
}

func (h *Hud) onMouseMove(ev *ps.Event) {
	if game.gui.hasMouseFocus() {
		return
	}
	
	p := ev.Data.(rx.EvMouseMoveData)
	h.mx = p.X
	h.my = p.Y
	//pp(h.mx, h.my)
	_log.Trc("hud.onmousemove", h.mx, h.my)

	if rx.Rxi().InputSys.Mouse.IsButtonPressed(glfw.MouseButtonLeft) {
		if !h.selecting {
			h._startSelection()
		} else {
			h._updateSelection()
		}
	}

	//h._doGroundPlaneHitTest()
	if h.debug {
		//_Scene.debugBox.SetPos(h.groundPoint)
	}
}

func (h *Hud) onMousePress(ev *ps.Event) {
	if game.gui.hasMouseFocus() {
		return
	}
	
	if h.mx == -1 && h.my == -1 {
		h.mx = _InputSys.Mouse.X
		h.my = _InputSys.Mouse.Y
		//pp(h.mx, h.my)
	}
	btn := ev.Data.(glfw.MouseButton)
	_log.Dbg("hud.onmousepress", btn)
	h._doGroundPlaneHitTest()

	if btn == glfw.MouseButtonLeft {
		switch h.mouseState {
		case HudMouseState_Default:
			for _, o := range h.selObjs {
				if o.objtype == ObjType_Unit {
					// Move units
					c_Unit(GetEntity(o)).Mover.moveTo(CPos{h.cx, h.cy})
				}
				// Unselect
				h.RemoveObjFromSelection(o)
			}
		case HudMouseState_PlaceBuilding:
			// Place building
			btype := BuildingType_Bunker
			h._placeBuilding(btype)

			// Store last placed building type
			h.lastBuildingType = btype

		default:
			pp("unknown mouseState:", h.mouseState)
		}

		// Update mouseState
		switch h.mouseState {
		case HudMouseState_PlaceBuilding:
			h.mouseState = HudMouseState_Default
			//default:
			//	pp("unknown mouseState:", h.mouseState)
		}
	} else if btn == glfw.MouseButtonRight {
		h.mouseState = HudMouseState_PlaceBuilding
	}
}

func (h *Hud) onMouseRelease(ev *ps.Event) {
	if game.gui.hasMouseFocus() {
		return
	}
	
	btn := ev.Data.(glfw.MouseButton)
	_log.Dbg("hud.onmouserelease", btn)
	if !h.selecting {
		//h._doSingleSelection()
	} else {
		h._stopSelection()
	}
}

func (h *Hud) onKeyPress(ev *ps.Event) {
	key := ev.Data.(glfw.Key)
	if key == _Keymap.getKey1("rebuildLastBuildingKey") {
		//pp("derp")
		// Put mouse state in place building mode
		// Fixme: need to unset previsous mouse state
		h.mouseState = HudMouseState_PlaceBuilding
	}
}

func (h *Hud) _startSelection() {
	if !h.selecting {
		h.selecting = true
		h.selRect[0] = float64(h.mx)
		h.selRect[1] = float64(h.my)

		// Make selStop == selStart
		h.selRect[2] = h.selRect[0]
		h.selRect[3] = h.selRect[1]
	}
}

func (h *Hud) _stopSelection() {
	_log.Inf("HUD Selection:", h.selRect)
	h.selecting = false
	h.selRect = Vec4Zero
	_log.Inf("HUD Selection:", h.selRect)
}

func (h *Hud) _updateSelection() {
	if h.selecting {
		h.selRect[2] = float64(h.mx)
		h.selRect[3] = float64(h.my)
		p(h.selRect)
	}
}

// Perform a single-click selection.
func (h *Hud) _doSingleSelection() {
	_log.Inf("[Hud] single selection")
	ray := h._castCameraRay()
	rc := rx.NewRaycaster()
	nodes := rc.TestRayScene(ray)
	pp("nodes:", nodes)
}

func (h *Hud) _doGroundPlaneHitTest() {
	// Update groundPoint
	ray := h._castCameraRay()
	//h.groundHit, h.groundPoint = h.groundPlane.IntersectsRay(ray)
	hit, p := h.groundPlane.IntersectsRay(ray)

	camn := rx.Rxi().Camera
	cam := camn.Camera
	proj := cam.GetProjectionMatrix()
	view := cam.GetViewMatrix()
	vp := proj.Mul(view)
	invVP := vp.Invert()
	_ = invVP
	//rm := camn.Mat().Upper3()
	//rm := camn.Mat()
	rm := camn.Mat()
	//rm.SetTranslation(0, 0, 0)
	rm = rm.Invert()
	m := camn.Mat()
	mw := camn.WorldMat()
	_, _ = m, mw
	//p_trans := mw.MulVec4(p.ToVec4()).ToVec3()
	//p_trans := p
	sx, sy := rx.Rxi().Renderer().Size()
	p_trans := UnProject(Vec3{float64(h.mx), float64(sy - h.my), 0}, view, proj, 0, 0, sx, sy)
	//p_trans := rm.MulVec3(p)
	fmt.Println("->", p, p_trans)
	fmt.Println("->", unitsToCells(p.X()), unitsToCells(p.Y()))
	fmt.Println("->", unitsToCells(p_trans.X()), unitsToCells(p_trans.Y()))
	//h.cx = unitsToCells(p.X())
	//h.cy = unitsToCells(p.Y())
	h.cx = unitsToCells(p_trans.X())
	h.cy = unitsToCells(p_trans.Y())

	h.groundHit = hit
	h.groundPoint = p_trans //p_trans
	//h.groundPoint = p

	if h.groundHit {
		_log.Inf("[Hud] ground hit at:", h.groundPoint)
		if h.debug {
			_Scene.debugBox.SetPos(h.groundPoint)
		}
	}
}

// Cast a ray from camera.
func (h *Hud) _castCameraRay() Ray {
	camn := _Scene.camera.cam
	ray := camn.Camera.CreateRayWithLenght(100)

	if h.debug && h.d_showMouseRay {
		//_Scene.debugBox.SetPos(ray.Direction)
		rx.DrawBegin()
		//rx.DrawSetAtV(ray.Direction)
		//rx.DrawCube(10)
		rx.DrawSetColorV(rx.ColorBlue)
		rx.DrawLineV(ray.Origin, ray.Direction)
		//rx.DrawLineV(ray.Origin, Vec3Zero)
		rx.DrawEnd()
	}
	return ray
}

// Place building at mouse position.
func (h *Hud) _placeBuilding(btype BuildingType) {
	en := makeBuilding(btype, getPlayer())
	//c_Building(en).setCPos(h.cx, h.cy)
	pos := h.groundPoint
	q := 5
	p(pos)
	//pos[0] = (float64(int(pos.X()*q)) / q)
	//pos[1] = (float64(int(pos.Y()*q)) / q)
	pos[0] = float64((int(pos.X()) / q) * q)
	pos[1] = float64((int(pos.Y()) / q) * q)
	//pos[2] = float64((int(pos.Z()) / q) * q)
	//pp(pos)
	//pos.SetY((pos.Y() / q) * q)
	c_Building(en).SetPosX(pos.X())
	c_Building(en).SetPosY(pos.Y())
	//c_Building(en).setZPos(pos.Z())
	spawnEntity(en)

	if true {
		en2 := makeUnit("gunbattery", getPlayer())
		c_Unit(en2).SetPosX(pos.X())
		c_Unit(en2).SetPosY(pos.Y())
		spawnEntity(en2)
		//c_Unit(en2).setZRot(rand.Float64() * 360.0)
		c_Turret(en2).rotateTo(float64(rand.Intn(360)))
		//(float64(rand.Intn(360)))
		//_ = rand.
	}
}

func (h *Hud) AddObjToSelection(o *Obj) {
	h.selObjs[o.id] = o
}

// Has the object in selection.
func (h *Hud) HasObjInSelection(o *Obj) bool {
	return h.selObjs[o.id] != nil
}

func (h *Hud) IsObjSelected(o *Obj) bool {
	return h.HasObjInSelection(o)
}

func (h *Hud) RemoveObjFromSelection(o *Obj) {
	if !h.HasObjInSelection(o) {
		pp("can't remove obj from selection, obj is not selected; obj:", o)
	}
	delete(h.selObjs, o.id)
}

func (h *Hud) start() {
	h.view = newHudView(h)
	h.enabled = true
	if h.debug {
		//_Scene.debugBox.SetVisible(true)
	}
}

func (h *Hud) update(dt float64) {
	if !h.enabled {
		return
	}
	//pp("derp")
	h.view.update(dt)

	// Pass the information to the gui
	_Gui.cellX = h.cx
	_Gui.cellY = h.cy
}
