package main

import (
	//. "math"
	"lib/ecs"
	"rx"

	. "rx/math"
)

var TurretViewT = &TurretView{}

// Component
type TurretView struct {
	*ecs.Component
	*View
	m                 *Turret
	firepointNode     *rx.Node
	firepointNodeName string
}

func newTurretView(en *ecs.Entity, tt *Turret) *TurretView {
	v := &TurretView{
		Component: ecs.NewComponent(),
		View: newView(en),
		m:    tt,
	}
	en.AddComponent(v)
	return v
}

func (v *TurretView) load() {
	if v.loaded {
		return
	}
	println("turretview.load()")
	ast := GetEntity(v).Get(&ViewAsset{}).(*ViewAsset).getAsset()
	//pp(v.nodeName)
	v.node = ast.getNode(v.nodeName)
	v.firepointNodeName = v.nodeName + "_firepoint" // "turret_firepoint"
	v.firepointNode = ast.getNode(v.firepointNodeName)
	v.loaded = true
}

func (v *TurretView) spawn() {
	println("turretview.spawn()")
	if !v.loaded {
		v.load()
	}
	//v.node.spawn()
	rx.Rxi().Scene().Add(v.node)
	rx.Rxi().Scene().Add(v.firepointNode)
	v.spawned = true
}

func (v *TurretView) delete() {
	println("turretview.delete()")
	rx.Rxi().Scene().RemoveMeshNode(v.node)
	rx.Rxi().Scene().RemoveMeshNode(v.firepointNode)
	v.spawned = false
}

func (v *TurretView) render() {
	//println("turretview.render()")
}

func (v *TurretView) update(dt float64) {
	//println("turretview.update()")
	//return

	// Update turret's position
	/*
	  newPos := v.m.linkedObj.Pos() //.Add(v.m.Pos()) // Center
	  curPos := v.node.Pos()
	  //offset := Vec3{-0.54876, -0.38443, 2.99107}
	  offset := Vec3Zero
	  if v.nodeName == "mg" {
	    //offset = Vec3{-0.54876, 0, -0.38443}
	    offset = Vec3{-0.54876, 0, 0.38443}
	  }
	*/
	/*
	  a := Radians(v.m.RotAngle())
	  x0, y0 := 0., 0.
	  //x0, y0 := curPos.X, curPos.Z
	  x, y := newPos.X, newPos.Z
	  //newX := curPos.X * Cos(a) - curPos.Z * Sin(a)
	  //newZ := curPos.X * Sin(a) + curPos.Z * Cos(a)
	  //newX := newPos.X + (curPos.X - newPos.X) * Cos(a) - (curPos.Z-newPos.Z) * Sin(a)
	  //newZ := newPos.Z + (curPos.X - newPos.X) * Sin(a) + (curPos.Z-newPos.Z) * Cos(a)
	  x1 := x0 + (x - x0) * Cos(a) - (y - y0) * Sin(a)
	  y1 := y0 + (y - y0) * Cos(a) + (x - x0) * Sin(a)
	*/
	//v.node.SetPos(Vec3{x1, curPos.Y, y1})
	//v.node.SetPos(offset.Add(Vec3{newPos.X, curPos.Y, newPos.Z}))
	//v.node.SetPos(Vec3{newPos.X, curPos.Y, newPos.Z})
	//v.node.SetPos(Vec3{newPos.X - curPos.X, curPos.Y, newPos.Z - curPos.Z})
	//v.node.SetPos(pos)
	/*
	   // Update turret's direction
	   rot := v.m.linkedObj.rot
	   ang := rot.Y + v.m.RotAngle()
	   rot.Y = ang
	   //v.node.SetRot(Vec3{0, ang, 0})
	   v.node.SetRot(rot)
	*/

	// Update turret's direction
	rot := Vec3Zero
	ang := rot.Z() + v.m.rotAngle()
	rot.SetZ(ang)
	//v.node.SetRot(Vec3{0, ang, 0})
	v.node.SetRot(rot)
}
