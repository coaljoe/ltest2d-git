// +build !static

package main

import (
	"fmt"
	"testing"
)

func TestTurret(tt *testing.T) {
	return

	fmt.Println("TestTurret derp")

	initTest()

	//en := _NewEntity()
	//tt := NewTurret(en)
	e1 := makeUnit("lighttank", getPlayer())
	u1 := e1.Get(UnitT).(*Unit)
	t := GetEntity(u1).Get(TurretT).(*Turret)

	e2 := makeUnit("lighttank", getPlayer())
	u2 := e2.Get(UnitT).(*Unit)
	//u2.setCampId(CampId_Suur)
	u2.setPlayer(game.playersys.players[1]) // FIXME?

	// Place units
	u1.setCPos(0, 0)
	u2.setCPos(1, 1) // 45 deg

	// Spawn
	//u1.Spawn()
	//u2.Spawn()
	spawnEntity(e1)
	spawnEntity(e2)

	// Attack
	u1.setTarget(GetEntity(u2))

	fmt.Println("t.dir.Angle():", t.dir.angle())

	/*
		for i := 0; i < 2; i++ {
			println("i:", i)
			u1.Update(0.1)
			u2.Update(0.1)
			fmt.Println(t.dir.Angle())
		}
	*/

	for i := 0; i < 10; i++ {
		println("i:", i)
		game.update(1.0 / 30.0) // 30 fps
		//game.Update(1.0)
		fmt.Println(t.dir.angle())
	}
}
