package main

import (
	"fmt"
	//. "math"
)

/////////
// Node

type Node struct {
	x, y   int
	fs     int
	parent *Node
}

func newNode(x, y int) *Node {
	return &Node{x: x, y: y}
}

func (n *Node) cmp(oth *Node) bool {
	if n.x == oth.x && n.y == oth.y {
		return true
	}
	return false
}

func (n *Node) String() string {
	return fmt.Sprintf("Node<%d, %d, fs=%d>", n.x, n.y, n.fs)
}

/////////
// Path

type Path []*Node

func (p Path) lastNode() *Node {
	return p[len(p)-1]
}

func pathfind(s, g CPos) Path {
	start := newNode(s.x, s.y)
	goal := newNode(g.x, g.y)
	cset := make(Path, 0)
	oset := make(Path, 0)
	oset = append(oset, start)
	path := make(Path, 0)
	fscore := _fscore_func
	field := game.field

	println("FROM", start, "TO", goal)
	for len(oset) > 0 {
		x := _get_lowest_fscore_node(oset)
		if x.cmp(goal) {
			println("GOAL")
			path = _traceback_path(x)
			println("PATH:", path)
			return path
		}

		//oset.remove(x)
		for i, n := range oset {
			if n.cmp(x) {
				oset[i] = nil // For gc safety
				oset = append(oset[:i], oset[i+1:]...)
			}
		}
		cset = append(cset, x)

		for _, p := range _neighbours(x.x, x.y) {
			y := newNode(p.x, p.y)

			// Y in cset
			for _, n := range cset {
				if n.cmp(y) {
					continue
				}
			}

			//println(field != nil, y.x, y.y)

			// Check empty
			if !field.isCellOpen(y.x, y.y) {
				continue
			}

			y.parent = x
			y.fs += fscore(y, goal)
			// Y not in oset
			y_in_oset := false
			for _, n := range oset {
				if n.cmp(y) {
					y_in_oset = true
				}
			}
			if !y_in_oset {
				oset = append(oset, y)
			}
		}
	}

	println("No path found")
	return path
}

func _neighbours(x, y int) []CPos {
	//r := make([]CPos, 7)
	r := make([]CPos, 0)

	next := func(x, y int) {
		// Filter negative
		if x < 0 || y < 0 {
			return
		}
		c := CPos{x, y}
		r = append(r, c)
	}

	next(x-1, y+1) // Top Left
	next(x, y+1)   // Top Center
	next(x+1, y+1) // Top Right

	next(x-1, y) // Center Left
	next(x+1, y) // Center Right

	next(x-1, y-1) // Bottom Left
	next(x, y-1)   // Bottom Center
	next(x+1, y-1) // Bottom Right

	return r
}

func _traceback_path(n *Node) Path {
	ret := make(Path, 0)
	node := n
	for node.parent != nil {
		ret = append(ret, node)
		node = node.parent
	}
	return ret
}

func _get_lowest_fscore_node(l Path) *Node {
	ret := l[0]
	for _, n := range l {
		if n.fs < ret.fs {
			ret = n
		}
	}
	return ret
}

func _fscore_func(a, b *Node) int {
	abs := func(x int) int {
		if x < 0 {
			return -x
		}
		return x
	}

	return abs(a.x-b.x) + abs(a.y-b.y) // Manhattan
}
