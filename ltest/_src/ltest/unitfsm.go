package main

var _ = `
import (
  //. "fmt"
  // . "math"
  //"lib/ecs"
  //. "ltest/util"
  . "github.com/artyom/fsm"
)

type StateType int
const (
  IdleState StateType = iota+1
  MovingState
)

// Component
type UnitFSM struct {
  fsm *Fsm
  state StateType
  u *Unit
  stepTime float64
}

func NewUnitFSM(u *Unit) *UnitFSM {
  var (
    idle = State{"idle"}
    moving = State{"moving"}
  )
  transitions :=  map[State]map[State]bool{
    idle: {moving: true},
    moving: {idle: true},
  }
  return &UnitFSM{
    fsm: NewFsm(idle, transitions),
    state: IdleState,
    u: u,
  }
}

func (s *UnitFSM) State() StateType { return s.state}

func (s *UnitFSM) IdleState() {
  //println("unitfsm.idlestate")
  s.state = IdleState
  s.u.speed = 0
}

func (s *UnitFSM) MovingState() {
  //println("unitfsm.stepstate")
  s.state = StepState
  s.stepTime = 0
  s.u.speed = s.u.maxSpeed
}

func (s *UnitFSM) Update(dt float64) {
  u := s.u

  // Update states
  switch s.state {
  case StepState:
    // Check
    if s.stepTime == 0 {
       s.stepTime = game.timer.time
    }

    // Total time of travel A -> B [constant]
    tt := (cell_size / u.speed) * 10
    //sPos := Pos{u.x * cell_size, u.y * cell_size}
    sPos := u.Pos2()
    dPos := u.dPos

    step := (game.timer.time - s.stepTime) / tt
    nextPos := Pos{geomLerp(sPos.x, dPos.x, step),
                   geomLerp(sPos.y, dPos.y, step)}
    //fmt.Println("step", fmt.Sprintf("%f", step))
    //fmt.Println(fmt.Sprintf("%f %f", game.timer.time, s.stepTime))
    if Round(step, 2) < 1.0 {
      u.SetPos2(nextPos)
    } else {
      u.SetPos2(*u.dPos)
      u.dPos = nil
      /*
      u.x = u.pos.x // Cell_size
      u.y = u.pos.z // Cell_size
      */
      //u.cpos = CPos{u.pos.x / cell_size, u.pos.y / cell_size}
      //return "idle"
      s.IdleState()
    }
  }
}
`
