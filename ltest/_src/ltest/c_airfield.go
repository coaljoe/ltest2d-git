package main

import "lib/ecs"

// Component
type Airfield struct {
	units *UnitContainer
}

func newAirfield(en *ecs.Entity) *Airfield {
	af := &Airfield{
		units: newUnitContainer(),
	}
	en.AddComponent(af)
	return af
}

func (a *Airfield) addUnit(u UnitI) bool {
	a.units.addItem(u)
	return true
}

func (a *Airfield) removeUnit(u UnitI) bool {
	a.units.removeItem(u)
	return true
}

func (a *Airfield) show() {
	println("Airfield component:\n units:")
	a.units.listItems()
}
