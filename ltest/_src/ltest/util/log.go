package main

import (
	"bytes"
	"fmt"
	"log"
)

//var logger

func init() {
	println("util.log init")
}

func Log(msg string) {
	var buf bytes.Buffer
	logger := log.New(&buf, "log: ", log.Lshortfile)
	logger.Print(msg)

	fmt.Print(&buf)
}
