package main

// AI
// Unit AI moving behavior

import (
	. "fmt"
	"math/rand"

	"lib/ecs"

	js "github.com/bitly/go-simplejson"
	"github.com/looplab/fsm"
)

type MovingBhvI interface {
	update(dt float64)
	loadFromData(def *js.Json) bool
}

type BaseMovingBhv struct {
	name string
}

func newBaseMovingBhv(en *ecs.Entity, name string) *BaseMovingBhv {
	bmb := &BaseMovingBhv{
		name: name,
	}
	return bmb
}

/***** Standard Moving Behavior *****/

type StandardMovingBhv struct {
	*BaseMovingBhv
	//targetLocateRadius float64
	targetPursuitTime           float64
	engageDelay, disengageDelay float64
	engageDelayTimer,
	disengageDelayTimer *Timer
	engaged      bool
	target       *ecs.Entity
	savedIdlePos CPos
	fsm          *fsm.FSM
	en           *ecs.Entity
}

func newStandardMovingBhv(en *ecs.Entity) *StandardMovingBhv {
	mb := &StandardMovingBhv{
		BaseMovingBhv:       newBaseMovingBhv(en, "Standard"),
		engageDelayTimer:    newTimer(false),
		disengageDelayTimer: newTimer(false),
		engageDelay:         2.0,
		disengageDelay:      5.0,
		en:                  en,
	}

	mb.fsm = fsm.NewFSM("idle",
		fsm.Events{
			{Name: "engage", Src: []string{"idle"}, Dst: "engage"},
			{Name: "disengage", Src: []string{"engage"}, Dst: "disengage"},
			{Name: "disengage", Src: []string{"disengage"}, Dst: "idle"},
			//{Name: "disenging", Src: []string{"disengage"}, Dst: "idle"},
		},
		fsm.Callbacks{
			/*
				"engage": func(e fsm.Event) {
					println("after engage")
				},
			*/
			"enter_state": func(e *fsm.Event) { mb._enterState(e) },
		},
	)

	return mb
}

// Fsm switch
func (mb *StandardMovingBhv) _enterState(e *fsm.Event) {
	Printf("::FSM:: MB from %s to %s [cur: %s]\n", e.Src, e.Dst, e.FSM.Current())
	uc := mb.en.Get(UnitT).(*Unit)

	switch e.Dst {
	case "engage":
		// Start
		trg := mb.target
		tpos := c_Obj(trg).pos2()
		p(uc.cpos())
		uc.moveTo(CPos{int(tpos.X()) / cell_size, int(tpos.Y()) / cell_size})

	case "disengage":
		// Start
		tpos := mb.savedIdlePos
		p(tpos, uc.pos2())
		//uc.DropPath()
		//uc.MoveTo(CPos{10, 10})
		//uc.TruncatePathTo(CPos{10, 10})
		//uc.TruncatePathTo(mb.savedIdlePos)
		//uc.TruncatePathTo(CPos{int(tpos.x) / cell_size, int(tpos.y) / cell_size})
		//uc.MoveTo(CPos{int(tpos.x) / cell_size, int(tpos.y) / cell_size})
		uc.moveTo(mb.savedIdlePos)
	}
}

func (mb *StandardMovingBhv) update(dt float64) {
	cc := mb.en.Get(CombatT).(*Combat)

	// Do not perform any actions (fixme?)
	if !cc.canAttack() {
		//pp("derp", mb.en.Get(ObjT).(*Obj).name)
		return
	}

	uc := mb.en.Get(UnitT).(*Unit)

	/*
	   	var trg AttackableI
	   	for _, t := range cc.GetTargets() {
	   		if t.Realm() == CGroundRealm {
	   			trg = t
	   			if mb.engageDelayTimer.Zero() {
	   				mb.engageDelayTimer.Start()
	   			}
	   			break
	   		}
	   	}

	   	if trg != nil && mb.engageDelayTimer.Dt() > mb.engageDelay {
	   	//if (!mb.engaged) && mb.engageDelayTimer.Dt() > mb.engageDelay {
	   		tpos := trg.Pos2()
	       uc.MoveTo(CPos{int(tpos.x) / cell_size, int(tpos.y) / cell_size})
	       mb.engaged = true
	   		mb.engageDelayTimer.Reset()
	   	}
	*/
	//if mb.target == nil {
	mb.target = nil
	canEngage := false
	for _, t := range cc.getTargets() {
		realm := getRealm(t)
		//realmi := t.GetComponent(RealmI).(RealmI)
		//realm := realmi.getRealm()
		//if t.getRealm() == RealmType_Ground {
		if realm == RealmType_Ground {
			mb.target = t
			// Engage check
			dist := distancePos2(uc.pos2(), c_Obj(mb.target).pos2())
			if dist > cc.getMaxAttackRadius() && dist <= cc.getMaxLocateRadius() {
				canEngage = true
				break
			}
		}
	}
	//	break
	//}

	// Fsm update
	switch mb.fsm.Current() {
	case "idle":
		// Check
		if canEngage {
			// Remember last idle pos
			mb.savedIdlePos = uc.cpos()

			tr := mb.engageDelayTimer
			if tr.zero() {
				tr.start()
			} else if tr.dt() > mb.engageDelay {
				mb.fsm.Event("engage")
				tr.restart()
			}
			/*
						go func(){
							sleepsec(mb.engageDelay)
				    		mb.fsm.Event("engage")
						}()
			*/
		}

	case "engage":
		// Check
		//p(mb.disengageDelayTimer.Dt(), mb.disengageDelayTimer.Zero())
		if mb.target == nil || !c_Health(mb.target).isAlive() {

			// Stop unit
			if uc.Mover.hasPath() {
				uc.Mover.cancelPath()
			}

			tr := mb.disengageDelayTimer
			if tr.zero() {
				tr.start()
				//pp(2)
			} else if tr.dt() > mb.disengageDelay {
				//pp(tr.Dt())
				mb.fsm.Event("disengage")
				tr.restart()
			}

			/*
						go func(){
							sleepsec(mb.disengageDelay)
				    		mb.fsm.Event("disengage")
						}()
			*/
		}

	case "disengage":
		// Check
		if uc.cpos() == mb.savedIdlePos {
			mb.fsm.Event("idle")
		}
	}
	/*
			if cc.GetTargets() > 0 {
		  	if mb.engageDelayTimer.Dt() > mb.engageDelay ||
		    	 mb.engageDelayTimer.Zero() { // First run(?)
		    	cc.LocateTargets()
		    	cc.locateTargetsTimer.Reset()
		  	}
		  }
	*/
}

func (mb *StandardMovingBhv) loadFromData(def *js.Json) bool {
	//if d, ok := def.CheckGet("targetLocateRadius"); ok {
	//  mb.targetLocateRadius = d.MustFloat64() } else { pp("fail") }
	if d, ok := def.CheckGet("targetPursuitTime"); ok {
		mb.targetPursuitTime = d.MustFloat64()
	} else {
		pp("fail")
	}
	return true
}

/***** Scout Moving Behavior *****/

type ScoutMovingBhv struct {
	*BaseMovingBhv
	en *ecs.Entity
}

func newScoutMovingBhv(en *ecs.Entity) *ScoutMovingBhv {
	mb := &ScoutMovingBhv{
		BaseMovingBhv: newBaseMovingBhv(en, "Scout"),
		en:            en,
	}
	return mb
}

func (mb *ScoutMovingBhv) loadFromData(def *js.Json) bool { return false }

func (mb *ScoutMovingBhv) update(dt float64) {
	uc := mb.en.Get(UnitT).(*Unit)
	if !uc.Mover.hasPath() {
		x := rand.Intn(_Field.w)
		y := rand.Intn(_Field.h)
		uc.moveTo(CPos{x, y})
	}
}

/***** Hold-Position Moving Behavior *****/

type HoldMovingBhv struct {
	*BaseMovingBhv
	en *ecs.Entity
}

func newHoldMovingBhv(en *ecs.Entity) *HoldMovingBhv {
	mb := &HoldMovingBhv{
		BaseMovingBhv: newBaseMovingBhv(en, "Hold"),
		en:            en,
	}
	return mb
}

func (mb *HoldMovingBhv) loadFromData(def *js.Json) bool { return false }

func (mb *HoldMovingBhv) update(dt float64) {
	// Do nothing
}

/***** functions *****/

func makeMovingBhv(name string, en *ecs.Entity) MovingBhvI {
	switch name {
	case "Standard":
		return newStandardMovingBhv(en)
	case "Scout":
		return newScoutMovingBhv(en)
	case "Hold":
		return newHoldMovingBhv(en)
	default:
		panic("unknown name: " + name)
	}
}
