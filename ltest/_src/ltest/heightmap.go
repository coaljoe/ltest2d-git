// Height map for field generation.
package main

import (
	"fmt"
	"image"
	"image/color"
	"image/draw"
	"image/png"
	"os"

	"github.com/drbig/perlin"
)

type HeightMapOptions struct {
	Alpha   float64 // Alpha Factor
	Beta    float64 // Beta Factor
	Octaves int     // Number of octaves
	Seed    int64   // Random Seed

	NoiseScale float64 // Noise Scale
}

// Default HeightMap options.
func newHeightMapOptions() HeightMapOptions {
	return HeightMapOptions{
		Alpha:      1.5,
		Beta:       2,
		Octaves:    6,
		Seed:       41,
		NoiseScale: 4.0,
	}
}

type HeightMap struct {
	data        [][]int // -n.. 0.. +n; 0 - sea/ground level(?)
	w, h        int
	levelStep   int // 256/8 // 32
	numLevels   int // 8
	minLevel    int // -4
	maxLevel    int // +4
	initialized bool
}

func newHeightMap() HeightMap {
	hm := HeightMap{
		levelStep:   256 / 8, // / 32,
		initialized: false,
	}
	hm.numLevels = 256 / hm.levelStep
	hm.minLevel = -(hm.numLevels / 2)
	hm.maxLevel = hm.numLevels / 2
	return hm
}

func (hm *HeightMap) generate(w, h int, opt HeightMapOptions) {
	hm.w = w
	hm.h = h

	// Fill data
	//data := hm.data
	hm.data = make([][]int, w)
	for i := range hm.data {
		hm.data[i] = make([]int, h)
	}
	//pp(hm.data)
	//pp(data[w][h])
	//pp(data[w-1][h-1])
	//pp(len(data))
	//hm.data = data

	// ./perlin -c -a 2 -b 2 -h 128 -w 128 -n 10 -s 3 -r 41 ex.png
	//g := perlin.NewGenerator(2, 2, 10, 41)
	//g := perlin.NewGenerator(1.5, 2, 6, 41)
	//g := perlin.NewGenerator(2, 2, 4, 41)
	g := perlin.NewGenerator(opt.Alpha, opt.Beta, opt.Octaves, opt.Seed)

	noiseScale := opt.NoiseScale //4.0
	sx := noiseScale / float64(hm.w)
	sy := noiseScale / float64(hm.h)

	for x := 0; x < hm.w; x++ {
		for y := 0; y < hm.h; y++ {
			//rmin, rmax := -1.0, 1.0
			// Normalize fw/fh range to -1, 1
			//nx := -1.0 + float64(x)/(float64(hm.w)/2.0)
			//ny := -1.0 + float64(y)/(float64(hm.h)/2.0)
			nx := float64(x) * sx
			ny := float64(y) * sy
			nv := g.Noise2D(nx, ny)
			v := int(128.0 - (nv*256)/2)
			//p(v)
			//fmt.Printf("%v ", v)
			// Map values in -4 .. +4 range, 0 is ground level 0
			//levelStep := 256/8 // 32
			//defaultLevel := 4
			defaultLevel := 4
			//defaultLevel := 3
			v = (v / hm.levelStep) - defaultLevel
			hm.data[x][y] = v
			//f.cells[x][y].z = v
		}
	}
	//p(hm.data)
	hm.initialized = true
	//pp(2)
}

func (hm *HeightMap) load(path string) {
	_log.Inf("[HeightMap] load; path:", path)
	r, err := os.Open(path)
	if err != nil {
		println("cant read file: " + path)
		panic(err)
	}
	defer r.Close()

	im, _, err := image.Decode(r)
	if err != nil {
		panic(err)
	}

	tmp := im.Bounds()
	iw, ih := tmp.Dx(), tmp.Dy()
	if hm.w != iw || hm.h != ih {
		pp("image and hightmap sizes don't match")
	}

	// Convert to grayscale
	im2 := image.NewGray(im.Bounds())
	draw.Draw(im2, im.Bounds(), im, image.Pt(0, 0), draw.Src)

	for x := 0; x < iw; x++ {
		for y := 0; y < ih; y++ {
			v := int(im2.GrayAt(x, y).Y)
			//v := 0
			//p("v:", v)

			// XXX: fixme use value pack/reconstruct functions
			// Map values in -4 .. +4 range, 0 is ground level 0
			levelStep := 256 / 8 // 32
			defaultLevel := 4
			v = (v / levelStep) - defaultLevel
			hm.data[x][y] = v
			//f.cells[x][y].z = ZLevel(v)
		}
	}
}

func (hm *HeightMap) save(path string) {
	_log.Inf("[HightMap] save; path:", path)
	f, err := os.Create(path)
	if err != nil {
		println("cant create file: " + path)
		panic(err)
	}
	defer f.Close()

	rect := image.Rect(0, 0, hm.w, hm.h)
	im := image.NewGray(rect)

	for x := 0; x < hm.w; x++ {
		for y := 0; y < hm.h; y++ {
			//p(hm.data[x][y], uint8(hm.data[x][y]))
			// Reconstructed value
			rv := 128 + (hm.data[x][y] * hm.levelStep)
			c := color.Gray{Y: uint8(rv)}
			im.SetGray(x, y, c)
		}
	}

	png.Encode(f, im)

	_log.Inf("heightmap saved to: ", path)
}

func (hm *HeightMap) saveTxt(path string) {
	_log.Inf("[HightMap] saveTxt; path:", path)
	f, err := os.Create(path)
	if err != nil {
		println("cant create file: " + path)
		panic(err)
	}
	defer f.Close()

	// Write header
	f.WriteString(fmt.Sprintf("# hm.w: %d, hm.h: %d\n", hm.w, hm.h))
	f.WriteString("# 0")
	for x := 1; x < hm.w; x++ {
		f.WriteString(fmt.Sprintf("%3d", x))
	}
	f.WriteString("\n\n\n")

	// Write data
	for y := 0; y < hm.h; y++ {
		for x := 0; x < hm.w; x++ {
			f.WriteString(fmt.Sprintf(" %2d", hm.data[x][y]))
		}
		f.WriteString("\n")
	}

	_log.Inf("heightmap txt saved to: ", path)
}

func (hm *HeightMap) clear() {
	if !hm.initialized || len(hm.data) == 0 {
		pp("heightmap is not initialized;",
			hm.initialized, len(hm.data))
	}
	//pp(len(hm.data), hm.w, hm.h, hm.w*hm.h)
	//pp(hm.data)
	//pp(hm.data[0][0])
	for x := 0; x < hm.w; x++ {
		for y := 0; y < hm.h; y++ {
			//p(x, y)
			hm.data[x][y] = 0
		}
	}
}
