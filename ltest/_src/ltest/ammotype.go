// Тип боеприпаса.
package main

type HitType int
type AffectType int

const (
	HitType_Explotion = iota
	HitType_None
	AffectType_Normal = iota
	AffectType_Fire
	AffectType_Toxic
)

// Wiki://Компоненты/AmmoType
type AmmoType struct {
	// Props
	caliber          float64    // калибр
	cost             int        // стоимость
	moveSpeed        float64    // скорость перед.
	affectRadius     int        // радиус поражения
	affectRadiusFade float64    // скорость затухания
	attackRating     int        // оценка атаки
	radius           float64    // радиус действия
	locateRadius     float64    // радиус обнаружения (используется MovingBhv)
	hitType          HitType    // тип попадания
	affectType       AffectType // тип поражения
	reliability      float64    // надежность
	accuracy         float64    // точность
	weight           float64    // вес
	projectileType   string     // тип снаряда
}

func newAmmoType() *AmmoType {
	t := &AmmoType{
		affectRadius:     1, // поражение в радиусе одной клетки
		affectRadiusFade: 0,
		attackRating:     0, // не может атаковать
		hitType:          HitType_Explotion,
		affectType:       AffectType_Normal,
		reliability:      1.0,
		accuracy:         1.0,
		projectileType:   "shell", // Shell by default
	}
	return t
}
