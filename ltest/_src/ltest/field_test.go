// +build static
// Is a static test.

package main

import (
	"fmt"
	"testing"
)

func TestField(t *testing.T) {
	initTest()

	//f := NewField(128, 128)
	//f := newField()
	f := game.field
	_ = f

	fsize := 128
	fw, fh := fsize, fsize
	hmW := fw * 2
	hmH := fh * 2
	//hmScale := 4.0
	hmOpt := newHeightMapOptions()
	fgOpt := newFieldGenerateOptions()
	fgOpt.disableEverything = true
	f.generate(fw, fh, hmW, hmH, hmOpt, fgOpt)

	r := f.isCellOpen(0, 0)
	fmt.Println("r", r)

	r = f.isCellOpen(1, 1)
	fmt.Println("r", r)

	f.cells[0][0].open = false
	r = f.isCellOpen(0, 0)
	fmt.Println("r", r)

	// Misc functions

	var tm = []struct {
		h []Cell
		w int
	}{
		{f.getRectSlice(1, 1, 3, 3), 9},   // Must be 9
		{f.getRectSlice(0, 0, 3, 3), 9},   // Must be 9
		{f.getRectSlice(-1, -1, 3, 3), 4}, // Must be 4
	}
	var _ = `
	//cs := f.getRectSlice(1, 1, 3, 3) // Must be 9
	cs := f.getRectSlice(0, 0, 3, 3) // Must be 4
	//cs := f.getRectSlice(-1, -1, 3, 3) // Must be 1
	fmt.Println(cs)
	//if len(cs) != 9 {
	if len(cs) != 4 {
		t.Errorf("bad getRectSlice, lenght: %d", len(cs))
		t.FailNow()
	}
	`

	for num, tt := range tm {
		cs := tt.h
		if len(cs) != tt.w {
			fmt.Println("cs:", cs)
			t.Errorf("bad getRectSlice #%d, lenght: %d, want: %d",
				num, len(cs), tt.w)
			t.FailNow()
		}
	}

	var ns []Cell
	ns = f.getCellNeighbours(CPos{0, 0})
	//ns = f.getCellNeighbours(CPos{-1, -1})
	if len(ns) != 3 {
		fmt.Println("ns:", ns)
		t.Errorf("bad getCellNeighbours at corner, lenght: %d", len(ns))
		t.FailNow()
	}

	ns = f.getCellNeighbours(CPos{1, 1})
	if len(ns) != 8 {
		//fmt.Println(ns)
		//t.Fail()
		t.Errorf("bad getCellNeighbours, length: %d", len(ns))
		t.FailNow()
	}

	/*
		yyyyy
		yxxxy
		yxoxy
		yxxxy
		yyyyy
	*/
	ns = f.getCellNeighboursDepth(CPos{2, 2}, 2)
	if len(ns) != 24 {
		//fmt.Println(ns)
		//t.Fail()
		t.Errorf("bad getCellNeighboursDepth depth=2, length: %d", len(ns))
	}

	ns = f.getCellNeighboursDepth(CPos{3, 3}, 3)
	if len(ns) != (7*7)-1 {
		t.Error("bad getCellNeighboursDepth depth=3, length: %d", len(ns))
	}

}
