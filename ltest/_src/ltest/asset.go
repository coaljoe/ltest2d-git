package main

import (
	_path "path"
	"rx"
)

// Asset - уже загруженные ноды с данными, не просто ресурс

type Asset struct {
	name, path, dir string
	nodes           map[string]*rx.Node
}

func newAsset() *Asset {
	a := &Asset{
		nodes: make(map[string]*rx.Node),
	}
	return a
}

func (a *Asset) load(path string) {
	a.dir = _path.Dir(path)
	a.path = path

	sl := rx.NewSceneLoader()
	nodes := sl.Load(path)
	/*
	  for i, n := range nodes {
	    p(i, n.Name(), n.MeshName())
	  }
	*/
	for _, n := range nodes {
		a.addNode(n)
	}
}

func (a *Asset) getNode(name string) *rx.Node {
	if !a.hasNode(name) {
		a.printNodes()
		panic("no such node; name: " + name)
	}
	return a.nodes[name]
}

func (a *Asset) addNode(n *rx.Node) {
	if a.hasNode(n.Mesh.MeshName()) {
		panic("already have node; name: " + n.Mesh.MeshName())
	}
	if n.Mesh.MeshName() == "" {
		panic("empty mesh name")
	}
	a.nodes[n.Mesh.MeshName()] = n
}

func (a *Asset) hasNode(name string) bool {
	return a.nodes[name] != nil
}

func (a *Asset) printNodes() {
	println("Asset.PrintNodes begin")
	for k := range a.nodes {
		println("  " + k)
	}
	println("Asset.PrintNodes end")
}

func (a *Asset) free() {
	println("Asset.Free not implemented")
}
