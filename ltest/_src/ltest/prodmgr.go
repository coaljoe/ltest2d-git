package main

import (
	. "fmt"
)

/* ProdInfo */

type ProdInfo struct {
	typename  string
	costMoney int
	costFuel  int
	buildTime float64
}

func getProdInfo(typename string) *ProdInfo {
	switch {

	case typename == "dumptruck":
		return &ProdInfo{
			typename:  typename,
			costMoney: 50,
			costFuel:  0,
			buildTime: 5.0,
		}

	case typename == "supplytruck":
		return &ProdInfo{
			typename:  typename,
			costMoney: 100,
			costFuel:  0,
			buildTime: 10.0,
		}

	default:
		panic(Sprintf("error: unknown typename=%s", typename))
	}
}

/* ProdItem */

type ProdQueueItem struct {
	typename  string
	timeAdded float64
}

func (it *ProdQueueItem) show() {
	println("ProdQueueItem\n typename:", it.typename,
		"\n timeAdded:", it.timeAdded)
}

/* ProdQueue */

type ProdQueue struct {
	items []*ProdQueueItem
	camp  *Camp
}

func newProdQueue(camp *Camp) *ProdQueue {
	return &ProdQueue{
		camp: camp,
	}
}

func (q *ProdQueue) canProduce(typename string) bool {
	pi := getProdInfo(typename)
	if pi.costMoney <= q.camp.money.amt {
		return true
	} else {
		return false
	}
}

func (q *ProdQueue) addProd(typename string) {
	it := &ProdQueueItem{
		typename:  typename,
		timeAdded: 0, // Fixme: Now
	}
	q.items = append(q.items, it)
}

func (q *ProdQueue) popItem() *ProdQueueItem {
	a := q.items
	var x *ProdQueueItem
	//  pop
	x, a = a[len(a)-1], a[:len(a)-1]
	q.items = a
	return x
}

func (q *ProdQueue) produceLast() *ProdQueueItem {
	it := q.popItem()
	pi := getProdInfo(it.typename)
	// Make charges
	q.camp.money.take(pi.costMoney)
	q.camp.fuel.take(pi.costFuel)
	return it
}

func (q *ProdQueue) list() {
	println("ProdQueue.List")
	for i, it := range q.items {
		Printf("%d. \n", i)
		it.show()
	}
}

func (q *ProdQueue) isEmpty() bool {
	return len(q.items) == 0
}

/* ProdMgr */

type ProdMgr struct {
	prodqs []*ProdQueue
}

func newProdMgr() *ProdMgr {
	return &ProdMgr{}
}

func (pm *ProdMgr) addProdQueue(q *ProdQueue) {
	pm.prodqs = append(pm.prodqs, q)
}
