package main

import "rx"

type FieldObjectView struct {
	node    *rx.Node
	m       *FieldObject
	ddir    string // DataDir
	loaded  bool
	visible bool
	static  bool // Is the view static
}

func newFieldObjectView(m *FieldObject) *FieldObjectView {
	v := &FieldObjectView{m: m,
		visible: true,
		static:  false}
	v.load()
	return v
}

func (v *FieldObjectView) load() {
	if v.loaded {
		return
	}
	v.ddir = "res/field/field_objects/" + v.m.typ

	/*
		if nodes, ok := rx.LoadResource(v.ddir + "/model.dae"); ok {

		}
	*/
	//v.node := rx.LoadResource(v.ddir + "/model.dae")[0]

	rxi := rx.Rxi()
	v.node = rxi.ResourceLoader.LoadScene(v.ddir + "/model.dae")[0]
	rxi.Scene().Add(v.node)

	// Tweak texture options
	tex := v.node.Mesh.Material().Texture()
	if tex != nil {
		tex.SetOpts(
			// Set nearest+mip filter
			rx.TextureOptions{
				//MinFilter: rx.TextureFilterNearest,
				//MagFilter: rx.TextureFilterNearest,
				MinFilter: rx.TextureFilterNearest,
				MagFilter: rx.TextureFilterNearest,
				Nomipmaps: true})
	}
}

func (v *FieldObjectView) update(dt float64) {
	if !v.visible || v.static {
		//pp("derp")
		return
	}
	m := v.m

	// Update node's pos
	v.node.SetPos(m.Pos())

	// Update node's rot
	v.node.SetRot(m.Rot())

	// Force static: update once
	v.static = true
}
