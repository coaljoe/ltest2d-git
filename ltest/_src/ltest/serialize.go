package main

// Правила сериализации:
// * Объекты должны сохраняться/восстанавливаться вместе
// с их системами.
//

import (
	"bytes"
	"encoding/gob"
	"encoding/json"
	"fmt"
	"lib/sr"
	"reflect"
)

func init() {
	p("XXX WARN: serialize init")
	sr.Register(&Obj{})
	sr.Register(&Position{})
}

// Timer

func (t *Timer) GobEncode() ([]byte, error) {
	w := new(bytes.Buffer)
	enc := gob.NewEncoder(w)
	err := enc.Encode(t.id)
	if err != nil {
		panic(err)
	}
	err = enc.Encode(t.speed)
	if err != nil {
		panic(err)
	}
	_gobEnc(enc, t.time)
	_gobEnc(enc, t.start_time)
	_gobEnc(enc, t.running_)
	return w.Bytes(), nil
}

func (t *Timer) GobDecode(buf []byte) error {
	r := bytes.NewBuffer(buf)
	dec := gob.NewDecoder(r)
	err := dec.Decode(&t.id)
	if err != nil {
		panic(err)
	}
	err = dec.Decode(&t.speed)
	if err != nil {
		panic(err)
	}
	_gobDec(dec, &t.time)
	_gobDec(dec, &t.start_time)
	_gobDec(dec, &t.running_)
	return nil
}

// TimerSys

func (s *TimerSys) GobEncode() ([]byte, error) {
	w, enc := _gobNewEnc()
	_gobEnc(enc, s.timers)
	return w.Bytes(), nil
}

func (s *TimerSys) GobDecode(buf []byte) error {
	dec := _gobNewDec(buf)
	_gobDec(dec, &s.timers)
	return nil
}

// Health

func (x *Health) GobEncode() ([]byte, error) {
	w, enc := _gobNewEnc()
	_gobEnc(enc, x.health_)
	_gobEnc(enc, x.maxHealth)
	return w.Bytes(), nil
}

func (x *Health) GobDecode(buf []byte) error {
	dec := _gobNewDec(buf)
	_gobDec(dec, &x.health_)
	_gobDec(dec, &x.maxHealth)
	return nil
}

// Transform

func (t *Transform) GobEncode() ([]byte, error) {
	return sr.SerializeByFields(
		t.Transform,
	), nil
}

func (t *Transform) GobDecode(b []byte) error {
	sr.DeserializeByFields(b,
		&t.Transform,
	)
	// XXX fixme
	sr.Finalize(t)
	return nil
}

// Obj

func (o *Obj) GobEncode() ([]byte, error) {
	return sr.SerializeByFields(
		o.Component,
		o.Transform,
		o.id, o.name, o.active,
		o.objtype, o.selectable,
	), nil
}

func (o *Obj) GobDecode(b []byte) error {
	sr.DeserializeByFields(b,
		&o.Component,
		&o.Transform,
		&o.id, &o.name, &o.active,
		&o.objtype, &o.selectable,
	)
	return nil
}

// ObjSys

func (x *ObjSys) GobEncode() ([]byte, error) {
	return sr.SerializeByFields(
		x.GameSystem,
	), nil
}

func (x *ObjSys) GobDecode(b []byte) error {
	sr.DeserializeByFields(b,
		&x.GameSystem,
	)
	return nil
}

// GameSystem

func (x *GameSystem) GobEncode() ([]byte, error) {
	return sr.SerializeByFields(
		x.System,
	), nil
}

func (x *GameSystem) GobDecode(b []byte) error {
	sr.DeserializeByFields(b,
		&x.System,
	)
	return nil
}

// System

func (x *System) GobEncode() ([]byte, error) {
	mElems := x.getElems()
	_ = mElems
	return sr.SerializeByFields(
		x.name, x.desc, mElems,
		//x.elems,
	), nil
}

func (x *System) GobDecode(b []byte) error {
	var mElems []interface{}
	_ = mElems
	sr.DeserializeByFields(b,
		&x.name, &x.desc, &mElems,
		//x.elems,
	)
	sr.Finalize(x)
	x.setElems(mElems)
	return nil
}

// Position

func (x *Position) GobEncode() ([]byte, error) {
	return sr.SerializeByFields(
		x.Component,
		x.dPos_, x.speed, &x.dir,
		x.stepTime, x.mtype, x.i,
	), nil
}

func (x *Position) GobDecode(b []byte) error {
	sr.DeserializeByFields(b,
		&x.Component,
		&x.dPos_, &x.speed, &x.dir,
		&x.stepTime, &x.mtype, &x.i,
	)
	return nil
}

// PositionSys

func (x *PositionSys) GobEncode() ([]byte, error) {
	return sr.SerializeByFields(
		x.GameSystem,
	), nil
}

func (x *PositionSys) GobDecode(b []byte) error {
	sr.DeserializeByFields(b,
		&x.GameSystem,
	)
	return nil
}

// Dir

func (x *Dir) GobEncode() ([]byte, error) {
	return sr.SerializeByFields(
		x.rot, x._sRot, x._dRot,
		x.rotSpeed, x.rotDir,
	), nil
}

func (x *Dir) GobDecode(b []byte) error {
	sr.DeserializeByFields(b,
		&x.rot, &x._sRot, &x._dRot,
		&x.rotSpeed, &x.rotDir,
	)
	return nil
}

// Gob helpers

func _gobNewEnc() (*bytes.Buffer, *gob.Encoder) {
	w := new(bytes.Buffer)
	enc := gob.NewEncoder(w)
	return w, enc
}
func _gobNewDec(buf []byte) *gob.Decoder {
	r := bytes.NewBuffer(buf)
	dec := gob.NewDecoder(r)
	return dec
}

func _gobEnc(enc *gob.Encoder, v interface{}) {
	err := enc.Encode(v)
	if err != nil {
		panic(err)
	}
}

func _gobDec(dec *gob.Decoder, v interface{}) {
	err := dec.Decode(v)
	if err != nil {
		panic(err)
	}
}

/////////////////
// Json version

func storeStruct(args ...interface{}) map[string]interface{} {
	m := make(map[string]interface{})

	di := args[0]
	d := reflect.ValueOf(di)
	_ = d
	m["__type"] = fmt.Sprintf("%T", di)
	m["__ptr"] = fmt.Sprintf("%p", di)
	//m["__name"] = reflect.TypeOf(di).Elem().Name()
	typeName := ""
	if t := reflect.TypeOf(di); t.Kind() == reflect.Ptr {
		typeName = t.Elem().Name() // Can add "*" here
	} else {
		typeName = t.Name()
	}
	m["__name"] = typeName
	//pp(m)

	storePtrsAsAddrs := true

	for i := 1; i < len(args); {
		k := args[i].(string)
		v := args[i+1]

		if storePtrsAsAddrs {
			if t := reflect.TypeOf(v); t.Kind() == reflect.Ptr {
				// Replace v with its addr
				//v = fmt.Sprintf("%p", reflect.ValueOf(v).Addr().Pointer())
				v = fmt.Sprintf("0x%x", reflect.ValueOf(v).Elem().Addr().Pointer())
			}
			//if t := reflect.TypeOf(v); t.Kind() == reflect.Map {
			//	v = storeStruct(v)
			//}
		}

		m[k] = v

		i += 2
	}

	// Pack into another map
	//mt := make(map[string]interface{})
	//mt[typeName] = m

	return m
}

func structToJson(m map[string]interface{}) string {
	bytes, err := json.Marshal(m)
	if err != nil {
		panic(err)
	}
	return string(bytes)
}

func restoreStruct(s string) map[string]interface{} {
	var m map[string]interface{}
	json.Unmarshal([]byte(s), &m)
	return m
}

func m_int(m map[string]interface{}, k string) int {
	return int(m[k].(float64))
}

func m_float(m map[string]interface{}, k string) float64 {
	return m[k].(float64)
}

func m_bool(m map[string]interface{}, k string) bool {
	return m[k].(bool)
}

func storeGame() string {
	st := game
	m := storeStruct(st,
		"timer", st.timer,
		//"timer", storeTimer(st.timer),
		"initialized", st.initialized,
		"gameCreated", st.gameCreated,
		"frame", st.frame)
	js := structToJson(m)
	p(js)
	return js
}

func restoreGame(s string) {

}

func storeTimer(st *Timer) string {
	m := storeStruct(st,
		"id", st.id,
		"speed", st.speed,
		"time", st.time,
		"start_time", st.start_time,
		"running_", st.running_)
	js := structToJson(m)
	p(js)
	return js
}

func restoreTimer(s string) *Timer {
	m := restoreStruct(s)
	//st := newTimer(false)
	st := &Timer{}
	st.id = m_int(m, "id")
	st.speed = m_float(m, "speed")
	st.time = m_float(m, "time")
	st.start_time = m_float(m, "start_time")
	st.running_ = m_bool(m, "running_")
	return st
}

func storeTimerSys(st *TimerSys) string {
	m := storeStruct(st,
		"timers", st.timers)
	pp(m)
	js := structToJson(m)
	p(js)
	return js
}
