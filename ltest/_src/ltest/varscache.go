package main

// Global varsCache. Fixme: remove?
var varsCache VarsCache

func init() {
	varsCache = make(VarsCache, 0)
}

type VarsCacheRec struct {
	addedTime  float64
	expireTime float64
	value      interface{}
}

type VarsCache map[string]VarsCacheRec

func newVarsCache() VarsCache {
	return VarsCache{}
}

func (vc *VarsCache) put(name string, expireTime float64, value interface{}) {
	(*vc)[name] = VarsCacheRec{game.timer.time, expireTime, value}
}

func (vc VarsCache) getNotExpired(name string) (bool, interface{}) {
	rec := vc[name]
	if game.timer.dt() > (rec.addedTime + rec.expireTime) {
		return false, nil
	}
	return true, rec.value
}

/*
func (vc *varsCache) update(dt float64) {
	for k, v := vc {

	}
}
*/

//type VarsCache struct {
//
//}
