package main

import "fmt"

type A struct {
	y uint
}

type Z struct {
	x uint
	a A
}

func NewZ() Z {
	return Z{x: 2, a: A{y: 2}}
}

func (z *Z) ChangeZ() {
	z.x -= 1
	z.a.y -= 1
}

func (a *A) ChangeA() {
	a.y = 100
}

func (z Z) testZ() {
	fmt.Println("Z:", z)
}

func main() {
	z := NewZ()
	z2 := NewZ()
	fmt.Println("Z:", z)
	z.ChangeZ()
	fmt.Println("Z:", z)
	z.ChangeZ()
	fmt.Println("Z:", z)
	z.a.ChangeA()
	//fmt.Println("Z:", z)
	//z.ChangeA()
	fmt.Println("Z:", z)
	z.testZ()
	z2.testZ()
}
