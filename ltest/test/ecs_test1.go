package main

// components
type Chimney struct {
}

func (c *Chimney) Render() {
	println("chimney smoking")
}

type Windows struct {
}

func (c *Windows) Render() {
	println("windows blinking")
}

// entity
type Building struct {
	*Chimney // optional
	*Windows // optional
	Name     string
}

func (b *Building) Render() {
	println(b.Name)
	if b.Chimney != nil {
		println("has chimney")
	}
	if b.Windows != nil {
		println("has windows")
	}
}

func (b *Building) Destroy() {
	println("destroy", b.Name)
}

func main() {
	b1 := &Building{
		Name:    "Housing",
		Windows: &Windows{},
	}
	b1.Render()

	b2 := &Building{
		Name:    "Factory",
		Windows: &Windows{},
		Chimney: &Chimney{},
	}
	b2.Render()

	b1.Destroy()
	b2.Destroy()

	b2.Chimney.Render()
	b2.Windows.Render()

	b1.Chimney.Render() // no chimney, should be error or component checking
	b1.Windows.Render()
}
