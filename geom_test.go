package main_module

import (
	"fmt"
	"testing"
)

func TestGeom(t *testing.T) {

	fw, fh := game.field.size()
	fmt.Println("f.size: ", fw, fh)

	d := wrapCellDistX(0, 2)
	fmt.Println("d:", d)

	d2 := wrapCellDistX(0, fw-2)
	fmt.Println("d2:", d2)

}