package main_module

import (
	"fmt"
)

type Cell struct {
	// Redundant but useful. use with caution.
	cx, cy int
	z      ZLevel
	// Open for travel
	open       bool
	river      RiverState
	shore      ShoreState
	tileId     int
	buildingId int
	// 0 - no building (area), 1 - building
	buildingMask bool
	unitId int
	// Cell holder, default nil (no-one)
	//playerId int
	//holder *Player // XXX don't use pointers?
	// Holder player id (-1 = no-one)
	holderId int8
}
func (c *Cell) Cx() int { return c.cx }
func (c *Cell) Cy() int { return c.cy }
func (c *Cell) Z() ZLevel { return c.z }
func (c *Cell) Open() bool { return c.open }
func (c *Cell) TileId() int { return c.tileId }

func newCell() Cell {
	c := Cell{
		tileId:     -1,
		buildingId: -1,
		unitId: -1,
		holderId: -1,
	}
	return c
}

// Walkable for ground units
// doesn't take buildings or other non-field data into account?
func (c Cell) Walkable() bool {
	if !(c.z.IsGround() && !c.z.IsRock() && c.shore == ShoreState_None) {
		return false
	}
	if c.z == ZLevel_Ground0 && !(c.tileId == 46 || c.tileId == 0) {
		return false
	}
	return true
	//(c.z == ZLevel_Ground0 && (c.tileId == 46 || c.tileId == 0))
}

func (c Cell) IsShore() bool {
	return c.shore != ShoreState_None
}

// Is the cell hold by a player.
func (c Cell) IsHoldByPlayer(pl *Player) bool {
	return c.holderId == int8(pl.id)
}

func (c Cell) HasBuilding() bool {
	return c.buildingId != -1
}

func (c Cell) GetBuilding() *Building {
	return game.buildingsys.getBuildingById(c.buildingId)
}

func (c Cell) HasUnit() bool {
       return c.unitId != -1
}

func (c Cell) GetUnit() *Unit {
       return game.unitsys.getUnitById(c.unitId)
}

func (c Cell) String() string {
	return fmt.Sprintf("Cell<%d,%d>", c.cx, c.cy)
}

func (c Cell) printNeighbours() {
	p("Cell.printNeighbours;", c)
	neighbours := game.field.getCellNeighbours(c.cx, c.cy, true)
	for y := 0; y < 3; y++ {
		for x := 0; x < 3; x++ {
			fmt.Printf("%#v", neighbours[y*3+x])
			if x < 2 {
				print(" | ")
			}
		}
		println()
	}
}
