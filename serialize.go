package main_module

import (
	"kristallos.ga/lib/sr"
	"bytes"
	"encoding/gob"
)

func init() {
	p("XXX WARN: serialize init")
	sr.Register(&Obj{})
	sr.Register(&Position{})
	sr.Register(&Cell{})
	sr.Register(&[][]Cell{})
	sr.Register(&HeightMap{})
	sr.Register(&FieldGen{})
	sr.Register(&[][]int{})
	sr.Register([8]int{})
	sr.Register(map[string]int{})
	sr.Register(map[int][]CRect{})
	sr.Register(ZLevel(0)) // XXX
	sr.Register(RiverState(0)) // XXX
	sr.Register(ShoreState(0)) // XXX
	//pp(2)
}

// Timer

func (t *Timer) GobEncode() ([]byte, error) {
	w := new(bytes.Buffer)
	enc := gob.NewEncoder(w)
	err := enc.Encode(t.id)
	if err != nil {
		panic(err)
	}
	err = enc.Encode(t.speed)
	if err != nil {
		panic(err)
	}
	_gobEnc(enc, t.time)
	_gobEnc(enc, t.start_time)
	_gobEnc(enc, t.running_)
	return w.Bytes(), nil
}

func (t *Timer) GobDecode(buf []byte) error {
	r := bytes.NewBuffer(buf)
	dec := gob.NewDecoder(r)
	err := dec.Decode(&t.id)
	if err != nil {
		panic(err)
	}
	err = dec.Decode(&t.speed)
	if err != nil {
		panic(err)
	}
	_gobDec(dec, &t.time)
	_gobDec(dec, &t.start_time)
	_gobDec(dec, &t.running_)
	return nil
}

// TimerSys

func (s *TimerSys) GobEncode() ([]byte, error) {
	w, enc := _gobNewEnc()
	_gobEnc(enc, s.timers)
	return w.Bytes(), nil
}

func (s *TimerSys) GobDecode(buf []byte) error {
	dec := _gobNewDec(buf)
	_gobDec(dec, &s.timers)
	return nil
}

// Transform

func (t *Transform) GobEncode() ([]byte, error) {
	return sr.SerializeByFields(
		t.Transform,
	), nil
}

func (t *Transform) GobDecode(b []byte) error {
	sr.DeserializeByFields(b,
		&t.Transform,
	)
	// XXX fixme
	sr.Finalize(t)
	return nil
}

// HeightMap

func (hm HeightMap) GobEncode() ([]byte, error) {
	return sr.SerializeByFields(
		hm.data, hm.dataOrig, hm.w, hm.h,
		hm.levelStep, hm.numLevels, hm.minLevel,
		hm.maxLevel, hm.regions,
		// hm.opts
		hm.initialized,
	), nil
}

func (hm *HeightMap) GobDecode(b []byte) error {
	sr.DeserializeByFields(b,
		&hm.data, &hm.dataOrig, &hm.w, &hm.h,
		&hm.levelStep, &hm.numLevels, &hm.minLevel,
		&hm.maxLevel, &hm.regions,
		// hm.opts
		&hm.initialized,
	)
	return nil
}

// Field

func (f *Field) GobEncode() ([]byte, error) {
	return sr.SerializeByFields(
		f.w, f.h, f.aspect, f.cells, f.hm,
		f.generated,
		f.fg,
		//f.view,
	), nil
}

func (f *Field) GobDecode(b []byte) error {
	sr.DeserializeByFields(b,
		&f.w, &f.h, &f.aspect, &f.cells, &f.hm,
		&f.generated,
		&f.fg,
		//&f.view,
	)
	return nil
}

// FieldGen

func (fg *FieldGen) GobEncode() ([]byte, error) {
	return sr.SerializeByFields(
		fg.requiredBuildings,
		fg.campSites,
	), nil
}

func (fg *FieldGen) GobDecode(b []byte) error {
	sr.DeserializeByFields(b,
		&fg.requiredBuildings,
		&fg.campSites,
	)
	return nil
}

// Cell

func (c Cell) GobEncode() ([]byte, error) {
	return sr.SerializeByFields(
		c.cx, c.cy, c.z, c.open,
		c.river, c.shore, c.tileId,
		c.buildingId,
	), nil
}

func (c *Cell) GobDecode(b []byte) error {
	sr.DeserializeByFields(b,
		&c.cx, &c.cy, &c.z, &c.open,
		&c.river, &c.shore, &c.tileId,
		&c.buildingId,
	)
	return nil
}

// CPos

func (p CPos) GobEncode() ([]byte, error) {
	return sr.SerializeByFields(
		p.x, p.y,
	), nil
}

func (p *CPos) GobDecode(b []byte) error {
	sr.DeserializeByFields(b,
		&p.x, &p.y,
	)
	return nil
}

// CRect

func (r CRect) GobEncode() ([]byte, error) {
	return sr.SerializeByFields(
		r.x, r.y, r.w, r.h,
	), nil
}

func (r *CRect) GobDecode(b []byte) error {
	sr.DeserializeByFields(b,
		&r.x, &r.y, &r.w, &r.h,
	)
	return nil
}

/*
// Building

func (b *Building) GobEncode() ([]byte, error) {
	return sr.SerializeByFields(
		b.id, b.name, b.guiName,
		//b.player,
		b.campId, b.cx,
		b.cy, b.dimX
	), nil
}

func (b *Building) GobDecode(b []byte) error {
	sr.DeserializeByFields(b,
		
	)
	return nil
}
*/

// Obj

/*
func (o *Obj) GobEncode() ([]byte, error) {
	return sr.SerializeByFields(
		o.Component,
		o.Transform,
		o.id, o.name, o.active,
		o.objtype, o.selectable,
	), nil
}

func (o *Obj) GobDecode(b []byte) error {
	sr.DeserializeByFields(b,
		&o.Component,
		&o.Transform,
		&o.id, &o.name, &o.active,
		&o.objtype, &o.selectable,
	)
	return nil
}
*/

// ObjSys

/*
func (x *ObjSys) GobEncode() ([]byte, error) {
	return sr.SerializeByFields(
		x.GameSystem,
	), nil
}

func (x *ObjSys) GobDecode(b []byte) error {
	sr.DeserializeByFields(b,
		&x.GameSystem,
	)
	return nil
}
*/

// GameSystem

func (x *GameSystem) GobEncode() ([]byte, error) {
	return sr.SerializeByFields(
		x.System,
	), nil
}

func (x *GameSystem) GobDecode(b []byte) error {
	sr.DeserializeByFields(b,
		&x.System,
	)
	return nil
}

// System

func (x *System) GobEncode() ([]byte, error) {
	mElems := x.getElems()
	_ = mElems
	return sr.SerializeByFields(
		x.name, x.desc, mElems,
		//x.elems,
	), nil
}

func (x *System) GobDecode(b []byte) error {
	var mElems []interface{}
	_ = mElems
	sr.DeserializeByFields(b,
		&x.name, &x.desc, &mElems,
		//x.elems,
	)
	sr.Finalize(x)
	x.setElems(mElems)
	return nil
}

// Position

func (x *Position) GobEncode() ([]byte, error) {
	return sr.SerializeByFields(
		x.dPos_, x.speed, &x.dir,
		x.stepTime, x.i,
	), nil
}

func (x *Position) GobDecode(b []byte) error {
	sr.DeserializeByFields(b,
		&x.dPos_, &x.speed, &x.dir,
		&x.stepTime, &x.i,
	)
	return nil
}

// PositionSys

/*
func (x *PositionSys) GobEncode() ([]byte, error) {
	return sr.SerializeByFields(
		x.GameSystem,
	), nil
}

func (x *PositionSys) GobDecode(b []byte) error {
	sr.DeserializeByFields(b,
		&x.GameSystem,
	)
	return nil
}
*/

// Dir

func (x *Dir) GobEncode() ([]byte, error) {
	return sr.SerializeByFields(
		x.rot, x._sRot, x._dRot,
		x.rotSpeed, x.rotDir,
	), nil
}

func (x *Dir) GobDecode(b []byte) error {
	sr.DeserializeByFields(b,
		&x.rot, &x._sRot, &x._dRot,
		&x.rotSpeed, &x.rotDir,
	)
	return nil
}

// Gob helpers

func _gobNewEnc() (*bytes.Buffer, *gob.Encoder) {
	w := new(bytes.Buffer)
	enc := gob.NewEncoder(w)
	return w, enc
}
func _gobNewDec(buf []byte) *gob.Decoder {
	r := bytes.NewBuffer(buf)
	dec := gob.NewDecoder(r)
	return dec
}

func _gobEnc(enc *gob.Encoder, v interface{}) {
	err := enc.Encode(v)
	if err != nil {
		panic(err)
	}
}

func _gobDec(dec *gob.Decoder, v interface{}) {
	err := dec.Decode(v)
	if err != nil {
		panic(err)
	}
}
