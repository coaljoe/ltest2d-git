package main_module

var _ = `

import (
	"math"
)

// component
type ColObj struct {
	obj    *Obj // link
	radius float64
}

func newColObj(obj *Obj, r float64) *ColObj {
	co := &ColObj{
		obj:    obj,
		radius: r,
	}
	return co
}

func (co *ColObj) update(dt float64) {
	//pp(2)
	for _, oi := range game.objsys.getElems() {
		o := oi.(*Obj)
		if o.colobj == nil {
			continue
		}
		// Skip same id
		if o.id == co.obj.id {
			continue
		}
		// C1
		x1 := o.Pos().X()
		y1 := o.Pos().Y()
		r1 := o.colobj.radius
		// C2
		x2 := co.obj.Pos().X()
		y2 := co.obj.Pos().Y()
		r2 := co.radius

		// https://stackoverflow.com/questions/1736734/circle-circle-collision
		// (x2-x1)^2 + (y1-y2)^2 <= (r1+r2)^2
		if math.Pow(x2-x1, 2)+math.Pow(y1-y2, 2) < math.Pow(r1+r2, 2) {
			//p("collision:", o.id, co.obj.id)
			game.collisionsys.onCollision(o, co.obj)
		} else {
			//p("no collision:", o.id, co.obj.id)
		}
	}
}
`
