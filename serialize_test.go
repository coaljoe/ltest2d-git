package main_module

var _ = `

import (
	"kristallos.ga/lib/sr"
	"bytes"
	"encoding/gob"
	"fmt"
	"reflect"
	"testing"

	"github.com/kylelemons/godebug/pretty"
)


func TestSerialize(t *testing.T) {
	initTest()
	s := storeGame()
	_ = s

	tr := newTimer(false)
	tr.update(100)
	s = storeTimer(tr)
	_ = s
	tr2 := restoreTimer(s)

	if tr2.time != tr.time {
		t.Logf("tr2.time: %f, tr.time: %f", tr2.time, tr.time)
		t.Fail()
	}

	// Gob test
	buf := new(bytes.Buffer)
	// Writing
	enc := gob.NewEncoder(buf)
	err := enc.Encode(tr)
	if err != nil {
		t.Log(err)
		t.Fail()
	}
	// Reading
	buf = bytes.NewBuffer(buf.Bytes())
	tr3 := newTimer(false)
	tr3.update(100)
	dump(tr3)
	dec := gob.NewDecoder(buf)
	err = dec.Decode(tr3)
	if err != nil {
		t.Log(err)
		t.Fail()
	}

	pv(tr3)
	dump(tr3)

	if !reflect.DeepEqual(tr, tr3) {
		t.Fail()
	}

	// TimerSys
	buf = new(bytes.Buffer)
	// Writing
	enc = gob.NewEncoder(buf)
	err = enc.Encode(game.timersys)
	if err != nil {
		t.Log(err)
		t.Fail()
	}
	// Reading
	buf = bytes.NewBuffer(buf.Bytes())
	//ts2 := newTimerSys() // XXX Returns singleton
	ts2 := &TimerSys{}
	dump(game.timersys)
	dump(ts2)
	if true {
		dec = gob.NewDecoder(buf)
		err = dec.Decode(ts2)
		if err != nil {
			t.Log(err)
			t.Fail()
		}
	}

	if !reflect.DeepEqual(game.timersys, ts2) {
		t.Fail()
	}

	/*
		// Multiple store
		b := sr.SerializeValue(tr)
		_ = b
		tr2 = &Timer{}
		sr.DeserializeValue(b, tr2)

		if !reflect.DeepEqual(tr, tr2) {
			t.Fail()
		}
	*/
	// Obj
	e1 := newEntity()
	ob := newObj(e1, "test obj")

	b := sr.SerializeValue(ob)
	_ = b
	ob2 := &Obj{}
	//fmt.Println(pretty.Compare(ob, ob2))
	sr.DeserializeValue(b, ob2)

	fmt.Printf("ob: %#v\n\n", ob)
	fmt.Printf("ob2: %#v\n\n", ob2)

	//fmt.Printf("ob: %#v\n\n", ob.Transform)
	//fmt.Printf("ob2: %#v\n\n", ob2.Transform)

	//dump(ob.Transform)
	//dump(ob2.Transform)

	if !reflect.DeepEqual(ob.Transform, ob2.Transform) {
		t.Log("Not equal")
		t.Fail()
	}

	if !reflect.DeepEqual(ob, ob2) {
		t.Log("Not equal")
		fmt.Println(pretty.Compare(ob, ob2))
		t.Fail()
	}

	// System
	s1 := newSystem("test system", "description")
	s2 := &System{}

	b = sr.SerializeValue(s1)
	sr.DeserializeValue(b, s2)

	fmt.Printf("s1: %#v\n\n", s1)
	fmt.Printf("s2: %#v\n\n", s2)

	//fmt.Printf("ob: %#v\n\n", ob.Transform)
	//fmt.Printf("ob2: %#v\n\n", ob2.Transform)

	//dump(ob.Transform)
	//dump(ob2.Transform)
	//dump(s1)
	//dump(s2)

	if !reflect.DeepEqual(s1, s2) {
		t.Log("Not equal")
		fmt.Println("compare:", pretty.Compare(s1, s2))
		t.Fail()
	}

	{
		// GameSystem
		gs1 := &GameSystem{
			System: newSystem("test system", "description")}
		gs2 := &GameSystem{}

		b = sr.SerializeValue(gs1)
		sr.DeserializeValue(b, gs2)

		fmt.Printf("gs1: %#v\n\n", gs1)
		fmt.Printf("gs2: %#v\n\n", gs2)

		if !reflect.DeepEqual(gs1, gs2) {
			t.Log("Not equal")
			fmt.Println(pretty.Compare(gs1, gs2))
			t.Fail()
		}
	}
	{
		// ObjSys
		s1 := newObjSys()
		s1.addElem(ob)
		s2 := &ObjSys{}

		b = sr.SerializeValue(s1)
		sr.DeserializeValue(b, &s2)

		fmt.Printf("s1: %#v\n\n", s1)
		fmt.Printf("s2: %#v\n\n", s2)

		fmt.Println("xx:", s1.size())
		fmt.Println("xx:", s2.size())

		//dumpDepth(s1, 12)
		//dumpDepth(s2, 12)

		if !reflect.DeepEqual(s1, s2) {
			t.Log("Not equal")
			fmt.Println("compare:", pretty.Compare(s1, s2))
			t.Fail()
		}
	}
	{
		println("\n- test PositionSys -\n")

		// PositionSys
		s1 := newPositionSys() // XXX fixme: singleton system
		//s1 := &PositionSys{} // doesn't work
		s1.listElems()
		p(s1.size())

		// Add element
		e1 := newEntity()
		p1 := newPosition(e1) // XXX fixme? automatically added here

		// Attempt to fix NaN comparison
		// DeepEqual works this way
		//*p1.dir._sRot = -1.0
		//*p1.dir._dRot = -1.0

		dump(p1)
		s1.listElems()
		//s1.addElem(p1) // XXX double add bug

		//s1.addElem(ob)
		s2 := &PositionSys{}

		println("\nbefore:")
		fmt.Println("xx:", s1.size())
		//fmt.Println("xx:", s2.elems.Size()) // XXX doesn't work on raw struct

		b = sr.SerializeValue(s1)
		sr.DeserializeValue(b, &s2)

		fmt.Printf("s1: %#v\n\n", s1)
		fmt.Printf("s2: %#v\n\n", s2)

		println("\nafter:")
		fmt.Println("xx:", s1.size())
		fmt.Println("xx:", s2.size())

		//dumpDepth(s1, 5)
		//dumpDepth(s2, 5)
		dumpDepth(s1, 8)
		dumpDepth(s2, 8)

		// XXX cant work with NaN values?
		// They're never equal. See godoc
		if !reflect.DeepEqual(s1, s2) {
			t.Log("Not equal")
			println("not equal?")
			fmt.Println("compare:", pretty.Compare(s1, s2))
			t.Fail()
		}

		println("PositionSys test done")
	}
}
`
