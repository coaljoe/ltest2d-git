// Json utilities.
package main_module

import (
	"bytes"
	"fmt"
	"log"
	"os"
	"regexp"

	js "github.com/bitly/go-simplejson"
	//js5 "github.com/flynn/json5"
)

func _readFloat(def *js.Json, name string) float64 {
	if d, ok := def.CheckGet(name); ok {
		return d.MustFloat64()
	} else {
		panic("read fail key=" + name)
	}
}
func _readString(def *js.Json, name string) string {
	if d, ok := def.CheckGet(name); ok {
		return d.MustString()
	} else {
		panic("read fail key=" + name)
	}
}
func _readInt(def *js.Json, name string) int {
	if d, ok := def.CheckGet(name); ok {
		return d.MustInt()
	} else {
		panic("read fail key=" + name)
	}
}

/*
func _getObject2(
	data map[string]interface{}, name string) map[string]interface{} {
	return data[name].(map[string]interface{})
}
*/
// Convert json5 data to json5 object.
func _getObject(data interface{}) map[string]interface{} {
	return data.(map[string]interface{})
}

func _getObjectCheck(data interface{}) (map[string]interface{}, bool) {
	if data == nil {
		return nil, false
	}
	return data.(map[string]interface{}), true
}

// Get string value from json5 def.
func _getString(def map[string]interface{}, name string) string {
	if v := def[name]; v != nil {
		return v.(string)
	} else {
		panic("read fail key=" + name)
	}
}

// Get float value from json5 def.
func _getFloat(def map[string]interface{}, name string) float64 {
	if v := def[name]; v != nil {

		return v.(float64)
	} else {
		panic("read fail key=" + name)
	}
}

// Get int value from json5 def.
func _getInt(def map[string]interface{}, name string) int {
	vf := _getFloat(def, name)
	// Check if value is integer
	if vf != float64(int(vf)) {
		pp("error: json value is not integer:", vf)
	}
	return int(vf)
}

func _getJsonFromFile(fp string) *js.Json {
	r, err := os.Open(fp)
	if err != nil {
		wd, _ := os.Getwd()
		println("wd:", wd)
		println("cant read file: " + fp)
		log.Fatal(err)
	}

	buf := new(bytes.Buffer)
	buf.ReadFrom(r)
	s := buf.String()

	reg, err := regexp.Compile("//.*?\n")
	if err != nil {
		log.Fatal(err)
	}
	safe := reg.ReplaceAllString(s, "\n")
	//Println(safe)

	//d, err := js.NewFromReader(r)
	js, err := js.NewJson([]byte(safe))
	if err != nil {
		fmt.Println(safe)
		println("cant read json from file: " + fp)
		log.Fatal(err)
	}

	return js
}

var _ = `
func _getJson5DataFromFile(fp string) interface{} {
	r, err := os.Open(fp)
	if err != nil {
		wd, _ := os.Getwd()
		println("wd:", wd)
		println("cant read file: " + fp)
		log.Fatal(err)
	}
	defer r.Close()

	buf := new(bytes.Buffer)
	buf.ReadFrom(r)
	s := buf.String()

	var data interface{}
	err = js5.Unmarshal([]byte(s), &data)
	if err != nil {
		println("cant read json from file: " + fp)
		p(data)
		log.Fatal(err)
	}

	return data
}
`
