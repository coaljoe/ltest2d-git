package main_module

import (
	"fmt"
)

type Player struct {
	id    int
	ptype PlayerType
	name  string
	// Game slot id
	playerSlotId int
	score int
	camp  *Camp
	ai    *PlayerAi
	// Stats
	//militaryScore    int
	//statsUpdateTime  float64
	//statsUpdateTimer *Timer
	//vc       VarsCache
	disabled bool
}
func (p *Player) Name() string { return p.name }
func (p *Player) SetName(v string) { p.name = v}
func (p *Player) Camp() *Camp { return p.camp }

func NewPlayer(ptype PlayerType, campId CampId) *Player {
	p := &Player{
		id:    _ider.GetNextId("Player"),
		ptype: ptype,
		name:  "Unknown",
		camp:  newCamp(campId),
		//statsUpdateTime:  2, //10,
		//statsUpdateTimer: newTimer(true),
		//vc:       newVarsCache(),
		disabled: false,
	}
	if ptype == PlayerType_AI {
		p.ai = newPlayerAi(p)
	}

	// Automatically add to PlayerSys
	//game.playersys.addPlayer(p) // Fixme: remove?
	return p
}

func (p *Player) IsHuman() bool {
	if p.ptype == PlayerType_Human {
		return true
	}
	return false
}

func (p *Player) IsAi() bool {
	return !p.IsHuman()
}

// Get CampId
func (p *Player) campId() CampId {
	return p.camp.Id
}

func (p *Player) getScore() int {
	return 0
}

func (p *Player) takeLives() bool {
	/*
		p.lives -= 1
		if p.lives < 0 {
			p.lives = 0
			return false
		}
	*/
	return true
}

func (p *Player) addLives(amt int) {
	/*
		p.lives += amt
		if p.lives > p.maxLives {
			p.lives = p.maxLives
		}
	*/
}

/*
func (p *Player) _updateStats() {
	ms := p.getMilitaryScore()
	p.militaryScore = ms
}
*/

func (p *Player) getMilitaryScore() int {
	_log.Dbg("%F")

	var _ = `
	// Return vars cache
	//vcId := fmt.Sprintf("militaryRating_%d", p.id)
	vcId := "militaryScore"
	if ok, val := p.vc.getNotExpired(vcId); ok {
		return val.(int)
	}

	mspu := p.getMilitaryScorePerUnit()
	//pp(mspu)
	unitsNum := 0
	unitsPower := 0.0
	for id, ms := range mspu {
		u := _UnitSys.getUnitById(id)
		uc := c_Unit(u)

		unitPower := ms

		unitsPower += unitPower
		unitsNum += 1
		__p(uc.utype)
		__p(uc.getPlayer())
	}

	//pp(unitsPower, unitsNum)

	militaryScore := int(unitsPower)

	// Save vars cache
	p.vc.put(vcId, 10, militaryScore)

	return militaryScore
	`

	mspu := p.getMilitaryScorePerUnit()
	//pp(mspu)
	unitsNum := 0
	unitsPower := 0.0
	for id, ms := range mspu {
		u := game.unitsys.getUnitById(id)

		unitPower := ms

		unitsPower += unitPower
		unitsNum += 1
		__p(u.name)
		//__p(u.getPlayer())
		__p(u.player)
	}

	__p("unitsPower:", unitsPower, "unitsNum:", unitsNum)

	//pp(2)

	militaryScore := int(unitsPower)

	_log.Dbg("done %F")
	return militaryScore
}

func (p *Player) getMilitaryScorePerUnit() map[int]float64 {

	var _ = `
	// Return vars cache
	vcId := "militaryScorePerUnit"
	if ok, val := p.vc.getNotExpired(vcId); ok {
		return val.(map[int]float64)
	}

	r := make(map[int]float64, 0)

	// Https://www.reddit.com/r/civ/comments/1d0lz5/how_the_ai_computes_your_military_strength_based/
	//pp(pai.p)
	units := game.unitsys.getUnitsForPlayer(p)
	//pp(units)
	//units := game.unitsys.getUnits()
	for _, u := range units {
		uc := c_Unit(u)
		unitPower := uc.getUnitMilitaryScore()
		r[u.Id()] = unitPower
	}

	// Save vars cache
	p.vc.put(vcId, 10, r)

	return r

	`

	r := make(map[int]float64, 0)

	// Https://www.reddit.com/r/civ/comments/1d0lz5/how_the_ai_computes_your_military_strength_based/
	//pp(pai.p)
	units := game.unitsys.getUnitsForPlayer(p)
	//pp(units)
	//units := game.unitsys.getUnits()
	for _, u := range units {
		unitPower := u.getUnitMilitaryScore()
		r[u.id] = unitPower
	}

	return r
}

func (p *Player) GetFow() *Fow {
	return game.fowsys.getFowForPlayer(p)
}

func (p *Player) isDefaultPlayer() bool {
	return p == GetPlayer()
}

func (p *Player) start() {
	if p.ai != nil {
		p.ai.start()
	}
}

func (p *Player) String() string {
	return fmt.Sprintf("Player<name: %s (%s)>", p.name, p.ptype)
}

func (p *Player) update(dt float64) {
	if p.disabled {
		return
	}

	if p.ai != nil {
		p.ai.update(dt)
	}
}

///////////////
// PlayerType

type PlayerType int

const (
	PlayerType_Human PlayerType = iota
	PlayerType_AI
)

func (p PlayerType) String() string {
	switch p {
	case PlayerType_Human:
		return "Human"
	case PlayerType_AI:
		return "AI"
	default:
		panic("unknown PlayerType")
	}
}
