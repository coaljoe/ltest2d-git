package main_module

type TimerTask struct {
	taskTime  float64
	taskTimer *Timer
	cb        func()
	isSet     bool
}

func newTimerTask() *TimerTask {
	tt := &TimerTask{
		taskTimer: newTimer(true),
	}
	return tt
}

func (tt *TimerTask) setIn(delay float64) {
	pp("not implemented")
}

func (tt *TimerTask) setEvery(taskTime float64, cb func()) {
	tt.taskTime = taskTime
	tt.cb = cb
	tt.isSet = true
}

func (tt *TimerTask) update() {
	if !tt.isSet {
		return
	}

	if tt.taskTimer.dt() > tt.taskTime {
		tt.cb()
		tt.taskTimer.restart()
	}
}
