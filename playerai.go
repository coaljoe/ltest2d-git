package main_module

// AI for a player.
type PlayerAi struct {
	p *Player
	// _Props_
	// Minimum set of buildings the camp should have
	//requiredBuildings map[string]int
	buildingPlaces map[string][]CPos
	// Logic
	logicUpdateTime  float64
	logicUpdateTimer *Timer
	manageBaseTask   *TimerTask
	active           bool
}

func newPlayerAi(p *Player) *PlayerAi {
	pai := &PlayerAi{
		p: p,
		//requiredBuildings: make(map[string]int),
		buildingPlaces:   make(map[string][]CPos),
		logicUpdateTime:  2, //10,
		logicUpdateTimer: newTimer(true),
		manageBaseTask:   newTimerTask(),
		active:           true,
	}
	//pai.requiredBuildings["hq"] = 1 // XXX optional?
	//pai.requiredBuildings["factory"] = 1
	//pai.requiredBuildings["housing"] = 3
	// Disable logger
	//_log.EnableFilterPrefix("[PlayerAi]")
	return pai
}

func (pai *PlayerAi) test() {

	if true {
		px := 5
		py := 5
		for y := py; y < py+4; y += 2 {
			for x := px; x < px+4; x += 2 {
				pai.placeBuilding("bunker", x, y)
			}
		}
	}

	if false {
		ox, oy := 4, 4
		for y := 0; y < 4; y++ {
			for x := 0; x < 4; x++ {
				u := pai.placeUnit("light_tank", ox+x, oy+y)
				_ = u
				//p(pai.p.name)
				//pp(c_Unit(u).getPlayer())
			}
		}
	}
}

func (pai *PlayerAi) start() {
	//pp(2)

	// Do initial setup
	// Find places for building

	// Build initial base
	//pai.test()

	if false {
		pai.buildingPlaces["factory"] = append(pai.buildingPlaces["factory"], CPos{10, 10})

		pai.buildingPlaces["housing"] = append(pai.buildingPlaces["housing"], CPos{10, 15})
		pai.buildingPlaces["housing"] = append(pai.buildingPlaces["housing"], CPos{10, 18})
		pai.buildingPlaces["housing"] = append(pai.buildingPlaces["housing"], CPos{10, 21})
	}

	takenPlaces := make([]CPos, 0)
	for name, num := range game.field.fg.requiredBuildings {
		bDef := &Building{}
		readBuildingDef(name, pai.p.campId(), bDef)
		//bSize := CPos{bDef.areaX, bDef.areaY}
		for n := 0; n < num; n++ {
			for _, rect := range game.field.fg.campSites[pai.p.id] {
				if rect.w == bDef.areaX && rect.h == bDef.areaY {
					pos := CPos{rect.x, rect.y}
					free := true
					//for _, xpos := range pai.buildingPlaces[name] {
					for _, xpos := range takenPlaces {
						// Position is already used
						if xpos == pos {
							free = false
							break
						}
					}
					if free {
						pai.buildingPlaces[name] = append(pai.buildingPlaces[name], pos)
						takenPlaces = append(takenPlaces, pos)
						break
					}
				}
			}
		}
	}
	//pp(pai.buildingPlaces)

	pai.manageBase()

	pai.manageBaseTask.setEvery(5.0, func() {
		pai.manageBase()
		//pp(2)
	})
}

func (pai *PlayerAi) manageBase() {
	for name, poss := range pai.buildingPlaces {
		for _, pos := range poss {
			b := game.buildingsys.getBuildingByPosCheck(pos.x, pos.y)
			if b == nil {
				pai.placeBuilding(name, pos.x, pos.y)
			}
		}
	}
}

func (pai *PlayerAi) updateMilitaryLogic() {
	// Find strongest player
	maxMilitaryScore := 0
	var topPlayer *Player

	//pp(len(_PlayerSys.players))
	for _, pl := range game.playersys.players {
		ms := pl.getMilitaryScore()
		p("military score for player:", pl, ms)
		if ms > maxMilitaryScore {
			maxMilitaryScore = ms
			topPlayer = pl
		}
	}

	p("Top player:", topPlayer, "Max military score:", maxMilitaryScore)

	// Military score for known units
	// XXX: in what cell are air units?
	knownUnitsMilitaryScore := 0.0
	knownUnitsCount := 0
	for _, pl := range game.playersys.players {
		mspu := pl.getMilitaryScorePerUnit()
		for id, ms := range mspu {
			u := game.unitsys.getUnitById(id)
			if game.unitsys.isUnitVisibleForPlayer(u, pai.p) {
				knownUnitsMilitaryScore += ms
				knownUnitsCount += 1
			}
		}
	}

	//pdump(pai.p.getFow())
	p("Known units military score:", knownUnitsMilitaryScore)
	p("Known units count:", knownUnitsCount)
	p("Total units count:", len(game.unitsys.getUnits()))

	//pp(2)
}

////////////////////////////////
// Micro management operations

// Pick a suitable place for a new building.
func (pai *PlayerAi) findAPlaceForBuilding(bname string) CPos {
	/*
		if ok := _BuildingSys.placeBuilding(cx, cy, btype, pai.p); !ok {
			pp("[PlayerAi] placeBuilding failed")
		}
	*/

	r := CPos{}
	return r
}

// Place building on behalf of Player
// XXX: should be in player class?
func (pai *PlayerAi) placeBuilding(bname string, cx, cy int) {
	cknil(game.buildingsys)
	cknil(pai)
	cknil(pai.p.camp)
	_log.Dbg("%F bname:", bname, "cx, cy:", cx, cy)
	//pp(_BuildingSys)
	//pp(pai.p.camp.id)
	b := MakeBuildingForPlayer(bname, pai.p)
	SpawnEntity(b)
	b.placeAt(cx, cy)
}

// Place unit on behalf of Player
func (pai *PlayerAi) placeUnit(uname string, cx, cy int) *Unit {
	var _ = `
	var u *ecs.Entity
	var ok bool
	//pp(pai.p.name)
	if ok, u = _UnitSys.placeUnit(cx, cy, utype, pai.p); !ok {
		pp("[PlayerAi] placeUnit failed")
	}
	return u
	`
	return nil
}

func (pai *PlayerAi) update(dt float64) {
	if !pai.active {
		return
	}

	//pp(2)

	//pai.updateMilitaryLogic()
	if pai.logicUpdateTimer.dt() > pai.logicUpdateTime {
		//p._updateStats()
		pai.updateMilitaryLogic()
		//pai.manageBase()
		pai.logicUpdateTimer.restart()
	}

	pai.manageBaseTask.update()
}
