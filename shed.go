package main_module

/* Synchronous sheduler */

import "fmt"

var maxShedId = 0

type Shed struct {
	id        int
	timeStart float64
	cb        func(s *Shed)
	done      bool
}

func newShed(dtime float64, cb func(s *Shed)) *Shed {
	s := &Shed{
		id:        maxShedId,
		timeStart: game.timer.time + dtime,
		cb:        cb,
		done:      false,
	}
	maxShedId++
	_log.Trc("NewShed: ", s)

	// Adding shed to shedsys
	game.shedsys.addShed(s)
	return s
}

func (s *Shed) queue(delay float64, cb func(s2 *Shed)) {
	pp("not implemented")
}

func (s *Shed) String() string {
	return fmt.Sprintf("Shed<id: %d, timeStart: %f>", s.id, s.timeStart)
}

func (s *Shed) update(dt float64) {
	//p("shed upd")
	if game.timer.time >= s.timeStart {
		//p("shed", game.timer.time, s.timeStart)
		s.cb(s)
		s.done = true
		//_ShedSys.RemoveShed(s) // Fixme?
	}
}

/***** ShedSys *****/

type ShedSys struct {
	sheds map[*Shed]bool
}

func newShedSys() *ShedSys {
	s := &ShedSys{
		sheds: make(map[*Shed]bool),
	}
	return s
}

func (ss *ShedSys) addShed(s *Shed) {
	_log.Trc("Adding Shed", s)
	ss.sheds[s] = true
}

func (ss *ShedSys) removeShed(s *Shed) {
	_log.Trc("Removing Shed", s)
	delete(ss.sheds, s)
}

func (ss *ShedSys) clear() {
	/*
		for s := range ss.sheds {
			ss.removeShed(s)
		}
	*/
	ss.sheds = make(map[*Shed]bool)
}

func (ss *ShedSys) update(dt float64) {
	// Safer way to update sheds map?
	var remList []*Shed
	remList = make([]*Shed, 0)

	for s := range ss.sheds {
		cknil(s)
		s.update(dt)
		if s.done {
			//ss.RemoveShed(s)
			remList = append(remList, s)
		}
	}
	for _, s := range remList {
		//p(ss.sheds)
		ss.removeShed(s)
	}
}
