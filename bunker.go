package main_module

// component
type Bunker struct {
}

func newBunker() *Bunker {
	b := &Bunker{}

	return b
}

func (b *Bunker) start() {}
