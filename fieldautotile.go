package main_module

// Autotile field
func (f *Field) autotile(x_, y_, w, h int, autotileRivers bool) {
	p("FieldView.autotile:", x_, y_, w, h, autotileRivers)
	//for closedTileZLevel := ZLevel_Min + 1; closedTileZLevel <= ZLevel_Max; closedTileZLevel++ {
	//closedTileZLevel := ZLevel_Ground0
	//closedTileZLevel := ZLevel_Ground1
	closedTileZLevels := []ZLevel{
		ZLevel_Sea3,
		ZLevel_Sea2,
		ZLevel_Sea1,
		ZLevel_Sea0,
		ZLevel_Ground0,
		ZLevel_Ground1,
		ZLevel_Ground2,
		ZLevel_Ground3,
	}
	for _, closedTileZLevel := range closedTileZLevels {

		//closedTileZLevel := ZLevel_Ground0
		//openTileZLevel := ZLevel_Sea0
		openTileZLevel := closedTileZLevel - 1

		for y := y_; y < y_+h; y++ {
			for x := x_; x < x_+w; x++ {
				//for y := 0; y < f.h; y++ {
				//	for x := 0; x < f.w; x++ {
				//for y := 1; y < f.h-1; y++ {
				//	for x := 1; x < f.w-1; x++ {

				// Skip non-closed tiles
				cell := f.GetCell(x, y)
				if cell.z != closedTileZLevel {
					continue
				}

				cells := f.getCellNeighbours(x, y, true)
				var data [9]int
				for i, x := range cells {
					data[i] = int(x.z)
				}
				//pp(data)
				tileId := f.autotileSegment(data, int(openTileZLevel), int(closedTileZLevel))
				if tileId != -1 {
					//f.cells[x][y].tileId = tileId
					cell := f.GetCell(x, y)
					cell.tileId = tileId
				}
			}
		}
	}

	// Autotile rivers
	//if true {
	if autotileRivers {
		for y := y_; y < y_+h; y++ {
			for x := x_; x < x_+w; x++ {
				//for y := 1; y < f.h-1; y++ {
				//	for x := 1; x < f.w-1; x++ {

				// Skip non-closed tiles
				cell := f.GetCell(x, y)
				if cell.river == RiverState_None {
					continue
				}

				cells := f.getCellNeighbours(x, y, true)
				var data [9]int
				for i, x := range cells {
					if x.river != RiverState_None {
						data[i] = 1
					}
				}
				//pp(data)
				tileId := f.autotileSegment(data, 0, 1)
				if tileId != -1 {
					f.cells[x][y].tileId = tileId
				}
			}
		}
	}
}

// Pick tile by autotiling segment of data
func (f *Field) autotileSegment(data [9]int, openTileZLevel int, closedTileZLevel int) int {
	var _ = `
	autotile := true
	if autotile {
		//for y := 0; y < f.h; y++ {
		for y := 1; y < f.h-1; y++ {
			for x := 1; x < f.w-1; x++ {
				cell := &f.cells[x][y]

				/*
					northTile := 0
					westTile := 0
					eastTile := 0
					southTile := 0

					//if cell.z != ZLevel_Sea0 {
					if f.cells[x][y-1].z != 0 {
						northTile = 1
					}
					if f.cells[x-1][y].z != 0 {
						westTile = 1
					}
					if f.cells[x+1][y].z != 0 {
						eastTile = 1
					}
					if f.cells[x][y+1].z != 0 {
						southTile = 1
					//}
				}

					bitmask := 1 * northTile + 2 * westTile + 4 * eastTile + 8 * southTile
				*/


				/*
					topLeft := 0
					topRight := 0
					bottomLeft := 0
					bottomRight := 0


					if f.cells[x-1][y-1].z != cell.z {
						topLeft = 1
					}
					if f.cells[x+1][y-1].z != cell.z {
						topRight = 1
					}
					if f.cells[x-1][y+1].z != cell.z {
						bottomLeft = 1
					}
					if f.cells[x+1][y+1].z != cell.z {
						bottomRight = 1
					}

					//tile_index = topLeft + 2 * topRight + 4 * bottomLeft + 8 * bottomRight
					bitmask := topLeft + 2 * topRight + 4 * bottomLeft + 8 * bottomRight
				*/
				

				topLeft := 0
				topRight := 0
				bottomLeft := 0
				bottomRight := 0

				/*
					if f.cells[x-1][y-1].z != cell.z {
						topLeft = 1
					}
					if f.cells[x+1][y-1].z != cell.z {
						topRight = 1
					}
					if f.cells[x-1][y+1].z != cell.z {
						bottomLeft = 1
					}
					if f.cells[x+1][y+1].z != cell.z {
						bottomRight = 1
					}
				*/

				/*
					// Y-inv
					if f.cells[x-1][y+1].z != cell.z {
						topLeft = 1
					}
					if f.cells[x+1][y+1].z != cell.z {
						topRight = 1
					}
					if f.cells[x-1][y-1].z != cell.z {
						bottomLeft = 1
					}
					if f.cells[x+1][y-1].z != cell.z {
						bottomRight = 1
					}
				*/

				/*
					if f.cells[x-1][y-1].tileId != cell.tileId {
						topLeft = 1
					}
					if f.cells[x+1][y-1].tileId != cell.tileId {
						topRight = 1
					}
					if f.cells[x-1][y+1].tileId != cell.tileId {
						bottomLeft = 1
					}
					if f.cells[x+1][y+1].tileId != cell.tileId {
						bottomRight = 1
					}
				*/

				/*
					// Y-inv
					if f.cells[x-1][y+1].tileId != cell.tileId {
						topLeft = 1
					}
					if f.cells[x+1][y+1].tileId != cell.tileId {
						topRight = 1
					}
					if f.cells[x-1][y-1].tileId != cell.tileId {
						bottomLeft = 1
					}
					if f.cells[x+1][y-1].tileId != cell.tileId {
						bottomRight = 1
					}
				*/

				/*
					if f.cells[x-1][y-1].tileId == 15 {
						topLeft = 1
					}
					if f.cells[x+1][y-1].tileId == 15 {
						topRight = 1
					}
					if f.cells[x-1][y+1].tileId == 15 {
						bottomLeft = 1
					}
					if f.cells[x+1][y+1].tileId == 15 {
						bottomRight = 1
					}
				*/

				/*
					// Y-inv
					if f.cells[x-1][y+1].tileId == 15 {
						topLeft = 1
					}
					if f.cells[x+1][y+1].tileId == 15 {
						topRight = 1
					}
					if f.cells[x-1][y-1].tileId == 15 {
						bottomLeft = 1
					}
					if f.cells[x+1][y-1].tileId == 15 {
						bottomRight = 1
					}
				*/

				/*
					if f.cells[x-1][y-1].tileId != -1 {
						topLeft = 1
					}
					if f.cells[x+1][y-1].tileId != -1 {
						topRight = 1
					}
					if f.cells[x-1][y+1].tileId != -1 {
						bottomLeft = 1
					}
					if f.cells[x+1][y+1].tileId != -1 {
						bottomRight = 1
					}
				*/

				/*
					// Y-inv
					if f.cells[x-1][y+1].tileId != -1 {
						topLeft = 1
					}
					if f.cells[x+1][y+1].tileId != -1 {
						topRight = 1
					}
					if f.cells[x-1][y-1].tileId != -1 {
						bottomLeft = 1
					}
					if f.cells[x+1][y-1].tileId != -1 {
						bottomRight = 1
					}
				*/

				if cell.z != 0 {

				if f.cells[x-1][y-1].z != ZLevel_Sea0 {
					topLeft = 1
				}
				if f.cells[x+1][y-1].z != ZLevel_Sea0 {
					topRight = 1
				}
				if f.cells[x-1][y+1].z != ZLevel_Sea0 {
					bottomLeft = 1
				}
				if f.cells[x+1][y+1].z != ZLevel_Sea0 {
					bottomRight = 1
				}
				
				} else {
					continue
				}

				/*
					// Y-inv
					if f.cells[x-1][y+1].z != ZLevel_Sea0 {
						topLeft = 1
					}
					if f.cells[x+1][y+1].z != ZLevel_Sea0 {
						topRight = 1
					}
					if f.cells[x-1][y-1].z != ZLevel_Sea0 {
						bottomLeft = 1
					}
					if f.cells[x+1][y-1].z != ZLevel_Sea0 {
						bottomRight = 1
					}
				*/

				//tile_index = topLeft + 2 * topRight + 4 * bottomLeft + 8 * bottomRight
				bitmask := 8*topLeft + 1*topRight + 4*bottomLeft + 2*bottomRight
				//bitmask := topLeft + 2*topRight + 4*bottomLeft + 8*bottomRight

				//if bitmask == 0 {
				//	bitmask = 15
				//}
				

				cell.tileId = bitmask
				
				p("bitmask:", bitmask, "corners:", topLeft, topRight, bottomLeft, bottomRight, "x, y:", x, y, "cell.z:", cell.z)
				//p("bitmask:", bitmask, "corners:", northTile, westTile, eastTile, southTile, "x, y:", x, y, "cell.z:", cell.z)

				/*
				if cell.tileId == -1 {
					cell.tileId = 15
				}
				*/
				
			}
			
		}
	`

	var _ = `
	autotile := true
	if autotile {
		//for y := 0; y < f.h; y++ {
		for y := 1; y < f.h-1; y++ {
			for x := 1; x < f.w-1; x++ {
				cell := &f.cells[x][y]

				top := 0
				right := 0
				bottom := 0
				left := 0

				if cell.z != ZLevel_Sea0 {

				if f.cells[x][y-1].z != ZLevel_Sea0 {
					top = 1
				}
				if f.cells[x+1][y].z != ZLevel_Sea0 {
					right = 1
				}
				if f.cells[x][y+1].z != ZLevel_Sea0 {
					bottom = 1
				}
				if f.cells[x-1][y].z != ZLevel_Sea0 {
					left = 1
				}

				}


				//tile_index = topLeft + 2 * topRight + 4 * bottomLeft + 8 * bottomRight
				//bitmask := 8*topLeft + 1*topRight + 4*bottomLeft + 2*bottomRight
				bitmask := left*8 + bottom*4 + right*2 + top*1

				cell.tileId = bitmask
				p("bitmask:", bitmask, "corners:", top, right, bottom, left, "x, y:", x, y, "cell.z:", cell.z)
			}
		}
		`

	var _ = `
	autotile := true
	if autotile {
		//for closedTileZLevel := ZLevel_Min + 1; closedTileZLevel <= ZLevel_Max; closedTileZLevel++ {
		//closedTileZLevel := ZLevel_Ground0
		//closedTileZLevel := ZLevel_Ground1
		closedTileZLevels := []ZLevel{
			ZLevel_Ground0,
			ZLevel_Ground1,
		}
		for _, closedTileZLevel := range closedTileZLevels {

			//for y := 0; y < f.h; y++ {
			//for y := 1; y < f.h-1; y++ {
			//	for x := 1; x < f.w-1; x++ {
			for y := 0; y < f.h; y++ {
				for x := 0; x < f.w; x++ {
					//cell := &f.cells[x][y]
					cell := f.getCell(x, y)

					topLeft := 0
					topRight := 0
					bottomLeft := 0
					bottomRight := 0

					/*
						topLeft := 1
						topRight := 1
						bottomLeft := 1
						bottomRight := 1
					*/

					top := 0
					bottom := 0
					left := 0
					right := 0

					//closedTileZLevel := ZLevel_Ground0
					//openTileZLevel := ZLevel_Sea0
					openTileZLevel := closedTileZLevel - 1

					//if cell.z != 0 {
					if cell.z == closedTileZLevel {

						if f.getCell(x-1, y-1).z != openTileZLevel {
							topLeft = 1
						}
						if f.getCell(x+1, y-1).z != openTileZLevel {
							topRight = 1
						}
						if f.getCell(x-1, y+1).z != openTileZLevel {
							bottomLeft = 1
						}
						if f.getCell(x+1, y+1).z != openTileZLevel {
							bottomRight = 1
						}

						if f.getCell(x, y-1).z != openTileZLevel {
							top = 1
						}
						if f.getCell(x+1, y).z != openTileZLevel {
							right = 1
						}
						if f.getCell(x, y+1).z != openTileZLevel {
							bottom = 1
						}
						if f.getCell(x-1, y).z != openTileZLevel {
							left = 1
						}

						/*
							if f.cells[x-1][y-1].z == ZLevel_Ground0 {
								topLeft = 1
							}
							if f.cells[x+1][y-1].z == ZLevel_Ground0 {
								topRight = 1
							}
							if f.cells[x-1][y+1].z == ZLevel_Ground0 {
								bottomLeft = 1
							}
							if f.cells[x+1][y+1].z == ZLevel_Ground0 {
								bottomRight = 1
							}

							if f.cells[x][y-1].z == ZLevel_Ground0 {
								top = 1
							}
							if f.cells[x+1][y].z == ZLevel_Ground0 {
								right = 1
							}
							if f.cells[x][y+1].z == ZLevel_Ground0 {
								bottom = 1
							}
							if f.cells[x-1][y].z == ZLevel_Ground0 {
								left = 1
							}
						*/

						/*
							// From:
							// https://www.boristhebrave.com/2013/07/14/tileset-roundup/
							if left == 0 && top == 0 {
								topLeft = 0
							}
							if left == 0 && bottom == 0 {
								bottomLeft = 0
							}
							if right == 0 && top == 0 {
								topRight = 0
							}
							if right == 0 && bottom == 0 {
								bottomRight = 0
							}
						*/

						// Extra boolean checks for corner tiles
						// https://cms-assets.tutsplus.com/uploads/users/90/posts/25673/image/182-to-22.png
						// https://gamedevelopment.tutsplus.com/tutorials/how-to-use-tile-bitmasking-to-auto-tile-your-level-layouts--cms-25673
						if topLeft == 1 {
							if top == 0 || left == 0 {
								topLeft = 0
							}
						}
						if topRight == 1 {
							if top == 0 || right == 0 {
								topRight = 0
							}
						}
						if bottomLeft == 1 {
							if bottom == 0 || left == 0 {
								bottomLeft = 0
							}
						}
						if bottomRight == 1 {
							if bottom == 0 || right == 0 {
								bottomRight = 0
							}
						}

					} else {
						// Empty/Sea tile
						//emptyTile := "l5_0"
						continue
					}

					// bitmask
					// http://www.cr31.co.uk/stagecast/wang/blob.html
					//neighbour_index := top*1 + topRight*2 + right*4 + bottomRight*8 + bottom*16 + bottomLeft*32 + left*64 + topLeft*128
					// https://gamedevelopment.tutsplus.com/tutorials/how-to-use-tile-bitmasking-to-auto-tile-your-level-layouts--cms-25673
					neighbour_index := topLeft*1 + top*2 + topRight*4 + left*8 + right*16 + bottomLeft*32 + bottom*64 + bottomRight*128

					//tile_index = topLeft + 2 * topRight + 4 * bottomLeft + 8 * bottomRight
					//bitmask := 8*topLeft + 1*topRight + 4*bottomLeft + 2*bottomRight
					//bitmask := topLeft + 2*topRight + 4*bottomLeft + 8*bottomRight

					//if bitmask == 0 {
					//	bitmask = 15
					//}
					/*
						pick_tile := map[int]int{0: 0, 2: 1, 8: 2, 10: 3, 11: 4, 16: 5, 18: 6,
							22: 7, 24: 8, 26: 9, 27: 10, 30: 11, 31: 12, 64: 13,
							66: 14, 72: 15, 74: 16, 75: 17, 80: 18, 82: 19, 86: 20,
							88: 21, 90: 22, 91: 23, 94: 24, 95: 25, 104: 26, 106: 27,
						   107: 28, 120: 29, 122: 30, 123: 31, 126: 32, 127: 33,
						   208: 34, 210: 35, 214: 36, 216: 37, 218: 38, 219: 39,
						   222: 40, 223: 41, 248: 42, 250: 43, 251: 44, 254: 45, 255: 46}
					*/
					/*
						// http://www.cr31.co.uk/stagecast/wang/blob.html
						pick_tile := map[int]int{0: 0,
							4: 1, 16: 1, 64: 1,
							20: 5, 80: 5, 65: 5,
							28: 7, 112: 7, 193: 7,
							68: 17,
							84: 21, 81: 21, 69: 21,
							92: 23, 113: 23, 197: 23,
							116: 29, 209: 29, 71: 29,
							124: 31, 241: 31, 199: 31,
							93: 87, 117: 93, 213: 93,
							125: 95, 245: 95, 215: 95,
							221: 119,
							253: 127, 247: 127, 223: 127,
						}
					*/
					// https://gamedevelopment.tutsplus.com/tutorials/how-to-use-tile-bitmasking-to-auto-tile-your-level-layouts--cms-25673
					//{ 2 = 1, 8 = 2, 10 = 3, 11 = 4, 16 = 5, 18 = 6, 22 = 7, 24 = 8, 26 = 9,
					//	27 = 10, 30 = 11, 31 = 12, 64 = 13, 66 = 14, 72 = 15, 74 = 16, 75 = 17,
					//  80 = 18, 82 = 19, 86 = 20, 88 = 21, 90 = 22, 91 = 23, 94 = 24, 95 = 25,
					// 104 = 26, 106 = 27, 107 = 28, 120 = 29, 122 = 30, 123 = 31, 126 = 32,
					// 127 = 33, 208 = 34, 210 = 35, 214 = 36, 216 = 37, 218 = 38, 219 = 39,
					// 222 = 40, 223 = 41, 248 = 42, 250 = 43, 251 = 44, 254 = 45, 255 = 46, 0 = 47 }
					pick_tile := map[int]int{
						2: 1, 8: 2, 10: 3, 11: 4, 16: 5, 18: 6, 22: 7, 24: 8, 26: 9,
						27: 10, 30: 11, 31: 12, 64: 13, 66: 14, 72: 15, 74: 16, 75: 17,
						80: 18, 82: 19, 86: 20, 88: 21, 90: 22, 91: 23, 94: 24, 95: 25,
						104: 26, 106: 27, 107: 28, 120: 29, 122: 30, 123: 31, 126: 32,
						127: 33, 208: 34, 210: 35, 214: 36, 216: 37, 218: 38, 219: 39,
						222: 40, 223: 41, 248: 42, 250: 43, 251: 44, 254: 45, 255: 46, 0: 47,
					}
					var tile_index int
					//if (neighbour_index != 0) && (pick_tile[neighbour_index] != 0) {
					if _, have := pick_tile[neighbour_index]; have != false {
						tile_index = pick_tile[neighbour_index]
					} else {
						p("XXX neighbour_index:", neighbour_index, "tile_index:", tile_index, "corners:",
							"tl:", topLeft, "t:", top, "tr:", topRight, "l:", left, "r:", right, "bl:", bottomLeft, "b:", bottom, "br:", bottomRight,
							"x, y:", x, y, "cell.z:", cell.z)
						pp("can't pick tile with nighgbour_index:", neighbour_index)
						tile_index = tile_index
					}

					cell.tileId = tile_index

					//p("neighbour_index:", neighbour_index, "tile_index:", tile_index, "corners:", top, topRight, right, bottomRight, bottom, bottomLeft, left, topLeft,
					//	"x, y:", x, y, "cell.z:", cell.z)
					p("neighbour_index:", neighbour_index, "tile_index:", tile_index, "corners:",
						"tl:", topLeft, "t:", top, "tr:", topRight, "l:", left, "r:", right, "bl:", bottomLeft, "b:", bottom, "br:", bottomRight,
						"x, y:", x, y, "cell.z:", cell.z)
					//p("bitmask:", bitmask, "corners:", northTile, westTile, eastTile, southTile, "x, y:", x, y, "cell.z:", cell.z)

					/*
						if cell.tileId == -1 {
							cell.tileId = 15
						}
					*/
				}
			}
		}
	}
	`

	//pp("data:", data)

	/*
		data = [0, 1, 2,
				3, 4, 5,
				6, 7, 8]
	*/

	/*
		getDataAt := func(x, y int) int {
			pp("x:", x, "y:", y)
			return data[y*3+x]
		}
	*/

	tileId := -1

	//for y := 0; y < f.h; y++ {
	//for y := 1; y < f.h-1; y++ {
	//	for x := 1; x < f.w-1; x++ {
	//for y := 0; y < 3; y++ {
	//	for x := 0; x < 3; x++ {
	//cell := &f.cells[x][y]
	//cell := f.getCell(x, y)
	//cellZ := data[y * 3 + x]
	//cellZ := getDataAt(x, y)
	cellZ := data[4]

	topLeft := 0
	topRight := 0
	bottomLeft := 0
	bottomRight := 0

	/*
		topLeft := 1
		topRight := 1
		bottomLeft := 1
		bottomRight := 1
	*/

	top := 0
	bottom := 0
	left := 0
	right := 0

	//if cell.z != 0 {
	if cellZ == closedTileZLevel {

		//if getDataAt(x-1, y-1) != openTileZLevel {
		if data[0] != openTileZLevel {
			topLeft = 1
		}
		//if getDataAt(x+1, y-1) != openTileZLevel {
		if data[2] != openTileZLevel {
			topRight = 1
		}
		//if getDataAt(x-1, y+1) != openTileZLevel {
		if data[6] != openTileZLevel {
			bottomLeft = 1
		}
		//if getDataAt(x+1, y+1) != openTileZLevel {
		if data[8] != openTileZLevel {
			bottomRight = 1
		}

		//if getDataAt(x, y-1) != openTileZLevel {
		if data[1] != openTileZLevel {
			top = 1
		}
		//if getDataAt(x+1, y) != openTileZLevel {
		if data[5] != openTileZLevel {
			right = 1
		}
		//if getDataAt(x, y+1) != openTileZLevel {
		if data[7] != openTileZLevel {
			bottom = 1
		}
		//if getDataAt(x-1, y) != openTileZLevel {
		if data[3] != openTileZLevel {
			left = 1
		}

		/*
			if f.cells[x-1][y-1].z == ZLevel_Ground0 {
				topLeft = 1
			}
			if f.cells[x+1][y-1].z == ZLevel_Ground0 {
				topRight = 1
			}
			if f.cells[x-1][y+1].z == ZLevel_Ground0 {
				bottomLeft = 1
			}
			if f.cells[x+1][y+1].z == ZLevel_Ground0 {
				bottomRight = 1
			}

			if f.cells[x][y-1].z == ZLevel_Ground0 {
				top = 1
			}
			if f.cells[x+1][y].z == ZLevel_Ground0 {
				right = 1
			}
			if f.cells[x][y+1].z == ZLevel_Ground0 {
				bottom = 1
			}
			if f.cells[x-1][y].z == ZLevel_Ground0 {
				left = 1
			}
		*/

		/*
			// From:
			// https://www.boristhebrave.com/2013/07/14/tileset-roundup/
			if left == 0 && top == 0 {
				topLeft = 0
			}
			if left == 0 && bottom == 0 {
				bottomLeft = 0
			}
			if right == 0 && top == 0 {
				topRight = 0
			}
			if right == 0 && bottom == 0 {
				bottomRight = 0
			}
		*/

		// Extra boolean checks for corner tiles
		// https://cms-assets.tutsplus.com/uploads/users/90/posts/25673/image/182-to-22.png
		// https://gamedevelopment.tutsplus.com/tutorials/how-to-use-tile-bitmasking-to-auto-tile-your-level-layouts--cms-25673
		if topLeft == 1 {
			if top == 0 || left == 0 {
				topLeft = 0
			}
		}
		if topRight == 1 {
			if top == 0 || right == 0 {
				topRight = 0
			}
		}
		if bottomLeft == 1 {
			if bottom == 0 || left == 0 {
				bottomLeft = 0
			}
		}
		if bottomRight == 1 {
			if bottom == 0 || right == 0 {
				bottomRight = 0
			}
		}

	} else {
		// Empty/Sea tile
		//emptyTile := "l5_0"
		//continue
		return -1
		//return data[4]
	}

	// bitmask
	// http://www.cr31.co.uk/stagecast/wang/blob.html
	//neighbour_index := top*1 + topRight*2 + right*4 + bottomRight*8 + bottom*16 + bottomLeft*32 + left*64 + topLeft*128
	// https://gamedevelopment.tutsplus.com/tutorials/how-to-use-tile-bitmasking-to-auto-tile-your-level-layouts--cms-25673
	neighbour_index := topLeft*1 + top*2 + topRight*4 + left*8 + right*16 + bottomLeft*32 + bottom*64 + bottomRight*128

	//tile_index = topLeft + 2 * topRight + 4 * bottomLeft + 8 * bottomRight
	//bitmask := 8*topLeft + 1*topRight + 4*bottomLeft + 2*bottomRight
	//bitmask := topLeft + 2*topRight + 4*bottomLeft + 8*bottomRight

	//if bitmask == 0 {
	//	bitmask = 15
	//}
	/*
		pick_tile := map[int]int{0: 0, 2: 1, 8: 2, 10: 3, 11: 4, 16: 5, 18: 6,
			22: 7, 24: 8, 26: 9, 27: 10, 30: 11, 31: 12, 64: 13,
			66: 14, 72: 15, 74: 16, 75: 17, 80: 18, 82: 19, 86: 20,
			88: 21, 90: 22, 91: 23, 94: 24, 95: 25, 104: 26, 106: 27,
		   107: 28, 120: 29, 122: 30, 123: 31, 126: 32, 127: 33,
		   208: 34, 210: 35, 214: 36, 216: 37, 218: 38, 219: 39,
		   222: 40, 223: 41, 248: 42, 250: 43, 251: 44, 254: 45, 255: 46}
	*/
	/*
		// http://www.cr31.co.uk/stagecast/wang/blob.html
		pick_tile := map[int]int{0: 0,
			4: 1, 16: 1, 64: 1,
			20: 5, 80: 5, 65: 5,
			28: 7, 112: 7, 193: 7,
			68: 17,
			84: 21, 81: 21, 69: 21,
			92: 23, 113: 23, 197: 23,
			116: 29, 209: 29, 71: 29,
			124: 31, 241: 31, 199: 31,
			93: 87, 117: 93, 213: 93,
			125: 95, 245: 95, 215: 95,
			221: 119,
			253: 127, 247: 127, 223: 127,
		}
	*/
	// https://gamedevelopment.tutsplus.com/tutorials/how-to-use-tile-bitmasking-to-auto-tile-your-level-layouts--cms-25673
	//{ 2 = 1, 8 = 2, 10 = 3, 11 = 4, 16 = 5, 18 = 6, 22 = 7, 24 = 8, 26 = 9,
	//	27 = 10, 30 = 11, 31 = 12, 64 = 13, 66 = 14, 72 = 15, 74 = 16, 75 = 17,
	//  80 = 18, 82 = 19, 86 = 20, 88 = 21, 90 = 22, 91 = 23, 94 = 24, 95 = 25,
	// 104 = 26, 106 = 27, 107 = 28, 120 = 29, 122 = 30, 123 = 31, 126 = 32,
	// 127 = 33, 208 = 34, 210 = 35, 214 = 36, 216 = 37, 218 = 38, 219 = 39,
	// 222 = 40, 223 = 41, 248 = 42, 250 = 43, 251 = 44, 254 = 45, 255 = 46, 0 = 47 }
	pick_tile := map[int]int{
		2: 1, 8: 2, 10: 3, 11: 4, 16: 5, 18: 6, 22: 7, 24: 8, 26: 9,
		27: 10, 30: 11, 31: 12, 64: 13, 66: 14, 72: 15, 74: 16, 75: 17,
		80: 18, 82: 19, 86: 20, 88: 21, 90: 22, 91: 23, 94: 24, 95: 25,
		104: 26, 106: 27, 107: 28, 120: 29, 122: 30, 123: 31, 126: 32,
		127: 33, 208: 34, 210: 35, 214: 36, 216: 37, 218: 38, 219: 39,
		222: 40, 223: 41, 248: 42, 250: 43, 251: 44, 254: 45, 255: 46, 0: 47,
	}
	var tile_index int
	//if (neighbour_index != 0) && (pick_tile[neighbour_index] != 0) {
	if _, have := pick_tile[neighbour_index]; have != false {
		tile_index = pick_tile[neighbour_index]
	} else {
		p("XXX neighbour_index:", neighbour_index, "tile_index:", tile_index, "corners:",
			"tl:", topLeft, "t:", top, "tr:", topRight, "l:", left, "r:", right, "bl:", bottomLeft, "b:", bottom, "br:", bottomRight,
			"cellZ:", cellZ)
		pp("can't pick tile with nighgbour_index:", neighbour_index)
		tile_index = tile_index
	}

	tileId = tile_index

	//p("neighbour_index:", neighbour_index, "tile_index:", tile_index, "corners:", top, topRight, right, bottomRight, bottom, bottomLeft, left, topLeft,
	//	"x, y:", x, y, "cell.z:", cell.z)
	p("neighbour_index:", neighbour_index, "tile_index:", tile_index, "corners:",
		"tl:", topLeft, "t:", top, "tr:", topRight, "l:", left, "r:", right, "bl:", bottomLeft, "b:", bottom, "br:", bottomRight,
		"cellZ:", cellZ)
	//p("bitmask:", bitmask, "corners:", northTile, westTile, eastTile, southTile, "x, y:", x, y, "cell.z:", cell.z)

	/*
		if cell.tileId == -1 {
			cell.tileId = 15
		}
	*/
	//}
	//}

	return tileId
}
