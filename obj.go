package main_module

import "fmt"

var maxObjId = 0

type ObjI interface {
	getId() int
	getObjType() string
}

// component?
// game object
type Obj struct {
	*Transform
	//colobj  *ColObj
	objId   int
	objtype string
	dead    bool
	view    ViewI
}

func newObj(objtype string) *Obj {
	o := &Obj{
		objId:     maxObjId,
		Transform: newTransform(),
		objtype:   objtype,
		dead:      false,
	}
	maxObjId++
	game.objsys.addElem(o)
	return o
}

func (o *Obj) getId() int {
	return o.objId
}

func (o *Obj) getObjType() string {
	return o.objtype
}

func (o *Obj) isDead() bool {
	return o.dead
}

// override
func (o *Obj) start() {
}

// override
func (o *Obj) destroy() {
	game.objsys.removeElem(o)
}

func (o *Obj) hasView() bool {
	return o.view != nil
}

func (o *Obj) getView() ViewI {
	cknil(o.view)
	return o.view
}

func (o *Obj) String() string {
	return fmt.Sprintf("Obj<id: %d, objtype: %s, pos: %f %f %f>",
		o.objId, o.objtype, o.Transform.Pos().X(), o.Transform.Pos().Y(), o.Transform.Pos().Z())
}

func (o *Obj) update(dt float64) {
	/*
		if o.colobj != nil {
			o.colobj.update(dt)
		}
	*/
}
