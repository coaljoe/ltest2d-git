package main_module

import (
	//"kristallos.ga/ltest2d/gui_base"
	"kristallos.ga/ltest2d/gui/gui_base"
)

var (
	game *Game = nil
)

type Game struct {
	id          int
	gopt        GameOptions // Game Options
	systems     map[string]GameSystemI
	timer       *Timer
	frame       int
	keymap      *Keymap
	vp          *Viewport
	timersys    *TimerSys
	shedsys     *ShedSys
	objsys      *ObjSys
	entitysys   *EntitySys
	fxsys       *FxSys
	field       *Field
	//gui         *Gui
	gui         gui_base.GuiI
	hud         *Hud
	playersys   *PlayerSys
	fowsys      *FowSys
	buildingsys *BuildingSys
	unitsys     *UnitSys
	newgametool *NewGameTool
	prodsys     *ProdSys
	spritesys   *SpriteSys
	pathfinder  *Pathfinder
	testfw      *TestFw
	// State
	// A game was created
	gameCreated bool
	paused      bool
	initialized bool
}

func newGame() *Game {
	if game != nil {
		return game
	}

	println("game.new")
	g := &Game{
		frame: 0,
		//gopt:    newDefaultGameOptions(),
		timer:       newTimer(true),
		systems:     make(map[string]GameSystemI),
		paused:      false,
		initialized: false,
	}
	game = g // Early linking

	// Set Game Options
	g.setGameOptions(newDefaultGameOptions())

	g.keymap = newKeymap()
	g.vp = newViewport()
	g.objsys = newObjSys()
	g.entitysys = newEntitySys()
	g.fxsys = newFxSys()
	g.timersys = newTimerSys()
	g.shedsys = newShedSys()
	g.field = newField()
	//g.gui = newGui()
	//g.gui = gui_base.NewGui()
	g.hud = newHud()
	g.playersys = newPlayerSys()
	g.fowsys = newFowSys()
	g.buildingsys = newBuildingSys()
	g.unitsys = newUnitSys()
	g.newgametool = newNewGameTool()
	g.prodsys = newProdSys()
	g.spritesys = newSpriteSys()
	g.pathfinder = newPathfinder()
	g.testfw = newTestFw()

	initDebug()
	
	// XXX FIXME reapply game options for later systems (ugly)
	//g._updateGameOptions()

	// Init complete
	g.initialized = true

	// Automatically start the game after creation (fixme)
	//g.start()

	return game
}

func (g *Game) setGameOptions(gopt GameOptions) {
	_log.Inf("[Game] setGameOptions")

	g.gopt = gopt
	g._updateGameOptions()
}

func (g *Game) _updateGameOptions() {
	_log.Inf("[Game] _updateGameOptions")

	gOpt := &g.gopt // Shortcut
	// Update game speed
	if gOpt._gameSpeed >= 0.1 {
		g.timer.speed = gOpt._gameSpeed
	} else {
		pp("gameSpeed is too low:", gOpt._gameSpeed)
	}

	// Finally
	gOpt.dirty = false
}

func (g *Game) addSystem(name string, s GameSystemI) {
	//g.systems = append(g.systems, s)
	g.systems[name] = s
}

func (g *Game) GetSystem(name string) GameSystemI {
	return g.systems[name]
}

// XXX rename to startNewGame?
// Start new game
func (g *Game) start() {
	game.id = _ider.GetNextId("game")
	//g._setDefaultGame()
	game.createNewDefaultGame()
	//g.hud.start()
	if !g.gameCreated {
		pp("no game was created")
	}

	// Start registered subsystems
	for _, si := range g.systems {
		if s, ok := si.(StartableSystemI); ok {
			if si.Autostart() {
				_log.Inf("[Game] start subsystem:", si.Name())
				s.start()
			}
		}
	}
}

// Stop the game
func (g *Game) stop() {
	// Stop registered subsystems
	for _, si := range g.systems {
		if s, ok := si.(StopableI); ok {
			_log.Inf("[Game] stop subsystem:", si.Name())
			s.stop()
		}
	}

	g._resetGame()
}

func (g *Game) pause(v bool) {
	//pp("not implemented")
	g.paused = v
}

func (g *Game) clearGame() {
}

//func game *Game { return gamei }

// Reset the game.
func (g *Game) _resetGame() {
	g.gameCreated = false
	g.clearGame()
	//g.entitysys.listEntities()
	g.entitysys.clearElems()
	g.shedsys.clear()
	//pp(2)
}

// Create new default empty game.
func (g *Game) createNewDefaultGame() {
	g._resetGame()
	g._initBaseGame()
	g._initDefaultGame()
	// Finally
	g.gameCreated = true
}

// Create new test empty game.
func (g *Game) createNewTestGame() {
	g._resetGame()
	g._initBaseGame()
	g._initTestGame()
	// Finally
	g.gameCreated = true
}

// Init base game structure.
func (g *Game) _initBaseGame() {
	game.newgametool.initBaseGame()

}

func (g *Game) _initDefaultGame() {
	// TODO: move field here from main
}

func (g *Game) _initTestGame() {
}

func (g *Game) zero() bool {
	return g.frame == 0
}

func (g *Game) draw() {
	// Reset Color
	renderer.SetDrawColor(0, 0, 0, 255)
	// Clear
	renderer.Clear()
	// Draw game state
	if gameState != nil { // XXX fixme?
		gameState.draw()
	}
	// Draw objects XXX should be in gs_game?
	for _, oi := range g.objsys.getElems() {
		o := oi.(*Obj)
		if o.view != nil {
			o.view.draw()
		}
	}
	// Draw spritesys sprites
	g.spritesys.draw()
	// XXX fow FIXME move to gameState.postDraw()?
	//g.field.view.drawFow()
	if gameState != nil { // XXX fixme?
		gameState.postDraw()
	}
	g.hud.draw()
	g.gui.Draw()
	renderer.Present()
}

func (g *Game) update(dt float64) {
	if game.paused {
		return
	}
	dt = dt * g.timer.speed

	// Update vars
	vars.dt = dt
	vars.dtGameSpeed = dt / g.gopt._gameSpeed

	// Update game options
	if g.gopt.dirty {
		g._updateGameOptions()
	}

	debug.update(dt)

	g.timersys.update(dt)
	g.shedsys.update(dt)
	/*
		g.field.update(dt)
		g.unitsys.update(dt)
		g.playersys.update(dt)
		g.fxsys.update(dt)
		g.scene.update(dt)
		//g.hud.update(dt)
		g.gui.Update(dt)
	*/
	g.vp.update(dt)
	//g.player.update(dt)
	g.gui.Update(dt)
	g.playersys.update(dt)

	// Update registered systems
	for _, s := range g.systems {
		s.update(dt)
		//fmt.Println("Update", n, s)
	}
	//fmt.Println(g.systems)
	//g.hud.update(dt)
	updateGameState(dt)

	g.timer.update(dt)

	g.frame += 1
}
